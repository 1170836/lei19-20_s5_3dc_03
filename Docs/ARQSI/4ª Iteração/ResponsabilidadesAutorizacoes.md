# Identificação e justificação de atribuição das responsabilidades decisão, cumprimento (enforcement), informação e administração de autorização

## PRP e PIP - Ponto do Sistema que armazena as autorizações - Base de Dados

As autorizações vão ser lidas e armazenadas numa Base de Dados, de forma a que em qualquer momento seja possível consultá-las, e tomar decisões sobre os pedidos com as informações das mesmas.

## PAP e PDP - Ponto do Sistema que gere as autorizações e toma decisões sobre as mesmas - Gestão Encomendas

O módulo Gestão de Encomendas vai fazer a gestão das autorizações, inclusivamente ordenar o seu armazenamento depois do devido processamento.

Quando chega um pedido ao GE, é pesquisada a autorização relacionada com o pedido, e dessa forma o GE toma uma decisão, que pode ser permitir a ação ou negá-la.

## PEP - Ponto do Sistema que faz o Pedido - UI

Realiza o pedido, e atua sobre a decisão obtida.
