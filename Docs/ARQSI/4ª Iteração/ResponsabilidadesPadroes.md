# Responsabilidades

#### MDF e MDP

  Têm a responsabilidade de gerir toda a informação relativa à fabrica e aos produtos.

#### GE

  Deve tratar toda a informação relativa à gestão de encomendas e de clientes. Além disso foi-lhe atribuída a responsabilidade de gerir as autorizações relativas ao Módulo.

#### PP

  Este módulo tem a responsabilidade de fazer o tratamento do Planeamento de Produção. É maioritariamente utilizado o módulo 'swipl' para comunicar com os ficheiros Prolog desenvolvidos para a Unidade Curricular de ALGAV.

### ============================================================

Dentro de cada módulo do Sistema podemos dividir as responsabilidades em 3 grupos;

#### Controller

O Controller recebe o pedido HTTP. Dependendo do mesmo, e da sua autorização (positiva ou negativa), envia um pedido ao Serviço, que vai processar o mesmo. No fim, envia uma resposta adequada ao utilizador.

#### Serviço

No Serviço acontece o processamento do pedido. Ou seja, vai verificar a informação proveniente do Controller, e tratá-la. Sempre que necessário algum contacto com uma Base de Dados, chama o Repositório.

#### Repositório

O Repositório serve para comunicar com a Base de Dados. Não deve tratar a informação, mas sim recebe-la e armazená-la.

# Padrões

### Padrão Rest

Para o funcionamento da nossa aplicação, consideramos que o padrão REST e a melhor opção tendo em conta a utilização do padrão HTTP. Este padrão também e bastante útil para uma das funcionalidades fulcrais da nossa aplicação, o login. Sendo que com o REST podemos utilizar a cache e é neste local que guardamos informações sobre qual o utilizador atual do sistema.

### Padrão Repository

O Repository permite um encapsulamento da lógica de acesso a dados, proporcionando uma visão mais orientada a objetos. Com o uso deste padrão, aplicamos na nossa camada de domínio o princípio da persistência, ou seja, as nossas entidades da camada de negócio, não devem sofrer impactos pela forma como são persistidas na base de dados.

Os grandes benefícios ao utilizar este padrão são:
 - Permitir a troca da base de dados utilizada sem afetar o sistema como um todo.
 - Código centralizado num único ponto, evitando duplicação.
 - Facilita a implementação de testes unitários.
 - Diminui o acoplamento entre classes.
 - Padronização de códigos e serviços.

### Padrão Service

Este padrão é uma das bases para a implementação da lógica do negócio.
Os benefícios da camada de serviço consistem num conjunto comum de operações disponíveis para diferentes clientes e coordena a resposta em cada operação. Numa aplicação que possui mais de um tipo de cliente, como a nossa, que tem casos de uso complexos que envolvem vários recursos transacionais.
