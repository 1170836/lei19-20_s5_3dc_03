# 1. Requisitos


**Planeamento de Produção**  - Planear a produção para o dia

# 2. Análise

 - Esta funcionalidade consiste em definir que linhas vão produzir determinados lotes de produtos, associados a encomendas.

 - É também necessário saber quanto tempo vai durar a produção de um lote de produtos, e em que momento do dia vai ser produzido, para a mesma produção ser integrada com o módulo de visualização.

 - Esses dados vão ser armazenados na base de conhecimento, e depois consultados através do SWI-Prolog.


# 3. Design

  PP - Planeamento de Produção | GE - Gestão Encomendas

  Responsabilidade   | Módulo
  --------- | ------
  Verificar se já foi efetuado um planeamento no dia atual | GE
  Distribuir as encomendas (lotes de produtos) pelas linhas de produção | PP
  Sequenciar a sua execução | PP
  Mudar estado das encomendas | GE
  Guardar planeamento na BD | GE

## Vista Processo - GE
![Diagrama de Sequência](Planeamento_GE_Processo.png)


# 4. Testes

 - Planeamento sem encomendas
 - Planeamento já efetuado
 - Máquinas insuficientes para produzir uma encomenda
 - Planeamento com duração de mais de 1 dia
 - Planeamento bem sucedido  
