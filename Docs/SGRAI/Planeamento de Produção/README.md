# 1. Requisitos


**Visualização gráfica do planeamento de produção**  - Animação do plano de produção

# 2. Análise

 - Esta funcionalidade apenas pode ser realizada depois do Planeamento de Produção estar totalmente desenvolvido.

 - A fábrica deve ter uma hora de início de produção.

 - A fábrica deve ter uma hora de fim de produção, ou uma duração de produção.

 - É necessário saber para cada linha, em cada momento, que produto está a ser produzido.


# 3. Design

   PP - Planeamento de Produção | GE - Gestão Encomendas

   Responsabilidade   | Módulo
   --------- | ------
   Realizar planeamento de Produção | PP
   Saber que produto está a ser produzido numa determinada linha num determinado momento | GE
   Representar produção graficamente | Visualização


# 4. Testes

  - Produto numa linha antes do início do trabalho na fábrica
  - Produto numa linha depois do fim do trabalho na fábrica
  - Mostrar planeamento
