# 1. Requisitos


**Edição avançada da Fábrica**  -Edição de chão de fábrica com o recurso aos widgets. A representação gráfica deverá reflectir as alterações efectuadas nos widgets.

A partir daqui são retiradas 3 funcionalidades pretendidas:
 - Editar Linha de Produção
 - Editar Máquina
 - Adicionar Máquina

# 2. Análise

 - A funcionalidade Adicionar Máquina já foi desenvolvida previamente, logo, só é necessário implementar o módulo de visualização corretamente com o Master Data Fábrica.

 - As funcionalidades Editar Linha de Produção e Editar Máquina vão ter que ser completamente desenvolvidas, e devidamente integradas com o módulo de visualização.

 - Na edição da linha de produção é pretendido alterar a sua posição, e consequentemente é necessário verificar se essa posição é valida no contexto atual da fábrica (evitar colisão com outras linhas, etc.). Isso vai alterar o nosso modelo do domínio, visto que, até aqui as linhas eram colocadas de forma aleatória na fábrica, e agora vão ter uma posição predefinida.

 - Na edição da máquina pretendemos alterar a sua posição dentro da linha de produção, ou seja, o seu número de sequência (posição relativa na linha de produção). Para isso é necessário controlar esse mesmo número, sendo o mínimo 1 e o máximo o número de máquinas na linha.

# 3. Design

 - Todas as funcionalidades vão ser realizadas no módulo Master Data Fábrica (MDF).

 - No módulo de visualização (Front-end) vai ser apresentado um "popup" para cada uma das funcionalidades, espoletando cada uma das tarefas para as realizar.

 - Como todas as tarefas são realizadas no MDF, apresentamos apenas uma Vista de Processo, visto que todas seguem os mesmos princípios de separação de responsabilidades.

 ![Diagrama de Sequência](VistaProcesso-EditarLinhaProdução.png)

# 4. Testes

 - Adicionar Máquina
  - Testes unitários e de integração já realizados na 1ª iteração de ARQSI

- Atualizar Linha de produção
  - Posições negativas
  - Posição inválida devido a colisão com outra linha
  - Posição inválida devido ao tamanho da fábrica
  - Linha corretamente atualizada
  - Produção muda corretamente a posição


- Atualizar Máquina
  - Número de sequência negativo
  - Número de sequência maior que o número de máquinas da linha
  - Máquina corretamente atualizada
  - Tooltip muda a posição
