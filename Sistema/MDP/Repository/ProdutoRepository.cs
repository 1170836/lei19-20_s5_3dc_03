
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using MDP.Models;
using Microsoft.EntityFrameworkCore;
using MDP.Models.Produtos;

namespace MDP.Repository
{
    public class ProdutoRepository
    {
        private MDPContext _context = null;
        private DbSet<Produto> table = null;
        public ProdutoRepository(MDPContext context)
        {
            this._context = context;
            table = _context.Set<Produto>();
        }
        public IEnumerable<Produto> GetAll()
        {
            return table.Include(produto => produto.planoFabrico).ToList();
        }
        public Produto GetById(object id)
        {
            return table.Find(id);
        }

        public Produto GetByDesc(string desc)
        {
            IEnumerable<Produto> produtos = this.table.Where(t => t.descricao.EqualsToString(desc));
            int i = (int)produtos.LongCount();
            if (i == 1)
            {
                return produtos.Single();
            }
            return null;
        }
        public Guid Insert(Preco preco, DescricaoProduto descricao, PlanoFabrico pf, DuracaoFabrico dur)
        {
            Produto p = new Produto(preco, descricao, pf, dur);

            table.Add(p);
            return p.Id;
        }
        public void Update(Produto obj)
        {
            table.Attach(obj);
            _context.Entry(obj).State = EntityState.Modified;
        }
        public bool Delete(object id)
        {
            Produto existing = GetById(id);
            if (existing != null)
            {
                table.Remove(existing);
                return true;
            }
            return false;
        }

        /*         public PlanoFabrico GetPlanoFabrico(string desc){
                    Produto p= this.GetByDesc(desc);

                    PlanoFabrico planoFabrico= p.planoFabrico;
                    return planoFabrico;

                }
         */
        public void Save()
        {
            _context.SaveChanges();
        }
    }
}