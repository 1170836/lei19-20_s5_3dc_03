using MDF.Models.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using MDP.Models;
using Microsoft.EntityFrameworkCore;
using MDF.Models.Operacoes;

namespace MDP.Repository
{
    public class OperacaoRepository
    {
        private MDPContext _context = null;
        private DbSet<Operacao> table = null;
        public OperacaoRepository(MDPContext context)
        {
            this._context = context;
            table = _context.Set<Operacao>();
        }
        public IEnumerable<Operacao> GetAll()
        {
            return table.ToList();
        }
        public Operacao GetById(object id)
        {
            return table.Find(id);
        }

        public Operacao GetByDesc(string desc){
            IEnumerable<Operacao> operacoes = this.table.Where(o => o.descricao.EqualsToString(desc));
            int i = (int)operacoes.LongCount();
            if (i == 1)
            {
                return operacoes.Single();
            }
            return null;
        }
        public void Insert(Operacao obj)
        {
            table.Add(obj);
        }
        public void Update(Operacao obj)
        {
            table.Attach(obj);
            _context.Entry(obj).State = EntityState.Modified;
        }
        public void Delete(object id)
        {
            Operacao existing = table.Find(id);
            table.Remove(existing);
        }
        public void Save()
        {
            _context.SaveChanges();
        }
    }
}