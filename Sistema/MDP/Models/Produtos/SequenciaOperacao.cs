using MDF.Models.Shared;
using System.ComponentModel.DataAnnotations.Schema;
using MDF.Models.Operacoes;
using System;

namespace MDP.Models.Produtos
{
    public class SequenciaOperacao : Entity
    {
        public int nSequencia {get; set;}

        public Guid operacaoId{get; set;}
        [ForeignKey("operacaoId")]
        public Operacao operacao { get; set; }

        protected SequenciaOperacao(){
        }

        public SequenciaOperacao(int nSequencia, Operacao operacao)
        {
            this.nSequencia = nSequencia;
            this.operacao = operacao;
        }
    }
}