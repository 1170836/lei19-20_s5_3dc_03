using System.Collections.Generic;
using MDF.Models.Shared;

namespace MDP.Models.Produtos
{
    public class Preco : ValueObject
    {
        public float preco {get; set;}

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return this.preco;
        }

        public Preco(float preco)
        {
            this.preco = preco;
        }
    }
}