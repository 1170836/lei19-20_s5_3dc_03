using System.Collections.Generic;
using MDF.Models.Shared;

namespace MDP.Models.Produtos
{
    public class DuracaoFabrico : ValueObject
    {
        public int duracao {get; set;}

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return this.duracao;
        }

        public DuracaoFabrico(int duracao)
        {
            this.duracao = duracao;
        }

    }
}