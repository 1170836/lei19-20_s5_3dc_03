using System.Collections.Generic;
using MDF.Models.Shared;

namespace MDP.Models.Produtos
{
    public class DescricaoProduto : ValueObject
    {
        public string descricao {get; set;}

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return this.descricao;
        }

        public DescricaoProduto(string descricao)
        {
            this.descricao = descricao;
        }

        public bool EqualsToString(string descricao2){
            return this.descricao.Equals(descricao2);
        }
    }
}