using MDF.Models.Shared;
using System.Collections.Generic;

namespace MDP.Models.Produtos
{
    public class PlanoFabrico : Entity
    {
        public List<SequenciaOperacao> sequenciaOperacoes { get; set; }

        protected PlanoFabrico() { }
        public PlanoFabrico(List<SequenciaOperacao> sequenciaOperacoesL)
        {
            this.sequenciaOperacoes = sequenciaOperacoesL;
        }
    }
}