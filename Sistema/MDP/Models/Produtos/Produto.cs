using MDF.Models.Shared;
using MDP.DTO;
using System;
using System.Collections.Generic;
using MDF.Models.Operacoes;
using System.ComponentModel.DataAnnotations.Schema;

namespace MDP.Models.Produtos
{
    public class Produto : Entity
    {

        public Preco preco { get; set; }

        public DescricaoProduto descricao { get; set; }

        public PlanoFabrico planoFabrico { get; set; }

        public DuracaoFabrico duracao { get; set; }

        protected Produto()
        {

        }

        public Produto(Preco preco, DescricaoProduto descricao, PlanoFabrico planoFabrico, DuracaoFabrico duracao)
        {
            if (planoFabrico != null)
            {
                this.preco = preco;
                this.descricao = descricao;
                this.planoFabrico = planoFabrico;
                this.duracao = duracao;
            }
            else
            {
                throw new ArgumentException();
            }
        }

        public ProdutoDTO toDTO()
        {
            List<string> plano = new List<string>();
            foreach (SequenciaOperacao so in this.planoFabrico.sequenciaOperacoes)
            {
                plano.Add(so.operacaoId.ToString());
            }
            return new ProdutoDTO(this.preco.preco.ToString(), this.descricao.descricao, plano, this.duracao.duracao.ToString());
        }

        public ProdutoSemPlanoDTO toDTOSemPlano()
        {
            return new ProdutoSemPlanoDTO(this.preco.preco.ToString(), this.descricao.descricao);
        }

        public PlanoFabricoDTO toDTOPlano()
        {
            List<string> plano = new List<string>();
            foreach (SequenciaOperacao so in this.planoFabrico.sequenciaOperacoes)
            {
                plano.Add(so.operacaoId.ToString());
            }
            return new PlanoFabricoDTO(plano);
        }
    }

}