using MDF.DTO;
using System.Collections.Generic;
using System;

namespace MDP.DTO
{
    public class ProdutoDTO
    {
        public string preco { get; set; }
        public string descricao { get; set; }

        public List<string> planoFabrico { get; set; }

        public string duracao { get; set; }

        public ProdutoDTO(string precoDTO, string descricaoDTO, List<string> planoDTO, string duracao)
        {
            this.preco = precoDTO;
            this.descricao = descricaoDTO;
            this.planoFabrico = planoDTO;
            this.duracao = duracao;
        }

        public void setPlano(List<string> plano)
        {
            this.planoFabrico = plano;
        }
    }
}