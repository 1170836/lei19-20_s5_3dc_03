using MDF.DTO;
using System.Collections.Generic;
using System;

namespace MDP.DTO
{
    public class PlanoFabricoDTO{

        public List<string> planoFabrico {get;set;}

        public PlanoFabricoDTO(List<string> planoDTO){
            this.planoFabrico = planoDTO;
        }

        public void setPlano(List<string> plano){
            this.planoFabrico = plano;
        }
    }
}