using MDF.DTO;
using System.Collections.Generic;
using System;

namespace MDP.DTO
{
    public class ProdutoSemPlanoDTO
    {
        public string preco{get; set;}
        public string descricao{get; set;}

        public ProdutoSemPlanoDTO(string precoDTO, string descricaoDTO)
        {
            this.preco = precoDTO;
            this.descricao = descricaoDTO;
        }
    }
}