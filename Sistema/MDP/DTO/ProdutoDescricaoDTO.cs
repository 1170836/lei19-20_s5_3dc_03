using System;
using System.Collections.Generic;

namespace MDP.DTO
{
    public class ProdutoDescricaoDTO
    {
        public string descricao { get; set; }
        public double preco { get; set; }

        public int duracao { get; set; }

        public ProdutoDescricaoDTO(string descricao, double preco, int duracao)
        {
            this.descricao = descricao;
            this.preco = preco;
            this.duracao = duracao;
        }
    }
}