using MDF.DTO;
using System.Collections.Generic;
using System;

namespace MDP.DTO
{
    public class ProdutoIdDTO
    {
        public string preco{get; set;}
        public string descricao{get; set;}

        public List<string> planoFabrico {get;set;}

        public string id{get;set;}

        public ProdutoIdDTO(string ID, string precoDTO, string descricaoDTO, List<string> planoDTO)
        {
            this.id = ID;
            this.preco = precoDTO;
            this.descricao = descricaoDTO;
            this.planoFabrico = planoDTO;
        }

        public void setPlano(List<string> plano){
            this.planoFabrico = plano;
        }
    }
}