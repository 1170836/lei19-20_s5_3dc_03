using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using MDF.Models.Operacoes;
using MDP.Models;
using MDP.Models.Produtos;
using MDP.Repository;
using MDP.DTO;
using System;

namespace MDP.Controllers
{
    [Route("api/MDP/Produto")]
    [ApiController]
    public class ProdutoController : ControllerBase
    {

        private ProdutoRepository repository = null;
        private OperacaoRepository repositoryOperacao = null;

        public ProdutoController(MDPContext context)
        {
            this.repository = new ProdutoRepository(context);
            this.repositoryOperacao = new OperacaoRepository(context);
        }

        [HttpGet]
        public ActionResult Index()
        {
            List<ProdutoDTO> dtos = new List<ProdutoDTO>();

            foreach (Produto p in repository.GetAll())
            {
                dtos.Add(p.toDTO());
            }

            return Ok(dtos);
        }

        [HttpPost]
        public ActionResult AddProduto(ProdutoDTO dto)
        {
            if (ModelState.IsValid)
            {
                if (dto != null)
                {
                    try
                    {
                        int nrSequencia = 1;
                        List<SequenciaOperacao> lPlano = new List<SequenciaOperacao>();
                        foreach (string tryOperacao in dto.planoFabrico)
                        {

                            if (tryOperacao != null)
                            {
                                Operacao op = this.repositoryOperacao.GetByDesc(tryOperacao);
                                if (op != null)
                                {
                                    SequenciaOperacao so = new SequenciaOperacao(nrSequencia, op);
                                    lPlano.Add(so);
                                    nrSequencia++;
                                }
                            }
                        }

                        if (lPlano.LongCount() != 0)
                        {
                            float dtoPr = (float)Convert.ToDouble(dto.preco);
                            int duracaoPr = Convert.ToInt32(dto.duracao);
                            Preco p = new Preco(dtoPr);
                            DescricaoProduto desc = new DescricaoProduto(dto.descricao);
                            PlanoFabrico plano = new PlanoFabrico(lPlano);
                            DuracaoFabrico duracao = new DuracaoFabrico(duracaoPr);

                            Guid pid = this.repository.Insert(p, desc, plano, duracao);

                            this.repository.Save();

                            return Ok(new ProdutoIdDTO(pid.ToString(), dto.preco, dto.descricao, dto.planoFabrico));
                        }
                        else
                        {
                            return BadRequest("Nenhuma operação válida na lista introduzida.");
                        }
                    }
                    catch (ArgumentException)
                    {
                        return ValidationProblem();
                    }
                }
                else
                {
                    return BadRequest("Produto não introduzido.");
                }
            }
            return null;
        }


        [HttpGet("{desc}")]
        public ActionResult GetProduto(string desc)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            try
            {
                Produto prod = this.repository.GetByDesc(desc);

                if (prod != null)
                {
                    return Ok(prod.toDTOSemPlano());
                }
                return NotFound();
            }
            catch (FormatException)
            {
                return BadRequest();
            }
        }


        [HttpGet("PlanoFabrico/{desc}")]
        public ActionResult GetPlanoFabrico(string desc)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            try
            {
                Produto prod = this.repository.GetByDesc(desc);

                Guid guids;
                List<string> opr = new List<string>();

                if (prod != null)
                {
                    PlanoFabricoDTO dto = prod.toDTOPlano();

                    foreach (string s in dto.planoFabrico)
                    {
                        guids = Guid.Parse(s);
                        opr.Add(repositoryOperacao.GetById(guids).descricao.descricao);
                    }

                    dto.setPlano(opr);
                    return Ok(dto);
                }
                return NotFound();
            }
            catch (FormatException)
            {
                return BadRequest();
            }
        }

        [HttpGet("Duracao/{desc}")]
        public ActionResult Get(string desc)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            try
            {
                Produto prod = this.repository.GetByDesc(desc);

                Guid guids;

                if (prod != null)
                {
                    PlanoFabricoDTO dto = prod.toDTOPlano();
                    var soma = 0;

                    foreach (string s in dto.planoFabrico)
                    {
                        guids = Guid.Parse(s);
                        soma += repositoryOperacao.GetById(guids).duracao.duracao;
                    }

                    return Ok(soma);
                }
                return NotFound();
            }
            catch (FormatException)
            {
                return BadRequest();
            }
        }

        [HttpDelete("{id}")]
        public ActionResult DeleteProduto(string id)
        {
            if (ModelState.IsValid)
            {
                Guid newId = Guid.Parse(id);
                if (this.repository.Delete(newId))
                {
                    this.repository.Save();
                    return Ok();
                }
                return NotFound();
            }
            return BadRequest();
        }

        [HttpGet("AllProdutos")]
        public ActionResult GetProdutos()
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                IEnumerable<Produto> pd = this.repository.GetAll();
                List<ProdutoDescricaoDTO> dtos = new List<ProdutoDescricaoDTO>();
                if (pd.LongCount() != 0)
                {
                    foreach (var prod in pd)
                    {
                        dtos.Add(new ProdutoDescricaoDTO(prod.descricao.descricao, prod.preco.preco, prod.duracao.duracao));
                    }
                }
                return Ok(dtos);
            }
            catch (FormatException)
            {
                return BadRequest();
            }
        }
    }
}