﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MDP.Migrations
{
    public partial class Estado : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "OrdemFabrico");

            migrationBuilder.DropTable(
                name: "PlanosDeProducao");

            migrationBuilder.AddColumn<bool>(
                name: "estado_estado",
                table: "Maquinas",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "estado_estado",
                table: "Maquinas");

            migrationBuilder.CreateTable(
                name: "PlanosDeProducao",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    diaProducao_diaProducao = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlanosDeProducao", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "OrdemFabrico",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    PlanoProducaoId = table.Column<Guid>(nullable: true),
                    produtoId = table.Column<Guid>(nullable: false),
                    estado_estado = table.Column<bool>(nullable: false),
                    nSequencia_nSequencia = table.Column<int>(nullable: false),
                    prazo_prazo = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrdemFabrico", x => x.Id);
                    table.ForeignKey(
                        name: "FK_OrdemFabrico_PlanosDeProducao_PlanoProducaoId",
                        column: x => x.PlanoProducaoId,
                        principalTable: "PlanosDeProducao",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_OrdemFabrico_Produtos_produtoId",
                        column: x => x.produtoId,
                        principalTable: "Produtos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_OrdemFabrico_PlanoProducaoId",
                table: "OrdemFabrico",
                column: "PlanoProducaoId");

            migrationBuilder.CreateIndex(
                name: "IX_OrdemFabrico_produtoId",
                table: "OrdemFabrico",
                column: "produtoId");
        }
    }
}
