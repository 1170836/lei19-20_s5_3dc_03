﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MDP.Migrations
{
    public partial class FerramentaOperacao : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ferramenta_ferramenta",
                table: "Operacoes",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ferramenta_ferramenta",
                table: "Operacoes");
        }
    }
}
