﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MDP.Migrations
{
    public partial class DuracaoFabrico : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "duracao_duracao",
                table: "Produtos",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "duracao_duracao",
                table: "Produtos");
        }
    }
}
