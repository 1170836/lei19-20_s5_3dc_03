﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MDP.Migrations
{
    public partial class MDP_MDF : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "LinhasProducao",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LinhasProducao", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Operacoes",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    duracao_duracao = table.Column<int>(nullable: false),
                    descricao_descricao = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Operacoes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PlanosDeProducao",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    diaProducao_diaProducao = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlanosDeProducao", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Produtos",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    preco_preco = table.Column<float>(nullable: false),
                    descricao_descricao = table.Column<string>(nullable: true),
                    planoFabricoId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Produtos", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TipoMaquinas",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    descricao_descricao = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TipoMaquinas", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "OrdemFabrico",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    prazo_prazo = table.Column<DateTime>(nullable: false),
                    nSequencia_nSequencia = table.Column<int>(nullable: false),
                    estado_estado = table.Column<bool>(nullable: false),
                    produtoId = table.Column<Guid>(nullable: false),
                    PlanoProducaoId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrdemFabrico", x => x.Id);
                    table.ForeignKey(
                        name: "FK_OrdemFabrico_PlanosDeProducao_PlanoProducaoId",
                        column: x => x.PlanoProducaoId,
                        principalTable: "PlanosDeProducao",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_OrdemFabrico_Produtos_produtoId",
                        column: x => x.produtoId,
                        principalTable: "Produtos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SequenciaOperacao",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    nSequencia = table.Column<int>(nullable: false),
                    operacaoId = table.Column<Guid>(nullable: false),
                    PlanoFabricoId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SequenciaOperacao", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SequenciaOperacao_Produtos_PlanoFabricoId",
                        column: x => x.PlanoFabricoId,
                        principalTable: "Produtos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SequenciaOperacao_Operacoes_operacaoId",
                        column: x => x.operacaoId,
                        principalTable: "Operacoes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AssociationTipoMaquinaOperacao",
                columns: table => new
                {
                    tMaquinaId = table.Column<Guid>(nullable: false),
                    operacaoId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AssociationTipoMaquinaOperacao", x => new { x.tMaquinaId, x.operacaoId });
                    table.ForeignKey(
                        name: "FK_AssociationTipoMaquinaOperacao_Operacoes_operacaoId",
                        column: x => x.operacaoId,
                        principalTable: "Operacoes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AssociationTipoMaquinaOperacao_TipoMaquinas_tMaquinaId",
                        column: x => x.tMaquinaId,
                        principalTable: "TipoMaquinas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Maquinas",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    marca_marca = table.Column<string>(nullable: true),
                    modelo_modelo = table.Column<string>(nullable: true),
                    nSequencia_numeroSeq = table.Column<int>(nullable: false),
                    tMaquinaId = table.Column<Guid>(nullable: false),
                    LinhaProducaoId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Maquinas", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Maquinas_LinhasProducao_LinhaProducaoId",
                        column: x => x.LinhaProducaoId,
                        principalTable: "LinhasProducao",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Maquinas_TipoMaquinas_tMaquinaId",
                        column: x => x.tMaquinaId,
                        principalTable: "TipoMaquinas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AssociationTipoMaquinaOperacao_operacaoId",
                table: "AssociationTipoMaquinaOperacao",
                column: "operacaoId");

            migrationBuilder.CreateIndex(
                name: "IX_Maquinas_LinhaProducaoId",
                table: "Maquinas",
                column: "LinhaProducaoId");

            migrationBuilder.CreateIndex(
                name: "IX_Maquinas_tMaquinaId",
                table: "Maquinas",
                column: "tMaquinaId");

            migrationBuilder.CreateIndex(
                name: "IX_OrdemFabrico_PlanoProducaoId",
                table: "OrdemFabrico",
                column: "PlanoProducaoId");

            migrationBuilder.CreateIndex(
                name: "IX_OrdemFabrico_produtoId",
                table: "OrdemFabrico",
                column: "produtoId");

            migrationBuilder.CreateIndex(
                name: "IX_SequenciaOperacao_PlanoFabricoId",
                table: "SequenciaOperacao",
                column: "PlanoFabricoId");

            migrationBuilder.CreateIndex(
                name: "IX_SequenciaOperacao_operacaoId",
                table: "SequenciaOperacao",
                column: "operacaoId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AssociationTipoMaquinaOperacao");

            migrationBuilder.DropTable(
                name: "Maquinas");

            migrationBuilder.DropTable(
                name: "OrdemFabrico");

            migrationBuilder.DropTable(
                name: "SequenciaOperacao");

            migrationBuilder.DropTable(
                name: "LinhasProducao");

            migrationBuilder.DropTable(
                name: "TipoMaquinas");

            migrationBuilder.DropTable(
                name: "PlanosDeProducao");

            migrationBuilder.DropTable(
                name: "Produtos");

            migrationBuilder.DropTable(
                name: "Operacoes");
        }
    }
}
