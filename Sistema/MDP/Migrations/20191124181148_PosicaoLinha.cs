﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MDP.Migrations
{
    public partial class PosicaoLinha : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "pos_x",
                table: "LinhasProducao",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "pos_y",
                table: "LinhasProducao",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "pos_x",
                table: "LinhasProducao");

            migrationBuilder.DropColumn(
                name: "pos_y",
                table: "LinhasProducao");
        }
    }
}
