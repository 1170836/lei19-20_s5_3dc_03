﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MDF.Migrations
{
    public partial class TipoMaquinasOperacoes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AssociationTipoMaquinaOperacao",
                columns: table => new
                {
                    tMaquinaId = table.Column<Guid>(nullable: false),
                    operacaoId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AssociationTipoMaquinaOperacao", x => new { x.tMaquinaId, x.operacaoId });
                    table.ForeignKey(
                        name: "FK_AssociationTipoMaquinaOperacao_Operacoes_operacaoId",
                        column: x => x.operacaoId,
                        principalTable: "Operacoes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AssociationTipoMaquinaOperacao_TipoMaquinas_tMaquinaId",
                        column: x => x.tMaquinaId,
                        principalTable: "TipoMaquinas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AssociationTipoMaquinaOperacao_operacaoId",
                table: "AssociationTipoMaquinaOperacao",
                column: "operacaoId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AssociationTipoMaquinaOperacao");
        }
    }
}
