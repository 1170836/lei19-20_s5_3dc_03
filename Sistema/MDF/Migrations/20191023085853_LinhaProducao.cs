﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MDF.Migrations
{
    public partial class LinhaProducao : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "LinhaProducaoId",
                table: "Maquinas",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "LinhasProducao",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LinhasProducao", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Maquinas_LinhaProducaoId",
                table: "Maquinas",
                column: "LinhaProducaoId");

            migrationBuilder.AddForeignKey(
                name: "FK_Maquinas_LinhasProducao_LinhaProducaoId",
                table: "Maquinas",
                column: "LinhaProducaoId",
                principalTable: "LinhasProducao",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Maquinas_LinhasProducao_LinhaProducaoId",
                table: "Maquinas");

            migrationBuilder.DropTable(
                name: "LinhasProducao");

            migrationBuilder.DropIndex(
                name: "IX_Maquinas_LinhaProducaoId",
                table: "Maquinas");

            migrationBuilder.DropColumn(
                name: "LinhaProducaoId",
                table: "Maquinas");
        }
    }
}
