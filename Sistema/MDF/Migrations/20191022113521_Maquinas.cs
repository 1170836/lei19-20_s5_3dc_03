﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MDF.Migrations
{
    public partial class Maquinas : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Maquinas",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    marca_marca = table.Column<string>(nullable: true),
                    modelo_modelo = table.Column<string>(nullable: true),
                    nSequencia_numeroSeq = table.Column<int>(nullable: false),
                    tMaquinaId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Maquinas", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Maquinas_TipoMaquinas_tMaquinaId",
                        column: x => x.tMaquinaId,
                        principalTable: "TipoMaquinas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Maquinas_tMaquinaId",
                table: "Maquinas",
                column: "tMaquinaId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Maquinas");
        }
    }
}
