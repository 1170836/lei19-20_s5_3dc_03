﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MDF.Migrations
{
    public partial class Operacoes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Operacoes",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    duracao_duracao = table.Column<int>(nullable: false),
                    descricao_descricao = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Operacoes", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Operacoes");
        }
    }
}
