﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MDF.Migrations
{
    public partial class NumeroSerie : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "numeroSerie_numeroSerie",
                table: "Maquinas",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "numeroSerie_numeroSerie",
                table: "Maquinas");
        }
    }
}
