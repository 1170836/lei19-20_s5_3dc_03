﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MDF.Migrations
{
    public partial class Estado : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "estado_estado",
                table: "Maquinas",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "estado_estado",
                table: "Maquinas");
        }
    }
}
