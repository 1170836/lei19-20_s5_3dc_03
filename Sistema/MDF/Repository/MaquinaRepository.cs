using MDF.Models.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using MDF.Models;
using Microsoft.EntityFrameworkCore;
using MDF.Models.Maquinas;
using MDF.Models.TiposMaquina;
using MDF.DTO;

namespace MDF.Models.Repository
{
    public class MaquinaRepository
    {
        private MDFContext _context = null;
        private DbSet<Maquina> table = null;
        public MaquinaRepository(MDFContext context)
        {
            this._context = context;
            table = _context.Set<Maquina>();
        }
        public IEnumerable<Maquina> GetAll()
        {
            return table.ToList();
        }

        public Maquina GetById(object id)
        {
            Maquina m = table.Find(id);

            if (m != null)
            {
                m.tipoMaquina = this._context.TipoMaquinas.Find(m.tMaquinaId);
            }

            return m;
        }

        public Guid Insert(string marca, string modelo, TipoMaquina tipo, string numeroSerie, bool estado)
        {
            Maquina newMachine = new Maquina(marca, modelo, tipo, numeroSerie, estado);


            table.Add(newMachine);
            return newMachine.Id;
        }
        public MaquinaDTO UpdateTipo(Guid id, TipoMaquina novoTipoMaquina)
        {
            Maquina m = this.GetById(id);

            if (m != null)
            {
                m.tipoMaquina = novoTipoMaquina;
                this.Update(m);
                return m.toDTO();
            }
            return null;

        }

        public MaquinaDTO UpdateEstado(Guid id)
        {
            Maquina m = this.GetById(id);

            if (m != null)
            {
                bool oldEstado = m.estado.estado;
                Estado newEstado = new Estado(!oldEstado);

                m.estado = newEstado;
                this.Update(m);
                return m.toDTO();
            }
            return null;

        }

        public void Update(Maquina m)
        {
            table.Attach(m);
            _context.Entry(m).State = EntityState.Modified;
        }
        public bool Delete(object id)
        {
            Maquina existing = table.Find(id);
            if (existing != null)
            {
                table.Remove(existing);
                return true;
            }
            return false;

        }
        public void Save()
        {
            _context.SaveChanges();
        }

        public IEnumerable<Maquina> GetMaquinasByTipo(string id)
        {
            if (table.LongCount() != 0)
            {
                return this.table.Where(m => m.tipoMaquina.descricao.EqualsToString(id));
            }
            return null;
        }


        public IEnumerable<Maquina> GetMaquinasDisponiveisParaLinhaProducao()
        {
            return this.table.Where(m => m.nSequencia.numeroSeq == 0);
        }

        public IEnumerable<Maquina> GetMaquinasDisponiveisParaVisualizacao()
        {
            return this.table.Where(m => m.nSequencia.numeroSeq != 0);
        }

        public Maquina GetMaquinaByNumeroSerie(string nSerie)
        {
            IEnumerable<Maquina> maquinas = this.table.Where(m => m.numeroSerie.EqualsToString(nSerie));
            int i = (int)maquinas.LongCount();
            if (i == 1)
            {
                return maquinas.Single();
            }
            else
            {
                return null;
            }
        }

    }
}