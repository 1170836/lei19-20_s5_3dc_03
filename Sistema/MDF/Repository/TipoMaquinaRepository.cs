using MDF.Models.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using MDF.Models;
using Microsoft.EntityFrameworkCore;
using MDF.Models.TiposMaquina;
using MDF.Models.Operacoes;
using MDF.Models.Association;
using MDF.DTO;

namespace MDF.Models.Repository
{
    public class TipoMaquinaRepository
    {
        private MDFContext _context = null;
        private DbSet<TipoMaquina> table = null;
        private DbSet<TipoMaquinaOperacao> table_asso = null;
        public TipoMaquinaRepository(MDFContext context)
        {
            this._context = context;
            table = _context.Set<TipoMaquina>();
            table_asso = _context.Set<TipoMaquinaOperacao>();
        }
        public IEnumerable<TipoMaquina> GetAll()
        {
            return table.ToList();
        }
        public TipoMaquina GetById(object id)
        {
            return table.Find(id);
        }

        public TipoMaquina GetByDesc(string desc)
        {
            IEnumerable<TipoMaquina> tiposMaquina= this.table.Where(t => t.descricao.EqualsToString(desc));
            int i= (int) tiposMaquina.LongCount();
            if (i==1)
            {
                return tiposMaquina.Single();
            }
            return null;
        }
        public Guid Insert(string desc)
        {
            TipoMaquina tp = new TipoMaquina(desc);

            table.Add(tp);
            return tp.Id;
        }
        public void Update(TipoMaquina obj)
        {
            table.Attach(obj);
            _context.Entry(obj).State = EntityState.Modified;
        }
        public bool Delete(object id)
        {
            TipoMaquina existing = table.Find(id);
            if (existing != null)
            {
                table.Remove(existing);
                return true;
            }
            return false;
        }
        public void Save()
        {
            _context.SaveChanges();
        }

        public void InsertOperacao(TipoMaquina tm, Operacao op)
        {
            TipoMaquinaOperacao newtpmop = new TipoMaquinaOperacao(tm, op);
            table_asso.Add(newtpmop);
        }

        public bool IsOperacaoInTipoMaquina(TipoMaquina tipo, Operacao operacao)
        {
            foreach (TipoMaquinaOperacao item in table_asso)
            {
                TipoMaquina tipoMaquina=item.tipoMaquina;

                if (item.tMaquinaId.Equals(tipo.Id))
                {
                    if (item.operacaoId.Equals(operacao.Id))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public List<Operacao> GetOperacoesByTipoMaquina(TipoMaquina tm){
            List<Operacao> lOp = new List<Operacao>();
            foreach (TipoMaquinaOperacao item in table_asso)
            {
                if (item.tMaquinaId.Equals(tm.Id))
                {
                    Operacao op = item.operacao;

                    if(op==null){
                        op = this._context.Operacoes.Find(item.operacaoId);
                    }
                    
                    lOp.Add(op);
                }
            }
            return lOp;
        }
    }
}