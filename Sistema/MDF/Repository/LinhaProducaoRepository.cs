using MDF.Models.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;
using MDF.Models.Maquinas;
using MDF.Models.TiposMaquina;
using MDF.Models.LinhasProducao;
using MDF.DTO;

namespace MDF.Models.Repository
{
    public class LinhaProducaoRepository
    {
        private MDFContext _context = null;
        private DbSet<LinhaProducao> table = null;
        public LinhaProducaoRepository(MDFContext context)
        {
            this._context = context;
            table = _context.Set<LinhaProducao>();
        }
        public IEnumerable<LinhaProducao> GetAll()
        {
            return table.Include(linha => linha.maquinas).ToList();
        }
        public LinhaProducao GetById(object id)
        {
            return table.Find(id);
        }
        public Guid Insert(List<Maquina> maquinas, int x, int y)
        {
            LinhaProducao newLinha = new LinhaProducao(maquinas,x,y);
            table.Add(newLinha);
            return newLinha.Id;
        }


        public void Update(LinhaProducao obj)
        {
            table.Attach(obj);
            _context.Entry(obj).State = EntityState.Modified;
        }
        public bool Delete(object id)
        {
            LinhaProducao existing = table.Find(id);

            if (existing != null)
            {
                table.Remove(existing);
                return true;
            }
            return false;
        }

        public List<Maquina> maquinasNaLinha(object id)
        {
            LinhaProducao existing = table.Find(id);

            List<LinhaProducao> linhas = _context.LinhasProducao.Include(linha => linha.maquinas).ToList();

            foreach (LinhaProducao linha in linhas)
            {
                if (existing.Id.Equals(linha.Id))
                {
                    return linha.maquinas;
                }
            }

            return null;
        }

        public LinhaProducao getLinhaDeMaquina(Guid id)
        {
            List<LinhaProducao> linhas = _context.LinhasProducao.Include(linha => linha.maquinas).ToList();

            foreach (LinhaProducao linha in linhas)
            {
                foreach (Maquina m in linha.maquinas)
                {
                    if (m.Id.Equals(id))
                    {
                        return linha;
                    }
                }
            }

            return null;
        }
        public void Save()
        {
            _context.SaveChanges();
        }

        public List<MaquinaDTO> maquinasToDTO(List<Maquina> maquinasModelo)
        {
            List<MaquinaDTO> maquinasDTO = new List<MaquinaDTO>();

            foreach (Maquina m in maquinasModelo)
            {
                maquinasDTO.Add(m.toDTO());
            }

            return maquinasDTO;
        }



    }
}