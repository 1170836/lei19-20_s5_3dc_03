using MDF.Models.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using MDF.Models;
using Microsoft.EntityFrameworkCore;
using MDF.Models.Operacoes;
using MDF.DTO;

namespace MDF.Models.Repository
{
    public class OperacaoRepository
    {
        private MDFContext _context = null;
        private DbSet<Operacao> table = null;
        public OperacaoRepository(MDFContext context)
        {
            this._context = context;
            table = _context.Set<Operacao>();
        }
        public IEnumerable<Operacao> GetAll()
        {
            return table.ToList();
        }
        public Operacao GetById(object id)
        {
            return table.Find(id);
        }
 
        public Operacao GetByDesc(string desc)
        {
            IEnumerable<Operacao> operacoes = this.table.Where(o => o.descricao.EqualsToString(desc));
            int i = (int)operacoes.LongCount();
            if (i == 1)
            {
                return operacoes.Single();
            }
            return null;
        }

        public Guid Insert(OperacaoDTO obj)
        {
            Operacao operacao = new Operacao(obj.duracao, obj.descricao, obj.ferramenta);
            table.Add(operacao);
            return operacao.Id;
        }
        public void Update(Operacao obj)
        {
            table.Attach(obj);
            _context.Entry(obj).State = EntityState.Modified;
        }
        public bool Delete(Guid id)
        {

            Operacao existing = table.Find(id);
            if (existing != null)
            {
                table.Remove(existing);
                return true;
            }
            return false;
        }
        public void Save()
        {
            _context.SaveChanges();
        }
    }
}