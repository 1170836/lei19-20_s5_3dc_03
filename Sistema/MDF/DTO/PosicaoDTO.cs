using MDF.DTO;
using System.Collections.Generic;

namespace MDF.DTO
{
    public class PosicaoDTO
    {
        public int x { get; set; }
        public int y { get; set; }

        public PosicaoDTO(int x, int y)
        {
            this.x = x;
            this.y = y;
        }
    }
}