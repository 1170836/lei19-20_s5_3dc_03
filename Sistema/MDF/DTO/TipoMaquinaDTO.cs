using System;
using System.Collections.Generic;

namespace MDF.DTO
{
    public class TipoMaquinaDTO
    {
        public string descricao { get; set; }
        public List<string> lOperacoes{get; set;}
        public TipoMaquinaDTO(string descricao, List<string> lOperacoesP)
        {
            this.descricao = descricao;
            this.lOperacoes = lOperacoesP;
        }
    }
}