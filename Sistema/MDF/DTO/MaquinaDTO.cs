namespace MDF.DTO
{
    public class MaquinaDTO
    {
        public string marca{get;set;}
        public string modelo{get;set;}
        public string descricaoTipoMaquina{get;set;}
         public string numeroSerie{get;set;}


        public MaquinaDTO(string marca, string modelo, string descricaoTipoMaquina,string numeroSerie)
        {
            this.marca = marca;
            this.modelo = modelo;
            this.descricaoTipoMaquina = descricaoTipoMaquina;
            this.numeroSerie=numeroSerie;
        }

    }
}