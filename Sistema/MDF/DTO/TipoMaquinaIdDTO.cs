using System;
using System.Collections.Generic;

namespace MDF.DTO
{
    public class TipoMaquinaIdDTO
    {
        public string descricao { get; set; }
        public List<string> lOperacoes { get; set; }
        public string id { get; set; }
        public TipoMaquinaIdDTO(string id, string descricao, List<string> lOperacoesP)
        {
            this.id = id;
            this.descricao = descricao;
            this.lOperacoes = lOperacoesP;
        }
    }
}