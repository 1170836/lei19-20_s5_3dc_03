namespace MDF.DTO
{
    public class OperacaoDTO
    {
        public string descricao { get; set; }
        public int duracao { get; set; }
        public string ferramenta { get; set; }

        public OperacaoDTO(int duracao, string descricao, string ferramenta)
        {
            this.duracao = duracao;
            this.descricao = descricao;
            this.ferramenta = ferramenta;
        }
    }
}