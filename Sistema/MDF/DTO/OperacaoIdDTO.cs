namespace MDF.DTO
{
    public class OperacaoIdDTO
    {
        public string id { get; set; }
        public string descricao { get; set; }
        public int duracao { get; set; }

        public string ferramenta { get; set; }


        public OperacaoIdDTO(string id, int duracao, string descricao, string ferramenta)
        {
            this.id = id;
            this.duracao = duracao;
            this.descricao = descricao;
            this.ferramenta = ferramenta;
        }
    }
}