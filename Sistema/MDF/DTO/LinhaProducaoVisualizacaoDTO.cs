using MDF.DTO;
using System.Collections.Generic;

namespace MDF.DTO
{
    public class LinhaProducaoVisualizacaoDTO
    {
        public string id { get; set; }
        public List<MaquinaVisualizacaoLinhaDTO> maquinas { get; set; }
        public int x { get; set; }
        public int y { get; set; }

        public LinhaProducaoVisualizacaoDTO(string id, List<MaquinaVisualizacaoLinhaDTO> maquinas, int x, int y)
        {
            this.id = id;
            this.maquinas = maquinas;
            this.x = x;
            this.y = y;
        }
    }
}