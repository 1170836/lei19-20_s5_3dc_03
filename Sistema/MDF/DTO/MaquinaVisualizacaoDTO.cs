namespace MDF.DTO
{
    public class MaquinaVisualizacaoDTO
    {
        public string marca { get; set; }
        public string modelo { get; set; }
        public string descricaoTipoMaquina { get; set; }
        public int nSequencia { get; set; }
        public string linhaProducao { get; set; }
        public string numeroSerie {get; set;}


        public MaquinaVisualizacaoDTO(string marca, string modelo, string descricaoTipoMaquina, int nSequencia, string linhaProducao, string numeroSerie)
        {
            this.marca = marca;
            this.modelo = modelo;
            this.descricaoTipoMaquina = descricaoTipoMaquina;
            this.nSequencia = nSequencia;
            this.linhaProducao = linhaProducao;
            this.numeroSerie=numeroSerie;
        }

    }
}