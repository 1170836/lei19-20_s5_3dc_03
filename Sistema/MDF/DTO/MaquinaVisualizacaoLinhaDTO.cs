namespace MDF.DTO
{
    public class MaquinaVisualizacaoLinhaDTO
    {
        public string marca { get; set; }
        public string modelo { get; set; }
        public int nSequencia { get; set; }
        public string linhaProducao { get; set; }
        public string numeroSerie {get; set;}


        public MaquinaVisualizacaoLinhaDTO(string marca, string modelo, int nSequencia, string linhaProducao, string numeroSerie)
        {
            this.marca = marca;
            this.modelo = modelo;
            this.nSequencia = nSequencia;
            this.linhaProducao = linhaProducao;
            this.numeroSerie=numeroSerie;
        }

    }
}