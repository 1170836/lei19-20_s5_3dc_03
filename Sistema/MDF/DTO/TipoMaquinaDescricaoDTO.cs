using System;
using System.Collections.Generic;

namespace MDF.DTO
{
    public class TipoMaquinaDescricaoDTO
    {
        public string descricao { get; set; }
        public TipoMaquinaDescricaoDTO(string descricao)
        {
            this.descricao = descricao;
        }
    }
}