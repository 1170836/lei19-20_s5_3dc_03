namespace MDF.DTO
{

    public class DescricaoTipoMaquinaDTO
    {

        public string descricaoTipoMaquina { get; set; }

        public DescricaoTipoMaquinaDTO(string descricaoTipoMaquina)
        {
            this.descricaoTipoMaquina = descricaoTipoMaquina;
        }
    }
}