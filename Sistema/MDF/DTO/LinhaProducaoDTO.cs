using MDF.DTO;
using System.Collections.Generic;

namespace MDF.DTO
{
    public class LinhaProducaoDTO
    {
        public List<string> maquinas { get; set; }
        public int x { get; set; }
        public int y { get; set; }

        public LinhaProducaoDTO(List<string> maquinas, int x, int y)
        {
            this.maquinas = maquinas;
            this.x = x;
            this.y = y;
        }
    }
}