using MDF.DTO;
using System.Collections.Generic;

namespace MDF.DTO
{
    public class LinhaProducaoIdDTO
    {
        public string id { get; set; }
        public List<string> maquinas { get; set; }
        public int x { get; set; }
        public int y { get; set; }

        public LinhaProducaoIdDTO(string id, List<string> maquinas, int x, int y)
        {
            this.id = id;
            this.maquinas = maquinas;
            this.x = x;
            this.y = y;
        }
    }
}