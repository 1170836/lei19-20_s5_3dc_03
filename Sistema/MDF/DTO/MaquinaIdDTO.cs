namespace MDF.DTO
{
    public class MaquinaIdDTO
    {
        public string id { get; set; }
        public string marca { get; set; }
        public string modelo { get; set; }
        public string descricaoTipoMaquina { get; set; }
        public string numeroSerie { get; set; }
        public bool estado { get; set; }


        public MaquinaIdDTO(string id, string marca, string modelo, string descricaoTipoMaquina, string numeroSerie, bool estado)
        {
            this.id = id;
            this.marca = marca;
            this.modelo = modelo;
            this.descricaoTipoMaquina = descricaoTipoMaquina;
            this.numeroSerie = numeroSerie;
            this.estado = estado;
        }

    }
}