namespace MDF.DTO
{
    public class MaquinaNumeroSequenciaDTO
    {
        public int nSequencia { get; set; }
        public string numeroSerie { get; set; }


        public MaquinaNumeroSequenciaDTO(int nSequencia, string numeroSerie)
        {
            this.nSequencia = nSequencia;
            this.numeroSerie = numeroSerie;
        }

    }
}