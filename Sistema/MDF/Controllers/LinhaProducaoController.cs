using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using MDF.Models;
using MDF.Models.Maquinas;
using System;
using MDF.Models.Repository;
using MDF.Models.LinhasProducao;
using MDF.DTO;

namespace MDF.Controllers
{
    [Route("api/MDF/[controller]")]
    [ApiController]
    public class LinhaProducaoController : ControllerBase
    {

        private MaquinaRepository repository = null;
        private LinhaProducaoRepository repositoryTipo = null;

        public LinhaProducaoController(MDFContext context)
        {
            this.repositoryTipo = new LinhaProducaoRepository(context);
            this.repository = new MaquinaRepository(context);
        }

        [HttpGet]
        public ActionResult GetLinhas()
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                IEnumerable<LinhaProducao> linhas = this.repositoryTipo.GetAll();
                List<LinhaProducaoVisualizacaoDTO> dtos = new List<LinhaProducaoVisualizacaoDTO>();
                if (linhas.LongCount() != 0)
                {
                    foreach (var l in linhas)
                    {
                        List<Maquina> maqs = this.repositoryTipo.maquinasNaLinha(l.Id);
                        List<MaquinaVisualizacaoLinhaDTO> maqsDto = new List<MaquinaVisualizacaoLinhaDTO>();

                        foreach (var m in maqs)
                        {
                            maqsDto.Add(m.toVisualizacaoLinhaDTO(l.Id.ToString()));
                        }

                        dtos.Add(new LinhaProducaoVisualizacaoDTO(l.Id.ToString(), maqsDto, l.pos.x, l.pos.y));
                    }

                }
                return Ok(dtos);
            }
            catch (FormatException)
            {
                return BadRequest();
            }
        }

        [HttpGet("maquinasAtivas")]
        public ActionResult GetLinhasMaquinasAtivas()
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                IEnumerable<LinhaProducao> linhas = this.repositoryTipo.GetAll();
                List<LinhaProducaoVisualizacaoDTO> dtos = new List<LinhaProducaoVisualizacaoDTO>();
                if (linhas.LongCount() != 0)
                {
                    foreach (var l in linhas)
                    {
                        List<Maquina> maqs = this.repositoryTipo.maquinasNaLinha(l.Id);
                        List<MaquinaVisualizacaoLinhaDTO> maqsDto = new List<MaquinaVisualizacaoLinhaDTO>();

                        foreach (var m in maqs)
                        {
                            if (m.estado.estado)
                            {
                                maqsDto.Add(m.toVisualizacaoLinhaDTO(l.Id.ToString()));
                            }
                        }

                        dtos.Add(new LinhaProducaoVisualizacaoDTO(l.Id.ToString(), maqsDto, l.pos.x, l.pos.y));
                    }

                }
                return Ok(dtos);
            }
            catch (FormatException)
            {
                return BadRequest();
            }
        }

        [HttpGet("{id}")]
        public ActionResult GetLinha(string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                Guid newId = Guid.Parse(id);
                LinhaProducao l = this.repositoryTipo.GetById(newId);

                if (l != null)
                {
                    List<Maquina> maqs = this.repositoryTipo.maquinasNaLinha(l.Id);
                    List<MaquinaVisualizacaoLinhaDTO> maqsDto = new List<MaquinaVisualizacaoLinhaDTO>();

                    foreach (var m in maqs)
                    {
                        maqsDto.Add(m.toVisualizacaoLinhaDTO(l.Id.ToString()));
                    }

                    return Ok(new LinhaProducaoVisualizacaoDTO(l.Id.ToString(), maqsDto, l.pos.x, l.pos.y));
                }
                else
                {
                    return NotFound("Linha não encontrada");
                }


            }
            catch (FormatException)
            {
                return BadRequest();
            }
        }


        [HttpPost]
        public ActionResult AddLinhaProducao(LinhaProducaoDTO dto)
        {
            if (ModelState.IsValid)
            {

                if (dto != null)
                {
                    try
                    {
                        int i = 1;
                        List<Maquina> lMaquinas = new List<Maquina>();

                        foreach (string tryMaquina in dto.maquinas)
                        {

                            if (tryMaquina != null)
                            {
                                Guid toCompare = Guid.Parse(tryMaquina);
                                Maquina m = this.repository.GetById(toCompare);
                                if (m != null)
                                {
                                    m.atualizarParaLinhaProducao(i++);
                                    this.repository.Update(m);

                                    lMaquinas.Add(m);
                                }
                            }
                        }

                        if (lMaquinas.LongCount() != 0 && this.controlarPosicaoNaFabrica(dto.x, dto.y, lMaquinas))
                        {
                            Guid newId = this.repositoryTipo.Insert(lMaquinas, dto.x, dto.y);
                            this.repository.Save();
                            this.repositoryTipo.Save();

                            return Ok(new LinhaProducaoIdDTO(newId.ToString(), dto.maquinas, dto.x, dto.y));
                        }
                        else
                        {
                            return BadRequest("Linha de Produção inválida");
                        }


                    }
                    catch (ArgumentException)
                    {
                        return BadRequest();
                    }
                    catch (InvalidOperationException)
                    {
                        return BadRequest();
                    }

                }
                else
                {
                    return BadRequest("Linha de Produção não introduzida.");
                }
            }
            return ValidationProblem();
        }

        private bool controlarPosicaoNaFabrica(int x, int y, List<Maquina> lMaquinas)
        {
            if (x < -43)
            {
                return false;
            }

            foreach (LinhaProducao l in this.repositoryTipo.GetAll())
            {
                if (l.pos.y - y > -6 && y - l.pos.y < 6)
                {
                    int xMaximoNova = x + (lMaquinas.Count - 1) * 7;

                    int xInicial = l.pos.x;
                    int xFinal = xInicial + (l.maquinas.Count - 1) * 7;

                    if (!(xMaximoNova < xInicial - 3 || x > xFinal + 3)) return false;
                }
            }

            int numMaximoMaquinas = (50 - x) / 7;
            if ((y <= -20 && lMaquinas.Count <= (numMaximoMaquinas - 2)) || (y > -20 && lMaquinas.Count <= numMaximoMaquinas))
            {
                return true;
            }
            return false;
        }

        [HttpPut("{id}")]
        public ActionResult EditPosicaoLinha(string id, PosicaoDTO dto)
        {
            if (ModelState.IsValid)
            {

                if (dto != null)
                {
                    try
                    {
                        Guid guid = Guid.Parse(id);
                        LinhaProducao l = this.repositoryTipo.GetById(guid);

                        if (l != null && this.controlarPosicaoNaFabricaExistente(guid, dto.x, dto.y, this.repositoryTipo.maquinasNaLinha(guid)))
                        {
                            l.atualizarPosicao(dto.x, dto.y);

                            this.repositoryTipo.Update(l);
                            this.repositoryTipo.Save();

                            return Ok(new PosicaoDTO(dto.x, dto.y));
                        }
                        else
                        {
                            return BadRequest("Linha de Produção inválida");
                        }


                    }
                    catch (ArgumentException)
                    {
                        return BadRequest();
                    }
                    catch (InvalidOperationException)
                    {
                        return BadRequest();
                    }

                }
                else
                {
                    return BadRequest("Linha de Produção não introduzida.");
                }
            }
            return ValidationProblem();
        }

        private bool controlarPosicaoNaFabricaExistente(Guid id, int x, int y, List<Maquina> lMaquinas)
        {
            if (x < -43)
            {
                return false;
            }

            foreach (LinhaProducao l in this.repositoryTipo.GetAll())
            {
                if (l.Id != id && l.pos.y - y > -6 && l.pos.y - y < 6)
                {
                    int xMaximoNova = x + (lMaquinas.Count - 1) * 7;

                    int xInicial = l.pos.x;
                    int xFinal = xInicial + (l.maquinas.Count - 1) * 7;

                    if (!(xMaximoNova < xInicial - 3 || x > xFinal + 3)) return false;
                }
            }

            int numMaximoMaquinas = (50 - x) / 7;
            if ((y <= -20 && lMaquinas.Count <= (numMaximoMaquinas - 2)) || (y > -20 && lMaquinas.Count <= numMaximoMaquinas))
            {
                return true;
            }
            return false;
        }

        [HttpPut("maquina/{id}")]
        public ActionResult EditPosicaoMaquinaNaLinha(string id, MaquinaNumeroSequenciaDTO dto)
        {
            if (ModelState.IsValid)
            {

                if (dto != null)
                {
                    try
                    {
                        Guid guid = Guid.Parse(id);
                        LinhaProducao l = this.repositoryTipo.GetById(guid);

                        Maquina m = this.repository.GetMaquinaByNumeroSerie(dto.numeroSerie);

                        if (m != null && l != null)
                        {

                            int nSequenciaAnt = m.nSequencia.numeroSeq;
                            List<Maquina> maquinas = this.repositoryTipo.maquinasNaLinha(guid);

                            if (dto.nSequencia < 1 || dto.nSequencia > maquinas.Count)
                            {
                                return BadRequest();
                            }

                            foreach (Maquina maq in maquinas)
                            {

                                if (maq.nSequencia.numeroSeq == dto.nSequencia)
                                {
                                    maq.atualizarNumeroSequencia(nSequenciaAnt);
                                    this.repository.Update(maq);
                                    break;
                                }
                            }

                            m.atualizarNumeroSequencia(dto.nSequencia);
                            this.repository.Update(m);
                            this.repository.Save();

                            return Ok(new MaquinaVisualizacaoLinhaDTO(m.marca.marca, m.modelo.modelo, m.nSequencia.numeroSeq, id, m.numeroSerie.numeroSerie));
                        }
                        else
                        {
                            return BadRequest();
                        }

                    }
                    catch (ArgumentException)
                    {
                        return BadRequest();
                    }
                    catch (InvalidOperationException)
                    {
                        return BadRequest();
                    }

                }
                else
                {
                    return BadRequest("Linha de Produção não introduzida.");
                }
            }
            return ValidationProblem();
        }

        [HttpDelete("{id}")]
        public ActionResult DeleteLinha(string id)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    Guid newId = Guid.Parse(id);

                    List<Maquina> maquinas = this.repositoryTipo.maquinasNaLinha(newId);
                    if (maquinas == null || maquinas.Count != 0)
                    {
                        return BadRequest();
                    }

                    if (this.repositoryTipo.Delete(newId))
                    {
                        this.repositoryTipo.Save();
                        return Ok("Linha eliminada");
                    }
                }
                catch (FormatException)
                {
                    return BadRequest("Id inválido");
                }

                return NotFound();
            }

            return BadRequest();
        }

    }
}