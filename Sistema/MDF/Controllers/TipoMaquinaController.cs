using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MDF.Models;
using MDF.Models.TiposMaquina;
using MDF.Models.Repository;
using System.IO;
using Newtonsoft.Json.Linq;
using System;
using MDF.Models.Operacoes;
using MDF.DTO;

namespace MDF.Controllers
{
    [Route("api/MDF/[controller]")]
    [ApiController]
    public class TipoMaquinaController : ControllerBase
    {
        private TipoMaquinaRepository repository = null;
        private OperacaoRepository repositoryOp = null;
        private bool tpmflag = false;

        public TipoMaquinaController(MDFContext context)
        {
            this.repository = new TipoMaquinaRepository(context);
            this.repositoryOp = new OperacaoRepository(context);
        }

        [HttpGet]
        public ActionResult GetTiposMaquina()
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                IEnumerable<TipoMaquina> tm = this.repository.GetAll();
                List<TipoMaquinaDescricaoDTO> dtos = new List<TipoMaquinaDescricaoDTO>();
                if (tm.LongCount() != 0)
                {
                    foreach (var maq in tm)
                    {
                        dtos.Add(new TipoMaquinaDescricaoDTO(maq.descricao.descricao));
                    }

                }
                return Ok(dtos);
            }
            catch (FormatException)
            {
                return BadRequest();
            }
        }

        [HttpPost]
        public ActionResult AddTipoMaquina(TipoMaquinaDTO tmdto)
        {
            if (ModelState.IsValid)
            {
                if (tmdto != null)
                {
                    if (this.repository.GetByDesc(tmdto.descricao) != null)
                    {
                        return Ok("Tipo de Máquina já existente!");
                    }

                    try
                    {
                        List<Operacao> lOps = new List<Operacao>();

                        if (tmdto.lOperacoes.LongCount() == 0)
                        {
                            return BadRequest();
                        }
                        foreach (string item in tmdto.lOperacoes)
                        {
                            if (item != null)
                            {
                                Operacao op = this.repositoryOp.GetByDesc(item);

                                if (op != null)
                                {
                                    lOps.Add(op);
                                }
                            }
                        }

                        if (lOps.LongCount() == 0)
                        {
                            return BadRequest();
                        }

                        Guid tpid = this.repository.Insert(tmdto.descricao);

                        this.repository.Save();

                        foreach (Operacao item in lOps)
                        {
                            this.repository.InsertOperacao(this.repository.GetByDesc(tmdto.descricao), item);
                        }

                        this.repository.Save();

                        return Ok(new TipoMaquinaIdDTO(tpid.ToString(), tmdto.descricao, tmdto.lOperacoes));

                    }
                    catch (ArgumentException)
                    {
                        return ValidationProblem();
                    }
                }
                else
                {
                    return BadRequest();
                }
            }
            return Ok();
        }
        [HttpPut]
        public ActionResult EditTipoMaquina(TipoMaquinaDTO tmdto)
        {
            if (ModelState.IsValid)
            {
                if (tmdto != null)
                {
                    try
                    {
                        List<Operacao> lOps = new List<Operacao>();

                        if (tmdto.lOperacoes.LongCount() == 0)
                        {
                            return BadRequest();
                        }

                        foreach (string item in tmdto.lOperacoes)
                        {
                            if (item != null)
                            {
                                Operacao op = this.repositoryOp.GetByDesc(item);

                                if (op != null)
                                {
                                    lOps.Add(op);
                                }
                            }
                        }

                        if (lOps.LongCount() == 0)
                        {
                            return BadRequest();
                        }

                        TipoMaquina tp = this.repository.GetByDesc(tmdto.descricao);

                        foreach (Operacao item in lOps)
                        {
                            if (!this.repository.IsOperacaoInTipoMaquina(tp, item))
                            {
                                tpmflag = true;
                                this.repository.InsertOperacao(tp, item);
                                this.repository.Save();
                            }
                        }
                        if (tpmflag)
                        {
                            return Ok("Tipo de Máquina atualizado com sucesso.");
                        }
                        else
                        {
                            return Ok("Tipo de Máquina já contém operação.");
                        }

                    }
                    catch (ArgumentException)
                    {
                        return ValidationProblem();
                    }
                }
                else
                {
                    return BadRequest();
                }
            }
            return Ok();
        }



        // GET: api/Todo/tipomaquina/Operacoes/"desc"
        //Retorna as Operações do Tipo de Máquina.
        [HttpGet("Operacoes/{desc}")]
        public ActionResult<TipoMaquina> GetOpsByTipoMaquina(string desc)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    TipoMaquina tp = this.repository.GetByDesc(desc);
                    if (tp != null)
                    {
                        List<Operacao> lOps = this.repository.GetOperacoesByTipoMaquina(tp);

                        if (lOps != null)
                        {
                            return Ok(tp.toDTO(lOps));
                        }

                    }
                    return NotFound();
                }
                catch (FormatException)
                {
                    return BadRequest("Descrição inválida.");
                }
            }
            else
            {
                return BadRequest(ModelState);
            }
        }

        // GET: api/Todo/tipomaquina/id
        // Retorna a descrição do tipo de Máquina
        [HttpGet("{id}")]
        public ActionResult<TipoMaquina> GetTipoMaquina(string id)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    Guid newId = Guid.Parse(id);

                    TipoMaquina tp = this.repository.GetById(newId);

                    if (tp != null)
                    {
                        return Ok(tp.descricao.descricao);
                    }
                    return NotFound();
                }
                catch (FormatException)
                {
                    return BadRequest("Id inválido.");
                }
            }
            else
            {
                return BadRequest(ModelState);
            }
        }

        [HttpGet("isTM/{descricao}")]
        public ActionResult<TipoMaquina> isTipoMaquina(string descricao)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    
                    TipoMaquina tp = this.repository.GetByDesc(descricao);

                    if (tp != null)
                    {
                        return Ok("Tipo de Máquina existente");
                    }
                    else
                    {
                        return NotFound();
                    }

                }
                catch (FormatException)
                {
                    return BadRequest("Descrição inválida.");
                }
            }
            else
            {
                return BadRequest(ModelState);
            }
        }

        [HttpDelete("{id}")]
        public ActionResult DeleteMaquina(string id)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    Guid newId = Guid.Parse(id);

                    if (this.repository.Delete(newId))
                    {
                        this.repository.Save();
                        return Ok("Tipo de Máquina eliminado");
                    }
                    return NotFound();
                }
                catch (FormatException)
                {
                    return BadRequest("Id inválido.");
                }
            }
            return BadRequest();
        }

    }
}