using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MDF.Models;
using MDF.Models.TiposMaquina;
using MDF.Models.Maquinas;
using MDF.Models.Repository;
using System.IO;
using Newtonsoft.Json.Linq;
using System;
using MDF.DTO;

namespace MDF.Controllers
{
    [Route("api/MDF/[controller]")]
    [ApiController]
    public class MaquinaController : ControllerBase
    {

        private MaquinaRepository repository = null;
        private TipoMaquinaRepository repositoryTipo = null;
        private LinhaProducaoRepository repositoryLinha = null;

        public MaquinaController(MDFContext context)
        {
            this.repository = new MaquinaRepository(context);
            this.repositoryTipo = new TipoMaquinaRepository(context);
            this.repositoryLinha = new LinhaProducaoRepository(context);
        }

        [HttpGet]
        public ActionResult GetMaquinas()
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                IEnumerable<Maquina> m = this.repository.GetAll();
                List<MaquinaIdDTO> dtos = new List<MaquinaIdDTO>();
                if (m.LongCount() != 0)
                {
                    foreach (var maq in m)
                    {
                        string descricaoTipoMaquina = this.repositoryTipo.GetById(maq.tMaquinaId).descricao.descricao;
                        dtos.Add(new MaquinaIdDTO(maq.Id.ToString(), maq.marca.marca, maq.modelo.modelo, descricaoTipoMaquina, maq.numeroSerie.numeroSerie, maq.estado.estado));
                    }

                }
                return Ok(dtos);
            }
            catch (FormatException)
            {
                return BadRequest();
            }
        }

        [HttpGet("visualizacao")]
        public ActionResult GetMaquinasVisualizacao()
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            List<MaquinaVisualizacaoDTO> dtos = new List<MaquinaVisualizacaoDTO>();

            foreach (Maquina maq in this.repository.GetMaquinasDisponiveisParaVisualizacao())
            {
                string descricaoTipoMaquina = this.repositoryTipo.GetById(maq.tMaquinaId).descricao.descricao;

                dtos.Add(new MaquinaVisualizacaoDTO(maq.marca.marca, maq.modelo.modelo, descricaoTipoMaquina, maq.nSequencia.numeroSeq, this.repositoryLinha.getLinhaDeMaquina(maq.Id).Id.ToString(), maq.numeroSerie.numeroSerie));
            }
            return Ok(dtos);
        }

        [HttpGet("{id}")]
        public ActionResult GetMaquina(string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                Guid newId = Guid.Parse(id);
                Maquina m = this.repository.GetById(newId);

                if (m != null)
                {
                    return Ok(m.toDTO());
                }

                return NotFound();
            }
            catch (FormatException)
            {
                return BadRequest("Id inválido");
            }
        }

        [HttpGet("numeroSerie/{id}")]
        public ActionResult GetMaquinaByNumeroSerie(string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {

                Maquina m = this.repository.GetMaquinaByNumeroSerie(id);
                if (m != null)
                {

                    string descricaoTipoMaquina = this.repositoryTipo.GetById(m.tMaquinaId).descricao.descricao;

                    return Ok(new MaquinaDTO(m.marca.marca, m.modelo.modelo, descricaoTipoMaquina, m.numeroSerie.numeroSerie));

                }
                return NotFound();
            }
            catch (FormatException)
            {
                return BadRequest("Id inválido");
            }
        }



        [HttpGet("tipoMaquina/{id}")]
        public ActionResult GetMaquinaByDesc(string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            List<MaquinaDTO> dtos = new List<MaquinaDTO>();

            foreach (Maquina m in this.repository.GetMaquinasByTipo(id))
            {
                dtos.Add(new MaquinaDTO(m.marca.marca, m.modelo.modelo, id, m.numeroSerie.numeroSerie));
            }
            return Ok(dtos);
        }

        [HttpGet("linhaProducao")]
        public ActionResult GetMaquinaDisponiveisParaLinhaProducao()
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            List<MaquinaIdDTO> dtos = new List<MaquinaIdDTO>();

            foreach (Maquina maq in this.repository.GetMaquinasDisponiveisParaLinhaProducao())
            {
                string descricaoTipoMaquina = this.repositoryTipo.GetById(maq.tMaquinaId).descricao.descricao;
                dtos.Add(new MaquinaIdDTO(maq.Id.ToString(), maq.marca.marca, maq.modelo.modelo, descricaoTipoMaquina, maq.numeroSerie.numeroSerie, maq.estado.estado));
            }
            return Ok(dtos);
        }

        [HttpPost]
        public ActionResult AddMaquina(MaquinaDTO dto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }


            if (dto.marca != null && dto.modelo != null && dto.descricaoTipoMaquina != null && dto.numeroSerie != null)
            {
                try
                {
                    if (this.repository.GetMaquinaByNumeroSerie(dto.numeroSerie) == null)
                    {
                        Guid newId = this.repository.Insert(dto.marca, dto.modelo, this.repositoryTipo.GetByDesc(dto.descricaoTipoMaquina), dto.numeroSerie, true);
                        this.repository.Save();

                        return Ok(new MaquinaIdDTO(newId.ToString(), dto.marca, dto.modelo, dto.descricaoTipoMaquina, dto.numeroSerie, true));
                    }
                    return BadRequest("Número de Serie já existe");
                }
                catch (ArgumentException)
                {
                    return ValidationProblem();
                }

            }
            else
            {
                return BadRequest();
            }
        }

        [HttpPut("estado/{id}")]
        public ActionResult alterarEstado(string id)
        {
            if (ModelState.IsValid)
            {
                try
                {

                    Guid guid = Guid.Parse(id);
                    MaquinaDTO dto = this.repository.UpdateEstado(guid);
                    if (dto != null)
                    {
                        this.repository.Save();
                        return Ok("Estado alterado");
                    }
                    return ValidationProblem();
                }
                catch (FormatException)
                {
                    return BadRequest("Id inválido");
                }
            }
            return BadRequest();
        }

        [HttpPut("{id}")]
        public ActionResult alterarTipoMaquina(string id, DescricaoTipoMaquinaDTO maquina)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    TipoMaquina novoTipoMaquina = this.repositoryTipo.GetByDesc(maquina.descricaoTipoMaquina);

                    if (novoTipoMaquina != null)
                    {
                        Guid guid = Guid.Parse(id);
                        MaquinaDTO dto = this.repository.UpdateTipo(guid, novoTipoMaquina);
                        if (dto != null)
                        {
                            this.repository.Save();
                            return Ok(dto);
                        }
                        return ValidationProblem();
                    }
                    return ValidationProblem();
                }
                catch (FormatException)
                {
                    return BadRequest("Id inválido");
                }
            }
            return BadRequest();
        }

        [HttpDelete("{id}")]
        public ActionResult DeleteMaquina(string id)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    Guid newId = Guid.Parse(id);

                    if (this.repository.Delete(newId))
                    {
                        this.repository.Save();
                        return Ok("Máquina eliminada");
                    }
                    return NotFound();
                }
                catch (FormatException)
                {
                    return BadRequest("Id inválido");
                }
            }
            return BadRequest();
        }


    }
}