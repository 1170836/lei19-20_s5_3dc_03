using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MDF.Models;
using MDF.Models.Operacoes;
using MDF.Models.Repository;
using System.IO;
using Newtonsoft.Json.Linq;
using MDF.DTO;
using System;

namespace MDF.Controllers
{
    [Route("api/MDF/Operacao")]
    [ApiController]
    public class OperacoesController : ControllerBase
    {

        private OperacaoRepository repository = null;

        public OperacoesController(MDFContext context)
        {
            this.repository = new OperacaoRepository(context);
        }

        [HttpGet]
        public ActionResult GetOperacoes()
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }


            IEnumerable<Operacao> ops = this.repository.GetAll();
            List<OperacaoIdDTO> dtos = new List<OperacaoIdDTO>();
            if (ops.LongCount() != 0)
            {
                foreach (var o in ops)
                {
                    dtos.Add(new OperacaoIdDTO(o.Id.ToString(), o.duracao.duracao, o.descricao.descricao, o.ferramenta.ferramenta));
                }

            }
            return Ok(dtos);
        }


        [HttpGet("{desc}")]
        public ActionResult GetOperacao(string desc)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            try
            {

                Operacao op = this.repository.GetByDesc(desc);

                if (op != null)
                {
                    OperacaoIdDTO dto = new OperacaoIdDTO(op.Id.ToString(), op.duracao.duracao, op.descricao.descricao, op.ferramenta.ferramenta);
                    return Ok(dto);
                }
                return NotFound();
            }
            catch (FormatException)
            {
                return BadRequest("Id inválido");
            }

        }


        [HttpPost]
        public ActionResult AddOperacao(OperacaoDTO op)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    Operacao operacao = this.repository.GetByDesc(op.descricao);
                    if (operacao == null)
                    {
                        Guid guid = repository.Insert(op);
                        OperacaoIdDTO dto = new OperacaoIdDTO(guid.ToString(), op.duracao, op.descricao, op.ferramenta);
                        repository.Save();
                        return Ok(dto);
                    }
                    return Ok("Ja existe uma operação com essa descrição!");
                }
                catch (ArgumentException)
                {
                    ValidationProblem();
                }

            }
            return BadRequest();
        }

        [HttpDelete("{id}")]
        public ActionResult DeleteOperacao(string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            try
            {
                Guid newId = Guid.Parse(id);
                if (this.repository.Delete(newId))
                {
                    this.repository.Save();
                    return Ok();
                }

                return NotFound();
            }
            catch (FormatException)
            {
                return BadRequest("Id inválido");
            }

        }


    }
}