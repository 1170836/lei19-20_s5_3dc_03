using MDF.Models.TiposMaquina;
using MDF.Models.Operacoes;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace MDF.Models.Association{

    public class TipoMaquinaOperacao{

        public Guid tMaquinaId{get; set;}
        [ForeignKey("tMaquinaId")]
        public TipoMaquina tipoMaquina { get; set; }

        public Guid operacaoId{get; set;}
        [ForeignKey("operacaoId")]
        public Operacao operacao { get; set; }

        public TipoMaquinaOperacao()
        {
        }
        

        public TipoMaquinaOperacao(TipoMaquina tipoMaquina, Operacao operacao)
        {
            this.tipoMaquina = tipoMaquina;
            this.operacao = operacao;
        }
    }
}