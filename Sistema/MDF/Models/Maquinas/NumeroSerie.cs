using System.Collections.Generic;
using MDF.Models.Shared;

namespace MDF.Models.Maquinas
{
    public class NumeroSerie : ValueObject
    {
        public string numeroSerie { get; set; }

        public NumeroSerie(string numeroSerie)
        {
            this.numeroSerie = numeroSerie;
        }

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return this.numeroSerie;
        }
        protected NumeroSerie(){
            
        }

         public bool EqualsToString(string nSerie){
            return this.numeroSerie.Equals(nSerie);
        }
    }
}