using System.Collections.Generic;
using MDF.Models.Shared;

namespace MDF.Models.Maquinas
{
    public class NumeroSequencia : ValueObject
    {
        public int numeroSeq { get; set; }

        public NumeroSequencia(int numeroSeq)
        {
            this.numeroSeq = numeroSeq;
        }

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return this.numeroSeq;
        }
    }
}