using System.Collections.Generic;
using MDF.Models.Shared;

namespace MDF.Models.Maquinas
{
    public class Modelo : ValueObject
    {
        public string modelo { get; set; }

        public Modelo(string modelo)
        {
            this.modelo = modelo;
        }

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return this.modelo;
        }
    }
}