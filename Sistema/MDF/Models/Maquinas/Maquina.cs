using System;
using System.ComponentModel.DataAnnotations.Schema;
using MDF.Models.TiposMaquina;
using MDF.Models.Shared;
using MDF.DTO;

namespace MDF.Models.Maquinas
{
    public class Maquina : Entity
    {
        public Marca marca { get; set; }
        public Modelo modelo { get; set; }
        public NumeroSequencia nSequencia { get; set; }

        public Guid tMaquinaId { get; set; }
        [ForeignKey("tMaquinaId")]
        public TipoMaquina tipoMaquina { get; set; }

        public NumeroSerie numeroSerie { get; set; }

        public Estado estado { get; set; }

        protected Maquina()
        {
        }

        public Maquina(string marca, string modelo, TipoMaquina tipoMaquina, string numeroSerie,bool estado)
        {
            if (tipoMaquina != null)
            {
                this.marca = new Marca(marca);
                this.modelo = new Modelo(modelo);
                this.tipoMaquina = tipoMaquina;
                this.nSequencia = new NumeroSequencia(0);
                this.numeroSerie = new NumeroSerie(numeroSerie);
                this.estado = new Estado(estado);
            }
            else
            {
                throw new ArgumentException();
            }

        }

        public void atualizarParaLinhaProducao(int nSequencia)
        {
            if (this.nSequencia.numeroSeq == 0)
            {
                this.nSequencia = new NumeroSequencia(nSequencia);
            }
            else
            {
                throw new InvalidOperationException();
            }
        }

        public void atualizarNumeroSequencia(int nSequencia)
        {
            this.nSequencia = new NumeroSequencia(nSequencia);
        }

        public MaquinaDTO toDTO()
        {
            return new MaquinaDTO(this.marca.marca, this.modelo.modelo, this.tipoMaquina.descricao.descricao, this.numeroSerie.numeroSerie);
        }

        public MaquinaVisualizacaoLinhaDTO toVisualizacaoLinhaDTO(string linhaProd)
        {
            return new MaquinaVisualizacaoLinhaDTO(this.marca.marca, this.modelo.modelo, this.nSequencia.numeroSeq, linhaProd, this.numeroSerie.numeroSerie);
        }
    }
}