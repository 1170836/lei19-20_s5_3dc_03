using System.Collections.Generic;
using MDF.Models.Shared;

namespace MDF.Models.Maquinas
{
    public class Marca : ValueObject
    {
        public string marca { get; set; }

        public Marca(string marca)
        {
            this.marca = marca;
        }

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return this.marca;
        }
    }
}