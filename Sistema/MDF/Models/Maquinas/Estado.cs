using System.Collections.Generic;
using MDF.Models.Shared;

namespace MDF.Models.Maquinas
{
    public class Estado : ValueObject
    {
        public bool estado { get; set; }

        public Estado(bool estado)
        {
            this.estado = estado;
        }

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return this.estado;
        }
    }
}