using System.Collections.Generic;
using System;

namespace MDF.Models.Shared
{
    /// {summary}
    /// Base class for entities
    /// {/summary}

    public abstract class Entity
    {
        public virtual Guid Id{get; set;}
    }
}