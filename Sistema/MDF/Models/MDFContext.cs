using Microsoft.EntityFrameworkCore;
using MDF.Models.Operacoes;
using MDF.Models.TiposMaquina;
using MDF.Models.Maquinas;
using MDF.Models.Association;
using MDF.Models.LinhasProducao;

namespace MDF.Models
{
    public class MDFContext : DbContext
    {
        public MDFContext(DbContextOptions<MDFContext> options)
            : base(options)
        {
        }

        protected MDFContext(){}

        public DbSet<Operacao> Operacoes { get; set; }
        public DbSet<TipoMaquina> TipoMaquinas { get; set; }
        public DbSet<Maquina> Maquinas { get; set; }
        public DbSet<LinhaProducao> LinhasProducao { get; set; }
        public DbSet<TipoMaquinaOperacao> AssociationTipoMaquinaOperacao{ get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<Operacao>().HasKey(p => p.Id);
            builder.Entity<Operacao>().OwnsOne<DuracaoOperacao>(p => p.duracao);
            builder.Entity<Operacao>().OwnsOne<DescricaoOperacao>(p => p.descricao);
            builder.Entity<Operacao>().OwnsOne<Ferramenta>(p => p.ferramenta);

            builder.Entity<TipoMaquina>().HasKey(t => t.Id);
            builder.Entity<TipoMaquina>().OwnsOne<DescricaoTipo>(t => t.descricao);

            builder.Entity<TipoMaquinaOperacao>().HasKey(bc => new { bc.tMaquinaId, bc.operacaoId });
           
            builder.Entity<Maquina>().HasKey(m => m.Id);
            builder.Entity<Maquina>().OwnsOne<Marca>(m => m.marca);
            builder.Entity<Maquina>().OwnsOne<Modelo>(m => m.modelo);
            builder.Entity<Maquina>().OwnsOne<NumeroSequencia>(m => m.nSequencia);
            builder.Entity<Maquina>().OwnsOne<NumeroSerie>(m => m.numeroSerie);
            builder.Entity<Maquina>().OwnsOne<Estado>(m => m.estado);

            builder.Entity<LinhaProducao>().HasMany<Maquina>(l => l.maquinas);
            builder.Entity<LinhaProducao>().OwnsOne<Posicao>(l => l.pos);
        }
    }
}
