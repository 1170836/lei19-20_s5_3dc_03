using MDF.Models.Shared;
using System.Collections.Generic;
using MDF.Models.Operacoes;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using MDF.DTO;
 
namespace MDF.Models.TiposMaquina
{
    public class TipoMaquina :  Entity
    {       

        [Column("Descricao")]
        public DescricaoTipo descricao { get; set;}

        protected TipoMaquina(){
        }
        
        public TipoMaquina(string desc){
            descricao = new DescricaoTipo(desc);
        }

        public TipoMaquinaDTO toDTO(List<Operacao> lOps)
        {
            List<string> lOpstoString = new List<string>();
            foreach (Operacao item in lOps)
            {
                lOpstoString.Add(item.descricao.descricao);
            }
            return new TipoMaquinaDTO(this.descricao.descricao, lOpstoString);
        }
    }
}