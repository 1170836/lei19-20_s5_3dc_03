using System.Collections.Generic;
using MDF.Models.Shared;

namespace MDF.Models.TiposMaquina
{
    public class DescricaoTipo : ValueObject
    {
        public string descricao { get; set; }

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return descricao;
        }

        public DescricaoTipo(string desc){
            descricao = desc;
        }
        
        protected DescricaoTipo(){
        }

        public bool EqualsToString(string descricao2){
            return this.descricao.Equals(descricao2);
        }
    }
}