using MDF.Models.Shared;
using System.Collections.Generic;
using MDF.Models.Maquinas;

namespace MDF.Models.LinhasProducao
{
    public class LinhaProducao : Entity
    {
        public Posicao pos { get; set; }
        public List<Maquina> maquinas { get; set; }

        protected LinhaProducao()
        {
        }
        public LinhaProducao(List<Maquina> maquinasP, int x, int y)
        {
            this.maquinas = maquinasP;
            this.pos = new Posicao(x, y);
        }

        public void atualizarPosicao(int x, int y)
        {
            this.pos = new Posicao(x, y);
        }
    }
}