using MDF.Models.Shared;
using System.Collections.Generic;
using MDF.Models.Maquinas;

namespace MDF.Models.LinhasProducao
{
    public class Posicao : ValueObject
    {
        public int x { get; set; }
        public int y { get; set; }

        protected Posicao()
        {
        }
        public Posicao(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return this.x;
            yield return this.y;
        }
    }
}