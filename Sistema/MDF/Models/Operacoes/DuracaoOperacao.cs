using MDF.Models.Shared;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MDF.Models.Operacoes
{
    public class DuracaoOperacao : ValueObject
    {

        public int duracao{ get; set; }
        protected DuracaoOperacao()
        {
        }

        public DuracaoOperacao(int value){
            this.duracao=value;
        }

        public int value(){
            return this.duracao;
        }

        protected override IEnumerable<object> GetAtomicValues()
        {
            // Using a yield return statement to return each element one at a time
            yield return duracao;
        }
    }
}