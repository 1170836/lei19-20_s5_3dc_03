using MDF.Models.Shared;
using System.Collections.Generic;

namespace MDF.Models.Operacoes
{
    public class Ferramenta : ValueObject
    {

        public string ferramenta { get; set; }
        protected Ferramenta()
        {
        }

        public Ferramenta(string value)
        {
            this.ferramenta = value;
        }

        public string value()
        {
            return this.ferramenta;
        }

        protected override IEnumerable<object> GetAtomicValues()
        {
            // Using a yield return statement to return each element one at a time
            yield return ferramenta;
        }
    }
}