using MDF.Models.Shared;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace MDF.Models.Operacoes
{
    public class Operacao : Entity
    {

        [Column("Duracao")]
        public DuracaoOperacao duracao { get; set; }

        [Column("Descricao")]
        public DescricaoOperacao descricao { get; set; }

        public Ferramenta ferramenta { get; set; }

        protected Operacao()
        {

        }

        public Operacao(int duracao, string descricao, string ferramenta)
        {
            this.duracao = new DuracaoOperacao(duracao);
            this.descricao = new DescricaoOperacao(descricao);
            this.ferramenta = new Ferramenta(ferramenta);
        }

    }
}