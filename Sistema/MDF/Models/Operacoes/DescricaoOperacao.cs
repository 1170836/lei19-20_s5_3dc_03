using MDF.Models.Shared;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MDF.Models.Operacoes
{
    public class DescricaoOperacao : ValueObject
    {

        public string descricao{ get; set; }
        protected DescricaoOperacao()
        {
        }

        public DescricaoOperacao(string value){
            this.descricao=value;
        }

        public string value(){
            return this.descricao;
        }

        protected override IEnumerable<object> GetAtomicValues()
        {
            // Using a yield return statement to return each element one at a time
            yield return this.descricao;
        }

        public bool EqualsToString(string descricao2){
            return this.descricao.Equals(descricao2);
        }
    }
}