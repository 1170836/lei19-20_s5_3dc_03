var GetClientDTO = require('../DTO/GetClientDTO');
var CartaoCredito = require('./CartaoCredito');

module.exports =  class Cliente {
    constructor(email, password, empresa, cliente, nif, morada, iban, numCartao, ccv2, validade, prioridade) {
        this.email = email;
        this.password = password;
        this.empresa = empresa;
        this.cliente = cliente;
        this.nif = nif;
        this.morada = morada;
        this.iban = iban;
        this.cartaoCredito = new CartaoCredito(numCartao,ccv2,validade);
        this.prioridade = prioridade;
    }

    toDto(){
        return GetClientDTO.getClientDTO(this);
    }
}
