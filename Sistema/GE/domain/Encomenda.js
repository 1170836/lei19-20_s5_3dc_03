var GetEncomendaDTO = require('../DTO/GetEncomendaDTO');

module.exports = class Cliente {
    constructor(dataIncial, produtos, tempoConclusao, estado, email) {
        this.dataIncial = dataIncial;
        this.produtos = produtos;
        this.tempoConclusao = tempoConclusao;
        this.estado = estado;
        this.email = email;
    }

    toDto() {
        return GetEncomendaDTO.GetEncomendaDTO(this);
    }
}
