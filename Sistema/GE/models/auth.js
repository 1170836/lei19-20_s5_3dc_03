var mongoose = require('mongoose');
var Schema = mongoose.Schema;
module.exports = mongoose.model('Auth', new Schema({
    role: String,
    autorizacoes: [{
        operacao: String,
        auth: String,
    }],
})
);