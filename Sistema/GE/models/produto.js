var mongoose = require('mongoose');
var Schema = mongoose.Schema;
module.exports = mongoose.model('Produto', new Schema({
    descricao: String,
    quantidade: Number
})
);