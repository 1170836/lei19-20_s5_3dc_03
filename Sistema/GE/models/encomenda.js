var mongoose = require('mongoose');
var Schema = mongoose.Schema;
module.exports = mongoose.model('Encomenda', new Schema({
    data: String,
    produtos: [{
        preco: Number,
        descricao: String,
        quantidade: Number
    }],
    tempoConc: String,
    estado: String,
    emailCliente: String,
    previsao: String,
    diaProcessamento: String
})
);