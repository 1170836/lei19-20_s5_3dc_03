var mongoose = require('mongoose');
var Schema = mongoose.Schema;
module.exports = mongoose.model('Admin', new Schema({
    nome: String,
    password: String,
    email: String,
    isAdmin: Boolean
})
);