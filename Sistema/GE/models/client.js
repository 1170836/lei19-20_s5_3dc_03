var mongoose = require('mongoose');
var Schema = mongoose.Schema;
module.exports = mongoose.model('Cliente', new Schema({
    email: String,
    password: String,
    empresa: String,
    cliente: String,
    nif: String,
    morada: String,
    iban: String,
    numCartao: String,
    ccv2: Number,
    validade: Date,
    prioridade: Number
})
);