var mongoose = require('mongoose');
var Schema = mongoose.Schema;
module.exports = mongoose.model('Planeamento', new Schema({
    dia: String,
    plano: [{
        idEnc: String,
        linha: String,
        produto: String,
        inicio: String,
        fim: String
    }]
})
);