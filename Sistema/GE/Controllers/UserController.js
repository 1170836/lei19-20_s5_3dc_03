var jwt = require('jsonwebtoken');
var UserService = require('../Services/UserService');
var ClientDTO = require('../DTO/ClientDTO');
var AdminDTO = require('../DTO/AdminDTO');
var Auth = require('../auth/VerifyToken');
const config = require('../config');
var AuthService = require('../Services/AuthService');

exports.registarUser = async function (req, res) {

    try {
        var user, status;

        if (req.body.isAdmin != undefined) {
            user = AdminDTO.adminDTO(req.body);
        } else {
            user = ClientDTO.clientDTO(req.body);
        }

        status = await UserService.registerUser(user);

        if (status == 500) {
            res.status(500)("Erro no servidor, tente mais tarde.");
        } else if (status == 200) {
            res.status(200).json({ message: "Utilizador registado!" });
        } else if (status == 400) {
            res.status(400).json({ message: "Erro no pedido!" });
        } else {
            res.status(200).json({ success: false, message: 'Já existe esse email na nossa Base de Dados. Por favor tente fazer Login' });
        }
    } catch (e) {
        res.status(400).json({ message: e.message });
    }

    return res;
}

exports.login = async function (req, res) {

    try {
        var status = await UserService.login(req.body);
        if (status == 200) {

            const payload = { user: req.body.email };
            var theToken = jwt.sign(payload, config.secretAdmin, { expiresIn: 86400 });
            res.status(200).json({ success: true, message: 'Administrador', token: theToken });

        } else if (status == 201) {

            const payload = { user: req.body.email };
            var theToken = jwt.sign(payload, config.secret, { expiresIn: 86400 });
            res.status(200).json({ success: true, message: 'Aqui tem o seu Token: ', token: theToken });

        } else if (status == 401) {
            res.status(401).json({ success: false, message: "Password inválida para o utilizador em questão." });
        } else {
            res.status(400).json({ success: false, message: "Utilizador não encontrado." });
        }


    } catch (e) {
        res.status(400).json({ message: e.message });
    }

    return res;
}

exports.perfil = async function (req, res) {

    var status = Auth.verifyToken(req, res);

    if (status == 403) {
        res.status(403).json({ message: "A sua sessão encontra-se inválida.\nPor favor, tente mais tarde." })
    } else if (status == 500) {
        res.status(500).json({ message: "Erro no servidor.\nPor favor, tente mais tarde." })
    } else {

        var user = await UserService.perfil(req.user);

        if (user == 400) {
            res.status(400).json("Sessão expirou");
        } else {
            res.status(200).json({ cliente: user });
        }
    }

    return res;
}

exports.clientes = async function (req, res) {

    try {
        var statusToken = Auth.verifyTokenAdmin(req, res);

        if (statusToken == 403) {
            res.status(403).json({ message: "A sua sessão encontra-se inválida.\nPor favor, tente mais tarde." })
        } else if (statusToken == 500) {
            res.status(500).json({ message: "Erro no servidor.\nPor favor, tente mais tarde." })
        } else {

            var statusAuth = await AuthService.checkAuthAdmin("Consultar Clientes");

            if (statusAuth == 200) {
                var clientesDb = await UserService.getClientes();
                res.status(200).json({ clientes: clientesDb });
            } else {
                res.status(403).json({ message: "Não tem permissão para efetuar esta tarefa." })
            }
        }

    } catch (e) {
        console.log(e.message);
        res.status(400).json({ message: e.message });
    }

    return res;
}

exports.clientesIntegracao = async function (req, res) {

    try {
        var clientesDb = await UserService.getClientes();
        res.status(200).json({ clientes: clientesDb });

    } catch (e) {
        console.log(e.message);
        res.status(400).json({ message: e.message });
    }

    return res;
}

exports.alterarPerfil = async function (req, res) {
    var status = Auth.verifyToken(req, res);

    if (status == 500 || status == 403) {
        res.status(400).json("Sessão expirou");
    }

    var statusAuth = await AuthService.checkAuthCliente("Alterar Perfil");

    if (statusAuth == 200) {
        let user = await UserService.alterarPerfil(req.user, req.body);
        res.status(200).json({ novoUser: user });
    } else {
        res.status(403).json({ message: "Não tem permissão para efetuar esta tarefa." })
    }

    return res;
}

exports.alterarPerfilAdmin = async function (req, res) {
    var status = Auth.verifyTokenAdmin(req, res);

    if (status == 500 || status == 403) {
        res.status(400).json("Sessão expirou");
    }

    var statusAuth = await AuthService.checkAuthAdmin("Alterar Cliente");

    if (statusAuth == 200) {
        let user = await UserService.alterarPerfilAdmin(req.body);
        res.status(200).json({ novoUser: user });
    } else {
        res.status(403).json({ message: "Não tem permissão para efetuar esta tarefa." })
    }

    return res
}

exports.alterarPagamento = async function (req, res) {
    var status = Auth.verifyToken(req, res);

    if (status == 500 || status == 403) {
        res.status(400).json("Sessão expirou");
    }

    var statusAuth = await AuthService.checkAuthCliente("Alterar Perfil");

    if (statusAuth == 200) {
        let user = await UserService.alterarPagamento(req.user, req.body);

        if (user == 400) {
            res.status(400).json("Sessão expirou");
        } else {
            res.status(200).json({ novoUser: user })
        }
    } else {
        res.status(403).json({ message: "Não tem permissão para efetuar esta tarefa." })
    }

    return res;

}
exports.alterarPassword = async function (req, res) {

    var status = Auth.verifyToken(req, res);

    if (status == 500 || status == 403) {
        res.status(400).json("Sessão expirou");
    }

    var statusAuth = await AuthService.checkAuthCliente("Alterar Perfil");

    if (statusAuth == 200) {
        let user = await UserService.alterarPassword(req.user, req.body);

        if (user == 401) {
            res.status(401).json({ message: "Password errada" });
        }
        else {
            res.status(200).json({ novoUser: user });
        }
    } else {
        res.status(403).json({ message: "Não tem permissão para efetuar esta tarefa." })
    }
    return res;
}

//Só para testes
exports.apagarUser = async function (req, res) {
    try {
        await UserService.apagarUser(req.params.email);
        res.status(200).json({ message: "Apagou user com sucesso" });
    } catch (e) {
        console.log(e.message);
        res.status(400).json({ message: e.message });
    }

    return res;
}
