var jwt = require('jsonwebtoken');
var EncomendaDTO = require('../DTO/EncomendaDTO');
var Auth = require('../auth/VerifyToken');
var EncomendaService = require('../Services/EncomendaService');
var AuthService = require('../Services/AuthService');

exports.registarEncomenda = async function (req, res) {

    try {
        var order, status, statusToken;

        statusToken = Auth.verifyToken(req, res);

        if (statusToken != 403 && statusToken != 500) {

            var statusAuth = await AuthService.checkAuthCliente("Fazer Encomenda");

            if (statusAuth == 200) {

                if (req.body != undefined) {
                    order = EncomendaDTO.encomendaDTO(req.body.produtos, req.body.tempoConc, req.user);
                }
                status = await EncomendaService.registarEncomenda(order);

                if (status == 500) {
                    res.status(500).json({ message: "Erro no servidor.\nPor favor, tente mais tarde." });
                } else if (status == 200) {
                    res.status(200).json({ message: "Encomenda realizada com sucesso!\nObrigado pela sua escolha!" })
                }
            } else {
                res.status(403).json({ message: "Não tem permissão para efetuar esta tarefa." })
            }

        } else if (statusToken == 403) {
            res.status(403).json({ message: "A sua sessão encontra-se inválida.\nPor favor, tente mais tarde." })
        } else {
            res.status(500).json({ message: "Erro no servidor.\nPor favor, tente mais tarde." })
        }

    } catch (e) {
        console.log(e.message);
        res.status(400).json({ message: e.message });
    }

    return res;
}

exports.getEncomendasUser = async function (req, res) {

    try {
        var statusToken = Auth.verifyToken(req, res);

        if (statusToken == 403) {
            res.status(403).json({ message: "A sua sessão encontra-se inválida.\nPor favor, tente mais tarde." })
        } else if (statusToken == 500) {
            res.status(500).json({ message: "Erro no servidor.\nPor favor, tente mais tarde." })
        } else {
            var encomendasDb = await EncomendaService.getEncomendasUser(req.user);
            res.status(200).json({ encomendas: encomendasDb });
        }

    } catch (e) {
        console.log(e.message);
        res.status(400).json({ message: e.message });
    }

    return res;
}

exports.getEncomendas = async function (req, res) {

    try {
        var statusToken = Auth.verifyTokenAdmin(req, res);

        if (statusToken == 403) {
            res.status(403).json({ message: "A sua sessão encontra-se inválida.\nPor favor, tente mais tarde." })
        } else if (statusToken == 500) {
            res.status(500).json({ message: "Erro no servidor.\nPor favor, tente mais tarde." })
        } else {

            var statusAuth = await AuthService.checkAuthAdmin("Consultar Encomendas");

            if (statusAuth == 200) {
                var encomendasDb = await EncomendaService.getEncomendas();
                res.status(200).json({ encomendas: encomendasDb });
            }
            else {
                res.status(403).json({ message: "Não tem permissão para efetuar esta tarefa." })
            }
        }

    } catch (e) {
        console.log(e.message);
        res.status(400).json({ message: e.message });
    }

    return res;
}

exports.getEncomendasEspera = async function (req, res) {

    try {
        var encomendasDb = await EncomendaService.getEncomendasEspera();
        res.status(200).json({ encomendas: encomendasDb });

    } catch (e) {
        console.log(e.message);
        res.status(400).json({ message: e.message });
    }

    return res;
}

exports.getEncomendasProcessamento = async function (req, res) {

    try {
        var statusToken = Auth.verifyTokenAdmin(req, res);

        if (statusToken == 403) {
            res.status(403).json({ message: "A sua sessão encontra-se inválida.\nPor favor, tente mais tarde." })
        } else if (statusToken == 500) {
            res.status(500).json({ message: "Erro no servidor.\nPor favor, tente mais tarde." })
        } else {
            var encomendasDb = await EncomendaService.getEncomendasProcessamento();
            res.status(200).json({ encomendas: encomendasDb });
        }

    } catch (e) {
        console.log(e.message);
        res.status(400).json({ message: e.message });
    }

    return res;
}

exports.getEncomenda = async function (req, res) {

    try {
        var encomendaDb = await EncomendaService.getEncomenda(req.params.id);
        if (encomendaDb != null) {
            console.log('vai retornar bem');
            res.status(200).json({ encomenda: encomendaDb });
        } else {
            res.status(400).json({ message: "Encomenda nao encontrada" });
        }
    } catch (e) {
        console.log(e.message);
        res.status(400).json({ message: e.message });
    }

    return res;
}

exports.alterarEncomenda = async function (req, res) {
    try {

        var statusToken = Auth.verifyTokenAdmin(req, res);


        if (statusToken == 403) {
            res.status(403).json({ message: "A sua sessão encontra-se inválida.\nPor favor, tente mais tarde." })
        } else if (statusToken == 500) {
            res.status(500).json({ message: "Erro no servidor.\nPor favor, tente mais tarde." })
        } else {

            var statusAuth = await AuthService.checkAuthAdmin("Alterar Encomenda");

            if (statusAuth == 200) {
                var encomendasDb = await EncomendaService.alterarEncomenda(req.body);
                res.status(200).json({ encomendas: encomendasDb });
            } else {
                res.status(403).json({ message: "Não tem permissão para efetuar esta tarefa." })
            }
        }
    } catch (e) {
        console.log(e.message);
        res.status(400).json({ message: e.message });
    }

    return res;
}

exports.alterarEncomendaCliente = async function (req, res) {

    try {
        var statusToken = Auth.verifyToken(req, res);

        
        if (statusToken == 403) {
            res.status(403).json({ message: "A sua sessão encontra-se inválida.\nPor favor, tente mais tarde." })
        } else if (statusToken == 500) {
            res.status(500).json({ message: "Erro no servidor.\nPor favor, tente mais tarde." })
        } else {

            var statusAuth = await AuthService.checkAuthCliente("Alterar Encomenda");

            if (statusAuth == 200) {
                var encomendasDb = await EncomendaService.alterarEncomendaCliente(req.body);
                res.status(200).json({ encomendas: encomendasDb });
            } else {
                res.status(403).json({ message: "Não tem permissão para efetuar esta tarefa." })
            }
        }
    } catch (e) {
        console.log(e.message);
        res.status(400).json({ message: e.message });
    }
    return res;
}


exports.cancelarEncomenda = async function (req, res) {
    try {
        var statusToken = Auth.verifyToken(req, res);

        if (statusToken == 200) {
            var encomendaDb = await EncomendaService.cancelarEncomenda(req.params.id);
            res.status(200).json({ encomenda: encomendaDb });
        } else {
            statusToken = Auth.verifyTokenAdmin(req, res);

            if (statusToken == 403) {
                res.status(403).json({ message: "A sua sessão encontra-se inválida.\nPor favor, tente mais tarde." })
            } else if (statusToken == 500) {
                res.status(500).json({ message: "Erro no servidor.\nPor favor, tente mais tarde." })
            } else {
                var encomendaDb = await EncomendaService.cancelarEncomenda(req.params.id);
                res.status(200).json({ encomenda: encomendaDb });
            }
        }



    } catch (e) {
        console.log(e.message);
        res.status(400).json({ message: e.message });
    }

    return res;

}
exports.apagarEncomenda = async function (req, res) {
    try {
        var statusToken = Auth.verifyTokenAdmin(req, res);

        if (statusToken == 403) {
            res.status(403).json({ message: "A sua sessão encontra-se inválida.\nPor favor, tente mais tarde." })
        } else if (statusToken == 500) {
            res.status(500).json({ message: "Erro no servidor.\nPor favor, tente mais tarde." })
        } else {

            var statusAuth = await AuthService.checkAuthAdmin("Cancelar Encomenda");

            if (statusAuth == 200) {
                var encomendasDb = await EncomendaService.apagarEncomenda(req.params.id);
                res.status(200).json({ encomendas: encomendasDb });
            } else {
                res.status(403).json({ message: "Não tem permissão para efetuar esta tarefa." })
            }
        }

    } catch (e) {
        console.log(e.message);
        res.status(400).json({ message: e.message });
    }

    return res;
}

exports.getProdutosMaisEncomendados = async function (req, res) {
    try {
        var statusToken = Auth.verifyToken(req, res);

        if (statusToken == 403) {
            res.status(403).json({ message: "A sua sessão encontra-se inválida.\nPor favor, tente mais tarde." })
        } else if (statusToken == 500) {
            res.status(500).json({ message: "Erro no servidor.\nPor favor, tente mais tarde." })
        } else {
            var produtosMaisEncomendados = await EncomendaService.getProdutosMaisEncomendados();
            res.status(200).json({ produtos: produtosMaisEncomendados });
        }
    } catch (e) {
        console.log(e.message);
        res.status(400).json({ message: e.message });
    }
    return res;
}

exports.getProdutosMaisVezesEncomendados = async function (req, res) {
    try {
        var statusToken = Auth.verifyToken(req, res);

        if (statusToken == 403) {
            res.status(403).json({ message: "A sua sessão encontra-se inválida.\nPor favor, tente mais tarde." })
        } else if (statusToken == 500) {
            res.status(500).json({ message: "Erro no servidor.\nPor favor, tente mais tarde." })
        } else {
            var produtosMaisVezesEncomendados = await EncomendaService.getProdutosMaisVezesEncomendados();

            res.status(200).json({ produtos: produtosMaisVezesEncomendados });
        }
    } catch (e) {
        console.log(e.message);
        res.status(400).json({ message: e.message });
    }
    return res;
}

exports.esperarEncomendas = async function (req, res) {
    try {
        var statusToken = Auth.verifyTokenAdmin(req, res);

        if (statusToken == 403) {
            res.status(403).json({ message: "A sua sessão encontra-se inválida.\nPor favor, tente mais tarde." })
        } else if (statusToken == 500) {
            res.status(500).json({ message: "Erro no servidor.\nPor favor, tente mais tarde." })
        } else {
            await EncomendaService.esperarEncomendas();
            res.status(200).json({ message: "Operação bem sucedida." });
        }
    } catch (e) {
        console.log(e.message);
        res.status(400).json({ message: e.message });
    }
    return res;
}