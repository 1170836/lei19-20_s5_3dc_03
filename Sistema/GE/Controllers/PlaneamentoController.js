var jwt = require('jsonwebtoken');
var Auth = require('../auth/VerifyToken');
var PlaneamentoService = require('../Services/PlaneamentoService');
var AuthService = require('../Services/AuthService');

/* exports.previsaoEncomendas = async function (req, res) {
    try {
        var statusToken = Auth.verifyTokenAdmin(req, res);

        if (statusToken == 403) {
            res.status(403).json({ message: "A sua sessão encontra-se inválida.\nPor favor, tente mais tarde." })
        } else if (statusToken == 500) {
            res.status(500).json({ message: "Erro no servidor.\nPor favor, tente mais tarde." })
        } else {
            await EncomendaService.previsaoEncomendas(req.body);
            res.status(200).json({ message: "Previsões bem sucedidas" });
        }
    } catch (e) {
        console.log(e.message);
        res.status(400).json({ message: e.message });
    }
    return res;
} */

exports.verificarPlaneamento = async function (req, res) {
    try {
        var statusToken = Auth.verifyTokenAdmin(req, res);

        if (statusToken == 403) {
            res.status(403).json({ message: "A sua sessão encontra-se inválida.\nPor favor, tente mais tarde." })
        } else if (statusToken == 500) {
            res.status(500).json({ message: "Erro no servidor.\nPor favor, tente mais tarde." })
        } else {
            var planeamentoEfetuado = await PlaneamentoService.verificarPlaneamento();
            res.status(200).json({ planeamento: planeamentoEfetuado });
        }
    } catch (e) {
        console.log(e.message);
        res.status(400).json({ message: e.message });
    }
    return res;
}

exports.guardarPlaneamento = async function (req, res) {
    try {
        var statusToken = Auth.verifyTokenAdmin(req, res);

        if (statusToken == 403) {
            res.status(403).json({ message: "A sua sessão encontra-se inválida.\nPor favor, tente mais tarde." })
        } else if (statusToken == 500) {
            res.status(500).json({ message: "Erro no servidor.\nPor favor, tente mais tarde." })
        } else {
            var planeamentoEfetuado = await PlaneamentoService.guardarPlaneamento(req.body);
            res.status(200).json({ planeamento: planeamentoEfetuado });
        }
    } catch (e) {
        console.log(e.message);
        res.status(400).json({ message: e.message });
    }
    return res;
}

exports.replaneamento = async function (req, res) {
    try {
        var statusToken = Auth.verifyTokenAdmin(req, res);

        if (statusToken == 403) {
            res.status(403).json({ message: "A sua sessão encontra-se inválida.\nPor favor, tente mais tarde." })
        } else if (statusToken == 500) {
            res.status(500).json({ message: "Erro no servidor.\nPor favor, tente mais tarde." })
        } else {
            var planeamentoEfetuado = await PlaneamentoService.replaneamento(req.body);
            res.status(200).json({ planeamento: planeamentoEfetuado });
        }
    } catch (e) {
        console.log(e.message);
        res.status(400).json({ message: e.message });
    }
    return res;
}

exports.planeamentoDia = async function (req, res) {
    try {
        var statusToken = Auth.verifyTokenAdmin(req, res);

        if (statusToken == 403) {
            res.status(403).json({ message: "A sua sessão encontra-se inválida.\nPor favor, tente mais tarde." })
        } else if (statusToken == 500) {
            res.status(500).json({ message: "Erro no servidor.\nPor favor, tente mais tarde." })
        } else {
            var planeamentoEfetuado = await PlaneamentoService.planeamentoDia();
            res.status(200).json({ planeamento: planeamentoEfetuado });
        }
    } catch (e) {
        console.log(e.message);
        res.status(400).json({ message: e.message });
    }
    return res;
}

exports.produtoEmProducaoLinha = async function (req, res) {
    try {
        var statusToken = Auth.verifyTokenAdmin(req, res);

        if (statusToken == 403) {
            res.status(403).json({ message: "A sua sessão encontra-se inválida.\nPor favor, tente mais tarde." })
        } else if (statusToken == 500) {
            res.status(500).json({ message: "Erro no servidor.\nPor favor, tente mais tarde." })
        } else {
            var planeamentoEfetuado = await PlaneamentoService.produtoEmProducaoLinha(req.params.linha);
            res.status(200).json({ planeamento: planeamentoEfetuado });
        }
    } catch (e) {
        console.log(e.message);
        res.status(400).json({ message: e.message });
    }
    return res;
}