var AuthService = require('../Services/AuthService');

exports.admin = async function (req, res) {

    var status = await AuthService.admin(req.body);

    if (status == 200) {
        res.status(200).json({ message: "Autorizações do Administrador submetidas com sucesso" })
    } else {
        res.status(400).json({ message: "Erro no pedido" })
    }
}

exports.cliente = async function (req, res) {

    var status = await AuthService.cliente(req.body);

    if (status == 200) {
        res.status(200).json({ message: "Autorizações do Cliente submetidas com sucesso" })
    } else {
        res.status(400).json({ message: "Erro no pedido" })
    }
}

exports.getAdmin = async function (req, res) {
    var adminAuth = await AuthService.getAdminAuth();
    res.status(200).json({ auth: adminAuth.autorizacoes })
}

exports.getCliente = async function (req, res) {

    var clienteAuth = await AuthService.getClienteAuth();
    res.status(200).json({ auth: clienteAuth.autorizacoes })

}
