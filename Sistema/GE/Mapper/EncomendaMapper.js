var Encomenda = require('../domain/Encomenda');

exports.mapModelToDomain = function (model) {
    return new Encomenda(model.email, model.password, model.empresa, model.cliente,
        model.nif, model.morada, model.iban, model.numCartao, model.ccv2, model.validade, model.prioridade);
}

exports.mapDomainToDTO = function (domain) {
    return domain.toDto();
}