var Cliente = require('../domain/Cliente');

exports.mapModelToDomain = function (model) {
    return new Cliente(model.email, model.password, model.empresa, model.cliente,
        model.nif, model.morada, model.iban, model.numCartao, model.ccv2, model.validade, model.prioridade);
}

exports.mapDomainToDTO = function (domain) {
    return domain.toDto();
}