var bcypt = require('bcryptjs');
var EncomendasRepo = require('../Repository/EncomendaRepository');

exports.registarEncomenda = async function (order) {

    var status = 200;

    try {
        await EncomendasRepo.registarEncomenda(order);
    } catch (e) {
        status = 500;
    }

    return status;
}

exports.getEncomendasUser = async function (user) {
    try {
        var encomendas = await EncomendasRepo.getEncomendasUtilizador(user);
    } catch (e) {
        return null;
    }

    return encomendas;
}

exports.getEncomenda = async function (id) {
    try {
        var encomenda = await EncomendasRepo.getEncomenda(id);
    } catch (e) {
        return null;
    }
    return encomenda;
}

exports.getEncomendas = async function () {
    try {
        var encomendas = await EncomendasRepo.getEncomendas();
    } catch (e) {
        return null;
    }

    return encomendas;
}

exports.getEncomendasEspera = async function () {
    try {
        var encomendas = await EncomendasRepo.getEncomendasEspera();
    } catch (e) {
        return null;
    }

    return encomendas;
}

exports.getEncomendasProcessamento = async function () {
    try {
        var encomendas = await EncomendasRepo.getEncomendasProcessamento();
    } catch (e) {
        return null;
    }

    return encomendas;
}

exports.alterarEncomenda = async function (dto) {
    var encomendaDb = await EncomendasRepo.alterarEncomenda(dto);
    if (!encomendaDb) {
        return 400;
    } else {
        return encomendaDb;
    }
}


exports.alterarEncomendaCliente = async function (dto) {
    var encomendaDb = await EncomendasRepo.alterarEncomendaCliente(dto);
    if (!encomendaDb) {
        return 400;
    } else {
        return encomendaDb;
    }
}

exports.cancelarEncomenda = async function (id) {
    console.log('entrou service---', id);
    var encomendaDb = await EncomendasRepo.cancelarEncomenda(id);
    if (!encomendaDb) {
        return 400;
    } else {
        return encomendaDb;
    }
}

exports.apagarEncomenda = async function (id) {
    var encomendaDb = await EncomendasRepo.apagarEncomenda(id);
    if (!encomendaDb) {
        return 400;
    } else {
        return encomendaDb;
    }
}

exports.getProdutosMaisEncomendados = async function () {
    var encomendasAll = await EncomendasRepo.getEncomendas();
    var produtosAll = [];
    if (encomendasAll) {
        for (const encomenda in encomendasAll) {
            produtosAll.push(encomendasAll[encomenda].produtos);
        }
    }

    var descricoes = [];
    for (let i = 0; i < produtosAll.length; i++) {
        var produtos = { produtos: produtosAll[i] }
        produtos.produtos.forEach(element => {
            descricoes.push(element.descricao)
        });
    }
    descricoes.sort();
    var a = [], b = [], prev;
    for (let i = 0; i < descricoes.length; i++) {
        if (descricoes[i] !== prev) {
            a.push(descricoes[i]);
            b.push(1);
        } else {
            b[b.length - 1]++;
        }
        prev = descricoes[i];
    }

    for (let i = 0; i < b.length - 1; i++) {
        for (let j = 0; j < b.length - i - 1; j++) {
            if (b[j] < b[j + 1]) {
                let temp = b[j];
                b[j] = b[j + 1];
                b[j + 1] = temp;
                let temp2 = a[j];
                a[j] = a[j + 1];
                a[j + 1] = temp2;
            }
        }
    }
    return [a, b];
}

exports.getProdutosMaisVezesEncomendados = async function () {
    var encomendasAll = await EncomendasRepo.getEncomendas();
    var produtosAll = [];
    if (encomendasAll) {
        for (const encomenda in encomendasAll) {
            produtosAll.push(encomendasAll[encomenda].produtos);
        }
    }

    var descricoes = [];
    var quantidades = [];
    for (let i = 0; i < produtosAll.length; i++) {
        var produtos = { produtos: produtosAll[i] }
        produtos.produtos.forEach(element => {
            descricoes.push(element.descricao)
            quantidades.push(element.quantidade)
        });
    }
    var descNoReps = [];
    console.log("DESCRICOES")
    console.log(descricoes);
    /* for (let index = 0; index < descricoes.length; index++) {
        if (index == 0) {
            descNoReps.push(descricoes[index])
        } else{
            for (let i = 0; i < descNoReps.length; i++) {
                if (descricoes[index] != descNoReps[i]) {
                    descNoReps.push(descricoes[index])
                }
                
            }
        }
    } */
    for (let i = 0; i < descricoes.length; i++) {
        if (descNoReps.indexOf(descricoes[i]) == -1) {
            descNoReps.push(descricoes[i])
        }
    }


    var qtsSomadas = [];
    for (let i = 0; i < descNoReps.length; i++) {
        qtsSomadas[i] = 0;
        for (let j = 0; j < descricoes.length; j++) {
            if (descNoReps[i] == descricoes[j]) {
                qtsSomadas[i] += quantidades[j];
            }
        }
    }

    for (let i = 0; i < qtsSomadas.length - 1; i++) {
        for (let j = 0; j < qtsSomadas.length - i - 1; j++) {
            if (qtsSomadas[j] < qtsSomadas[j + 1]) {
                let temp = qtsSomadas[j];
                qtsSomadas[j] = qtsSomadas[j + 1];
                qtsSomadas[j + 1] = temp;
                let temp2 = descNoReps[j];
                descNoReps[j] = descNoReps[j + 1];
                descNoReps[j + 1] = temp2;
            }
        }
    }
    console.log("DESCRICOES | QUANTIDADES")
    console.log([descricoes, quantidades])

    console.log("DESCRICOES SEM REPS")
    console.log([descNoReps, qtsSomadas]);
    return [descNoReps, qtsSomadas];
}

exports.esperarEncomendas = async function () {
    await EncomendasRepo.esperarEncomendas();
}