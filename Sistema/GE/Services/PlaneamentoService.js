var bcypt = require('bcryptjs');
var PlaneamentoRepo = require('../Repository/PlaneamentoRepository');
var EncomendaRepo = require('../Repository/EncomendaRepository');

exports.verificarPlaneamento = async function () {
    return await PlaneamentoRepo.verificarPlaneamento();
}

/* exports.previsaoEncomendas = async function (previsoes) {
    const segundosTrabalhoPorDia = 57600;
    var planeamentoEfetuadoHoje = await EncomendasRepo.verificarPlaneamento();

    if (planeamentoEfetuadoHoje == false) {

        await EncomendasRepo.concluirEncomendas();

        previsoes.forEach(async previsao => {
            var diasInicio = parseInt(previsao.final / segundosTrabalhoPorDia);
            await EncomendasRepo.adicionarPrevisao(previsao.idEnc, diasInicio);
        });

    }

} */

exports.guardarPlaneamento = async function (planeamento) {
    const segundosTrabalhoPorDia = 60 * 60 * 12; //12 horas em segundos

    var planeamentoEfetuadoHoje = await PlaneamentoRepo.verificarPlaneamento();

    if (planeamentoEfetuadoHoje == false) {

        await EncomendaRepo.concluirEncomendas();

        var listaEncomendasPodemConcluir = [];

        await planeamento.forEach(async plano => {
            if (plano.fim > segundosTrabalhoPorDia) {
                var index = listaEncomendasPodemConcluir.indexOf(plano.idEnc);
                if (index != -1) {
                    listaEncomendasPodemConcluir.splice(index, 1);
                }
            } else {
                var index = listaEncomendasPodemConcluir.indexOf(plano.idEnc);
                if (index == -1) {
                    listaEncomendasPodemConcluir.push(plano.idEnc);
                }
            }
        });


        var planeamentoCompleto = [];

        await planeamento.forEach(async plano => {
            if (listaEncomendasPodemConcluir.indexOf(plano.idEnc) != -1) {
                planeamentoCompleto.push(plano);
            }
        });

        await EncomendaRepo.processarEncomendas(listaEncomendasPodemConcluir);
        await PlaneamentoRepo.guardarPlaneamento(planeamentoCompleto);

    }
}

exports.replaneamento = async function (planeamento) {
    await PlaneamentoRepo.apagarPlaneamentoHoje();
    this.guardarPlaneamento(planeamento);
}

exports.planeamentoDia = async function () {
    return await PlaneamentoRepo.planeamentoDia();
}

exports.produtoEmProducaoLinha = async function (linha) {
    return await PlaneamentoRepo.produtoEmProducaoLinha(linha);
}