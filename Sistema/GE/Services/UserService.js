var bcrypt = require('bcryptjs');
var UserRepo = require('../Repository/UserRepository');

const Admin = require('../models/admin');

const UserMapper = require('../Mapper/UserMapper');

exports.registerUser = async function (user) {

    var status = 200;
    userDb = await UserRepo.findByEmail(user.email);

    if (!userDb) {
        try {
            if (user.password) {
                user.password = bcrypt.hashSync(user.password, 10);
                if (user.isAdmin != undefined) {
                    if (user.nome != undefined) {
                        await UserRepo.insertAdmin(user);
                    } else {
                        status = 400;
                    }

                } else {
                    if (user.empresa != undefined && user.cliente != undefined && user.nif != undefined) {
                        user.prioridade = Math.floor(Math.random() * 6) + 1;
                        await UserRepo.insertClient(user);
                    } else {
                        status = 400;
                    }
                }

            } else {
                status = 400;
            }

        } catch (e) {
            status = 500;
        }
    }

    return status;

}

exports.login = async function (user) {

    var status = 0;
    var userDb = await UserRepo.findByEmail(user.email);
    
    if (!userDb) {
        status = 400;
    }
    else {
        if (!bcrypt.compareSync(user.password, userDb.password)) {
            status = 401;
        } else {
            if (userDb instanceof Admin) {
                status = 200;
            } else {
                status = 201;
            }

        }
    }

    return status;

}

exports.perfil = async function (user) {

    var userDb = await UserRepo.findByEmailClient(user);
    if (!userDb) {
        return 400;
    } else {
        return UserMapper.mapDomainToDTO(userDb);
    }

}

exports.getClientes = async function () {

    var clientesDb = await UserRepo.getClientes();
    if (!clientesDb) {
        return 400;
    } else {
        return clientesDb;
    }
}

exports.alterarPagamento = async function (user, newCard) {

    var userDb = await UserRepo.updateCard(user, newCard);
    if (!userDb) {
        return 400;
    } else {
        return userDb;
    }
}

exports.alterarPerfil = async function (user, newUser) {

    var userDb = await UserRepo.updatePerfil(user, newUser);
    if (!userDb) {
        return 400;
    } else {
        return userDb;
    }
}

exports.alterarPerfilAdmin = async function (newUser) {

    var userDb = await UserRepo.updatePerfil(newUser.email, newUser);
    if (!userDb) {
        return 400;
    } else {
        return userDb;
    }
}

exports.alterarPassword = async function (user, dto) {

    var userDb = await UserRepo.findByEmail(user);
    console.log('UserDb Nome:', userDb.cliente);

    if (!userDb) {
        return 400;
    }
    else {
        if (!bcrypt.compareSync(dto.password, userDb.password)) {
            return 401;
        } else {
            newPass = bcrypt.hashSync(dto.passwordNova, 10);

            var userDb = await UserRepo.updatePassword(user, newPass);

            if (!userDb) {
                return 400;
            } else {
                return userDb;
            }
        }
    }
}

exports.apagarUser = async function (email) {
    await UserRepo.apagarUser(email);
}