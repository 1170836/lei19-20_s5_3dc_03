var AuthRepo = require('../Repository/AuthRepository');

exports.admin = async function (autorizacoes) {
    var autorizacoesCompletas = [];

    try {

        autorizacoes.forEach(aut => {
            var autorizacao = aut.split('-');
            autorizacoesCompletas.push({ operacao: autorizacao[0], auth: autorizacao[1] });
        });

        var adminAuth = await AuthRepo.findAdminAuth();
        if (!adminAuth) {
            await AuthRepo.saveAdminAuth(autorizacoesCompletas);
        } else {
            await AuthRepo.updateAdminAuth(autorizacoesCompletas);
        }

    } catch (e) {
        return 400;
    }

    return 200;
}

exports.cliente = async function (autorizacoes) {

    var autorizacoesCompletas = [];

    try {

        autorizacoes.forEach(aut => {
            var autorizacao = aut.split('-');
            autorizacoesCompletas.push({ operacao: autorizacao[0], auth: autorizacao[1] });
        });

        var adminAuth = await AuthRepo.findClienteAuth();
        if (!adminAuth) {
            await AuthRepo.saveClienteAuth(autorizacoesCompletas);
        } else {
            await AuthRepo.updateClienteAuth(autorizacoesCompletas);
        }

    } catch (e) {
        return 400;
    }

    return 200;
}

exports.getAdminAuth = async function () {
    try {

        var adminAuth = await AuthRepo.findAdminAuth();
        return adminAuth;

    } catch (e) {
        return null;
    }

}

exports.getClienteAuth = async function () {
    try {

        var adminAuth = await AuthRepo.findClienteAuth();
        return adminAuth;

    } catch (e) {
        return null;
    }

}

exports.checkAuthCliente = async function (operacao) {
    try {

        var status = 403;

        var adminAuth = await AuthRepo.findClienteAuth();

        adminAuth.autorizacoes.forEach(auth => {
            if (auth.operacao == operacao && auth.auth == 'true') {
                status = 200;
            }
        });

        return status;

    } catch (e) {
        return 500;
    }
}

exports.checkAuthAdmin = async function (operacao) {
    try {

        var status = 403;

        var adminAuth = await AuthRepo.findAdminAuth();

        adminAuth.autorizacoes.forEach(auth => {
            if (auth.operacao == operacao && auth.auth == 'true') {
                status = 200;
            }
        });

        return status;

    } catch (e) {
        return 500;
    }

}