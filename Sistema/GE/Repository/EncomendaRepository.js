var mongoose = require('mongoose');

var MongoClient = require('mongodb').MongoClient;
var uri = "mongodb+srv://HelderPereira:Helder321@arqsi-9evym.azure.mongodb.net/test?retryWrites=true&w=majority";

var client = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true });
client.connect();

const Encomenda = require('../models/encomenda');

exports.registarEncomenda = async function (order) {
    order.diaProcessamento = null;

    order.data = this.formatDateString(order.data);
    order.previsao = await this.atribuirDataPrevisao(order.data);
    
    var orderDb = new Encomenda(order);
    await client.db("ARQSI_3DC_3").collection("Encomenda").insertOne(orderDb);
}

this.atribuirDataPrevisao = async function (data) {
    const PRODUCAO_MEDIA = 5000;
    let encomendas_ee = await this.getEncomendasEspera();

    var quantidade_ee = 0;
    encomendas_ee.forEach(encomenda_ee => {
        encomenda_ee.produtos.forEach(produto => {
            quantidade_ee += produto.quantidade;
        })
    });

    var previsao = parseInt(quantidade_ee / PRODUCAO_MEDIA);
    var someDate = new Date();

    someDate.setDate(someDate.getDate() + previsao + 1);
    return someDate.getDate() + '/' + someDate.getMonth()+1 + '/' + someDate.getFullYear();
}

this.formatDateString = function (dateString) {
    var dataSeparada = dateString.split('-');

    var day, month, year;

    if (dataSeparada.length == 3) {
        day = dataSeparada[2];
        month = dataSeparada[1];
        year = dataSeparada[0];
    } else {
        dataSeparada = dateString.split('/');
        day = dataSeparada[1];
        month = dataSeparada[0];
        year = dataSeparada[2];
    }

    return day + '/' + month + '/' + year;
}

exports.getEncomendasUtilizador = async function (user) {
    var encomendas = [];

    await client.db("ARQSI_3DC_3").collection("Encomenda").find({ emailCliente: user }).forEach(function (enc) {
        if (enc != null) {
            encomendas.push(enc);
        }

    });

    return encomendas;
}

exports.getEncomendas = async function () {
    var encomendas = [];

    await client.db("ARQSI_3DC_3").collection("Encomenda").find().forEach(function (enc) {
        if (enc != null) {
            encomendas.push(enc);
        }

    });

    return encomendas;
}

exports.getEncomenda = async function (id) {

    var encomendaDB = await client.db("ARQSI_3DC_3").collection("Encomenda").findOne({ _id: mongoose.Types.ObjectId(id) });

    return encomendaDB;
}


exports.getEncomendasEspera = async function () {
    var encomendas = [];

    await client.db("ARQSI_3DC_3").collection("Encomenda").find().forEach(function (enc) {

        if (enc != null && enc.estado.includes("espera")) {

            enc.tempoConc = parseInt(Math.abs(Date.parse(enc.tempoConc) - Date.now()) / 36e5);
            enc.id = enc._id;

            if (isNaN(enc.tempoConc)) {
                enc.tempoConc = 1000;
            }
            encomendas.push(enc);
        }

    });

    return encomendas;
}

exports.getEncomendasProcessamento = async function () {
    var encomendas = [];

    await client.db("ARQSI_3DC_3").collection("Encomenda").find().forEach(function (enc) {

        if (enc != null && enc.estado.includes("processamento")) {
            encomendas.push(enc);
        }

    });

    return encomendas;
}

exports.alterarEncomenda = async function (dto) {

    var old = await client.db("ARQSI_3DC_3").collection("Encomenda").findOne({ _id: mongoose.Types.ObjectId(dto.encomenda) });

    var newEncomenda = new Encomenda(old);

    newEncomenda.tempoConc = dto.tempoConc;
    newEncomenda.estado = dto.estado;

    var encomendaDBNew = await client.db("ARQSI_3DC_3").collection("Encomenda").findOneAndUpdate({ _id: mongoose.Types.ObjectId(dto.encomenda) }, newEncomenda);

    return newEncomenda;
}

exports.cancelarEncomenda = async function (id) {
    console.log('entrou rep----', id);
    var old = await client.db("ARQSI_3DC_3").collection("Encomenda").findOne({ _id: mongoose.Types.ObjectId(id) });

    var newEncomenda = new Encomenda(old);

    newEncomenda.estado = 'Cancelada';

    var encomendaDBNew = await client.db("ARQSI_3DC_3").collection("Encomenda").findOneAndUpdate({ _id: mongoose.Types.ObjectId(id) }, newEncomenda);

    return newEncomenda;
}

exports.apagarEncomenda = async function (id) {
    await client.db("ARQSI_3DC_3").collection("Encomenda").deleteOne({ _id: mongoose.Types.ObjectId(id) });
}


exports.alterarEncomendaCliente = async function (dto) {
    console.log('id: ', dto.id);
    var old = await client.db("ARQSI_3DC_3").collection("Encomenda").findOne({ _id: mongoose.Types.ObjectId(dto.id) });

    var newEncomenda = new Encomenda(old);

    newEncomenda.tempoConc = dto.tempoConc;

    newEncomenda.produtos = dto.produtos;

    var encomendaDBNew = await client.db("ARQSI_3DC_3").collection("Encomenda").findOneAndUpdate({ _id: mongoose.Types.ObjectId(dto.id) }, newEncomenda);

    return newEncomenda;
}


exports.concluirEncomendas = async function () {
    await client.db("ARQSI_3DC_3").collection("Encomenda").find({ estado: "Em processamento..." }).forEach(async function (enc) {

        var newEncomenda = new Encomenda(enc);
        newEncomenda.estado = "Concluida";

        await client.db("ARQSI_3DC_3").collection("Encomenda").findOneAndUpdate({ _id: mongoose.Types.ObjectId(enc._id) }, newEncomenda);
    });
}

exports.esperarEncomendas = async function () {
    await client.db("ARQSI_3DC_3").collection("Encomenda").find({ estado: "Em processamento..." }).forEach(async function (enc) {

        var newEncomenda = new Encomenda(enc);
        newEncomenda.estado = "Em espera";
        newEncomenda.diaProcessamento = null;

        await client.db("ARQSI_3DC_3").collection("Encomenda").findOneAndUpdate({ _id: mongoose.Types.ObjectId(enc._id) }, newEncomenda);
    });
}

exports.processarEncomendas = async function (listaEncomendas) {
    await client.db("ARQSI_3DC_3").collection("Encomenda").find({ estado: "Em espera" }).forEach(async function (enc) {
        listaEncomendas.forEach(async encomenda => {
            if (encomenda == enc._id) {
                var newEncomenda = new Encomenda(enc);
                newEncomenda.estado = "Em processamento...";
                var someDate = new Date();
                newEncomenda.diaProcessamento = someDate.getDate() + '/' + someDate.getMonth()+1 + '/' + someDate.getFullYear();
                await client.db("ARQSI_3DC_3").collection("Encomenda").findOneAndUpdate({ _id: mongoose.Types.ObjectId(enc._id) }, newEncomenda);
            }
        })

    });
}


this.getHorasFromData = function (data) {
    var minutes = data.getMinutes();

    if (minutes < 10) {
        minutes = '0' + minutes;
    }
    return data.getHours() + ":" + minutes;
}


