var MongoClient = require('mongodb').MongoClient;
var uri = "mongodb+srv://HelderPereira:Helder321@arqsi-9evym.azure.mongodb.net/test?retryWrites=true&w=majority";

var client = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true });
client.connect();

const Auth = require('../models/auth');


exports.findAdminAuth = async function () {
    var authDb = await client.db("ARQSI_3DC_3").collection("Auth").findOne({ role: "admin" });
    return authDb;
}

exports.findClienteAuth = async function () {
    var authDb = await client.db("ARQSI_3DC_3").collection("Auth").findOne({ role: "cliente" });
    return authDb;
}

exports.saveAdminAuth = async function (autorizacoesCompletas) {
    var authDb = new Auth({ role: "admin", autorizacoes: autorizacoesCompletas });
    await client.db("ARQSI_3DC_3").collection("Auth").insertOne(authDb);
}

exports.saveClienteAuth = async function (autorizacoesCompletas) {
    var authDb = new Auth({ role: "cliente", autorizacoes: autorizacoesCompletas });
    await client.db("ARQSI_3DC_3").collection("Auth").insertOne(authDb);
}


exports.updateAdminAuth = async function (autorizacoesCompletas) {
    var old = await client.db("ARQSI_3DC_3").collection("Auth").findOne({ role: "admin" });

    var newAuth = new Auth(old);
    newAuth.autorizacoes = autorizacoesCompletas;

    await client.db("ARQSI_3DC_3").collection("Auth").findOneAndUpdate(old, newAuth);
}

exports.updateClienteAuth = async function (autorizacoesCompletas) {
    var old = await client.db("ARQSI_3DC_3").collection("Auth").findOne({ role: "cliente" });

    var newAuth = new Auth(old);
    newAuth.autorizacoes = autorizacoesCompletas;

    await client.db("ARQSI_3DC_3").collection("Auth").findOneAndUpdate(old, newAuth);
}

