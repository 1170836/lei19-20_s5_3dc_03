var mongoose = require('mongoose');

var MongoClient = require('mongodb').MongoClient;
var uri = "mongodb+srv://HelderPereira:Helder321@arqsi-9evym.azure.mongodb.net/test?retryWrites=true&w=majority";

var client = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true });
client.connect();

const Planeamento = require('../models/planeamento');

this.formatDateString = function (dateString) {
    var dataSeparada = dateString.split('-');

    var day, month, year;

    if (dataSeparada.length == 3) {
        day = dataSeparada[2];
        month = dataSeparada[1];
        year = dataSeparada[0];
    } else {
        dataSeparada = dateString.split('/');
        day = dataSeparada[1];
        month = dataSeparada[0];
        year = dataSeparada[2];
    }

    return day + '/' + month + '/' + year;
}

exports.verificarPlaneamento = async function () {

    var dataHoje = this.formatDateString(new Date().toLocaleDateString());
    var planeamento = false;

    await client.db("ARQSI_3DC_3").collection("Planeamento").find().forEach(function (plano) {
        if (plano.dia == dataHoje) {
            planeamento = true;
        }
    });

    return planeamento;
}


exports.apagarPlaneamentoHoje = async function () {
    var dataHoje = this.formatDateString(new Date().toLocaleDateString());
    await client.db("ARQSI_3DC_3").collection("Planeamento").deleteOne({ dia: dataHoje });

}

exports.planeamentoDia = async function () {
    var dataHoje = this.formatDateString(new Date().toLocaleDateString());
    return await client.db("ARQSI_3DC_3").collection("Planeamento").findOne({ dia: dataHoje });
}

exports.guardarPlaneamento = async function (planeamentoCompleto) {

    await planeamentoCompleto.forEach(planoTarefa => {
        var d = new Date();
        d.setHours(8);
        d.setMinutes(0);
        d.setSeconds(0);

        d.setSeconds(d.getSeconds() + planoTarefa.inicio);
        planoTarefa.inicio = this.getHorasFromData(d);

        d.setHours(8);
        d.setMinutes(0);
        d.setSeconds(0);

        d.setSeconds(d.getSeconds() + planoTarefa.fim);
        planoTarefa.fim = this.getHorasFromData(d);
    })

    var d = new Date();
    planeamentoCompleto.dia = d.getDate() + "/" + (d.getMonth() + 1) + "/" + d.getFullYear();
    planeamentoCompleto.plano = planeamentoCompleto;

    var planeamentoDb = new Planeamento(planeamentoCompleto);
    await client.db("ARQSI_3DC_3").collection("Planeamento").insertOne(planeamentoDb);
}

exports.produtoEmProducaoLinha = async function (linha) {
    var dataHoje = this.formatDateString(new Date().toLocaleDateString());
    var planeamento = await client.db("ARQSI_3DC_3").collection("Planeamento").findOne({ dia: dataHoje });

    var data = new Date();

    var horas = data.getHours();
    var minutos = data.getMinutes();

    var planoRet = null;

    await planeamento.plano.forEach(plano => {
        if (plano.linha == linha) {
            var horasInicio = plano.inicio.split(':')[0];
            var minutosInicio = plano.inicio.split(':')[1];

            if (parseInt(horasInicio) < horas || (parseInt(horasInicio) == horas && parseInt(minutosInicio) <= minutos)) {
                var horasFim = plano.fim.split(':')[0];
                var minutosFim = plano.fim.split(':')[1];

                if (parseInt(horasFim) > horas || (parseInt(horasFim) == horas && parseInt(minutosFim) >= minutos)) {
                    planoRet = plano;
                }
            }
        }
    });

    return planoRet;

}


this.getHorasFromData = function (data) {
    var minutes = data.getMinutes();

    if (minutes < 10) {
        minutes = '0' + minutes;
    }
    return data.getHours() + ":" + minutes;
}


