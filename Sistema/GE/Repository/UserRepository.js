var MongoClient = require('mongodb').MongoClient;
var uri = "mongodb+srv://HelderPereira:Helder321@arqsi-9evym.azure.mongodb.net/test?retryWrites=true&w=majority";

var client = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true });
client.connect();

const Cliente = require('../models/client');
const Admin = require('../models/admin');

const UserMapper =require('../Mapper/UserMapper');

exports.findByEmailClient = async function (userEmail) {
    var userDb = await client.db("ARQSI_3DC_3").collection("Client").findOne({ email: userEmail });

    if (!userDb) {
        return null;
    }

   var modelCliente = new Cliente(userDb);
   return UserMapper.mapModelToDomain(modelCliente);
}

exports.findByEmailAdmin = async function (userEmail) {
    var userDb = await client.db("ARQSI_3DC_3").collection("Admin").findOne({ email: userEmail });

    if (!userDb) {
        return null;
    }
    return new Admin(userDb);
}

exports.findByEmail = async function (userEmail) {
    var userDb = await client.db("ARQSI_3DC_3").collection("Admin").findOne({ email: userEmail });
    if (!userDb) {
        userDb = await client.db("ARQSI_3DC_3").collection("Client").findOne({ email: userEmail });
        if (!userDb) {
            return null;
        }
        return new Cliente(userDb);
    }
    return new Admin(userDb);
}

exports.insertClient = async function (user) {
    var userDb = new Cliente(user);
    await client.db("ARQSI_3DC_3").collection("Client").insertOne(userDb);
}

exports.insertAdmin = async function (user) {
    var userDb = new Admin(user);
    await client.db("ARQSI_3DC_3").collection("Admin").insertOne(userDb);
}

exports.getClientes = async function () {
    var clientes = [];

    await client.db("ARQSI_3DC_3").collection("Client").find({}).forEach(function (c) {
        if (c != null) {
            clientes.push(c);
        }
    });
    return clientes;
}

exports.updateCard = async function (userEmail, newCard) {

    console.log("\n\nNumCartao", newCard.numCartao, "ccv2 ", newCard.ccv2, "validade", newCard.validade);
    var old = await client.db("ARQSI_3DC_3").collection("Client").findOne({ email: userEmail });

    console.log('\n\n', old);

    var newUser = new Cliente(old);

    console.log("\n\nNumCartao", newUser.numCartao, "ccv2 ", newUser.ccv2, "validade", newUser.validade);

    newUser.numCartao = newCard.numCartao;

    newUser.ccv2 = newCard.ccv2;
    newUser.validade = newCard.validade;

    var userDbNew = await client.db("ARQSI_3DC_3").collection("Client").findOneAndUpdate(old, newUser);

    return newUser;

}

exports.updatePerfil = async function (userEmail, dto) {
    var old = await client.db("ARQSI_3DC_3").collection("Client").findOne({ email: userEmail });
    var newUser = new Cliente(old);

    newUser.cliente = dto.cliente;
    newUser.empresa = dto.empresa;
    newUser.iban = dto.iban;
    newUser.nif = dto.nif;
    newUser.email = dto.email;
    newUser.morada = dto.morada;

    var userDbNew = await client.db("ARQSI_3DC_3").collection("Client").findOneAndUpdate({ email: userEmail }, newUser);

    return newUser;

}

exports.updatePassword = async function (userEmail, newPass) {
    console.log("Repositorio para email: ", userEmail);
    var old = await client.db("ARQSI_3DC_3").collection("Client").findOne({ email: userEmail });

    var newUser = new Cliente(old);

    newUser.password = newPass;

    await client.db("ARQSI_3DC_3").collection("Client").findOneAndUpdate({ email: userEmail }, newUser);

    //console.log('\nOldPassword: ', old.password, '\nNewUser: ', newUser.password);
    return newUser;
}

exports.apagarUser = async function (emailUser) {
    await client.db("ARQSI_3DC_3").collection("Admin").deleteOne({ email:emailUser });
    await client.db("ARQSI_3DC_3").collection("Client").deleteOne({ email:emailUser });
}

