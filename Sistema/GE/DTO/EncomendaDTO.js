exports.encomendaDTO = function (order, tConc, user) {
    return {
        data: new Date().toLocaleDateString(),
        produtos: order,
        tempoConc: tConc,
        estado: 'Em espera',
        emailCliente: user
    }
}