exports.clientDTO = function (user) {
    return {
        empresa: user.empresa,
        cliente: user.cliente,
        email: user.email,
        password: user.password,
        nif: user.nif,
        morada: user.morada,
        iban: user.iban,
        numCartao: user.numCartao,
        ccv2: user.ccv2,
        validade: user.validade
    }
}