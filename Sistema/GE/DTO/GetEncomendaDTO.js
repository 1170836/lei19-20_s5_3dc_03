exports.GetEncomendaDTO = function (order) {
    return {
        data: order.dataIncial,
        produtos: order.produtos,
        tempoConc: order.tempoConclusao,
        estado: order.estado,
        emailCliente: order.email
    }
}