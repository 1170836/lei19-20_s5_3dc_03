exports.getClientDTO = function (user) {
    return {
        empresa: user.empresa,
        cliente: user.cliente,
        email: user.email,
        password: user.password,
        nif: user.nif,
        morada: user.morada,
        iban: user.iban,
        numCartao: user.cartaoCredito.numCartao,
        ccv2: user.cartaoCredito.ccv2,
        validade: user.cartaoCredito.validade,
        prioridade:user.prioridade
    }
}