exports.adminDTO = function (user) {
    return {
        email: user.email,
        password: user.password,
        nome: user.nome,
        isAdmin: user.isAdmin
    }
}