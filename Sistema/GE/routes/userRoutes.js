var express = require('express');
var router = express.Router();

var UserController = require('../Controllers/UserController');

router.post('/registo', UserController.registarUser);
router.post('/login', UserController.login);
router.get('/verPerfil', UserController.perfil);
router.get('/clientes',UserController.clientes);
router.get('/clientesIntegracao', UserController.clientesIntegracao);
router.put('/alterarPerfil', UserController.alterarPerfil);
router.put('/alterarPerfilAdmin', UserController.alterarPerfilAdmin);
router.put('/alterarPagamento', UserController.alterarPagamento);
router.put('/alterarPassword', UserController.alterarPassword); 
router.delete('/:email',UserController.apagarUser); //Efeitos de Teste

module.exports = router;
