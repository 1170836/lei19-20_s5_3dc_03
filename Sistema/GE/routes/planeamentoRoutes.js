var express = require('express');
var router = express.Router();

var PlaneamentoController = require('../Controllers/PlaneamentoController');

//router.post('/previsao', EncomendaController.previsaoEncomendas);
router.get('/verificarPlaneamento', PlaneamentoController.verificarPlaneamento);
router.post('/guardarPlaneamento', PlaneamentoController.guardarPlaneamento);
router.put('/replaneamento', PlaneamentoController.replaneamento);
router.get('/planeamentoDia', PlaneamentoController.planeamentoDia);
router.get('/produtoEmProducaoLinha/:linha', PlaneamentoController.produtoEmProducaoLinha);

module.exports = router;