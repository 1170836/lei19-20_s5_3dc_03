var express = require('express');
var router = express.Router();

var AuthController = require('../Controllers/AuthController');

router.get('/admin', AuthController.getAdmin);
router.get('/cliente', AuthController.getCliente);
router.post('/admin', AuthController.admin);
router.post('/cliente', AuthController.cliente);

module.exports = router;
