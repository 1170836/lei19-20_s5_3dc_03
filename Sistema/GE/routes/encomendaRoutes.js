var express = require('express');
var router = express.Router();

var EncomendaController = require('../Controllers/EncomendaController');

router.get('/', EncomendaController.getEncomendas);
router.get('/espera', EncomendaController.getEncomendasEspera);
router.get('/processamento', EncomendaController.getEncomendasProcessamento);
router.get('/user/', EncomendaController.getEncomendasUser);
router.get('/encomenda/:id',EncomendaController.getEncomenda);
router.put('/alterarEncomenda', EncomendaController.alterarEncomenda);
router.put('/alterarEncomendaCliente' , EncomendaController.alterarEncomendaCliente);
router.delete('/cancelarEncomenda/:id',EncomendaController.cancelarEncomenda);
router.delete('/apagarEncomenda/:id', EncomendaController.apagarEncomenda);
router.post('/registarEncomenda', EncomendaController.registarEncomenda);
router.get('/maisEncomendados', EncomendaController.getProdutosMaisEncomendados);
router.get('/maisVezesEncomendados', EncomendaController.getProdutosMaisVezesEncomendados);
router.put('/esperarEncomendas', EncomendaController.esperarEncomendas);

module.exports = router;