//During the test the env variable is set to test
process.env.NODE_ENV = 'test';

let mongoose = require("mongoose");
let Client = require('../models/client');

var Mockgoose = require('mockgoose').Mockgoose;
var mockgoose = new Mockgoose(mongoose);

let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../app');
let should = chai.should();
var expect = chai.expect;
chai.use(chaiHttp);

describe('Alterar Cliente', () => {


    describe('\nAlterar Cliente (MOCKS) \n', () => {

        before(function (done) {
            mockgoose.prepareStorage().then(function () {
                mongoose.connect('mongodb+srv://HelderPereira:Helder321@arqsi-9evym.azure.mongodb.net/test?retryWrites=true&w=majority', { useUnifiedTopology: true, useNewUrlParser: true }, function (err) {
                    done(err);
                });
            });
        });

        it("Deve criar cliente", function (done) {
            Client.create({
                email: "1234@1234.pt",
                password: "123456789",
                cliente: "Helder",
                empresa: "ISEP",
                morada: "Rua 1234",
                nif: "123456789",
                iban: "12345678987654",
                numCartao: "0987654321",
                ccv2: "321",
                validade: "2020/12/12"
            }, function (err) {
                expect(err).not.to.be.ok;
                done(err);
            });
        });

        it("Deve encontrar o cliente", function (done) {
            Client.findOne({ email: "1234@1234.pt" }, function (err, client) {
                expect(err).not.to.be.ok;
                expect(client).not.to.be.null;
                done(err);
            });
        });

        it("Deve alterar o cliente", function (done) {
            Client.updateOne({ email: "1234@1234.pt", email: "12345@12345.com" }, function (err, client) {
                expect(err).not.to.be.ok;
                expect(client).not.to.be.null;
                done(err);
            });
        });

        it("Não deve encontrar o cliente", function (done) {
            Client.findOne({ email: "1234@1234.pt" }, function (err, client) {
                expect(err).not.to.be.ok;
                expect(client).to.be.null;
                done(err);
            });
        });

        it("Deve encontrar o cliente", function (done) {
            Client.findOne({ email: "12345@12345.com" }, function (err, client) {
                expect(err).not.to.be.ok;
                expect(client).not.to.be.null;
                done(err);
            });
        });

        it("Deve apagar o cliente", function (done) {
            Client.deleteOne({ email: "12345@12345.com" }, function (err) {
                expect(err).not.to.be.ok;
                done(err);
            });
        });

        it("Não deve encontrar o cliente", function (done) {
            Client.findOne({ email: "12345@12345.com" }, function (err, client) {
                expect(err).not.to.be.ok;
                expect(client).to.be.null;
                done(err);
            });
        });

        it("reset", function (done) {
            mockgoose.helper.reset().then(function () {
                done();
            });
        });
    });

});