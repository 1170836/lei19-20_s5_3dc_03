//During the test the env variable is set to test
process.env.NODE_ENV = 'test';

let mongoose = require("mongoose");
let Encomenda = require('../models/encomenda');

var Mockgoose = require('mockgoose').Mockgoose;
var mockgoose = new Mockgoose(mongoose);

let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../app');
let should = chai.should();
var expect = chai.expect;
chai.use(chaiHttp);

describe('Criar encomenda', () => {


    describe('\nCriar encomenda (MOCKS) \n', () => {

        before(function (done) {
            mockgoose.prepareStorage().then(function () {
                mongoose.connect('mongodb+srv://HelderPereira:Helder321@arqsi-9evym.azure.mongodb.net/test?retryWrites=true&w=majority', { useUnifiedTopology: true, useNewUrlParser: true }, function (err) {
                    done(err);
                });
            });
        });

        it("Deve criar encomenda", function (done) {
            Encomenda.create({
                data: "12/03/2019",
                produtos: [{preco:10,descricao:"Garfo",quantidade:10},{preco:10,descricao:"Faca",quantidade:10}],
                tempoConc: "12/03/2020",
                estado: "true",
                emailCliente: "hjvp1999@hotmail.com"
            }, function (err) {
                expect(err).not.to.be.ok;
                done(err);
            });
        });

        it("Deve encontrar a encomenda", function (done) {
            Encomenda.findOne({ emailCliente: "hjvp1999@hotmail.com" }, function (err, enc) {
                expect(err).not.to.be.ok;
                expect(enc).not.to.be.null;
                done(err);
            });
        });

        it("Deve apagar a encomenda", function (done) {
            Encomenda.deleteOne({ emailCliente: "hjvp1999@hotmail.com" }, function (err, enc) {
                expect(err).not.to.be.ok;
                done(err);
            });
        });

        it("Não deve encontrar a encomenda", function (done) {
            Encomenda.findOne({ emailCliente: "hjvp1999@hotmail.com" }, function (err, enc) {
                expect(err).not.to.be.ok;
                expect(enc).to.be.null;
                done(err);
            });
        });

        it("reset", function (done) {
            mockgoose.helper.reset().then(function () {
                done();
            });
        });
    });

});