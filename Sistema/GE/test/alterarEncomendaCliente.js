process.env.NODE_ENV = 'test';

let mongoose = require("mongoose");
let Client = require('../models/client');
let Admin = require('../models/admin');

var Mockgoose = require('mockgoose').Mockgoose;
var mockgoose = new Mockgoose(mongoose);

let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../app');
let should = chai.should();
var expect = chai.expect;
chai.use(chaiHttp);

var token;
var id;

describe('Alterar Encomenda Cliente', () => {

    it('Deve adicionar Cliente à BD', (done) => {
        let cliente = {
            email: "murilo@teste.pt",
            password: "123456789",
            cliente: "muri",
            empresa: "ISEP",
            morada: "Rua 1234",
            nif: "123456789",
            iban: "12345678987654",
            numCartao: "0987654321",
            ccv2: "321",
            validade: "2020/12/12"
        }
        chai.request(server)
            .post('/users/registo')
            .send(cliente)
            .end((err, res) => {
                if (err) done(err);
                else {
                    res.should.have.status(200);
                    //   res.body.should.have.property('message').eql('Utilizador registado!');
                    done();
                }
            });
    });


    it('Login Cliente - Correto', (done) => {
        let admin = {
            email: "murilo@teste.pt",
            password: "123456789"
        }
        chai.request(server)
            .post('/users/login')
            .send(admin)
            .end((err, res) => {
                if (err) done(err);
                else {
                    res.should.have.status(200);
                    res.body.should.have.property('token');
                    this.token = res.body.token;
                    done();
                }
            });
    });

    it('Deve criar uma encomenda', (done) => {
        let encomenda = {
            data: '01/04/2020',
            produtos: [{
                preco: 10.0,
                descricao: 'Garfo',
                quantidade: 5
            }],
            tempoConc: '12/04/2021'
        }

        chai.request(server)
            .post('/encomendas/registarEncomenda')
            .set('x-access-token', this.token)
            .send(encomenda)
            .end((err, res) => {
                if (err) done(err);
                else {
                    res.should.have.status(200);
                    res.body.should.have.property('message').eql('Encomenda realizada com sucesso!\nObrigado pela sua escolha!');
                    done();
                }
            });

    });

    it('Deve dar Get de uma encomenda', (done) => {
        chai.request(server)
            .get('/encomendas/user')
            .set('x-access-token', this.token)
            .end((err, res) => {
                if (err) done(err);
                else {
                    res.should.have.status(200);
                    this.id = res.body.encomendas[0]._id;
                    //res.body.should.have.property('message').eql('Encomenda alterada com sucesso.');
                    done();
                }
            });
    });

    it('Deve alterar uma encomenda', (done) => {
        let idEnc= this.id;
        let encomenda = {
            id: idEnc,
            produtos: [{
                preco: 10.0,
                descricao: 'Garfo',
                quantidade: 5
            },
            {
                preco: 10.0,
                descricao: 'Faca',
                quantidade: 5
            }],
            tempoConc: '20/04/2021'
        }
        chai.request(server)
            .put('/encomendas/alterarEncomendaCliente')
            .set('x-access-token', this.token)
            .send(encomenda)
            .end((err, res) => {
                if (err) done(err);
                else {
                    res.should.have.status(200);
                    //   res.body.should.have.property('message').eql('Encomenda alterada!');
                    done();
                }
            });
    });


    it('Deve consultar Encomenda', (done) => {
        var enc;
        console.log(this.token, '\n');
        console.log(this.id, '\n');
        chai.request(server)
            .get('/encomendas/encomenda/'+ this.id)
            .set('x-access-token', this.token)
            .end((err, res) => {
                if (err) done(err);
                else {
                    res.should.have.status(200);
                    enc = res.body.encomenda;
                    done();
                }
            });
            
    });

    it('Deve cancelar Encomenda', (done) => {
        var enc;
        chai.request(server)
            .delete('/encomendas/cancelarEncomenda/'+ this.id)
            .set('x-access-token', this.token)
            .end((err, res) => {
                if (err) done(err);
                else {
                    res.should.have.status(200);
                    enc = res.body.encomenda;
                    done();
                }
            });
    });

    it('Deve eliminar Encomenda', (done) => {
        chai.request(server)
            .delete('/encomendas/apagarEncomenda/' + this.id)
            .set('x-access-token', this.token)
            .end((err, res) => {
                if (err) done(err);
                else {
                    res.should.have.status(200);
                    done();
                }
            });
    });

    it('Deve eliminar Cliente registado', (done) => {
        let email = "murilo@teste.pt";
        chai.request(server)
            .delete('/users/' + email)
            .end((err, res) => {
                if (err) done(err);
                else {
                    res.should.have.status(200);
                    done();
                }
            });
    });

});