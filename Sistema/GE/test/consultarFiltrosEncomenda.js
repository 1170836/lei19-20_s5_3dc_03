process.env.NODE_ENV = 'test';

let mongoose = require("mongoose");

var Mockgoose = require('mockgoose').Mockgoose;
var mockgoose = new Mockgoose(mongoose);

let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../app');
let should = chai.should();
var expect = chai.expect;
chai.use(chaiHttp);

var token;

describe('Consultar Filtros da Encomenda', () => {

    it('Deve adicionar Cliente à BD', (done) => {
        let cliente = {
            email: "1234@1234.pt",
            password: "123456789",
            cliente: "Helder",
            empresa: "ISEP",
            morada: "Rua 1234",
            nif: "123456789",
            iban: "12345678987654",
            numCartao: "0987654321",
            ccv2: "321",
            validade: "2020/12/12"
        }
        chai.request(server)
            .post('/users/registo')
            .send(cliente)
            .end((err, res) => {
                if (err) done(err);
                else {
                    res.should.have.status(200);
                    res.body.should.have.property('message').eql('Utilizador registado!');
                    done();
                }
            });
    });

    it('Login Cliente - Correto', (done) => {
        let admin = {
            email: "1234@1234.pt",
            password: "123456789"
        }
        chai.request(server)
            .post('/users/login')
            .send(admin)
            .end((err, res) => {
                if (err) done(err);
                else {
                    res.body.should.have.property('token');
                    this.token = res.body.token;
                    res.should.have.status(200);
                    done();
                }
            });
    });

    /* it('Deve criar várias encomendas', (done) => {
        let encomenda1 = {
            data: '2019-01-30',
            produtos: [{
                preco: 10.0,
                descricao: 'Garfo',
                quantidade: 500
            }],
            tempoConc: '2020-03-30'
        }
        let encomenda2 = {
            data: '2019-01-30',
            produtos: [{
                preco: 10.0,
                descricao: 'Faca',
                quantidade: 1
            }],
            tempoConc: '2020-03-30'
        }
        for (let i = 0; i < 2; i++) {
            chai.request(server)
                .post('/encomendas/registarEncomenda')
                .set('x-access-token', this.token)
                .send(encomenda1)
                .end((err, res) => {
                    if (err) done(err);
                    else {
                        res.should.have.status(200);
                        res.body.should.have.property('message').eql('Encomenda realizada com sucesso!\nObrigado pela sua escolha!');
                        done();
                    }
                });
        }
        for (let i = 0; i < 10; i++) {
            chai.request(server)
                .post('/encomendas/registarEncomenda')
                .set('x-access-token', this.token)
                .send(encomenda2)
                .end((err, res) => {
                    if (err) done(err);
                    else {
                        res.should.have.status(200);
                        res.body.should.have.property('message').eql('Encomenda realizada com sucesso!\nObrigado pela sua escolha!');
                        done();
                    }
                });
        }
    });

    it('Deve dar Get das encomendas criadas', (done) => {
        for (let i = 0; i < 12; i++) {
            chai.request(server)
                .get('/encomendas/user')
                .set('x-access-token', this.token)
                .end((err, res) => {
                    if (err) done(err);
                    else {
                        res.should.have.status(200);
                        this.id[i] = res.body.encomendas[i]._id;
                        console.log(this.id[i]);
                        done();
                    }
                });
        }

    }); */

    it('Deve consultar os produtos mais encomendados', (done) => {
        chai.request(server)
            .get('/encomendas/maisEncomendados')
            .set('x-access-token', this.token)
            .end((err, res) => {
                if (err) done(err);
                else {
                    res.should.have.status(200);
                    done();
                }
            });
    });

    it('Deve consultar os produtos mais vezes encomendados', (done) => {
        chai.request(server)
            .get('/encomendas/maisVezesEncomendados')
            .set('x-access-token', this.token)
            .end((err, res) => {
                if (err) done(err);
                else {
                    res.should.have.status(200);
                    done();
                }
            })
    });

    /* it('Deve eliminar Encomendas', (done) => {
        for (let i = 0; i < 12; i++) {
            chai.request(server)
                .delete('/encomendas/apagarEncomenda/' + this.id[i])
                .set('x-access-token', this.tokenAdmin)
                .end((err, res) => {
                    if (err) done(err);
                    else {
                        res.should.have.status(200);
                        done();
                    }
                });
        }
    });
 */
    it('Deve eliminar Cliente registado', (done) => {
        let email = "12345@12345.pt";
        chai.request(server)
            .delete('/users/' + email)
            .end((err, res) => {
                if (err) done(err);
                else {
                    res.should.have.status(200);
                    done();
                }
            });
    });
});