//During the test the env variable is set to test
process.env.NODE_ENV = 'test';

let mongoose = require("mongoose");
let Client = require('../models/client');

//Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../app');
let should = chai.should();

chai.use(chaiHttp);

var tokenCliente;
var tokenAdmin;


describe('Adicionar e alterar Cliente (admin)', () => {

    it('Deve adicionar Cliente à BD', (done) => {
        let cliente = {
            email: "cliente@1234.pt",
            password: "123456789",
            cliente: "Helder",
            empresa: "ISEP",
            morada: "Rua 1234",
            nif: "123456789",
            iban: "12345678987654",
            numCartao: "0987654321",
            ccv2: "321",
            validade: "2020/12/12"
        }
        chai.request(server)
            .post('/users/registo')
            .send(cliente)
            .end((err, res) => {
                if (err) done(err);
                else {
                    res.should.have.status(200);
                    res.body.should.have.property('message').eql('Utilizador registado!');
                    this.tokenCliente = res.body.token;
                    done();
                }
            });
    });


    it('Deve adicionar Admin à BD', (done) => {
        let admin = {
            email: "admin@1234.pt",
            password: "123456789",
            nome: "Helder",
            isAdmin: true
        }
        chai.request(server)
            .post('/users/registo')
            .send(admin)
            .end((err, res) => {
                if (err) done(err);
                else {
                    res.should.have.status(200);
                    res.body.should.have.property('message').eql('Utilizador registado!');
                    done();
                }
            });
    });

    it('Login Admin', (done) => {
        let admin = {
            email: "admin@1234.pt",
            password: "123456789"
        }
        chai.request(server)
            .post('/users/login')
            .send(admin)
            .end((err, res) => {
                if (err) done(err);
                else {
                    res.should.have.status(200);
                    res.body.should.have.property('token');
                    this.tokenAdmin=res.body.token;
                    done();
                }
            });
    });

    /* it('Deve alterar o cliente', (done) => {
        let client = {
            empresa: "ISEP",
            email: "cliente@1234.pt",
            nif: "123456789",
            iban: "12345678987654",
            cliente: "Murilo Couceiro",
            morada: "Rua Admin"
        }
        chai.request(server)
            .put('/users/alterarPerfil')
            .set('x-access-token', this.tokenAdmin)
            .send(client)
            .end((err, res) => {
                if (err) done(err);
                else {
                    console.log(res.body);
                    res.should.have.status(200);
                    res.body.should.have.property('novoUser');
                    done();
                }
            });
    }); */


    it('Deve eliminar utilizador registado', (done) => {
        let email = "admin@1234.pt";
        chai.request(server)
            .delete('/users/' + email)
            .end((err, res) => {
                if (err) done(err);
                else {
                    res.should.have.status(200);
                    done();
                }
            });
    });

    it('Deve eliminar utilizador registado', (done) => {
        let email = "cliente@1234.pt";
        chai.request(server)
            .delete('/users/' + email)
            .end((err, res) => {
                if (err) done(err);
                else {
                    res.should.have.status(200);
                    done();
                }
            });
    });
});

describe('Adicionar e alterar Cliente (cliente)', () => {

    it('Deve adicionar Cliente à BD', (done) => {
        let cliente = {
            email: "muriloteste@hotmail.pt",
            password: "123456789",
            cliente: "Helder",
            empresa: "ISEP",
            morada: "Rua 1234",
            nif: "123456789",
            iban: "12345678987654",
            numCartao: "0987654321",
            ccv2: "321",
            validade: "2020/12/12"
        }
        chai.request(server)
            .post('/users/registo')
            .send(cliente)
            .end((err, res) => {
                if (err) done(err);
                else {
                    res.should.have.status(200);
                    res.body.should.have.property('message').eql('Utilizador registado!');

                    done();
                }
            });
    });


    it('Login Cliente - Correto', (done) => {
        let admin = {
            email: "muriloteste@hotmail.pt",
            password: "123456789"
        }
        chai.request(server)
            .post('/users/login')
            .send(admin)
            .end((err, res) => {
                if (err) done(err);
                else {
                    res.body.should.have.property('token');
                    this.tokenCliente = res.body.token;
                    res.should.have.status(200);
                    done();
                }
            });
    });

    it('Deve alterar o cliente', (done) => {
        let client = {
            email: "567@hotmail.pt",
            cliente: "Murilo",
            empresa: "IPP",
            morada: "Rua 567",
            nif: "987654321"
        }
        chai.request(server)
            .put('/users/alterarPerfil')
            .set('x-access-token', this.tokenCliente)
            .send(client)
            .end((err, res) => {
                if (err) done(err);
                else {
                    console.log(res.body);
                    res.should.have.status(200);

                    res.body.should.have.property('novoUser');
                    done();
                }
            });
    });

    it('Login Cliente - Correto', (done) => {
        let cliente = {
            email: "567@hotmail.pt",
            password: "123456789"
        }
        chai.request(server)
            .post('/users/login')
            .send(cliente)
            .end((err, res) => {
                if (err) done(err);
                else {
                    console.log(res.body);
                    res.body.should.have.property('token');
                    res.should.have.status(200);
                    done();
                }
            });
    });

    it('Deve eliminar utilizador registado', (done) => {
        let email = "567@hotmail.pt";
        chai.request(server)
            .delete('/users/' + email)
            .end((err, res) => {
                if (err) done(err);
                else {
                    res.should.have.status(200);
                    done();
                }
            });
    });

});

