//During the test the env variable is set to test
process.env.NODE_ENV = 'test';

let mongoose = require("mongoose");
let Client = require('../models/client');

//Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../app');
let should = chai.should();

chai.use(chaiHttp);

describe('Adicionar e alterar Cliente (admin)', () => {

    it('Deve adicionar Cliente à BD', (done) => {
        let cliente = {
            email: "cliente@1234.pt",
            password: "123456789",
            cliente: "Helder",
            empresa: "ISEP",
            morada: "Rua 1234",
            nif: "123456789",
            iban: "12345678987654",
            numCartao: "0987654321",
            ccv2: "321",
            validade: "2020/12/12"
        }
        chai.request(server)
            .post('/users/registo')
            .send(cliente)
            .end((err, res) => {
                if (err) done(err);
                done();
            });
    });
});