//During the test the env variable is set to test
process.env.NODE_ENV = 'test';

let mongoose = require("mongoose");
let Client = require('../models/client');
let Admin = require('../models/admin');

var Mockgoose = require('mockgoose').Mockgoose;
var mockgoose = new Mockgoose(mongoose);

let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../app');
let should = chai.should();
var expect = chai.expect;
chai.use(chaiHttp);

var token,tokenAdmin;
var id;

describe('Alterar Encomenda', () => {
    it('Deve adicionar Admin à BD', (done) => {
        let admin = {
            email: "1234@1234.pt",
            password: "123456789",
            nome: "Helder",
            isAdmin: true
        }
        chai.request(server)
            .post('/users/registo')
            .send(admin)
            .end((err, res) => {
                if (err) done(err);
                else {
                    res.should.have.status(200);
                    res.body.should.have.property('message').eql('Utilizador registado!');
                    done();
                }
            });
    });

    it('Deve adicionar Cliente à BD', (done) => {
        let cliente = {
            email: "12345@12345.pt",
            password: "123456789",
            cliente: "Helder",
            empresa: "ISEP",
            morada: "Rua 1234",
            nif: "123456789",
            iban: "12345678987654",
            numCartao: "0987654321",
            ccv2: "321",
            validade: "2020/12/12"
        }
        chai.request(server)
            .post('/users/registo')
            .send(cliente)
            .end((err, res) => {
                if (err) done(err);
                else {
                    res.should.have.status(200);
                    res.body.should.have.property('message').eql('Utilizador registado!');
                    done();
                }
            });
    });

    it('Login Admin - Correto', (done) => {
        let admin = {
            email: "1234@1234.pt",
            password: "123456789"
        }
        chai.request(server)
            .post('/users/login')
            .send(admin)
            .end((err, res) => {
                if (err) done(err);
                else {
                    res.body.should.have.property('token');
                    res.should.have.status(200);
                    this.tokenAdmin = res.body.token;
                    done();
                }
            });
    });

    it('Login Cliente - Correto', (done) => {
        let admin = {
            email: "12345@12345.pt",
            password: "123456789"
        }
        chai.request(server)
            .post('/users/login')
            .send(admin)
            .end((err, res) => {
                if (err) done(err);
                else {
                    res.should.have.status(200);
                    res.body.should.have.property('token');
                    this.token = res.body.token;
                    done();
                }
            });
    });

    it('Deve criar uma encomenda', (done) => {
        let encomenda = {
            data: '01/04/2020',
            produtos: [{
                preco: 10.0,
                descricao: 'Garfo',
                quantidade: 5
            }],
            tempoConc: '12/04/2021'
        }

        chai.request(server)
            .post('/encomendas/registarEncomenda')
            .set('x-access-token', this.token)
            .send(encomenda)
            .end((err, res) => {
                if (err) done(err);
                else {
                    res.should.have.status(200);
                    res.body.should.have.property('message').eql('Encomenda realizada com sucesso!\nObrigado pela sua escolha!');
                    done();
                }
            });

    });

    it('Deve dar Get de uma encomenda', (done) => {
        chai.request(server)
            .get('/encomendas/user')
            .set('x-access-token', this.token)
            .end((err, res) => {
                if (err) done(err);
                else {
                    res.should.have.status(200);
                    this.id = res.body.encomendas[0]._id;
                    //res.body.should.have.property('message').eql('Encomenda alterada com sucesso.');
                    done();
                }
            });
    });

    it('Deve alterar uma encomenda', (done) => {
        let enc = {
            encomenda : this.id,
            tempoConc : '2020-01-30T00:00:00.000Z',
            estado : 'Concluída'
        };

        chai.request(server)
            .put('/encomendas/alterarEncomenda')
            .set('x-access-token', this.tokenAdmin)
            .send(enc)
            .end((err, res) => {
                if (err) done(err);
                else {
                    res.should.have.status(200);
                    //res.body.should.have.property('message').eql('Encomenda alterada com sucesso.');
                    done();
                }
            });
    });

    it('Deve eliminar Encomenda', (done) => {
        chai.request(server)
            .delete('/encomendas/apagarEncomenda/' + this.id)
            .set('x-access-token', this.tokenAdmin)
            .end((err, res) => {
                if (err) done(err);
                else {
                    res.should.have.status(200);
                    done();
                }
            });
    });

    it('Deve eliminar Admin registado', (done) => {
        let email = "1234@1234.pt";
        chai.request(server)
            .delete('/users/' + email)
            .end((err, res) => {
                if (err) done(err);
                else {
                    res.should.have.status(200);
                    done();
                }
            });
    });

    it('Deve eliminar Cliente registado', (done) => {
        let email = "12345@12345.pt";
        chai.request(server)
            .delete('/users/' + email)
            .end((err, res) => {
                if (err) done(err);
                else {
                    res.should.have.status(200);
                    done();
                }
            });
    });

});