//During the test the env variable is set to test
process.env.NODE_ENV = 'test';

let mongoose = require("mongoose");
let Client = require('../models/client');
let Admin = require('../models/admin');

var nock = require('nock');

let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../app');
let should = chai.should();
var expect = chai.expect;
chai.use(chaiHttp);

var responseLoginSuccess = {
    success: true,
    message: "Aqui tem o seu Token: ",
    token: "378rywdmiAHE8WT934N243UITYNEMMRWF"
}

describe('Registar e Login', () => {

    describe('\nRegistar e Login Admin \n', () => {
        it('Deve dar erro ao preencher com dados incompletos', (done) => {
            let client = {
                email: "The Lord of the Rings",
            }
            chai.request(server)
                .post('/users/registo')
                .send(client)
                .end((err, res) => {
                    if (err) done(err);
                    else {
                        res.should.have.status(400);
                        done();
                    }
                });
        });

        it('Deve adicionar Admin à BD', (done) => {
            let admin = {
                email: "1234@1234.pt",
                password: "123456789",
                nome: "Helder",
                isAdmin: true
            }
            chai.request(server)
                .post('/users/registo')
                .send(admin)
                .end((err, res) => {
                    if (err) done(err);
                    else {
                        res.should.have.status(200);
                        res.body.should.have.property('message').eql('Utilizador registado!');
                        done();
                    }
                });
        });

        it('Login Admin - Password Incorreta', (done) => {
            let admin = {
                email: "1234@1234.pt",
                password: "12345678"
            }
            chai.request(server)
                .post('/users/login')
                .send(admin)
                .end((err, res) => {
                    if (err) done(err);
                    else {
                        res.should.have.status(401);
                        res.body.should.have.property('message').eql('Password inválida para o utilizador em questão.');
                        done();
                    }
                });
        });

        it('Login Admin - Correto', (done) => {
            let admin = {
                email: "1234@1234.pt",
                password: "123456789"
            }
            chai.request(server)
                .post('/users/login')
                .send(admin)
                .end((err, res) => {
                    if (err) done(err);
                    else {
                        res.should.have.status(200);
                        res.body.should.have.property('token');
                        done();
                    }
                });
        });

        it('Deve eliminar utilizador registado', (done) => {
            let email = "1234@1234.pt";
            chai.request(server)
                .delete('/users/' + email)
                .end((err, res) => {
                    if (err) done(err);
                    else {
                        res.should.have.status(200);
                        done();
                    }
                });
        });
    });

    describe('\n Registar e Login Cliente \n', () => {

        it('Deve adicionar Cliente à BD', (done) => {
            let cliente = {
                email: "1234@1234.pt",
                password: "123456789",
                cliente: "Helder",
                empresa: "ISEP",
                morada: "Rua 1234",
                nif: "123456789",
                iban: "12345678987654",
                numCartao: "0987654321",
                ccv2: "321",
                validade: "2020/12/12"
            }
            chai.request(server)
                .post('/users/registo')
                .send(cliente)
                .end((err, res) => {
                    if (err) done(err);
                    else {
                        res.should.have.status(200);
                        res.body.should.have.property('message').eql('Utilizador registado!');
                        done();
                    }
                });
        });

        it('Login Cliente - Password Incorreta', (done) => {
            let admin = {
                email: "1234@1234.pt",
                password: "12345678"
            }
            chai.request(server)
                .post('/users/login')
                .send(admin)
                .end((err, res) => {
                    if (err) done(err);
                    else {
                        res.should.have.status(401);
                        res.body.should.have.property('message').eql('Password inválida para o utilizador em questão.');
                        done();
                    }
                });
        });

        it('Login Cliente - Correto', (done) => {
            let admin = {
                email: "1234@1234.pt",
                password: "123456789"
            }
            chai.request(server)
                .post('/users/login')
                .send(admin)
                .end((err, res) => {
                    if (err) done(err);
                    else {
                        res.should.have.status(200);
                        res.body.should.have.property('token');
                        done();
                    }
                });
        });

        it('Deve eliminar utilizador registado', (done) => {
            let email = "1234@1234.pt";
            chai.request(server)
                .delete('/users/' + email)
                .end((err, res) => {
                    if (err) done(err);
                    else {
                        res.should.have.status(200);
                        done();
                    }
                });
        });

    });

    /* describe('\n Login Cliente (MOCK) \n', () => {

        beforeEach(() => {
            nock('http://localhost:8080')
                .post('/users/login')
                .reply(200, responseLoginSuccess);
        });

        it('Deve fazer login com sucesso', () => {

            let client = {
                email: "1234@1234.pt",
                password: "123456789"
            }

            chai.request(server)
                .post('/users/login')
                .send(client)
                .end((err, res) => {
                    if (err) done(err);
                    else {
                        res.body.should.have.property('token');
                        res.should.have.status(200);
                        done();
                    }
                });
        });

    }); */


});