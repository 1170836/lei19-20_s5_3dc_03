% F�BRICA

% Linhas

linhas([lA,lB,lC]).


% Maquinas

% as m�quinas mf at� mj s�o iguais �s m�quinas ma at� me e constituem a linha lB igual a lA
% as m�quinas mk at� mm constituem a linha lC 
maquinas([ma,mb,mc,md,me,mf,mg,mh,mi,mj,mk,ml,mm]).



% Ferramentas

% as ferramentas fa1 at� fj1 s�o iguais �s ferramentas fa a fj, sendo usadas pelas m�quinas mf at� mj
 
ferramentas([fa,fb,fc,fd,fe,ff,fg,fh,fi,fj,fa1,fb1,fc1,fd1,fe1,ff1,fg1,fh1,fi1,fj1,fk,fl,fm]).


% Maquinas que constituem as Linhas

tipos_maq_linha(lA,[ma,mb,mc,md,me]).
tipos_maq_linha(lB,[mf,mg,mh,mi,mj]).
tipos_maq_linha(lC,[mk,ml,mm]).

% ...


% Opera��es

tipo_operacoes([opt1,opt2,opt3,opt4,opt5,opt6,opt7,opt8,opt9,opt10,opt11,opt12,opt13]).

% operacoes/1 vai ser criado dinamicamente
%no exemplo dara' uma lista com 92 opera��es: 50 operacoes pelos 10 lotes de produtos * 5 operacoes que poder�o ir para as linhas lA ou lB mais 14 lotes de produtos * 3 opera��es que ir�o para a linha lC

%operacoes_atrib_maq/2 vai ser criado dinamicamente
%no exemplo as m�quinas mk a mm ter�o 14 operacoes atribuidas, uma por cada lote de produtos que ir� para lC; as opera��es atribu�das �s m�quinas ma at� mj dependem do balanceamento que for feito 

% classif_operacoes/2 deve ser criado dinamicamente 
%no exemplo teremos 92 factos deste tipo, um para cada operacao


% Afeta��o de tipos de opera��es a tipos de m�quinas
% com ferramentas, tempos de setup e tempos de execucao)

operacao_maquina(opt1,ma,fa,1,1).
operacao_maquina(opt2,mb,fb,2.5,2).
operacao_maquina(opt3,mc,fc,1,3).
operacao_maquina(opt4,md,fd,1,1).
operacao_maquina(opt5,me,fe,2,3).
operacao_maquina(opt6,mb,ff,1,4).
operacao_maquina(opt7,md,fg,2,5).
operacao_maquina(opt8,ma,fh,1,6).
operacao_maquina(opt9,me,fi,1,7).
operacao_maquina(opt10,mc,fj,20,2).
operacao_maquina(opt1,mf,fa1,1,1).
operacao_maquina(opt2,mg,fb1,2.5,2).
operacao_maquina(opt3,mh,fc1,1,3).
operacao_maquina(opt4,mi,fd1,1,1).
operacao_maquina(opt5,mj,fe1,2,3).
operacao_maquina(opt6,mg,ff1,1,4).
operacao_maquina(opt7,mi,fg1,2,5).
operacao_maquina(opt8,mf,fh1,1,6).
operacao_maquina(opt9,mj,fi1,1,7).
operacao_maquina(opt10,mh,fj1,20,2).
operacao_maquina(opt11,mk,fk,3,2).
operacao_maquina(opt12,ml,fl,1,4).
operacao_maquina(opt13,mm,fm,1,3).






%...


% PRODUTOS

% os produtos pA at� pF podem ser fabricados em lA ou lB
% os produtos pG at� pJ s� podem ser fabricados em lC

produtos([pA,pB,pC,pD,pE,pF,pG,pH,pI,pJ]).

operacoes_produto(pA,[opt1,opt2,opt3,opt4,opt5]).
operacoes_produto(pB,[opt1,opt6,opt3,opt4,opt5]).
operacoes_produto(pC,[opt1,opt2,opt3,opt7,opt5]).
operacoes_produto(pD,[opt8,opt2,opt3,opt4,opt5]).
operacoes_produto(pE,[opt1,opt2,opt3,opt4,opt9]).
operacoes_produto(pF,[opt1,opt2,opt10,opt4,opt5]).
operacoes_produto(pG,[opt11,opt12,opt13]).
operacoes_produto(pH,[opt11,opt12,opt13]).
operacoes_produto(pI,[opt11,opt12,opt13]).
operacoes_produto(pJ,[opt11,opt12,opt13]).



% ENCOMENDAS

%Clientes

clientes([clA,clB,clC,clD,clE,clF,clG,clJ]).


% prioridades dos clientes

prioridade_cliente(clA,2).
prioridade_cliente(clB,1).
prioridade_cliente(clC,3).
prioridade_cliente(clD,1).
prioridade_cliente(clE,1).
prioridade_cliente(clF,1).
prioridade_cliente(clG,1).
prioridade_cliente(clJ,2).

% ...

% Encomendas do cliente, 
% termos e(<produto>,<n.unidades>,<tempo_conclusao>)

% encomendas que poder�o ir para a linha lA ou lB
encomenda(1,clA,[e(pA,4,50)]).
encomenda(2,clA,[e(pB,4,70)]).
encomenda(3,clB,[e(pC,3,30)]).
encomenda(4,clB,[e(pD,5,200)]).
encomenda(5,clC,[e(pE,4,60)]).
encomenda(6,clC,[e(pF,6,120)]).
encomenda(7,clA,[e(pD,1,500)]).
encomenda(8,clA,[e(pF,20,450)]).
encomenda(9,clB,[e(pB,4,100)]).
encomenda(10,clC,[e(pA,3,100)]).

% encomendas que ir�o para a linha lC
encomenda(11,clD,[e(pG,5,40)]).
encomenda(12,clE,[e(pH,3,30)]).
encomenda(13,clF,[e(pI,4,60)]).
encomenda(14,clJ,[e(pJ,5,140)]).
encomenda(15,clD,[e(pJ,5,200)]).
encomenda(16,clE,[e(pG,4,150)]).
encomenda(17,clF,[e(pH,3,180)]).
encomenda(18,clJ,[e(pI,2,100)]).
encomenda(19,clA,[e(pI,4,100)]).
encomenda(20,clA,[e(pH,5,170)]).
encomenda(21,clB,[e(pG,5,230)]).
encomenda(22,clB,[e(pJ,3,250)]).
encomenda(23,clC,[e(pG,6,280)]).
encomenda(24,clC,[e(pH,6,300)]).


% ====================== HEURISTICA BALANCEMANTO DE LINHAS =================================== 

gera_pp(PP):-
	retractall(tarefa_linha(_,_)),
	findall((IdTarefa,Tconc,Produto),tarefa(_,IdTarefa,_,Tconc,_,Produto),ListaTarefas),
	findall((Linha,ListaMaquinas),tipos_maq_linha(Linha,ListaMaquinas),ListaLinhas),
	%write('ListaTarefas'),write(ListaTarefas),nl,nl,
	%write('ListaLinhas'),write(ListaLinhas),nl,
	gera_pp(ListaTarefas,ListaLinhas,PP).
	
gera_pp(ListaTarefas,ListaLinhas,PP):-
	%write('entrou gera pp'),nl,
	cria_linhas(ListaLinhas,[],AuxPP),
	
	sort(2, @>=,ListaTarefas, ListaTarefasOrdenada), % ordena pelo Tconc de forma descendente
	
	gera_pp2(ListaTarefasOrdenada, AuxPP,AuxPP, PP).
	
gera_pp2([],_,PP,PP).
	
gera_pp2([(IdTarefa,Tconc,Produto)|ListaTarefas], TestePP, AuxPP, PP):-
	
	%write('entrou gera pp2'),nl,
	sort(2, @=<, TestePP, ListaLinhasOrdenada), % ordena pelo Tconc de forma ascendente 
	%write('Linha ordenada'),write(ListaLinhasOrdenada),nl ,
	escolhe_linha((IdTarefa,Tconc,Produto), ListaLinhasOrdenada, Linha),
	
	%write('Linha= '),write(Linha),nl,
	
	update_linha(AuxPP,Linha,(IdTarefa, Tconc,Produto),[],AuxPP2),
	%write('linhas novas'), nl ,write(AuxPP2),nl,
	
	gera_pp2(ListaTarefas, AuxPP2, AuxPP2,PP),!.
		

% =========== METODOS AUXILIARES ========		
cria_linhas([], AuxPP,AuxPP).

cria_linhas([(Linha,Maquinas)|ListaLinhas],AuxPP, Final):-

	%write('entrou criar linhas'),nl,
	get_operacoes(Maquinas,[],ListaOperacoes),
	
	cria_linhas(ListaLinhas, [(Linha,0,[],ListaOperacoes)|AuxPP], Final).
	
get_operacoes([],ListaOperacoes,ListaOperacoes).
	
get_operacoes([Maquina| Maquinas] ,Aux, ListaOperacoes):- 
	%write('entrou get oper'),nl,
	findall(Opt, operacao_maquina(Opt,Maquina,_,_,_),Ops),
	append(Ops,Aux,Join),
	sort(Join,Join2),
	get_operacoes(Maquinas, Join2 ,ListaOperacoes),!.
	
escolhe_linha((IdTarefa, Mk,P), [(Linha,_,_,OperacoesLinha)|AuxPP], LinhaEscolhida):-
	
	%write('entrou escolhe_linha'),nl,
	operacoes_produto(P, OpsProduto),

	%nl,nl,write('Operacoes Produto= '),write(OpsProduto),nl,
	
	%write('Operacoes  Linha= '),write(OperacoesLinha),nl,
	
	escolhe_linha2(OpsProduto, OperacoesLinha), 
	(
	%write('Linha EL= '),write(Linha),nl,
	%escolhe_linha(_, [], Linha),
	asserta(tarefa_linha(IdTarefa,Linha)),
	LinhaEscolhida = Linha),!
	; 	
	escolhe_linha((IdTarefa, Mk,P), AuxPP, LinhaEscolhida),!. 

escolhe_linha2([],_).

escolhe_linha2( [Op|OpsProduto], OperacoesLinha ):- 
	%write('Op= '), write(Op),nl,
	%write('OpsLinha= '), write(OperacoesLinha),nl,
	member(Op,OperacoesLinha), (escolhe_linha2(OpsProduto, OperacoesLinha),!); (false,!).
	
	
update_linha([],_,_,AuxPP2,AuxPP2).

update_linha([(Linha, TC, Tarefas, Ops)|AuxPP],LinhaAtualizar,(IdTarefa,Tconc,P),AuxPP2,NewPP):-
	
	%nl,nl,
	%write(Linha),nl,
	%write(LinhaAtualizar),nl,
	(Linha == LinhaAtualizar -> (TC2 is TC + Tconc, update_linha(AuxPP,LinhaAtualizar,(IdTarefa,Tconc,P),[(Linha,TC2,[IdTarefa|Tarefas],Ops)|AuxPP2],NewPP),!)
	;
	update_linha(AuxPP, LinhaAtualizar,(IdTarefa, Tconc,P),[(Linha,TC,Tarefas,Ops)|AuxPP2], NewPP),!).
	
% =================================  FIM  ==========================================

% =================================== AGENDAS TEMPORAIS ============================
:- dynamic agenda_maquina/2.

criar_agendas_maquinas:-
	retractall(agenda_maquina(_,_)),!,
	gera_pp(_),!,
	obtemMelhorSolucaoAlgoritmoGenetico(Melhor),!,
	
	%write(Melhor),nl,nl,nl,
	%write(PP),nl
	
	%write(Melhor),nl,
	linhas(Linhas),
	criar_agenda_linha(Melhor,Linhas),
	finaliza_agenda.
	
	
finaliza_agenda:-
	findall((Maquina,Agenda),agenda_maquina(Maquina,Agenda),ListaAgendas),
	finaliza_agenda(ListaAgendas).

finaliza_agenda([]).
finaliza_agenda([(Maquina,Agenda)|ListaAgendas]):-
	retractall(agenda_maquina(Maquina,Agenda)),
	reverse(Agenda,AgendaOrdem),
	asserta(agenda_maquina(Maquina,AgendaOrdem)),
	finaliza_agenda(ListaAgendas).
	
criar_agenda_linha(_,[]). 
criar_agenda_linha(Tarefas, [Linha| Linhas]):-
	%write(Tarefas),nl,
	
	get_operacoes_linhas(Tarefas,Linha, TarefasLinha, RestoTarefas),
	
	reverse(RestoTarefas, RestoTarefasOrdem),
	reverse(TarefasLinha, TarefasLinhaOrdem),
	
	%write('Rt= '),write(RestoTarefasOrdem),nl,
	%write('Tl= '),write(TarefasLinhaOrdem),nl,
	
	length(TarefasLinhaOrdem,NTarefas),
	%sleep(2),
	%write(NTarefas), nl,
	criar_agendas_maquinas2(TarefasLinhaOrdem,0,0,NTarefas),
	criar_agenda_linha(RestoTarefasOrdem, Linhas).
	
obtemMelhorSolucaoAlgoritmoGenetico(Melhor):-
	geraIntegracao,!,
	geracaoFinal(Melhor*_),!.
	
get_operacoes_linhas(Tarefas, Linha, TarefasLinha ,RestoTarefas):-
	get_operacoes_linhas(Tarefas, Linha,[],[],TarefasLinha ,RestoTarefas).
	
get_operacoes_linhas([],_,A,B,A,B).
	%write('Fim'),nl,nl,
	%write('TarefasLinha= '),write(A),nl, 
	%write('RestoTarefas= '),write(B),nl.	
	
get_operacoes_linhas([IdTarefa| Tarefas],Linha,TarefasLinha,RestoTarefas,TF,RF):-
	
	%write('Tarefas= '),write(Tarefas),nl,
	
	%write('TarefasLinha= '),write(TarefasLinha),nl,
	%write('RestoTarefas= '),write(RestoTarefas),nl,
	%sleep(2),
	
	%write('IdTarefa= '),write(IdTarefa),nl,
	%write('Linha= '),write(Linha),nl,
	
	tarefa_linha(IdTarefa, Linha),
	(!,	get_operacoes_linhas(Tarefas, Linha, [IdTarefa|TarefasLinha],RestoTarefas,TF,RF))
	;
	(!,get_operacoes_linhas(Tarefas, Linha, TarefasLinha, [IdTarefa|RestoTarefas],TF,RF)).
	
% =================================== CRIAR AGENDAS ============================
	
criar_agendas_maquinas2([],_,N,N).
criar_agendas_maquinas2([IdTarefa|RestoTarefas] ,InicioTarefa,0,NumeroTarefas):-

	tarefa(_,IdTarefa,Makespan,_,_,Produto),
	tarefa_linha(IdTarefa,Linha),
	tipos_maq_linha(Linha,Maquinas),
	
	sequenciarTarefaNasMaquinas(IdTarefa,Produto,Maquinas,InicioTarefa),
	
	FimTarefa is InicioTarefa + Makespan,
	criar_agendas_maquinas2(RestoTarefas,FimTarefa,1,NumeroTarefas).

criar_agendas_maquinas2([IdTarefa|RestoTarefas],InicioTarefa,TarefasTratadas,NumeroTarefas):-

	tarefa(_,IdTarefa,Makespan,_,_,Produto),
	tarefa_linha(IdTarefa,Linha),
	tipos_maq_linha(Linha,Maquinas),
	
	sequenciarTarefaNasMaquinas(IdTarefa,Produto,Maquinas,InicioTarefa),
	
	FimTarefa is InicioTarefa + Makespan,
	TarefasTratadasNovo is TarefasTratadas + 1,
	deslizamentos(Maquinas),
	criar_agendas_maquinas2(RestoTarefas,FimTarefa,TarefasTratadasNovo,NumeroTarefas).
	
sequenciarTarefaNasMaquinas(IdTarefa,Produto,Maquinas,InicioTarefa):-
	operacoes_produto(Produto,Operacoes),
	sequenciarTarefaNasMaquinas2(IdTarefa,Operacoes,Maquinas,InicioTarefa).
	
sequenciarTarefaNasMaquinas2(_,[],_,_).
sequenciarTarefaNasMaquinas2(IdTarefa,[Operacao|RestoOperacoes],Maquinas,InicioOperacao):-
	sequenciarOperacaoNasMaquinas(IdTarefa,Operacao,Maquinas,InicioOperacao,FimOperacao),
	sequenciarTarefaNasMaquinas2(IdTarefa,RestoOperacoes,Maquinas,FimOperacao).

sequenciarOperacaoNasMaquinas(IdTarefa,OperacaoAtual,[Maquina|RestoMaquinas],InicioOperacao,FimOperacao):-
	operacao_maquina(OperacaoAtual,Maquina,_,_,_),!,
	criar_ou_adicionar_agenda(IdTarefa,OperacaoAtual,Maquina,InicioOperacao,FimOperacao)
	;
	sequenciarOperacaoNasMaquinas(IdTarefa,OperacaoAtual,RestoMaquinas,InicioOperacao,FimOperacao).
	
criar_ou_adicionar_agenda(IdTarefa,Operacao,Maquina,InicioOperacao,FimOperacao):-
	agenda_maquina(Maquina,Agenda),
	
	retract(agenda_maquina(Maquina,_)),
	adicionar_setup_agenda(Agenda,NewAgenda,Operacao,Maquina,InicioOperacao,FinalTemp),
	adicionar_duracao_agenda(NewAgenda,NewAgenda2,IdTarefa,Operacao,Maquina,FinalTemp,FimOperacao),
	asserta(agenda_maquina(Maquina,NewAgenda2))
	;
	criar_agenda_com_setup(Operacao,Maquina,Agenda,InicioOperacao,FinalTemp),
	adicionar_duracao_agenda(Agenda,NewAgenda,IdTarefa,Operacao,Maquina,FinalTemp,FimOperacao),
	asserta(agenda_maquina(Maquina,NewAgenda)).
	
adicionar_setup_agenda(Agenda,NewAgenda,ProximaOperacao,Maquina,InicioOperacao,FimOperacao):-
	Agenda = [t(_,_,_,[Operacao,_,_,_,_])|_],
	
	
	operacao_maquina(Operacao,Maquina,Ferramenta,_,_),
	operacao_maquina(ProximaOperacao,Maquina,Ferramenta2,Setup,_),
	
	Ferramenta \== Ferramenta2,
	InicioNovo is InicioOperacao - Setup,
	(InicioNovo >= 0 -> (
		FimOperacao is InicioNovo + Setup,
		NewAgenda = [t(InicioNovo,FimOperacao,setup,[Ferramenta2])|Agenda]
	)
	;
	(
		FimOperacao is 0 + Setup,
		NewAgenda = [t(0,FimOperacao,setup,[Ferramenta2])|Agenda]
	))
	.

adicionar_setup_agenda(Agenda,Agenda,_,_,Inicio,Inicio).

criar_agenda_com_setup(Operacao,Maquina,[t(InicioReal,FimOperacao,setup,[Ferramenta])],InicioOperacao,FimOperacao):-
	operacao_maquina(Operacao,Maquina,Ferramenta,Setup,_),
	
	InicioNovo is InicioOperacao - Setup,
	(InicioNovo >= 0 -> (
		InicioReal is InicioNovo,
		FimOperacao is InicioNovo + Setup
	)
	;
	(
		InicioReal is 0,
		FimOperacao is 0 + Setup
	)).

adicionar_duracao_agenda(AgendaAtual,NewAgenda,IdTarefa,Operacao,Maquina,InicioOperacao,FimOperacao):-
	operacao_maquina(Operacao,Maquina,_,_,Duracao),
	tarefa(IdEnc,IdTarefa,_,_,_,Produto),
	quantidadeProdutoEncomenda(IdEnc,Produto,Quantidade),
	encomenda(IdEnc,Cliente,_),
	FimOperacao is InicioOperacao + Duracao,
	FimPlano is InicioOperacao + (Duracao*Quantidade),
	NewAgenda = [t(InicioOperacao,FimPlano,exec,[Operacao,Quantidade,Produto,Cliente,IdTarefa])|AgendaAtual].
	
quantidadeProdutoEncomenda(IdEnc,Produto,Quantidade):-
	encomenda(IdEnc,_,Produtos),
	quantidadeProdutoEncomenda2(Produtos,Produto,Quantidade).
	
quantidadeProdutoEncomenda2([e(Produto,Quantidade,_)|_],Produto,Quantidade).
quantidadeProdutoEncomenda2([e(_,_,_)|RestoProdutos],Produto,Quantidade):-
		quantidadeProdutoEncomenda2(RestoProdutos,Produto,Quantidade).
	

maior_setup(Produto,OperacaoMaiorSetup):-
	
	%tarefa(_,IdTarefa,_,_,Produto),
	operacoes_produto(Produto, Operacoes),
	
	%findall(Operacoes, operacoes_produto(pA,Operacoes), [Operacoes|_]),
	
	maior_setup2(Operacoes,[],OperacaoMaquina),
	sort(OperacaoMaquina,Aux),
	reverse(Aux,[OperacaoMaiorSetup|_]).
	
	
maior_setup2([],F,F).

maior_setup2([Op|Operacoes],Aux,OperacaoMaquina):-
	findall((Setup,Op,M,Fa,Tc), operacao_maquina(Op,M,Fa,Setup,Tc), Lista),
	append(Lista,Aux,Join),
	maior_setup2(Operacoes,Join, OperacaoMaquina).
	
% =================================== DESLIZAMENTOS  ============================

deslizamentos(Maquinas):-
	menorDeslizamento(Maquinas,Deslizamento),
	deslizarAgendas(Maquinas,Deslizamento).
	
menorDeslizamento(Maquinas,Deslizamento):-
	menorDeslizamento2(Maquinas,TodosDeslizamentos),
	%write('TodosDeslizamentos'),write(TodosDeslizamentos),nl,nl,
	sort(TodosDeslizamentos,TodosOrdenados),
	%write('TodosOrdenados'),write(TodosOrdenados),nl,nl,
	TodosOrdenados = [(Deslizamento,_)|_].

menorDeslizamento2([],[]).
menorDeslizamento2([Maquina|RestoMaquinas],[(Deslizamento,Maquina)|Resto]):-
	deslizamento(Maquina,Deslizamento),
	menorDeslizamento2(RestoMaquinas,Resto).
	
deslizamento(Maquina,Deslizamento):-
	agenda_maquina(Maquina,Agenda),
	%write('Agenda da M�quina: '),write(Maquina),write(' --- '),write(Agenda),nl,
	getDuasUltimasOperacoesAgenda(Agenda,Operacoes),
	%nl,write(Operacoes),
	
	Operacoes = [Ultimo|Resto],
	Resto = [Penultimo|_],
	
	calcularDeslizamento(Ultimo,Penultimo,Deslizamento).
	
calcularDeslizamento(t(Inicio,_,_,_),t(_,Fim,_,_),Deslizamento):-
	Deslizamento is Inicio - Fim.
	
getDuasUltimasOperacoesAgenda(Agenda,Operacoes):-
	Agenda = [Ultimo|RestoAgenda],
	RestoAgenda = [Penultimo|RestoAgenda2],
	RestoAgenda2 = [Antepenultimo|_],
	
	tipoOperacao(Penultimo,Tipo2),
	
	(Tipo2==exec -> (append([],[Ultimo],Ops),append(Ops,[Penultimo],Operacoes))
	;
	(append([],[Penultimo],Ops),append(Ops,[Antepenultimo],Operacoes))).
	
tipoOperacao(t(_,_,Op,_),Op).

deslizarAgendas([],_).
deslizarAgendas([Maquina|RestoMaquinas],Deslizamento):-
	agenda_maquina(Maquina,Agenda),
	retract(agenda_maquina(Maquina,_)),
	Agenda = [t(Inicio,Fim,exec,[Operacao,Quantidade,Produto,Cliente,Tarefa])|RestoAgenda],
	RestoAgenda = [Penultimo| RestoAgenda2],
	
	tipoOperacao(Penultimo,Tipo),
	(Tipo==setup -> 
	(
	gerarNovaOperacao(Penultimo,Deslizamento,Nova),
	append([Nova],RestoAgenda2,Temp),
	
	NovoInicio is Inicio-Deslizamento,
	NovoFim is Fim-Deslizamento,
	append([t(NovoInicio,NovoFim,exec,[Operacao,Quantidade,Produto,Cliente,Tarefa])],
	Temp,NovaAgenda)
	)
	
	;
	
	(
	NovoInicio is Inicio-Deslizamento,
	NovoFim is Fim-Deslizamento,
	append([t(NovoInicio,NovoFim,exec,[Operacao,Quantidade,Produto,Cliente,Tarefa])],
	RestoAgenda,NovaAgenda)
	)
	),
	
	%nl,write('Agenda da Maquina: '),write(Maquina),write(' --- '),write(NovaAgenda),nl,
	asserta(agenda_maquina(Maquina,NovaAgenda)),
	deslizarAgendas(RestoMaquinas,Deslizamento).
	

gerarNovaOperacao(t(Inicio,Fim,setup,[Ferramenta]),Deslizamento,t(NovoInicio,NovoFim,setup,[Ferramenta])):-
	NovoInicio is Inicio-Deslizamento,
	NovoFim is Fim-Deslizamento.
	
gerarNovaOperacao(t(Inicio,Fim,exec,[Op,Dur,Prod,Cl,Tar]),Deslizamento,t(NovoInicio,NovoFim,exec,[Op,Dur,Prod,Cl,Tar])):-
	NovoInicio is Inicio-Deslizamento,
	NovoFim is Fim-Deslizamento.
	
% =================================  FIM  ==========================================










% ============================  SPRINTS PASSADO  ====================================

% ========================================== CRIA��O DIN�MICA ======================================

% cria_op_enc - fizeram-se correcoes face a versao anterior

:- dynamic operacoes_atrib_maq/2.
:- dynamic classif_operacoes/2.
:- dynamic op_prod_client/9.
:- dynamic operacoes/1.


cria_op_enc:-retractall(operacoes(_)),
retractall(operacoes_atrib_maq(_,_)),retractall(classif_operacoes(_,_)),
retractall(op_prod_client(_,_,_,_,_,_,_,_,_)),
		findall(t(Cliente,Prod,Qt,TConc),
		(encomenda(_,Cliente,LE),member(e(Prod,Qt,TConc),LE)),
		LT),cria_ops(LT,0),
findall(Op,classif_operacoes(Op,_),LOp),asserta(operacoes(LOp)),
maquinas(LM),
 findall(_,
		(member(M,LM),
		 findall(Opx,op_prod_client(Opx,M,_,_,_,_,_,_,_),LOpx),
		 assertz(operacoes_atrib_maq(M,LOpx))),_).

cria_ops([],_).
cria_ops([t(Cliente,Prod,Qt,TConc)|LT],N):-
			operacoes_produto(Prod,LOpt),
	cria_ops_prod_cliente(LOpt,Cliente,Prod,Qt,TConc,N,N1),
			cria_ops(LT,N1).


cria_ops_prod_cliente([],_,_,_,_,Nf,Nf).
cria_ops_prod_cliente([Opt|LOpt],Client,Prod,Qt,TConc,N,Nf):-
		cria_ops_prod_cliente2(Opt,Prod,Client,Qt,TConc,N,Ni),
	cria_ops_prod_cliente(LOpt,Client,Prod,Qt,TConc,Ni,Nf).


cria_ops_prod_cliente2(Opt,Prod,Client,Qt,TConc,N,Ni):-
			Ni is N+1,
			atomic_concat(op,Ni,Op),
			assertz(classif_operacoes(Op,Opt)),
			operacao_maquina(Opt,M,F,Tsetup,Texec),
	assertz(op_prod_client(Op,M,F,Prod,Client,Qt,TConc,Tsetup,Texec)).



% ========================================== FIM ======================================

% ==============================    CRIACAO TAREFA    =======================================
geraTarefas:-
	
	retractall(tarefa(_,_,_,_,_,_)),
	retractall(tarefas(_)),
	
	findall((IdEnc,C,Encomenda),
	encomenda(IdEnc,C,Encomenda)
	,ListaEncomendas),
	
	%write(ListaEncomendas),nl,

	geraTarefasFabrica(ListaEncomendas,[],0,1,TarefasFabrica,N),
	assertTarefas(TarefasFabrica,N).

geraTarefasFabrica([],Final,_,N,Final,N):-!.

geraTarefasFabrica([Encomenda | ListaEncomendas] , Temp ,NTemp, SomaN ,TarefasFabrica , N):-
	
	geraTarefas(Encomenda, Tarefas, SomaN,NumeroTarefasEncomenda),
	append(Tarefas, Temp , Join),
	geraTarefasFabrica(ListaEncomendas,Join,NTemp,NumeroTarefasEncomenda,TarefasFabrica,N) , !.
	
geraTarefas((_,_,[]),_,_,_).
	
geraTarefas((IdEnc,ClA,[e(Pa,Unidades,Tconclusao) | ProdutosRestantes]),Tarefas,SomaN,N):-
	 
	prioridade_cliente(ClA,Prioridade),
	
	geraTarefas2(IdEnc,Prioridade,[e(Pa,Unidades,Tconclusao) | ProdutosRestantes],[],Tarefas,SomaN,N).

geraTarefas2(_,_,[],Tarefas,Tarefas,N,N):-!.

geraTarefas2(IdEnc,Prioridade, [e(Pa,Unidades,Tconclusao) | ProdutosRestantes], Tarefas, TarefasF ,SomaN, N):-
	operacoes_produto(Pa,ListaOpt),
	makespan(ListaOpt,Unidades,Mk),
	SomaN1 is SomaN + 1,
	geraTarefas2(IdEnc,Prioridade,ProdutosRestantes,[(IdEnc,SomaN,Mk,Tconclusao,Prioridade,Pa)|Tarefas],TarefasF,SomaN1,N), !.

makespan(ListaOpt,Unidades,Res):-
	makespan2(ListaOpt,[],ListaOrd),
	reverse(ListaOrd,ListaReverse),
	makespanTS(ListaReverse,0,0,MkTs),
	
	sort(1,@=<,ListaOrd,ListaOrd2),
	makespanTE(Unidades,ListaOrd2,0, MkTe),
	Res is MkTe - MkTs.

makespan2([],ListaT,ListaT):-!. 

makespan2([Opt|ListaOpt],ListaT,ListaFinal):-
	operacao_maquina(Opt,_,_,Tsetup,Texec),
	append([(Texec,Tsetup)],ListaT,ListaA),
	makespan2(ListaOpt, ListaA,ListaFinal),!.	
		
makespanTE(_,[],Som,Som):-!.
	
makespanTE(N,[(Te,_)],Som,Res):-
	Som1 is Som + (N * Te),
	makespanTE(N,[],Som1,Res),!.

makespanTE(N,[(Te,_)|Tempos],Som,Res):-
	Som1 is Som + Te,
	makespanTE(N,Tempos,Som1,Res),!.

makespanTS([],_,Min,Min):-!.

makespanTS([(Te,Ts)|Tempos],Som,Min,Res):-
	Som2 is (Som - Ts),
	(Som2 < Min, Min1 is Som2; Min1 is Min),
	Som1 is Som + Te,
	makespanTS(Tempos,Som1, Min1,Res),!.

assertTarefas([],N):-
	N1 is N-1,
	asserta(tarefas(N1)),!.
	
assertTarefas([(IdEnc,SomaN,Mk,Tconclusao,Prioridade,Produto)|ListaTarefas], N):-
	asserta(tarefa(IdEnc,SomaN,Mk,Tconclusao,Prioridade,Produto)),	
	
	assertTarefas(ListaTarefas,N),!.
	

% =======================================   FIM   ==========================================


% ================================ HEURISTICA EDD ==================================
% if tconc1 == tconc2 then verificar prioridades Ind = lista de tarefas R*S solu��o

edd(Ind,[R*S]):-
				ordenar_por_tempo_conclusao(Ind,R,S).

ordenar_por_tempo_conclusao(Ind,R,S):-
				associar_tempo_conclusao(Ind, ListaOptTConc),
				sort(ListaOptTConc, LOrdenada),
				somatorio_tconc(LOrdenada, 0,0,S),
				!,
				delete_tempo_conc(LOrdenada, LFinal),
				flatten(LFinal, R).


associar_tempo_conclusao([H],[[TConc,H]]):-
			    tarefa(_,H,_,TempoConclusao,_,_),
				TConc is TempoConclusao,
				!.
associar_tempo_conclusao([H|T],[[TConc,H]|LPairs]):-
				tarefa(_,H,_,TempoConclusao,_,_),
				TConc is TempoConclusao,
				associar_tempo_conclusao(T, LPairs),
				!.

somatorio_tconc([],_,SomaAtrasos,SomaAtrasos):-!.
somatorio_tconc([[H,Id]|ListaResto],SomaTProcess,SomaAtrasos, S2):-
				tarefa(_,Id,TProcessamento1,_,Prioridade,_),
				SomaTProcess1 is SomaTProcess + TProcessamento1,
				TAtraso is SomaTProcess1 - H,
				(TAtraso>0,
				SomaAt is TAtraso*Prioridade;
				SomaAt is 0),
				SomaAtrasos1 is SomaAtrasos + SomaAt,
				somatorio_tconc(ListaResto,SomaTProcess1, SomaAtrasos1, S2),!.
				

delete_tempo_conc([[_|OP]], OP).
delete_tempo_conc([[_|OP]|T], [OP|T1]):-
				delete_tempo_conc(T, T1), !.
				
% =======================================   FIM   ==========================================

% ============================= HEURISTICA CRITICAL RATIO ====================================
criticalRatio(Ind,[R*S]):-
	                        ordenar_por_menor_folga(Ind,R,S).

ordenar_por_menor_folga(Ind,R,S):-
				calcular_folga_criticalRatio(Ind, ListaOptFolga),
				ordenarListaPorOrdemCrescente(ListaOptFolga,LOrdenada),
				somatorio_folgas(LOrdenada, S),
				!,
				remover_folgas(LOrdenada, LFinal),
				flatten(LFinal, R).

calcular_folga_criticalRatio([H],[[Folga,H]]):-
			        tarefa(_,H,TempoProcessamento,TempoConclusao,_,_),
				F is  TempoConclusao - TempoProcessamento,
				Folga is F / TempoProcessamento,
				!.
calcular_folga_criticalRatio([H|T],[[Folga,H]|LPairs]):-
				tarefa(_,H,TempoProcessamento,TempoConclusao,_,_),
				F is  TempoConclusao - TempoProcessamento,
				Folga is F / TempoProcessamento,
				calcular_folga_criticalRatio(T, LPairs),
				!.
				
ordenarListaPorOrdemCrescente([[H,T]],[[H,T]]):-!.
ordenarListaPorOrdemCrescente(ListaOptFolga,[[H|T]|LOrdenada]):-
				sort(ListaOptFolga, [[H|T]|LOrdenada1]),
				escolherProximaOperacao([[H|T]|LOrdenada1],LOpSubstracao),
				ordenarListaPorOrdemCrescente(LOpSubstracao,LOrdenada),
				!.


somatorio_folgas([[H,_]], H):-
				H<0,
				!.
somatorio_folgas([[H,_]], 0):-
				H>=0,
				!.
somatorio_folgas([[H,Id]|ListaResto],S2):-
				H<0,
				Y is -H,
				somatorio_folgas(ListaResto, S),
				tarefa(_,Id,_,_,Prioridade,_),
				S1 is Y*Prioridade,
				S2 is S + S1,
				!.
somatorio_folgas([[_,_]|ListaResto],S):-
				somatorio_folgas(ListaResto, S),
				!.


remover_folgas([[_|OP]], OP).
remover_folgas([[_|OP]|T], [OP|T1]):-
				remover_folgas(T, T1), !.
				
escolherProximaOperacao([[H,T]],[[H,T]]):-!.
escolherProximaOperacao([[_,T1]|T2],LOpSubstracao):-
			        tarefa(_,T1,TempoProcessamento,_,_,_),
				TempoSubtrair is TempoProcessamento,
				removerValorTodosElemLista(T2,TempoSubtrair,LOpSubstracao),
				!.


removerValorTodosElemLista([[H|T]],N,[[NN|T]]):- NN is H-N,!.
removerValorTodosElemLista([[H|T]|T3], N, [[H1|T]|LNova]):-
				H1 is (H-N),
			        removerValorTodosElemLista(T3,N,LNova),
				!.

% =======================================   FIM   ==========================================
% ================================   ALGORITMO GEN�TICO   ===================================

geraIntegracao:-

	(retract(geracoes(_));true), asserta(geracoes(10)),
	(retract(populacao(_));true), asserta(populacao(2)),
	(retract(prob_cruzamento(_));true),	asserta(prob_cruzamento(75)),
	(retract(prob_mutacao(_));true), asserta(prob_mutacao(75)),
	
	gera_populacao(Pop),
	findall(Id, tarefa(_,Id,_,_,_,_), LTarefas),
	criticalRatio(LTarefas, [Ind_criticalRatio*_]),
	edd(LTarefas, [Ind_edd*_]),
	juntar_e_tirar_repetidos(Pop, Ind_criticalRatio, Ind_edd, PopFinal),
	%write('Pop='),write(PopFinal),nl,
	avalia_populacao(PopFinal,PopAv),
	%write('PopAv='),write(PopAv),nl,
	ordena_populacao(PopAv,PopOrd),
	geracoes(NG),
	gera_geracao(0,NG,PopOrd).
	
gera_geracao(G,G,Pop):-!,
	%write('Gera��o '), write(G), write(':'), nl, write(Pop), nl,
	assertaMelhorSolucao(Pop).

gera_geracao(N,G,Pop):-
	%write('Gera��o '), write(N), write(':'), nl, write(Pop), nl,
	random_permutation(Pop, LPop),
	cruzamento(LPop,NPop1),
	mutacao(NPop1,NPop),
	
	obtemDuasMelhoresSolucoes(LPop, NPop1, NPop, ListaParaTorneios,Melhores),
	torneios(ListaParaTorneios,ListaVencedora),
	append(Melhores,ListaVencedora,NovaPopulacao),
	ordena_populacao(NovaPopulacao,NovaPopulacaoOrdenada),
	
	N1 is N+1,
	
	gera_geracao(N1,G,NovaPopulacaoOrdenada).
	
assertaMelhorSolucao([Melhor|_]):-
	%write(Melhor),
	(retract(geracaoFinal(_));true),asserta(geracaoFinal(Melhor)).
	
juntar_e_tirar_repetidos(Pop, Cr, Edd, [CrFinal, EddFinal|Pop]):-
	verificar_duplicados(Pop, Cr, CrFinal),
	verificar_duplicados([CrFinal|Pop], Edd, EddFinal).

verificar_duplicados(Pop, Individuo, IndividuoFinal):-
	member(Individuo, Pop),
	tarefas(NrTarefas),
	random(1, NrTarefas, Posicao1),
	Posicao2 is Posicao1 - 1,
	alterar_Individuo(Individuo, Posicao2, Posicao1, NovoIndividuo), !,
	verificar_duplicados(Pop, NovoIndividuo, IndividuoFinal), !.

verificar_duplicados(_, Individuo, Individuo):-!.


alterar_Individuo(Individuo, Posicao2, Posicao1, NovoIndividuo) :-
   same_length(Individuo, NovoIndividuo),
   append(BeforeI,[AtI|PastI],Individuo),
   append(BeforeI,[AtJ|PastI],Bs),
   append(BeforeJ,[AtJ|PastJ],Bs),
   append(BeforeJ,[AtI|PastJ],NovoIndividuo),
   length(BeforeI,Posicao2),
   length(BeforeJ,Posicao1), !.
   
% ===============================================================================

gera_populacao(Pop):-
	populacao(TamPop),
	TamPopFinal is TamPop - 2,
	tarefas(NumT),
	findall(Tarefa,tarefa(_,Tarefa,_,_,_,_),ListaTarefas),
	gera_populacao(TamPopFinal,ListaTarefas,NumT,Pop).

gera_populacao(0,_,_,[]):-!.
gera_populacao(TamPop,ListaTarefas,NumT,[Ind|Resto]):-
	TamPop1 is TamPop-1,
	gera_populacao(TamPop1,ListaTarefas,NumT,Resto),
	gera_individuo(ListaTarefas,NumT,Ind),
	not(member(Ind,Resto)).
gera_populacao(TamPop,ListaTarefas,NumT,L):-
	gera_populacao(TamPop,ListaTarefas,NumT,L).

gera_individuo([G],1,[G]):-!.

gera_individuo(ListaTarefas,NumT,[G|Resto]):-
	NumTemp is NumT + 1, % To use with random
	random(1,NumTemp,N),
	retira(N,ListaTarefas,G,NovaLista),
	NumT1 is NumT-1,
	gera_individuo(NovaLista,NumT1,Resto).

retira(1,[G|Resto],G,Resto).
retira(N,[G1|Resto],G,[G1|Resto1]):-
	N1 is N-1,
	retira(N1,Resto,G,Resto1).

avalia_populacao([],[]).
avalia_populacao([Ind|Resto],[Ind*V|Resto1]):-
	avalia(Ind,V),
	avalia_populacao(Resto,Resto1).

avalia(Seq,V):-
	avalia(Seq,0,V).

avalia([],_,0).
avalia([T|Resto],Inst,V):-
	tarefa(_,T,Dur,Prazo,Pen,_),
	InstFim is Inst+Dur,
	avalia(Resto,InstFim,VResto),
	(
		(InstFim =< Prazo,!, VT is 0)
  ;
		(VT is (InstFim-Prazo)*Pen)
	),
	V is VT+VResto.

ordena_populacao(PopAv,PopAvOrd):-
	bsort(PopAv,PopAvOrd).

bsort([X],[X]):-!.
bsort([X|Xs],Ys):-
	bsort(Xs,Zs),
	btroca([X|Zs],Ys).


btroca([X],[X]):-!.

btroca([X*VX,Y*VY|L1],[Y*VY|L2]):-
	VX>VY,!,
	btroca([X*VX|L1],L2).

btroca([X|L1],[X|L2]):-btroca(L1,L2).


% ================================= TORNEIOS =======================================
torneios(ListaParaTorneios,ListaVencedora):-
	populacao(DimensaoPopulacao),
	DimensaoPopulacaoPretendida is DimensaoPopulacao-2,!,
	torneios2(ListaParaTorneios,ListaVencedora,0,DimensaoPopulacaoPretendida).
	
torneios2(_,[],N,N):-!.	

torneios2(ListaParaTorneios,[Vencedor|ListaVencedora],N,DimensaoPopulacaoPretendida):-
	
	N<DimensaoPopulacaoPretendida,
	
	obtemAleatorio(ListaParaTorneios,Elemento1),
	obtemAleatorio(ListaParaTorneios,Elemento2),
	
	
	Elemento1 \== Elemento2,
	N1 is N+1,
	realizarTorneio(Elemento1,Elemento2,Vencedor),
	delete(ListaParaTorneios,Vencedor,ListaParaTorneiosSemVencedor),
	!,torneios2(ListaParaTorneiosSemVencedor,ListaVencedora,N1,DimensaoPopulacaoPretendida).
	
torneios2(ListaParaTorneios,ListaVencedora,N,DimensaoPopulacaoPretendida):-
	torneios2(ListaParaTorneios,ListaVencedora,N,DimensaoPopulacaoPretendida),!.

realizarTorneio(Tarefas*Avaliacao1,_*Avaliacao2,Tarefas*Avaliacao1):-

	Random1 is Avaliacao1/ (Avaliacao1+Avaliacao2),
	Random2 is Avaliacao2/ (Avaliacao1+Avaliacao2),
	
	random(0, Random1, Av1),
	random(0, Random2, Av2),
	Av1<Av2,true.

realizarTorneio(_,Tarefas2*Avaliacao2,Tarefas2*Avaliacao2).

	
obtemAleatorio([], []).
obtemAleatorio(List, Elt) :-
        length(List, Length),
        random(0, Length, Index),
        nth0(Index, List, Elt).
		
% =====================================  FIM  ================================

% ========================== OBTER MELHORES SOLU��ES ==========================
obtemDuasMelhoresSolucoes(Atual, Cruzados, Mutados, ListaParaTorneios,Melhores):-
	avalia_populacao(Cruzados,CruzadosAvaliados),
	avalia_populacao(Mutados,MutadosAvaliados),
	
	append(Atual,CruzadosAvaliados,Lista1),
	append(Lista1,MutadosAvaliados,Todos),
	
	ordena_populacao(Todos,TodosOrdenados),
	tirarRepetidos(TodosOrdenados,TodosOrdenadosSemRepetidos),
	obtemDuasMelhoresSolucoes2(TodosOrdenadosSemRepetidos,ListaParaTorneios,Melhores).

obtemDuasMelhoresSolucoes2(Lista,ListaParaTorneios,Melhores):-
	Lista = [Melhor|Resto],
	Resto = [SegundoMelhor|ListaParaTorneios],
	append([Melhor],[SegundoMelhor],Melhores).
	
remove_duplicates([],[]).

remove_duplicates([H | T], List) :-    
     member(H, T),
     remove_duplicates( T, List).

remove_duplicates([H | T], [H|T1]) :- 
      \+member(H, T),
      remove_duplicates( T, T1).
	
tirarRepetidos([],[]).
tirarRepetidos([H],[H]).
tirarRepetidos([H ,H| T], List) :-remove_duplicates( [H|T], List).
tirarRepetidos([H,Y | T], [H|T1]):- Y \= H,remove_duplicates( [Y|T], T1).

% =======================================   FIM   ==========================================

% ================================    UTILIT�RIOS   =========================================
	
gerar_pontos_cruzamento(P1,P2):-
	gerar_pontos_cruzamento1(P1,P2).

gerar_pontos_cruzamento1(P1,P2):-
	tarefas(N),
	NTemp is N+1,
	random(1,NTemp,P11),
	random(1,NTemp,P21),
	P11\==P21,!,
	((P11<P21,!,P1=P11,P2=P21);(P1=P21,P2=P11)).
gerar_pontos_cruzamento1(P1,P2):-
	gerar_pontos_cruzamento1(P1,P2).


cruzamento([],[]).
cruzamento([Ind*_],[Ind]).
cruzamento([Ind1*_,Ind2*_|Resto],[NInd1,NInd2|Resto1]):-
	gerar_pontos_cruzamento(P1,P2),
	prob_cruzamento(Pcruz),random(0.0,1.0,Pc),
	((Pc =< Pcruz,!,
        cruzar(Ind1,Ind2,P1,P2,NInd1),
	  cruzar(Ind2,Ind1,P1,P2,NInd2))
	;
	(NInd1=Ind1,NInd2=Ind2)),
	cruzamento(Resto,Resto1).

preencheh([],[]).

preencheh([_|R1],[h|R2]):-
	preencheh(R1,R2).


sublista(L1,I1,I2,L):-
	I1 < I2,!,
	sublista1(L1,I1,I2,L).

sublista(L1,I1,I2,L):-
	sublista1(L1,I2,I1,L).

sublista1([X|R1],1,1,[X|H]):-!,
	preencheh(R1,H).

sublista1([X|R1],1,N2,[X|R2]):-!,
	N3 is N2 - 1,
	sublista1(R1,1,N3,R2).

sublista1([_|R1],N1,N2,[h|R2]):-
	N3 is N1 - 1,
	N4 is N2 - 1,
	sublista1(R1,N3,N4,R2).

rotate_right(L,K,L1):-
	tarefas(N),
	T is N - K,
	rr(T,L,L1).

rr(0,L,L):-!.

rr(N,[X|R],R2):-
	N1 is N - 1,
	append(R,[X],R1),
	rr(N1,R1,R2).


elimina([],_,[]):-!.

elimina([X|R1],L,[X|R2]):-
	not(member(X,L)),!,
	elimina(R1,L,R2).

elimina([_|R1],L,R2):-
	elimina(R1,L,R2).

insere([],L,_,L):-!.
insere([X|R],L,N,L2):-
	tarefas(T),
	((N>T,!,N1 is N mod T);N1 = N),
	insere1(X,N1,L,L1),
	N2 is N + 1,
	insere(R,L1,N2,L2).


insere1(X,1,L,[X|L]):-!.
insere1(X,N,[Y|L],[Y|L1]):-
	N1 is N-1,
	insere1(X,N1,L,L1).

cruzar(Ind1,Ind2,P1,P2,NInd11):-
	sublista(Ind1,P1,P2,Sub1),
	tarefas(NumT),
	R is NumT-P2,
	rotate_right(Ind2,R,Ind21),
	elimina(Ind21,Sub1,Sub2),
	P3 is P2 + 1,
	insere(Sub2,Sub1,P3,NInd1),
	eliminah(NInd1,NInd11).


eliminah([],[]).

eliminah([h|R1],R2):-!,
	eliminah(R1,R2).

eliminah([X|R1],[X|R2]):-
	eliminah(R1,R2).

mutacao([],[]).
mutacao([Ind|Rest],[NInd|Rest1]):-
	prob_mutacao(Pmut),
	random(0.0,1.0,Pm),
	((Pm < Pmut,!,mutacao1(Ind,NInd));NInd = Ind),
	mutacao(Rest,Rest1).

mutacao1(Ind,NInd):-
	gerar_pontos_cruzamento(P1,P2),
	mutacao22(Ind,P1,P2,NInd).

mutacao22([G1|Ind],1,P2,[G2|NInd]):-
	!, P21 is P2-1,
	mutacao23(G1,P21,Ind,G2,NInd).
mutacao22([G|Ind],P1,P2,[G|NInd]):-
	P11 is P1-1, P21 is P2-1,
	mutacao22(Ind,P11,P21,NInd).

mutacao23(G1,1,[G2|Ind],G2,[G1|Ind]):-!.
mutacao23(G1,P,[G|Ind],G2,[G|NInd]):-
	P1 is P-1,
	mutacao23(G1,P1,Ind,G2,NInd).

	
% =======================================   FIM   ==========================================

:-	cria_op_enc,!,
	geraTarefas,!.


	
