% Bibliotecas
:- use_module(library(http/thread_httpd)).
:- use_module(library(http/http_dispatch)).
:- use_module(library(http/http_parameters)).
:- use_module(library(http/http_open)).
:- use_module(library(http/json)).
:- use_module(library(http/json_convert)).
:- use_module(library(xpath)).
:- use_module(library(http/http_server)).
:- use_module(library(http/http_header)).

% ==================================== UTILITÁRIOS  =======================================

% permuta/2 gera permutações de listas
permuta([ ],[ ]).
permuta(L,[X|L1]):-apaga1(X,L,Li),permuta(Li,L1).

apaga1(X,[X|L],L).
apaga1(X,[Y|L],[Y|L1]):-apaga1(X,L,L1).
	
atualiza(LP,T):-
	melhor_sol_to(_,Tm),
	T<Tm,retract(melhor_sol_to(_,_)),asserta(melhor_sol_to(LP,T)),!.

% ===================================== FIM  ===============================================


% ============================ GERAÇÃO DE TODAS - OCUPAÇÃO  ================================

% permuta_tempo/3 faz uma permutação das operações atribuídas a uma maquina e calcula tempo de ocupação incluindo trocas de ferramentas
permuta_tempo(M,LP,Tempo):- operacoes_atrib_maq(M,L),permuta(L,LP),soma_tempos(semfer,M,LP,Tempo).


soma_tempos(_,_,[],0).
soma_tempos(Fer,M,[Op|LOp],Tempo):- classif_operacoes(Op,Opt),
	operacao_maquina(Opt,M,Fer1,Tsetup,Texec),
	soma_tempos(Fer1,M,LOp,Tempo1),
	((Fer1==Fer,!,Tempo is Texec+Tempo1);
			Tempo is Tsetup+Texec+Tempo1).
				
:- dynamic melhor_sol_to/2.
melhor_escalonamento1(M,Lm,Tm):-
	get_time(Ti),
	(melhor_escalonamento11(M);true),retract(melhor_sol_to(Lm,Tm)),
	get_time(Tf),Tcomp is Tf-Ti,
	write('GERADO EM '),write(Tcomp),
	write(' SEGUNDOS'),nl.
	
melhor_escalonamento11(M):- 
	asserta(melhor_sol_to(_,10000)),!,
	permuta_tempo(M,LP,Tempo),
	atualiza(LP,Tempo),
	fail.

% ====================================== FIM ===============================================

% ============================ GERAÇÃO DE TODAS - ATRASOS  ================================

% permuta_tempo_atrasos/3 faz uma permutação das operações atribuídas a uma maquina e calcula tempo de ocupação incluindo trocas de ferramentas
permuta_tempo_atrasos(M,LP,Tempo):- operacoes_atrib_maq(M,L),permuta(L,LP),soma_tempos_atrasos(semfer,M,LP,Tempo,0).


soma_tempos_atrasos(_,_,[],0,_).
soma_tempos_atrasos(Fer,M,[Op|LOp],TempoAtraso,TExecucaoMomento):-
	op_prod_client(Op,M,Fer1,_,_,_,TConc,TSetup,Texec),
	
	(Fer1==Fer,!,TExecucaoMomento2 is TExecucaoMomento + Texec;
			TExecucaoMomento2 is TExecucaoMomento + Texec+ TSetup),
	
	soma_tempos_atrasos(Fer1,M,LOp,Tempo1,TExecucaoMomento2),
	
	TAtrasoNovo is TExecucaoMomento2-TConc,
	(TAtrasoNovo>0,!,TempoAtraso is Tempo1 + TAtrasoNovo;TempoAtraso is Tempo1).
				
:- dynamic melhor_sol_to/2.
melhor_escalonamento1_atrasos(M,Lm,Tm):-
	get_time(Ti),
	(melhor_escalonamento11_atrasos(M);true),retract(melhor_sol_to(Lm,Tm)),
	get_time(Tf),Tcomp is Tf-Ti,
	write('GERADO EM '),write(Tcomp),
	write(' SEGUNDOS'),nl.
	
melhor_escalonamento11_atrasos(M):- 
	asserta(melhor_sol_to(_,10000)),!,
	permuta_tempo_atrasos(M,LP,Tempo),
	atualiza(LP,Tempo),
	fail.

% ====================================== FIM ===============================================
		

% ============================ A* - TEMPOS DE OCUPAÇÃO ==========================================
	
aStarTemposDeOcupacao([X|LFaltam], Sequencia, Custo):-
	classif_operacoes(X,TipoOperacao),
	operacao_maquina(TipoOperacao,_,_,TSetup,Duracao),
	TProc is Duracao + TSetup,
    aStarTemposDeOcupacao2([(_,TProc,[X],LFaltam)],Sequencia,Custo).

aStarTemposDeOcupacao2([(_,Custo,Sequencia2,[])|_],Sequencia,Custo):-
	reverse(Sequencia2,Sequencia),!.

aStarTemposDeOcupacao2([(_,Ca,LTratadas, LFaltam)|Outros],Sequencia,Custo):-
	LTratadas=[Act|_],
	findall((CEX,CaX,[X|LTratadas],LFaltamNovos),
		(
		classif_operacoes(Act,TipoOperacao), 
		classif_operacoes(X,TipoOperacao2), 
		
		\+ member(X,LTratadas), 
		member(X,LFaltam),
		delete(LFaltam,X,LFaltamNovos),
		
		operacao_maquina(TipoOperacao,_,Ferramenta1,_,_),
		operacao_maquina(TipoOperacao2,_,Ferramenta2,TSetup,Duracao),
		
		(Ferramenta1==Ferramenta2 -> CaX is Ca + Duracao ; CaX is Ca + TSetup + Duracao),
		estimativa(LFaltamNovos,EstX),
		CEX is CaX +EstX),Novos),
	append(Outros,Novos,Todos),
	sort(Todos,TodosOrd),
	aStarTemposDeOcupacao2(TodosOrd,Sequencia,Custo).
	
%Estimativa para tempos de setup
estimativa(LOp,Estimativa):-
	findall(p(FOp,Tsetup),
		(member(Op,LOp),op_prod_client(Op,_,FOp,_,_,_,_,Tsetup,_))
		,LFTsetup),
		elimina_repetidos(LFTsetup,L),
		soma_setups(L,Estimativa).
		
elimina_repetidos([],[]).
elimina_repetidos([X|L],L1):-member(X,L),!,elimina_repetidos(L,L1).
elimina_repetidos([X|L],[X|L1]):-elimina_repetidos(L,L1).

soma_setups([],0).
soma_setups([p(_,Tsetup)|L],Ttotal):- soma_setups(L,T1), Ttotal is Tsetup+T1.
	
% ====================================== FIM ===============================================

% ============================ A* - TEMPOS DE ATRASO ============================================
	
aStarTemposDeAtraso(ListaOp,Sequencia,Atraso):-
	aStarTemposDeAtraso2([(_,0,0,[],ListaOp,semFer)],Sequencia, Atraso).

	
aStarTemposDeAtraso2([(_,_,Atraso,Seq,[],_)], Sequencia, Atraso):-
	reverse(Seq,Sequencia),!.
	
aStarTemposDeAtraso2([(_,Ca,Catraso, LTratadas,LFaltam,Ferramenta1)],Sequencia,Atraso):-

	findall((CatrasoE,CaX,CatrasoNovo,[X|LTratadas],LFaltamNovos,Ferramenta2),
		(
		classif_operacoes(X,TipoOperacao), 
		operacao_maquina(TipoOperacao,_,Ferramenta2,TSetup,Duracao),
		
		\+ member(X,LTratadas),
		member(X,LFaltam),
		delete(LFaltam,X,LFaltamNovos),
		
		(Ferramenta1==Ferramenta2 -> CaX is Ca + Duracao ; CaX is Ca + TSetup + Duracao),
		
		estimativaAtraso(X,CaX,EstX),
		CatrasoE is Catraso + EstX,
		
		(EstX>0 -> CatrasoNovo is Catraso + EstX; CatrasoNovo is Catraso)),Novos),
		
	sort(Novos,TodosOrd),reverse(TodosOrd, TodosAtraso),
	TodosAtraso = [Seguinte|_],
	aStarTemposDeAtraso2([Seguinte],Sequencia,Atraso).
	 
	
estimativaAtraso(Op,Custo,Estimativa):-
		op_prod_client(Op,_,_,_,_,_,TConc,_,_),
		Estimativa is Custo-TConc.
			
% ====================================== FIM ===============================================


:-dynamic geracoes/1.
:-dynamic populacao/1.
:-dynamic prob_cruzamento/1.
:-dynamic prob_mutacao/1.


% ========================================== CRIAÇÃO DINÂMICA ======================================

% cria_op_enc - fizeram-se correcoes face a versao anterior

:- dynamic operacoes_atrib_maq/2.
:- dynamic classif_operacoes/2.
:- dynamic op_prod_client/9.
:- dynamic operacoes/1.


cria_op_enc:-retractall(operacoes(_)),
retractall(operacoes_atrib_maq(_,_)),retractall(classif_operacoes(_,_)),
retractall(op_prod_client(_,_,_,_,_,_,_,_,_)),
		findall(t(Cliente,Prod,Qt,TConc),
		(encomenda(_,Cliente,LE),member(e(Prod,Qt,TConc),LE)),
		LT),cria_ops(LT,0),
findall(Op,classif_operacoes(Op,_),LOp),asserta(operacoes(LOp)),
maquinas(LM),
 findall(_,
		(member(M,LM),
		 findall(Opx,op_prod_client(Opx,M,_,_,_,_,_,_,_),LOpx),
		 assertz(operacoes_atrib_maq(M,LOpx))),_).

cria_ops([],_).
cria_ops([t(Cliente,Prod,Qt,TConc)|LT],N):-
			operacoes_produto(Prod,LOpt),
	cria_ops_prod_cliente(LOpt,Cliente,Prod,Qt,TConc,N,N1),
			cria_ops(LT,N1).


cria_ops_prod_cliente([],_,_,_,_,Nf,Nf).
cria_ops_prod_cliente([Opt|LOpt],Client,Prod,Qt,TConc,N,Nf):-
		cria_ops_prod_cliente2(Opt,Prod,Client,Qt,TConc,N,Ni),
	cria_ops_prod_cliente(LOpt,Client,Prod,Qt,TConc,Ni,Nf).


cria_ops_prod_cliente2(Opt,Prod,Client,Qt,TConc,N,Ni):-
			Ni is N+1,
			atomic_concat(op,Ni,Op),
			assertz(classif_operacoes(Op,Opt)),
			operacao_maquina(Opt,M,F,Tsetup,Texec),
	assertz(op_prod_client(Op,M,F,Prod,Client,Qt,TConc,Tsetup,Texec)).



% ========================================== FIM ======================================

% ==============================    CRIACAO TAREFA    =======================================
geraTarefas:-
	
	retractall(tarefa(_,_,_,_,_,_)),
	retractall(tarefas(_)),
	
	findall((IdEnc,C,Encomenda),
	encomenda(IdEnc,C,Encomenda)
	,ListaEncomendas),
	
	%write(ListaEncomendas),nl,

	geraTarefasFabrica(ListaEncomendas,[],0,1,TarefasFabrica,N),
	%write(TarefasFabrica),nl,
	assertTarefas(TarefasFabrica,N).

geraTarefasFabrica([],Final,_,N,Final,N):-!.

geraTarefasFabrica([Encomenda | ListaEncomendas] , Temp ,NTemp, SomaN ,TarefasFabrica , N):-
	
	geraTarefas(Encomenda, Tarefas, SomaN,NumeroTarefasEncomenda),
	append(Tarefas, Temp , Join),
	geraTarefasFabrica(ListaEncomendas,Join,NTemp,NumeroTarefasEncomenda,TarefasFabrica,N) , !.
	
geraTarefas((_,_,[]),_,_,_).
	
geraTarefas((IdEnc,ClA,[e(Pa,Unidades,Tconclusao) | ProdutosRestantes]),Tarefas,SomaN,N):-
	 
	prioridade_cliente(ClA,Prioridade),
	
	geraTarefas2(IdEnc,Prioridade,[e(Pa,Unidades,Tconclusao) | ProdutosRestantes],[],Tarefas,SomaN,N).

geraTarefas2(_,_,[],Tarefas,Tarefas,N,N):-!.

geraTarefas2(IdEnc,Prioridade, [e(Pa,Unidades,Tconclusao) | ProdutosRestantes], Tarefas, TarefasF ,SomaN, N):-
	operacoes_produto(Pa,ListaOpt),
	makespan(ListaOpt,Unidades,Mk),
	SomaN1 is SomaN + 1,
	geraTarefas2(IdEnc,Prioridade,ProdutosRestantes,[(IdEnc,SomaN,Mk,Tconclusao,Prioridade,Pa)|Tarefas],TarefasF,SomaN1,N), !.

makespan(ListaOpt,Unidades,Res):-
	makespan2(ListaOpt,[],ListaOrd),
	
	reverse(ListaOrd,ListaReverse),
	makespanTS(ListaReverse,0,0,MkTs),
	
	sort(1,@=<,ListaOrd,ListaOrd2),
	makespanTE(Unidades,ListaOrd2,0, MkTe),
	Res is MkTe - MkTs.

makespan2([],ListaT,ListaT):-!. 

makespan2([Opt|ListaOpt],ListaT,ListaFinal):-
	operacao_maquina(Opt,_,_,Tsetup,Texec),
	%write(Tsetup),write(Texec),nl,
	append([(Texec,Tsetup)],ListaT,ListaA),
	makespan2(ListaOpt, ListaA,ListaFinal),!.	
		
makespanTE(_,[],Som,Som):-!.
	
makespanTE(N,[(Te,_)],Som,Res):-
	Som1 is Som + (N * Te),
	makespanTE(N,[],Som1,Res),!.

makespanTE(N,[(Te,_)|Tempos],Som,Res):-
	Som1 is Som + Te,
	makespanTE(N,Tempos,Som1,Res),!.

makespanTS([],_,Min,Min):-!.

makespanTS([(Te,Ts)|Tempos],Som,Min,Res):-
	Som2 is (Som - Ts),
	(Som2 < Min, Min1 is Som2; Min1 is Min),
	Som1 is Som + Te,
	makespanTS(Tempos,Som1, Min1,Res),!.

assertTarefas([],N):-
	N1 is N-1,
	asserta(tarefas(N1)),!.
	
assertTarefas([(IdEnc,SomaN,Mk,Tconclusao,Prioridade,Produto)|ListaTarefas], N):-
	%write((IdEnc,SomaN,Mk,Tconclusao,Prioridade,Produto)),nl,
	asserta(tarefa(IdEnc,SomaN,Mk,Tconclusao,Prioridade,Produto)),	
	
	assertTarefas(ListaTarefas,N),!.
	

% =======================================   FIM   ==========================================

% ====================== HEURISTICA BALANCEMANTO DE LINHAS =================================== 

gera_pp(PP):-
	retractall(tarefa_linha(_,_)),
	findall((IdTarefa,Tconc,Produto),tarefa(_,IdTarefa,_,Tconc,_,Produto),ListaTarefas),
	findall((Linha,ListaMaquinas),tipos_maq_linha(Linha,ListaMaquinas),ListaLinhas),
	%write('ListaTarefas'),write(ListaTarefas),nl,nl,
	%write('ListaLinhas'),write(ListaLinhas),nl,
	gera_pp(ListaTarefas,ListaLinhas,PP).
	
gera_pp(ListaTarefas,ListaLinhas,PP):-
	%write('entrou gera pp'),nl,
	cria_linhas(ListaLinhas,[],AuxPP),
	
	sort(2, @>=,ListaTarefas, ListaTarefasOrdenada), % ordena pelo Tconc de forma descendente
	
	gera_pp2(ListaTarefasOrdenada, AuxPP,AuxPP, PP).
	
gera_pp2([],_,PP,PP).
	
gera_pp2([(IdTarefa,Tconc,Produto)|ListaTarefas], TestePP, AuxPP, PP):-
	
	%write('entrou gera pp2'),nl,
	sort(2, @=<, TestePP, ListaLinhasOrdenada), % ordena pelo Tconc de forma ascendente 
	%write('Linha ordenada'),write(ListaLinhasOrdenada),nl ,
	escolhe_linha((IdTarefa,Tconc,Produto), ListaLinhasOrdenada, Linha),
	
	%write('Linha= '),write(Linha),nl,
	
	update_linha(AuxPP,Linha,(IdTarefa, Tconc,Produto),[],AuxPP2),
	%write('linhas novas'), nl ,write(AuxPP2),nl,
	
	gera_pp2(ListaTarefas, AuxPP2, AuxPP2,PP),!.
		

% =========== METODOS AUXILIARES ========		
cria_linhas([], AuxPP,AuxPP).

cria_linhas([(Linha,Maquinas)|ListaLinhas],AuxPP, Final):-

	%write('entrou criar linhas'),nl,
	get_operacoes(Maquinas,[],ListaOperacoes),
	
	cria_linhas(ListaLinhas, [(Linha,0,[],ListaOperacoes)|AuxPP], Final).
	
get_operacoes([],ListaOperacoes,ListaOperacoes).
	
get_operacoes([Maquina| Maquinas] ,Aux, ListaOperacoes):- 
	%write('entrou get oper'),nl,
	findall(Opt, operacao_maquina(Opt,Maquina,_,_,_),Ops),
	append(Ops,Aux,Join),
	sort(Join,Join2),
	get_operacoes(Maquinas, Join2 ,ListaOperacoes),!.
	
escolhe_linha((IdTarefa, Mk,P), [(Linha,_,_,OperacoesLinha)|AuxPP], LinhaEscolhida):-
	
	%write('entrou escolhe_linha'),nl,
	operacoes_produto(P, OpsProduto),

	%nl,nl,write('Operacoes Produto= '),write(OpsProduto),nl,
	
	%write('Operacoes  Linha= '),write(OperacoesLinha),nl,
	
	escolhe_linha2(OpsProduto, OperacoesLinha), 
	(
	%write('Linha EL= '),write(Linha),nl,
	%escolhe_linha(_, [], Linha),
	asserta(tarefa_linha(IdTarefa,Linha)),
	LinhaEscolhida = Linha),!
	; 	
	escolhe_linha((IdTarefa, Mk,P), AuxPP, LinhaEscolhida),!. 

escolhe_linha2([],_).

escolhe_linha2( [Op|OpsProduto], OperacoesLinha ):- 
	%write('Op= '), write(Op),nl,
	%write('OpsLinha= '), write(OperacoesLinha),nl,
	member(Op,OperacoesLinha), (escolhe_linha2(OpsProduto, OperacoesLinha),!); (false,!).
	
	
update_linha([],_,_,AuxPP2,AuxPP2).

update_linha([(Linha, TC, Tarefas, Ops)|AuxPP],LinhaAtualizar,(IdTarefa,Tconc,P),AuxPP2,NewPP):-
	
	%nl,nl,
	%write(Linha),nl,
	%write(LinhaAtualizar),nl,
	(Linha == LinhaAtualizar -> (TC2 is TC + Tconc, update_linha(AuxPP,LinhaAtualizar,(IdTarefa,Tconc,P),[(Linha,TC2,[IdTarefa|Tarefas],Ops)|AuxPP2],NewPP),!)
	;
	update_linha(AuxPP, LinhaAtualizar,(IdTarefa, Tconc,P),[(Linha,TC,Tarefas,Ops)|AuxPP2], NewPP),!).
	
% =================================  FIM  ==========================================


% ================================ HEURISTICA EDD ==================================
% if tconc1 == tconc2 then verificar prioridades Ind = lista de tarefas R*S solução

edd(Ind,[R*S]):-
				ordenar_por_tempo_conclusao(Ind,R,S).

ordenar_por_tempo_conclusao(Ind,R,S):-
				associar_tempo_conclusao(Ind, ListaOptTConc),
				sort(ListaOptTConc, LOrdenada),
				somatorio_tconc(LOrdenada, 0,0,S),
				!,
				delete_tempo_conc(LOrdenada, LFinal),
				flatten(LFinal, R).


associar_tempo_conclusao([H],[[TConc,H]]):-
			    tarefa(_,H,_,TempoConclusao,_,_),
				TConc is TempoConclusao,
				!.
associar_tempo_conclusao([H|T],[[TConc,H]|LPairs]):-
				tarefa(_,H,_,TempoConclusao,_,_),
				TConc is TempoConclusao,
				associar_tempo_conclusao(T, LPairs),
				!.

somatorio_tconc([],_,SomaAtrasos,SomaAtrasos):-!.
somatorio_tconc([[H,Id]|ListaResto],SomaTProcess,SomaAtrasos, S2):-
				tarefa(_,Id,TProcessamento1,_,Prioridade,_),
				SomaTProcess1 is SomaTProcess + TProcessamento1,
				TAtraso is SomaTProcess1 - H,
				(TAtraso>0,
				SomaAt is TAtraso*Prioridade;
				SomaAt is 0),
				SomaAtrasos1 is SomaAtrasos + SomaAt,
				somatorio_tconc(ListaResto,SomaTProcess1, SomaAtrasos1, S2),!.
				

delete_tempo_conc([[_|OP]], OP).
delete_tempo_conc([[_|OP]|T], [OP|T1]):-
				delete_tempo_conc(T, T1), !.
				
% =======================================   FIM   ==========================================

% ============================= HEURISTICA CRITICAL RATIO ====================================
criticalRatio(Ind,[R*S]):-
	                        ordenar_por_menor_folga(Ind,R,S).

ordenar_por_menor_folga(Ind,R,S):-
				calcular_folga_criticalRatio(Ind, ListaOptFolga),
				ordenarListaPorOrdemCrescente(ListaOptFolga,LOrdenada),
				somatorio_folgas(LOrdenada, S),
				!,
				remover_folgas(LOrdenada, LFinal),
				flatten(LFinal, R).

calcular_folga_criticalRatio([H],[[Folga,H]]):-
			        tarefa(_,H,TempoProcessamento,TempoConclusao,_,_),
				F is  TempoConclusao - TempoProcessamento,
				Folga is F / TempoProcessamento,
				!.
calcular_folga_criticalRatio([H|T],[[Folga,H]|LPairs]):-
				tarefa(_,H,TempoProcessamento,TempoConclusao,_,_),
				F is  TempoConclusao - TempoProcessamento,
				Folga is F / TempoProcessamento,
				calcular_folga_criticalRatio(T, LPairs),
				!.
				
ordenarListaPorOrdemCrescente([[H,T]],[[H,T]]):-!.
ordenarListaPorOrdemCrescente(ListaOptFolga,[[H|T]|LOrdenada]):-
				sort(ListaOptFolga, [[H|T]|LOrdenada1]),
				escolherProximaOperacao([[H|T]|LOrdenada1],LOpSubstracao),
				ordenarListaPorOrdemCrescente(LOpSubstracao,LOrdenada),
				!.


somatorio_folgas([[H,_]], H):-
				H<0,
				!.
somatorio_folgas([[H,_]], 0):-
				H>=0,
				!.
somatorio_folgas([[H,Id]|ListaResto],S2):-
				H<0,
				Y is -H,
				somatorio_folgas(ListaResto, S),
				tarefa(_,Id,_,_,Prioridade,_),
				S1 is Y*Prioridade,
				S2 is S + S1,
				!.
somatorio_folgas([[_,_]|ListaResto],S):-
				somatorio_folgas(ListaResto, S),
				!.


remover_folgas([[_|OP]], OP).
remover_folgas([[_|OP]|T], [OP|T1]):-
				remover_folgas(T, T1), !.
				
escolherProximaOperacao([[H,T]],[[H,T]]):-!.
escolherProximaOperacao([[_,T1]|T2],LOpSubstracao):-
			        tarefa(_,T1,TempoProcessamento,_,_,_),
				TempoSubtrair is TempoProcessamento,
				removerValorTodosElemLista(T2,TempoSubtrair,LOpSubstracao),
				!.


removerValorTodosElemLista([[H|T]],N,[[NN|T]]):- NN is H-N,!.
removerValorTodosElemLista([[H|T]|T3], N, [[H1|T]|LNova]):-
				H1 is (H-N),
			        removerValorTodosElemLista(T3,N,LNova),
				!.

% =======================================   FIM   ==========================================
% ================================   ALGORITMO GENÉTICO   ===================================

inicializa:-write('Numero de novas Geracoes: '),read(NG),
        (retract(geracoes(_));true), asserta(geracoes(NG)),
	write('Dimensao da Populacao: '),read(DP),
	(retract(populacao(_));true), asserta(populacao(DP)),
	write('Probabilidade de Cruzamento (%):'), read(P1),
	PC is P1/100,
	(retract(prob_cruzamento(_));true),	asserta(prob_cruzamento(PC)),
	write('Probabilidade de Mutacao (%):'), read(P2),
	PM is P2/100,
	(retract(prob_mutacao(_));true), asserta(prob_mutacao(PM)).
	
geraIntegracao:-

	(retract(geracoes(_));true), asserta(geracoes(10)),
	(retract(populacao(_));true), asserta(populacao(5)),
	(retract(prob_cruzamento(_));true),	asserta(prob_cruzamento(75)),
	(retract(prob_mutacao(_));true), asserta(prob_mutacao(75)),
	
	gera_populacao(Pop),
        findall(Id, tarefa(_,Id,_,_,_,_), LTarefas),
	criticalRatio(LTarefas, [Ind_criticalRatio*_]),
	edd(LTarefas, [Ind_edd*_]),
	juntar_e_tirar_repetidos(Pop, Ind_criticalRatio, Ind_edd, PopFinal),
	%write('Pop='),write(PopFinal),nl,
	avalia_populacao(PopFinal,PopAv),
	%write('PopAv='),write(PopAv),nl,
	ordena_populacao(PopAv,PopOrd),
	geracoes(NG),
	gera_geracao(0,NG,PopOrd).

gera:-
	inicializa,
	gera_populacao(Pop),
        findall(Id, tarefa(_,Id,_,_,_,_), LTarefas),
	criticalRatio(LTarefas, [Ind_criticalRatio*_]),
	edd(LTarefas, [Ind_edd*_]),
	juntar_e_tirar_repetidos(Pop, Ind_criticalRatio, Ind_edd, PopFinal),
	write('Pop='),write(PopFinal),nl,
	avalia_populacao(PopFinal,PopAv),
	write('PopAv='),write(PopAv),nl,
	ordena_populacao(PopAv,PopOrd),
	geracoes(NG),
	gera_geracao(0,NG,PopOrd).
	
gera_geracao(G,G,Pop):-!,
	%write('Geração '), write(G), write(':'), nl, write(Pop), nl,
	assertaMelhorSolucao(Pop).

gera_geracao(N,G,Pop):-
	%write('Geração '), write(N), write(':'), nl, write(Pop), nl,
	random_permutation(Pop, LPop),
	cruzamento(LPop,NPop1),
	mutacao(NPop1,NPop),
	
	obtemDuasMelhoresSolucoes(LPop, NPop1, NPop, ListaParaTorneios,Melhores),
	torneios(ListaParaTorneios,ListaVencedora),
	append(Melhores,ListaVencedora,NovaPopulacao),
	ordena_populacao(NovaPopulacao,NovaPopulacaoOrdenada),
	
	N1 is N+1,
	
	gera_geracao(N1,G,NovaPopulacaoOrdenada).
	
assertaMelhorSolucao([Melhor|_]):-
	%write(Melhor),
	(retract(geracaoFinal(_));true),asserta(geracaoFinal(Melhor)).
	
% ===================================== GERA  -- TEMPOS ==============================
	
geraTempos:-
	inicializaTempos,
	gera_populacao(Pop),
        findall(Id, tarefa(_,Id,_,_,_,_), LTarefas),
	criticalRatio(LTarefas, [Ind_criticalRatio*_]),
	edd(LTarefas, [Ind_edd*_]),
	juntar_e_tirar_repetidos(Pop, Ind_criticalRatio, Ind_edd, PopFinal),
	write('Pop='),write(PopFinal),nl,
	avalia_populacao(PopFinal,PopAv),
	write('PopAv='),write(PopAv),nl,
	ordena_populacao(PopAv,PopOrd),
	get_time(TInicial),
	gera_geracaoTempos(0,TInicial,PopOrd).
	

gera_geracaoTempos(N,T,Pop):-
	write('Geração '), write(N), write(':'), nl, write(Pop), nl,
		
	sleep(0.1),
	get_time(TI),
	TInstante is TI - T,
	tempo(TS),
	TInstante<TS,
	
	random_permutation(Pop, LPop),
	cruzamento(LPop,NPop1),
	mutacao(NPop1,NPop),
	
	obtemDuasMelhoresSolucoes(LPop, NPop1, NPop, ListaParaTorneios,Melhores),
	torneios(ListaParaTorneios,ListaVencedora),
	append(Melhores,ListaVencedora,NovaPopulacao),
	ordena_populacao(NovaPopulacao,NovaPopulacaoOrdenada),
	
	N1 is N+1,
	gera_geracaoTempos(N1,T,NovaPopulacaoOrdenada).
	

gera_geracaoTempos(_,_,_):-!.

inicializaTempos:-write('Tempo em segundos: '),read(TS),
        (retract(tempo(_));true), asserta(tempo(TS)),
	write('Dimensao da Populacao: '),read(DP),
	(retract(populacao(_));true), asserta(populacao(DP)),
	write('Probabilidade de Cruzamento (%):'), read(P1),
	PC is P1/100,
	(retract(prob_cruzamento(_));true),	asserta(prob_cruzamento(PC)),
	write('Probabilidade de Mutacao (%):'), read(P2),
	PM is P2/100,
	(retract(prob_mutacao(_));true), asserta(prob_mutacao(PM)).
	
% =====================================  FIM  ================================

% ===================================== GERA  -- VALOR ==============================

inicializaValor:-write('Valor para parar: '),read(VP),
        (retract(tempo(_));true), asserta(valorParar(VP)),
		write('Tempo máximo de processamento em segundos: '),read(TS),
        (retract(tempo(_));true), asserta(tempo(TS)),
	write('Dimensao da Populacao: '),read(DP),
	(retract(populacao(_));true), asserta(populacao(DP)),
	write('Probabilidade de Cruzamento (%):'), read(P1),
	PC is P1/100,
	(retract(prob_cruzamento(_));true),	asserta(prob_cruzamento(PC)),
	write('Probabilidade de Mutacao (%):'), read(P2),
	PM is P2/100,
	(retract(prob_mutacao(_));true), asserta(prob_mutacao(PM)).
	
geraValor:-
	inicializaValor,
	gera_populacao(Pop),
        findall(Id, tarefa(_,Id,_,_,_,_), LTarefas),
	criticalRatio(LTarefas, [Ind_criticalRatio*_]),
	edd(LTarefas, [Ind_edd*_]),
	juntar_e_tirar_repetidos(Pop, Ind_criticalRatio, Ind_edd, PopFinal),
	write('Pop='),write(PopFinal),nl,
	avalia_populacao(PopFinal,PopAv),
	write('PopAv='),write(PopAv),nl,
	ordena_populacao(PopAv,PopOrd),
	get_time(TInicial),
	valorParar(Valor),!,
	gera_geracaoValor(0,TInicial,Valor,PopOrd).
	

gera_geracaoValor(N,T,Valor,Pop):-
	write('Geração '), write(N), write(':'), nl, write(Pop), nl,
	Pop = [_*Avaliacao|_],
	
	Avaliacao>Valor,
	
	get_time(TI),
	TInstante is TI - T,
	tempo(TS),
	TInstante<TS,
	
	random_permutation(Pop, LPop),
	cruzamento(LPop,NPop1),
	mutacao(NPop1,NPop),
	
	obtemDuasMelhoresSolucoes(LPop, NPop1, NPop, ListaParaTorneios,Melhores),
	torneios(ListaParaTorneios,ListaVencedora),
	append(Melhores,ListaVencedora,NovaPopulacao),
	ordena_populacao(NovaPopulacao,NovaPopulacaoOrdenada),
	
	N1 is N+1,
	gera_geracaoValor(N1,T,Valor,NovaPopulacaoOrdenada).
	
gera_geracaoValor(_,_,_,_):-!.

	
% =====================================  FIM  ================================

% ===================================== GERA  -- ESTABILIZAÇÃO ==============================

inicializaEstavel:-
	write('Dimensao da Populacao: '),read(DP),
	(retract(populacao(_));true), asserta(populacao(DP)),
	write('Probabilidade de Cruzamento (%):'), read(P1),
	PC is P1/100,
	(retract(prob_cruzamento(_));true),	asserta(prob_cruzamento(PC)),
	write('Probabilidade de Mutacao (%):'), read(P2),
	PM is P2/100,
	(retract(prob_mutacao(_));true), asserta(prob_mutacao(PM)).
	
geraEstavel:-
	inicializaEstavel,
	gera_populacao(Pop),
        findall(Id, tarefa(_,Id,_,_,_,_), LTarefas),
	criticalRatio(LTarefas, [Ind_criticalRatio*_]),
	edd(LTarefas, [Ind_edd*_]),
	juntar_e_tirar_repetidos(Pop, Ind_criticalRatio, Ind_edd, PopFinal),
	write('Pop='),write(PopFinal),nl,
	avalia_populacao(PopFinal,PopAv),
	write('PopAv='),write(PopAv),nl,
	ordena_populacao(PopAv,PopOrd),!,
	gera_geracaoEstavel(0,[],PopOrd).
	

gera_geracaoEstavel(N,PopAntiga,NovaPop):-
	write('Geração '), write(N), write(':'), nl, write(NovaPop), nl,
	\+verificaPopulacaoesDiferentes(PopAntiga,NovaPop),
	
	random_permutation(NovaPop, LPop),
	cruzamento(LPop,NPop1),
	mutacao(NPop1,NPop),
	
	obtemDuasMelhoresSolucoes(LPop, NPop1, NPop, ListaParaTorneios,Melhores),
	torneios(ListaParaTorneios,ListaVencedora),
	append(Melhores,ListaVencedora,NovaPopulacao),
	ordena_populacao(NovaPopulacao,NovaPopulacaoOrdenada),
	
	N1 is N+1,
	gera_geracaoEstavel(N1,NovaPop,NovaPopulacaoOrdenada).

gera_geracaoEstavel(_,_,_):-!.

verificaPopulacaoesDiferentes([],[]):-!,true.

verificaPopulacaoesDiferentes([Atual*AvaliacaoAtual|Antiga],[Novo*AvaliacaoNovo|Resto]):-
	same_length(Antiga,Resto),
	AvaliacaoAtual == AvaliacaoNovo,
	Atual == Novo,
	verificaPopulacaoesDiferentes(Antiga,Resto).
	

	
% =====================================  FIM  ================================

juntar_e_tirar_repetidos(Pop, Cr, Edd, [CrFinal, EddFinal|Pop]):-
	verificar_duplicados(Pop, Cr, CrFinal),
	verificar_duplicados([CrFinal|Pop], Edd, EddFinal).

verificar_duplicados(Pop, Individuo, IndividuoFinal):-
	member(Individuo, Pop),
	tarefas(NrTarefas),
	random(1, NrTarefas, Posicao1),
	Posicao2 is Posicao1 - 1,
	alterar_Individuo(Individuo, Posicao2, Posicao1, NovoIndividuo), !,
	verificar_duplicados(Pop, NovoIndividuo, IndividuoFinal), !.

verificar_duplicados(_, Individuo, Individuo):-!.


alterar_Individuo(Individuo, Posicao2, Posicao1, NovoIndividuo) :-
   same_length(Individuo, NovoIndividuo),
   append(BeforeI,[AtI|PastI],Individuo),
   append(BeforeI,[AtJ|PastI],Bs),
   append(BeforeJ,[AtJ|PastJ],Bs),
   append(BeforeJ,[AtI|PastJ],NovoIndividuo),
   length(BeforeI,Posicao2),
   length(BeforeJ,Posicao1), !.
   
% ===============================================================================

gera_populacao(Pop):-
	populacao(TamPop),
	TamPopFinal is TamPop - 2,
	tarefas(NumT),
	findall(Tarefa,tarefa(_,Tarefa,_,_,_,_),ListaTarefas),
	gera_populacao(TamPopFinal,ListaTarefas,NumT,Pop).

gera_populacao(0,_,_,[]):-!.
gera_populacao(TamPop,ListaTarefas,NumT,[Ind|Resto]):-
	TamPop1 is TamPop-1,
	gera_populacao(TamPop1,ListaTarefas,NumT,Resto),
	gera_individuo(ListaTarefas,NumT,Ind),
	not(member(Ind,Resto)).
gera_populacao(TamPop,ListaTarefas,NumT,L):-
	gera_populacao(TamPop,ListaTarefas,NumT,L).

gera_individuo([G],1,[G]):-!.

gera_individuo(ListaTarefas,NumT,[G|Resto]):-
	NumTemp is NumT + 1, % To use with random
	random(1,NumTemp,N),
	retira(N,ListaTarefas,G,NovaLista),
	NumT1 is NumT-1,
	gera_individuo(NovaLista,NumT1,Resto).

retira(1,[G|Resto],G,Resto).
retira(N,[G1|Resto],G,[G1|Resto1]):-
	N1 is N-1,
	retira(N1,Resto,G,Resto1).

avalia_populacao([],[]).
avalia_populacao([Ind|Resto],[Ind*V|Resto1]):-
	avalia(Ind,V),
	avalia_populacao(Resto,Resto1).

avalia(Seq,V):-
	avalia(Seq,0,V).

avalia([],_,0).
avalia([T|Resto],Inst,V):-
	tarefa(_,T,Dur,Prazo,Pen,_),
	InstFim is Inst+Dur,
	avalia(Resto,InstFim,VResto),
	(
		(InstFim =< Prazo,!, VT is 0)
  ;
		(VT is (InstFim-Prazo)*Pen)
	),
	V is VT+VResto.

ordena_populacao(PopAv,PopAvOrd):-
	bsort(PopAv,PopAvOrd).

bsort([X],[X]):-!.
bsort([X|Xs],Ys):-
	bsort(Xs,Zs),
	btroca([X|Zs],Ys).


btroca([X],[X]):-!.

btroca([X*VX,Y*VY|L1],[Y*VY|L2]):-
	VX>VY,!,
	btroca([X*VX|L1],L2).

btroca([X|L1],[X|L2]):-btroca(L1,L2).


% ================================= TORNEIOS =======================================
torneios(ListaParaTorneios,ListaVencedora):-
	populacao(DimensaoPopulacao),
	DimensaoPopulacaoPretendida is DimensaoPopulacao-2,!,
	torneios2(ListaParaTorneios,ListaVencedora,0,DimensaoPopulacaoPretendida).
	
torneios2(_,[],N,N):-!.	

torneios2(ListaParaTorneios,[Vencedor|ListaVencedora],N,DimensaoPopulacaoPretendida):-
	
	N<DimensaoPopulacaoPretendida,
	
	obtemAleatorio(ListaParaTorneios,Elemento1),
	obtemAleatorio(ListaParaTorneios,Elemento2),
	
	
	Elemento1 \== Elemento2,
	N1 is N+1,
	realizarTorneio(Elemento1,Elemento2,Vencedor),
	delete(ListaParaTorneios,Vencedor,ListaParaTorneiosSemVencedor),
	!,torneios2(ListaParaTorneiosSemVencedor,ListaVencedora,N1,DimensaoPopulacaoPretendida).
	
torneios2(ListaParaTorneios,ListaVencedora,N,DimensaoPopulacaoPretendida):-
	torneios2(ListaParaTorneios,ListaVencedora,N,DimensaoPopulacaoPretendida),!.

realizarTorneio(Tarefas*Avaliacao1,_*Avaliacao2,Tarefas*Avaliacao1):-

	Random1 is Avaliacao1/ (Avaliacao1+Avaliacao2),
	Random2 is Avaliacao2/ (Avaliacao1+Avaliacao2),
	
	random(0, Random1, Av1),
	random(0, Random2, Av2),
	Av1<Av2,true.

realizarTorneio(_,Tarefas2*Avaliacao2,Tarefas2*Avaliacao2).

	
obtemAleatorio([], []).
obtemAleatorio(List, Elt) :-
        length(List, Length),
        random(0, Length, Index),
        nth0(Index, List, Elt).
		
% =====================================  FIM  ================================

% ========================== OBTER MELHORES SOLUÇÕES ==========================
obtemDuasMelhoresSolucoes(Atual, Cruzados, Mutados, ListaParaTorneios,Melhores):-
	avalia_populacao(Cruzados,CruzadosAvaliados),
	avalia_populacao(Mutados,MutadosAvaliados),
	
	append(Atual,CruzadosAvaliados,Lista1),
	append(Lista1,MutadosAvaliados,Todos),
	
	ordena_populacao(Todos,TodosOrdenados),
	tirarRepetidos(TodosOrdenados,TodosOrdenadosSemRepetidos),
	obtemDuasMelhoresSolucoes2(TodosOrdenadosSemRepetidos,ListaParaTorneios,Melhores).

obtemDuasMelhoresSolucoes2(Lista,ListaParaTorneios,Melhores):-
	Lista = [Melhor|Resto],
	Resto = [SegundoMelhor|ListaParaTorneios],
	append([Melhor],[SegundoMelhor],Melhores).
	
remove_duplicates([],[]).

remove_duplicates([H | T], List) :-    
     member(H, T),
     remove_duplicates( T, List).

remove_duplicates([H | T], [H|T1]) :- 
      \+member(H, T),
      remove_duplicates( T, T1).
	
tirarRepetidos([],[]).
tirarRepetidos([H],[H]).
tirarRepetidos([H ,H| T], List) :-remove_duplicates( [H|T], List).
tirarRepetidos([H,Y | T], [H|T1]):- Y \= H,remove_duplicates( [Y|T], T1).

% =======================================   FIM   ==========================================

% ================================    UTILITÁRIOS   =========================================
	
gerar_pontos_cruzamento(P1,P2):-
	gerar_pontos_cruzamento1(P1,P2).

gerar_pontos_cruzamento1(P1,P2):-
	tarefas(N),
	NTemp is N+1,
	random(1,NTemp,P11),
	random(1,NTemp,P21),
	P11\==P21,!,
	((P11<P21,!,P1=P11,P2=P21);(P1=P21,P2=P11)).
gerar_pontos_cruzamento1(P1,P2):-
	gerar_pontos_cruzamento1(P1,P2).


cruzamento([],[]).
cruzamento([Ind*_],[Ind]).
cruzamento([Ind1*_,Ind2*_|Resto],[NInd1,NInd2|Resto1]):-
	gerar_pontos_cruzamento(P1,P2),
	prob_cruzamento(Pcruz),random(0.0,1.0,Pc),
	((Pc =< Pcruz,!,
        cruzar(Ind1,Ind2,P1,P2,NInd1),
	  cruzar(Ind2,Ind1,P1,P2,NInd2))
	;
	(NInd1=Ind1,NInd2=Ind2)),
	cruzamento(Resto,Resto1).

preencheh([],[]).

preencheh([_|R1],[h|R2]):-
	preencheh(R1,R2).


sublista(L1,I1,I2,L):-
	I1 < I2,!,
	sublista1(L1,I1,I2,L).

sublista(L1,I1,I2,L):-
	sublista1(L1,I2,I1,L).

sublista1([X|R1],1,1,[X|H]):-!,
	preencheh(R1,H).

sublista1([X|R1],1,N2,[X|R2]):-!,
	N3 is N2 - 1,
	sublista1(R1,1,N3,R2).

sublista1([_|R1],N1,N2,[h|R2]):-
	N3 is N1 - 1,
	N4 is N2 - 1,
	sublista1(R1,N3,N4,R2).

rotate_right(L,K,L1):-
	tarefas(N),
	T is N - K,
	rr(T,L,L1).

rr(0,L,L):-!.

rr(N,[X|R],R2):-
	N1 is N - 1,
	append(R,[X],R1),
	rr(N1,R1,R2).


elimina([],_,[]):-!.

elimina([X|R1],L,[X|R2]):-
	not(member(X,L)),!,
	elimina(R1,L,R2).

elimina([_|R1],L,R2):-
	elimina(R1,L,R2).

insere([],L,_,L):-!.
insere([X|R],L,N,L2):-
	tarefas(T),
	((N>T,!,N1 is N mod T);N1 = N),
	insere1(X,N1,L,L1),
	N2 is N + 1,
	insere(R,L1,N2,L2).


insere1(X,1,L,[X|L]):-!.
insere1(X,N,[Y|L],[Y|L1]):-
	N1 is N-1,
	insere1(X,N1,L,L1).

cruzar(Ind1,Ind2,P1,P2,NInd11):-
	sublista(Ind1,P1,P2,Sub1),
	tarefas(NumT),
	R is NumT-P2,
	rotate_right(Ind2,R,Ind21),
	elimina(Ind21,Sub1,Sub2),
	P3 is P2 + 1,
	insere(Sub2,Sub1,P3,NInd1),
	eliminah(NInd1,NInd11).


eliminah([],[]).

eliminah([h|R1],R2):-!,
	eliminah(R1,R2).

eliminah([X|R1],[X|R2]):-
	eliminah(R1,R2).

mutacao([],[]).
mutacao([Ind|Rest],[NInd|Rest1]):-
	prob_mutacao(Pmut),
	random(0.0,1.0,Pm),
	((Pm < Pmut,!,mutacao1(Ind,NInd));NInd = Ind),
	mutacao(Rest,Rest1).

mutacao1(Ind,NInd):-
	gerar_pontos_cruzamento(P1,P2),
	mutacao22(Ind,P1,P2,NInd).

mutacao22([G1|Ind],1,P2,[G2|NInd]):-
	!, P21 is P2-1,
	mutacao23(G1,P21,Ind,G2,NInd).
mutacao22([G|Ind],P1,P2,[G|NInd]):-
	P11 is P1-1, P21 is P2-1,
	mutacao22(Ind,P11,P21,NInd).

mutacao23(G1,1,[G2|Ind],G2,[G1|Ind]):-!.
mutacao23(G1,P,[G|Ind],G2,[G|NInd]):-
	P1 is P-1,
	mutacao23(G1,P1,Ind,G2,NInd).

% =======================================   FIM   ==========================================

% ===================================   ==========================================

% =======================================   FIM   ==========================================
% ===================================  INTEGRAÇÃO ==========================================

% =====================================  MDF ===============================================

inicializarContexto:-
	inicializarOperacoes,!,
	inicializarLinhas,!,
	inicializarProdutos,!,
	inicializarClientes,!,
	inicializarEncomendas.
	
inicializarLinhas:-
	(retract(linhas(_));true),
	(retract(maquinas(_));true),
	
	getLinhasProducaoMDF(Linhas),
	inicializarLinhas2(Linhas,NewLinhas,NewMaquinas),
	
	asserta(linhas(NewLinhas)),
	
	flatten(NewMaquinas, NewMaquinasFinal),
	asserta(maquinas(NewMaquinasFinal)).
	
inicializarLinhas2([],[],[]).
inicializarLinhas2([X|Linhas],[Id|T],[NewMaquinas|Lista]):-
	Id = X.get(id),
	Maquinas = X.get(maquinas),
	
	inicializarMaquinas(Maquinas,NewMaquinas),
	asserta(tipos_maq_linha(Id,NewMaquinas)),
	
	inicializarLinhas2(Linhas,T,Lista).
	
inicializarMaquinas([],[]).
inicializarMaquinas([X|Maquinas],[NumeroSerie|T]):-
	NumeroSerie = X.get(numeroSerie),
	inicializarOperacoesMaquina(NumeroSerie),
	inicializarMaquinas(Maquinas,T).

	
inicializarOperacoesMaquina(NumeroSerie):-
	getMaquinaMDF(NumeroSerie,Maquina),
	DescricaoTipoMaquina = Maquina.get(descricaoTipoMaquina),
	getOperacoesTipoMaquinaMDF(DescricaoTipoMaquina,TipoMaquina),
	ListaOperacoes = TipoMaquina.get(lOperacoes),
	associarOperacoesComMaquina(NumeroSerie,ListaOperacoes).
	
associarOperacoesComMaquina(_,[]).
associarOperacoesComMaquina(NumeroSerie,[Operacao|Resto]):-
	getOperacaoMDF(Operacao,OperacaoCompleta),
	
	Duracao = OperacaoCompleta.get(duracao),
	Ferramenta = OperacaoCompleta.get(ferramenta),
	asserta(operacao_maquina(Operacao,NumeroSerie,Ferramenta,4,Duracao)),
	
	associarOperacoesComMaquina(NumeroSerie,Resto).


inicializarOperacoes:-
	(retract(tipo_operacoes(_));true),
	(retract(ferramentas(_));true),
	
	getOperacoesMDF(Operacoes),
	inicializarOperacoes2(Operacoes,NewOperacoes,NewFerramentas),
	
	asserta(tipo_operacoes(NewOperacoes)),
	asserta(ferramentas(NewFerramentas)).
	
inicializarOperacoes2([],[],[]).
inicializarOperacoes2([X|Operacoes],[DescricaoOperacao|T],[Ferramenta|T2]):-
	DescricaoOperacao = X.get(descricao),
	Ferramenta = X.get(ferramenta),
	inicializarOperacoes2(Operacoes,T,T2).
	
% =====================================  MDP ============================================

inicializarProdutos:-
	(retract(produtos(_));true),
	getProdutosMDP(Produtos),
	inicializarProdutos2(Produtos,NewProdutos,NewPlanos),
	
	asserta(produtos(NewProdutos)),
	associarProdutosComPlanoFabrico(NewProdutos,NewPlanos).

inicializarProdutos2([],[],[]).	
inicializarProdutos2([X|Produtos],[DescricaoProduto|T],[PlanoFabrico|T2]):-
	DescricaoProduto = X.get(descricao),
	inicializarPlanoFabrico(DescricaoProduto,PlanoFabrico),
	inicializarProdutos2(Produtos,T,T2).
	
inicializarPlanoFabrico(Descricao,PlanoFabrico):-
	getPlanoFabricoMDP(Descricao,Plano),
	PlanoFabrico = Plano.get(planoFabrico).
	
associarProdutosComPlanoFabrico([],[]).
associarProdutosComPlanoFabrico([Produto|Resto],[Plano|Resto2]):-
	asserta(operacoes_produto(Produto,Plano)),
	associarProdutosComPlanoFabrico(Resto,Resto2).
	
% =====================================  Gestao Encomendas ==================================

inicializarClientes:-
	(retract(clientes(_));true),
	getClientesGE(Clientes),
	ClientesGE = Clientes.get(clientes),
	
	inicializarClientes2(ClientesGE,NewClientes,NewPrioridades),
	
	asserta(clientes(NewClientes)),
	associarClientesComPrioridade(NewClientes,NewPrioridades).

inicializarClientes2([],[],[]).	
inicializarClientes2([X|Clientes],[EmailCliente|T],[Prioridade|T2]):-
	EmailCliente = X.get(email),
	Prioridade = X.get(prioridade),
	
	inicializarClientes2(Clientes,T,T2).
	
associarClientesComPrioridade([],[]).
associarClientesComPrioridade([Cliente|Resto],[Prioridade|Resto2]):-
	asserta(prioridade_cliente(Cliente,Prioridade)),
	associarClientesComPrioridade(Resto,Resto2).
	
inicializarEncomendas:-
	(retractall(encomenda(_,_,_));true),
	getEncomendaEmProcessamentoGE(Encomendas),
	EncomendasGE = Encomendas.get(encomendas),!,
	
	inicializarEncomendas2(EncomendasGE).
	
inicializarEncomendas2([]).	
inicializarEncomendas2([X|Encomendas]):-
	
	
	Cliente = X.get(emailCliente),
	Produtos = X.get(produtos),
	TempoConc = X.get(tempoConc),
	IdEncomenda = X.get(id),
	
	getProdutosEncomenda(Produtos,TempoConc,NewProdutos),
	asserta(encomenda(IdEncomenda,Cliente,NewProdutos)),!,
	inicializarEncomendas2(Encomendas).

getProdutosEncomenda([],_,[]).
getProdutosEncomenda([Prod|Produtos],TempoConc,[NewProduto|T]):-
	
	DescricaoProduto = Prod.get(descricao),
	Quantidade = Prod.get(quantidade),
	
	NewProduto = e(DescricaoProduto,Quantidade,TempoConc),
	getProdutosEncomenda(Produtos,TempoConc,T).


% =====================================  Fim ============================================
	
	
% ====================================== HTTP ===============================================
	
getLinhasProducaoMDF(Result):-
  setup_call_cleanup(
    http_open('https://arqsi-3dc-3-mdf.azurewebsites.net/api/MDF/LinhaProducao/MaquinasAtivas', In, [request_header('Accept'='application/json'),cert_verify_hook(cert_accept_any)]),
    json_read_dict(In, Result),
    close(In)
).	
	
getMaquinaMDF(NumeroSerie,Maquina):-
	string_concat('https://arqsi-3dc-3-mdf.azurewebsites.net/api/MDF/Maquina/NumeroSerie/',NumeroSerie,Final),
	setup_call_cleanup(
    http_open(Final, In, [request_header('Accept'='application/json'),cert_verify_hook(cert_accept_any)]),
    json_read_dict(In, Maquina),
    close(In)
).

getOperacoesTipoMaquinaMDF(Descricao,Operacoes):-
	string_concat('https://arqsi-3dc-3-mdf.azurewebsites.net/api/MDF/TipoMaquina/Operacoes/',Descricao,Final),
	setup_call_cleanup(
    http_open(Final, In, [request_header('Accept'='application/json'),cert_verify_hook(cert_accept_any)]),
    json_read_dict(In, Operacoes),
    close(In)
).

getOperacaoMDF(Operacao,OperacaoCompleta):-
	string_concat('https://arqsi-3dc-3-mdf.azurewebsites.net/api/MDF/Operacao/',Operacao,Final),
	setup_call_cleanup(
    http_open(Final, In, [request_header('Accept'='application/json'),cert_verify_hook(cert_accept_any)]),
    json_read_dict(In, OperacaoCompleta),
    close(In)
).

getOperacoesMDF(Result):-
  setup_call_cleanup(
    http_open('https://arqsi-3dc-3-mdf.azurewebsites.net/api/MDF/Operacao/', In, [request_header('Accept'='application/json'),cert_verify_hook(cert_accept_any)]),
    json_read_dict(In, Result),
    close(In)
).

getProdutosMDP(Result):-
  setup_call_cleanup(
    http_open('https://arqsi-3dc-3-mdp.azurewebsites.net/api/MDP/Produto', In, [request_header('Accept'='application/json'),cert_verify_hook(cert_accept_any)]),
    json_read_dict(In, Result),
    close(In)
).

getPlanoFabricoMDP(DescricaoProduto,Plano):-
	string_concat('https://arqsi-3dc-3-mdp.azurewebsites.net/api/MDP/Produto/PlanoFabrico/',DescricaoProduto,Final),
	setup_call_cleanup(
    http_open(Final, In, [request_header('Accept'='application/json'),cert_verify_hook(cert_accept_any)]),
    json_read_dict(In, Plano),
    close(In)
).

getClientesGE(Clientes):-
	setup_call_cleanup(
    http_open('https://arqsi-3dc-3-ge.azurewebsites.net/users/clientesIntegracao', In, [request_header('Accept'='application/json'),cert_verify_hook(cert_accept_any)]),
    json_read_dict(In, Clientes),
    close(In)
).

getEncomendaEmProcessamentoGE(Encomendas):-
	setup_call_cleanup(
    http_open('https://arqsi-3dc-3-ge.azurewebsites.net/encomendas/espera', In, [request_header('Accept'='application/json'),cert_verify_hook(cert_accept_any)]),
    json_read_dict(In, Encomendas),
    close(In)
).

% ====================================== FIM ===============================================	



:-	write('A iniciar o contexto. Por favor aguarde.'),
	inicializarContexto,!,
	cria_op_enc,!,
	geraTarefas,!,
	gera_pp(_),!,
	write('Concluído com sucesso!').