var express = require('express');
var router = express.Router();

var PlaneamentoController = require('../Controllers/PlaneamentoController');

router.get('/', PlaneamentoController.planeamentoProducao);
router.post('/duracao', PlaneamentoController.duracaoFabrico);

module.exports = router;