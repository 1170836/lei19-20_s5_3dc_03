var jwt = require('jsonwebtoken');
var Auth = require('../auth/VerifyToken');
var PlaneamentoService = require('../Services/PlaneamentoService');

exports.duracaoFabrico = async function (req, res) {

    try {
        var statusToken = Auth.verifyTokenAdmin(req, res);

        if (statusToken == 403) {
            res.status(403).json({ message: "A sua sessão encontra-se inválida.\nPor favor, tente mais tarde." })
        } else if (statusToken == 500) {
            res.status(500).json({ message: "Erro no servidor.\nPor favor, tente mais tarde." })
        } else {
            var duracao = await PlaneamentoService.duracaoFabrico(req.body);
            res.status(200).json({ duracao: duracao });
        }

    } catch (e) {
        console.log(e.message);
        res.status(400).json({ message: e.message });
    }

    return res;
}

exports.planeamentoProducao = async function (req, res) {

    try {
        var statusToken = Auth.verifyTokenAdmin(req, res);

        if (statusToken == 403) {
            res.status(403).json({ message: "A sua sessão encontra-se inválida.\nPor favor, tente mais tarde." })
        } else if (statusToken == 500) {
            res.status(500).json({ message: "Erro no servidor.\nPor favor, tente mais tarde." })
        } else {
            var planeamento = await PlaneamentoService.planeamentoProducao();
            res.status(200).json({ planeamento: planeamento });
        }
    } catch (e) {
        console.log(e.message);
        res.status(400).json({ message: e.message });
    }

    return res;
}

