const swipl = require('swipl');

exports.duracaoFabrico = async function (planoFabrico) {

    var duracao = 0;
    await planoFabrico.forEach(async operacao => {

        const query = new swipl.Query('operacao_maquina(Operacao,_,_,TS,Dur)');

        while (ret = query.next()) {
            if (ret.Operacao == operacao) {
                duracao += ret.TS + ret.Dur;
                break;
            }
        }

        query.close();

    });

    return duracao;
}

exports.planeamentoProducao = async function () {

    swipl.call('inicializarContexto');
    swipl.call('cria_op_enc');
    swipl.call('geraTarefas');
    swipl.call('gera_pp(_)');
    swipl.call('geraIntegracao');

    var listaTarefas = [];

    const ret = swipl.call('geracaoFinal(Melhor*_)');
    if (ret) {
        var tarefas = ret.Melhor;
        var tarefa = tarefas.head;

        listaTarefas.push(tarefa);

        tarefas = tarefas.tail;

        while (tarefa) {
            tarefa = tarefas.head;
            tarefas = tarefas.tail;

            if (tarefa) {
                listaTarefas.push(tarefa);
            }

        }
    }

    var listaLinhas = [];

    const queryLinhas = new swipl.Query('tipos_maq_linha(Linha,_)');

    let retLinhas;
    while (retLinhas = queryLinhas.next()) {
        if (!listaLinhas.includes(retLinhas.Linha)) {
            listaLinhas.push(retLinhas.Linha);
        }
    }

    queryLinhas.close();

    var listaLinhasDuracao = [];
    listaLinhas.forEach(linha => {
        listaLinhasDuracao.push({ linha: linha, duracao: 0 });
    });


    var listaCompleta = [];
    var duracaoAteAoMomento;

    listaTarefas.forEach(tarefa => {

        const query = new swipl.Query('tarefa_linha(Tarefa,Linha)');

        var ret, linha;
        while (ret = query.next()) {
            if (ret.Tarefa == tarefa) {
                linha = ret.Linha;
                break;
            }
        }

        query.close();

        const queryTarefa = new swipl.Query('tarefa(IdEnc, Tarefa, Makespan,_,_,Produto)');

        while (ret = queryTarefa.next()) {
            if (ret.Tarefa == tarefa) {
                listaLinhasDuracao.forEach(l => {
                    if (l.linha == linha) {
                        duracaoAteAoMomento = l.duracao;
                        duracaoAteAoMomento += ret.Makespan;
                        l.duracao = duracaoAteAoMomento;
                    }
                });

                listaCompleta.push({ idEnc: ret.IdEnc, tarefa: ret.Tarefa, linha: linha, inicio: duracaoAteAoMomento - ret.Makespan, fim: duracaoAteAoMomento, produto: ret.Produto });
                break;
            }
        }

        queryTarefa.close();
    });

    return listaCompleta;
}