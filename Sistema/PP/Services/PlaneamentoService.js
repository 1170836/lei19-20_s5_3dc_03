var PlaneamentoRepo = require('../Repository/PlaneamentoRepository');

exports.duracaoFabrico = async function (planoFabrico) {
    try {
        var duracao = await PlaneamentoRepo.duracaoFabrico(planoFabrico);
    } catch (e) {
        return null;
    }

    return duracao;
}

exports.planeamentoProducao = async function () {
    try {
        var planeamento = await PlaneamentoRepo.planeamentoProducao();
        planeamento.sort((a, b) => a.fim - b.fim);
        return planeamento;

    } catch (e) {
        return null;
    }
}
