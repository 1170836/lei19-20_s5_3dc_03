import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

interface LoginDTO {
  email: string;
  password: string;
}

@Injectable({
  providedIn: 'root'
})

export class LoginService {

  private geUrl = 'https://arqsi-3dc-3-ge.azurewebsites.net/users';

  constructor(private httpClient: HttpClient) { }

  login(email2: string, password2: string): Observable<any> {
    let usDto: LoginDTO = { email: email2, password: password2 };
    return this.httpClient.post(this.geUrl + "/login", usDto).pipe(map(this.extractData));
  }

  private extractData(res: Response) {
    return res || {};
  }
}
