import { Component, OnInit } from '@angular/core';
import { LoginService } from './login.service';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private loginServ: LoginService) { }

  ngOnInit() {
  }

  private login(email: string, password: string): void {
    if (!this.validaForm(email, password)) {
      alert("Formulário mal preenchido");
    } else {
      this.loginServ.login(email, password).subscribe(data => {
        this.handleUser(data);
      },
        error => { this.handleError(error); });
    }
  }

  handleError(error: HttpErrorResponse) {
    if (error.status == 400) {
      alert("Utilizador não encontrado");
    } else if (error.status == 401) {
      alert("Password inválida");
    } else {
      alert("Serviço indisponível, tente mais tarde.");
    }
  }

  handleUser(data) {

    if (data.message.includes("Administrador")) {
      window.sessionStorage.setItem('tokenAdmin', data.token);
      window.location.replace("/operacao");
    } else {  
      window.sessionStorage.setItem('token', data.token);
      window.location.replace("/menuCliente");
    }
  }

  validaForm(empresa: string, password1: string) {
    if (empresa != "" && password1 != "") {
      return true;
    }
    return false;
  }

}
