import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NavbarClientService {

  private geUrl = 'https://arqsi-3dc-3-ge.azurewebsites.net/auth/cliente';

  constructor(private httpClient: HttpClient) { }

  guardarAutorizacoesCliente(autorizacoes: string[]): Observable<any> {
    return this.httpClient.post(this.geUrl, autorizacoes);
  }
}
