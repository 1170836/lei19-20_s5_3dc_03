import { TestBed } from '@angular/core/testing';

import { NavbarClientService } from './navbar-client.service';

describe('NavbarClientService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: NavbarClientService = TestBed.get(NavbarClientService);
    expect(service).toBeTruthy();
  });
});
