import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NavbarClientService } from './navbar-client.service';


@Component({
  selector: 'app-navbar-client',
  templateUrl: './navbar-client.component.html',
  styleUrls: ['./navbar-client.component.css']
})
export class NavbarClientComponent implements OnInit {

  title = 'My Own Cutlery';
  private autorizacoes: string[];

  constructor(private http: HttpClient, private clienteService: NavbarClientService) { }

  ngOnInit() {
    this.lerFicheiroAutorizacao();
  }

  logout() {
    window.sessionStorage.removeItem('token');
    window.location.replace('login');
  }

  private lerFicheiroAutorizacao() {

    let fileString = "assets/autorizacoesCliente.txt";

    this.http.get(fileString, { responseType: 'text' })
      .subscribe(data => {
        var operacaoAut: string[] = data.split(':');
        this.autorizacoes = operacaoAut[1].split(',');
        this.guardarAutorizacoes();
        this.mostrarOperacoesCliente();
      });
  }

  private guardarAutorizacoes() {
    this.clienteService.guardarAutorizacoesCliente(this.autorizacoes).subscribe();
  }


  private mostrarOperacoesCliente() {
    var i: number = 0;

    this.autorizacoes.forEach(aut => {
      var autorizacao = aut.split('-');

      if (autorizacao[1] != 'true') {
       if (autorizacao[0] == "Alterar Perfil") {
          this.esconderAlterar();
          i++
        } else if (autorizacao[0] == "Fazer Encomenda") {
          this.esconderEncomendas();
        }
      }
    });

  }


  private esconderAlterar() {
    var elem = document.getElementById('alterarPerfil');
    elem.parentNode.removeChild(elem);
    elem = document.getElementById('divisaoAlterar');
    elem.parentNode.removeChild(elem);

    return false;
  }

  private esconderEncomendas() {
    var elem = document.getElementById('autEncomendas');
    elem.parentNode.removeChild(elem);
    return false;
  }

}
