import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { OperacaoComponent } from './operacao/operacao.component';
import { MaquinaComponent } from './maquina/maquina.component';
import { LinhaProducaoComponent } from './linha-producao/linha-producao.component';
import { TipoMaquinaComponent } from './tipo-maquina/tipo-maquina.component';
import { VisualizacaoComponent } from './visualizacao/visualizacao.component';
import { ProdutoComponent } from './produto/produto.component';
import { LoginComponent } from './login/login.component';
import { NavbarAdminComponent } from './navbar-admin/navbar-admin.component';
import { RegistoComponent } from './registo/registo.component';
import { NavbarClientComponent } from './navbar-client/navbar-client.component';
import { PerfilComponent } from './perfil/perfil.component';
import { MenuClienteComponent } from './menu-cliente/menu-cliente.component';
import { EncomendaComponent } from './encomenda/encomenda.component';
import { GestaoClientesComponent } from './gestao-clientes/gestao-clientes.component';
import { GestaoEncomendasComponent } from './gestao-encomendas/gestao-encomendas.component';
import { AtivacaoMaquinaComponent } from './ativacao-maquina/ativacao-maquina.component';
import { AlterarEncomendaComponent } from './alterar-encomenda/alterar-encomenda.component';
import { PlaneamentoComponent } from './planeamento/planeamento.component';

@NgModule({
  declarations: [
    AppComponent,
    OperacaoComponent,
    MaquinaComponent,
    LinhaProducaoComponent,
    TipoMaquinaComponent,
    VisualizacaoComponent,
    ProdutoComponent,
    LoginComponent,
    NavbarAdminComponent,
    RegistoComponent,
    NavbarClientComponent,
    PerfilComponent,
    MenuClienteComponent,
    EncomendaComponent,
    GestaoClientesComponent,
    GestaoEncomendasComponent,
    AtivacaoMaquinaComponent,
    AlterarEncomendaComponent,
    PlaneamentoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
