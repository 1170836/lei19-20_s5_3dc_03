export class Cliente {
    _id: string;
    empresa: string;
    cliente: string;
    email: string;
    password: string;
    nif: string;
    morada:string;
    iban: string;
    numCartao: string;
    ccv2: string;
    validade: string;
}