import { Component, OnInit, ɵConsole, SimpleChanges, Input } from '@angular/core';
import { Cliente } from './cliente';
import { MenuClientService } from './menu-client.service';
import { HttpErrorResponse } from '@angular/common/http';
import { Encomenda } from '../encomenda/encomenda';
import { Produto_Encomenda } from '../produto/produto_encomenda';

@Component({
  selector: 'app-menu-cliente',
  templateUrl: './menu-cliente.component.html',
  styleUrls: ['./menu-cliente.component.css']
})


export class MenuClienteComponent implements OnInit {

  produtos: Produto_Encomenda[] = [];
  token: string;
  user: Cliente;
  encomendas: Encomenda[];
  pagamento: string;
  previsao: number = 0;
  flagEstado: boolean = false; //true quando podemos alterar a encomenda

  private index: number = 0;
  private dataPrevisao: Date;

  private flagCancelar: boolean = false;
  private flagAlterar: boolean = false;

  private autorizacoes = [];

  constructor(private menuClienteService: MenuClientService) { }
  /* 
    mostrarPopupEncomendas() {
      document.getElementById("abrirDetalhesEncomenda-modal").style.display = "block";
    } */

  ngOnInit() {
    this.token = window.sessionStorage.getItem('token');

    if (this.token == null) {
      window.location.replace('login');
    }

    this.getClient();
    this.getEncomendas();
    this.getAutorizacoesCliente();
  }

  private getClient() {
    this.menuClienteService.getClient(this.token).subscribe(
      data => {
        this.user = data.cliente;
        this.pagamento = this.user.numCartao.slice((5));
      }
    ),
      error => { this.handleError(error); };
  }

  private getEncomendas() {

    this.menuClienteService.getEncomendas(this.token).subscribe(
      data => {
        this.encomendas = data.encomendas;
      }
    ),
      error => { this.handleError(error); };
  }

  abrirDetalhesEncomenda(enc) {
    if (enc.estado.includes("espera")) {
      this.flagEstado = true;
    } else {
      this.flagEstado = false;
    }

    window.sessionStorage.setItem('encomenda', enc._id);
    this.dataPrevisao = enc.previsao;

    for (let i = 0; i < enc.produtos.length; i++) {
      let produtoEncomenda = new Produto_Encomenda();
      produtoEncomenda.descricao = enc.produtos[i].descricao
      produtoEncomenda.preco = enc.produtos[i].preco
      produtoEncomenda.quantidade = enc.produtos[i].quantidade;
      this.produtos.push(produtoEncomenda);

    }

    let shand = document.getElementById('abrirDetalhesEncomenda-modal');
    if (shand) {
      shand.style.display = "flex";
    }

  }

  fecharDetalhesEncomenda() {
    let shand = document.getElementById('abrirDetalhesEncomenda-modal');
    if (shand) {
      shand.style.display = "none";
    }
    window.sessionStorage.removeItem('encomenda');
    this.produtos = [];
  }

  handleError(error: HttpErrorResponse) {
    alert('Sessão Expirou!')
  }

  cancelarEncomenda() {

    var result = confirm("A encomenda vai ser cancelada!");

    if (result) {
      var encomenda = window.sessionStorage.getItem('encomenda');
      this.menuClienteService.cancelarEncomenda(this.token, encomenda).subscribe(
        data => {
          alert("Encomenda cancelada");
          window.location.reload();
          window.sessionStorage.removeItem('encomenda');
        },
        error => { this.handleErrorCancelar(error); });
    }

  }

  handleErrorCancelar(error: HttpErrorResponse) {
    if (error.status == 403) {
      alert('Sessão Expirou!');
    } else {
      if (error.status == 500) {
        alert("Sistema indisponível! Tente mais tarde");
      }
    }
  }


  private getAutorizacoesCliente() {
    this.menuClienteService.getAutorizacoesCliente().subscribe(
      data => {
        this.autorizacoes = data.auth;
        this.verificarAuth();
      }
    )
  }

  private verificarAuth() {
    this.autorizacoes.forEach(
      auth => {
        if (auth.auth == 'true') {
          if (auth.operacao == 'Alterar Encomenda') {
            this.flagAlterar = true;
          }
          if (auth.operacao == 'Cancelar Encomenda') {
            this.flagCancelar = true;
          }
        }
      }
    )
  }
}
