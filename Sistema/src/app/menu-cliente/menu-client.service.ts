import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs'

@Injectable({
  providedIn: 'root'
})

export class MenuClientService {
  
 
  private encomendasUrl='https://arqsi-3dc-3-ge.azurewebsites.net/encomendas';
  private geUrl = 'https://arqsi-3dc-3-ge.azurewebsites.net/users';
  private geUrlEncomendas = 'https://arqsi-3dc-3-ge.azurewebsites.net/encomendas/user';
  
  private geUrlAuthCliente = 'https://arqsi-3dc-3-ge.azurewebsites.net/auth/cliente';

  constructor(private httpClient: HttpClient) {
  }

  getClient(token: string): Observable<any> {
    return this.httpClient.get(this.geUrl + '/verPerfil', { headers: new HttpHeaders({ 'x-access-token': token }) }).pipe(map(this.extractData));
  }

  getEncomendas(token: string): Observable<any> {
    return this.httpClient.get(this.geUrlEncomendas, { headers: new HttpHeaders({ 'x-access-token': token }) }).pipe(map(this.extractData));
  }

  cancelarEncomenda(token: string, encomenda): Observable<any> {
    return this.httpClient.delete(this.encomendasUrl + '/cancelarEncomenda/' + encomenda , { headers: new HttpHeaders({ 'x-access-token': token }) }).pipe(map(this.extractData));
  }
  private extractData(res: Response) {
    return res || {};
  }
  
  getAutorizacoesCliente(): Observable<any> {
    return this.httpClient.get(this.geUrlAuthCliente).pipe(map(this.extractData));;
  }  
}