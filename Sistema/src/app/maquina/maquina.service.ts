import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

interface MaquinaDTO {
  marca: string;
  modelo: string;
  descricaoTipoMaquina: string;
  numeroSerie: string;
}

interface TipoMaquinaDTO {
  descricaoTipoMaquina: string;
}

@Injectable({
  providedIn: 'root'
})
export class MaquinaService {

  private mdfUrl = 'https://arqsi-3dc-3-mdf.azurewebsites.net/api/MDF/Maquina/';
  private mdfUrlTipoMaquina = 'https://arqsi-3dc-3-mdf.azurewebsites.net/api/MDF/TipoMaquina/';
  private mdfVisualizacaoUrl = 'https://arqsi-3dc-3-mdf.azurewebsites.net/api/MDF/Maquina/Visualizacao';

  constructor(private httpClient: HttpClient) { }

  getMaquina(id: string): Observable<any> {
    return this.httpClient.get(this.mdfUrl + id).pipe(map(this.extractData));
  }
  
  getMaquinaByNumSerie(id: string): Observable<any> {
    return this.httpClient.get(this.mdfUrl + "numeroSerie/" + id).pipe(map(this.extractData));
  }

  addMaquina(marca2: string, modelo2: string, descricaoTipoMaquina2: string, numeroSerie2 :string): Observable<any> {
    let mDTO: MaquinaDTO = { marca: marca2, modelo: modelo2, descricaoTipoMaquina: descricaoTipoMaquina2 , numeroSerie: numeroSerie2};
    return this.httpClient.post(this.mdfUrl, mDTO).pipe(map(this.extractData));
  }

  alterarTipoMaquina(id2: string, descricaoTipoMaquina2: string): Observable<any> {
    let tmDTO: TipoMaquinaDTO = { descricaoTipoMaquina: descricaoTipoMaquina2 };
    return this.httpClient.put(this.mdfUrl + id2, tmDTO).pipe(map(this.extractData))
  }

  getMaquinas(): Observable<any> {
    return this.httpClient.get(this.mdfUrl).pipe(map(this.extractData));
  }

  getTiposMaquina(): Observable<any> {
    return this.httpClient.get(this.mdfUrlTipoMaquina).pipe(map(this.extractData));
  }
  getMaquinasVisualizacao(): Observable<any> {
    return this.httpClient.get(this.mdfVisualizacaoUrl).pipe(map(this.extractData));
  }

  private extractData(res: Response) {
    return res || {};
  }


  private handleError(err: HttpErrorResponse) {
    if (err.error instanceof ErrorEvent) {
      console.error('An error occurred: ', err.error.message);
    }
    else {
      console.error(
        `Web Api returned code ${err.status}, ` + ` Response body was: ${err.error}`
      );
    }
    return Observable.throw(err);
  }

}
