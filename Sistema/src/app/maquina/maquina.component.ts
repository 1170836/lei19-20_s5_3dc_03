import { Component, AfterViewInit } from '@angular/core';
import { Maquina } from './maquina';
import { TipoMaquina } from '../tipo-maquina/tipo-maquina';
import { MaquinaService } from './maquina.service';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-maquina',
  templateUrl: './maquina.component.html',
  styleUrls: ['./maquina.component.css']
})
export class MaquinaComponent implements AfterViewInit {

  constructor(private mService: MaquinaService) {

  }

  maquina: Maquina;
  statusMessage: string;
  maquinas: Maquina[];
  tiposMaquinas: TipoMaquina[];
  maquinaAtualizar: Maquina;
  maquinaSelecionada: Maquina;
  tipoMaquinaSelecionada: TipoMaquina;
  private token: string;

  ngAfterViewInit() {
    this.token = window.sessionStorage.getItem('tokenAdmin');
    if (this.token == null) {
      window.location.replace('login');
    }

    
    this.getMaquinas();
    this.getTiposMaquina();
  }

  private getMaquina(id: string): void {
    this.mService.getMaquinaByNumSerie(id).subscribe(data => {
      this.maquina = data;
      alert("Número de Série: " + this.maquina.numeroSerie + "\nMarca: " + this.maquina.marca + "\nModelo: " + this.maquina.modelo + "\nTipo de Maquina: " + this.maquina.descricaoTipoMaquina)
    }, error => { this.handleErrorGetMaquina(error); })
  }

  private addMaquina(marca: string, modelo: string, descricaoTipoMaquina: string, numeroSerie: string): void {
    this.mService.addMaquina(marca, modelo, descricaoTipoMaquina, numeroSerie).subscribe(data => {
      this.maquina = data;
      alert("Máquina adicionada" +
        "\nNúmero de Série: " + this.maquina.numeroSerie +
        "\nMarca: " + this.maquina.marca +
        "\nModelo: " + this.maquina.modelo +
        "\nTipo de Máquina: " + this.maquina.descricaoTipoMaquina);
      window.location.reload();
    },
      error => { this.handleErrorAddMaquina(error); });
  }

  private alterarTipoMaquina(): void {
    this.mService.alterarTipoMaquina(this.maquinaSelecionada.id, this.tipoMaquinaSelecionada.descricao).subscribe(
      data => {
        this.maquina = data;
        alert("Máquina alterada\nID:" + this.maquinaSelecionada.id +
          "\nNovo tipo de máquina: " + this.maquina.descricaoTipoMaquina);
        window.location.reload();
      },
      error => { this.handleErrorAtualizarMaquina(error); }
    );

  }

  private getMaquinas(): void {

    var table: HTMLTableElement = <HTMLTableElement>document.getElementById("maquinasAll");

    this.mService.getMaquinas().subscribe(data => {

      this.maquinas = data;

      if (this.maquinas.length == 0) {
        var row = table.insertRow();
        row.innerHTML = "Não existem máquinas criadas";
      }
    },
      error => { this.statusMessage = "Error: Service Unavailable"; });
  }

  private getTiposMaquina(): void {
    var table: HTMLTableElement = <HTMLTableElement>document.getElementById("tiposMaquinaAll");

    this.mService.getTiposMaquina().subscribe(data => {

      this.tiposMaquinas = data;
      if (this.tiposMaquinas.length == 0) {
        var row = table.insertRow();
        row.innerHTML = "Não existem tipos de maquina criados";
      }
    },
      error => { this.statusMessage = "Error: Service Unavailable"; });
  }

  handleErrorGetMaquina(error: HttpErrorResponse) {
    if (error.status == 404) {
      alert("Maquina não encontrada");
    } else if (error.status == 400) {
      alert("Id inválido");
    } else {
      alert("Serviço indisponível, tente mais tarde.");
    }
  }


  handleErrorAddMaquina(error: HttpErrorResponse) {
    if (error.status == 400) {
      alert("Dados inseridos nao estão corretos. Tente novamente");
    }
  }

  handleErrorAtualizarMaquina(error: HttpErrorResponse) {
    if (error.status == 400) {
      alert("Dados inseridos nao estão corretos. Tente novamente");
    }
  }

  radioMaquinaChangeHandler(maquina: any) {
    this.maquinaSelecionada = maquina;
  }

  radioTipoMaquinaChangeHandler(tipoMaquina: any) {
    this.tipoMaquinaSelecionada = tipoMaquina;
  }


}
