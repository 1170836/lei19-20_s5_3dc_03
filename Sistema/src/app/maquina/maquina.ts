export class Maquina {
    id: string;
    marca: string;
    modelo: string;
    descricaoTipoMaquina: string;
    numeroSerie: string;
    estado: boolean;
}