import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MaquinaComponent } from './maquina.component';
import { HttpClientModule } from '@angular/common/http';

describe('MaquinaComponent', () => {
  let component: MaquinaComponent;
  let fixture: ComponentFixture<MaquinaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MaquinaComponent],
      imports: [HttpClientModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MaquinaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  afterEach(() => {
    fixture.destroy();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
