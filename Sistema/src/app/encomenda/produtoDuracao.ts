import { Produto } from '../produto/produto';

export class ProdutoDuracao {
    produto: Produto;
    duracao: string;
}