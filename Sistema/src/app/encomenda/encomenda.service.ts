import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Produto } from '../produto/produto'
import { Encomenda } from './encomenda'
import { PlanoFabrico } from './planoFabrico';
import { ProdutoDuracao } from './produtoDuracao';

interface EncomendaDTO {
  produtos: Array<ProdutoDTO>;
  tempoConc: String;
}

interface ProdutoDTO {
  descricao: String;
  preco: Number;
  quantidade: Number;
}

@Injectable({
  providedIn: 'root'
})

export class EncomendaService {

  private geUrl = 'https://arqsi-3dc-3-ge.azurewebsites.net/encomendas';
  
  private ppDuracaoUrl = 'http://127.0.0.1:8080/planeamento/duracao';

  private mdpAllProdutos = 'https://arqsi-3dc-3-mdp.azurewebsites.net/api/MDP/Produto/AllProdutos/';
  private mdpProdutos = 'https://arqsi-3dc-3-mdp.azurewebsites.net/api/MDP/Produto';

  private produtos_Url = 'https://arqsi-3dc-3-ge.azurewebsites.net/encomendas';

  constructor(private httpClient: HttpClient) { }

  getProdutos(): Observable<any> {
    return this.httpClient.get(this.mdpAllProdutos).pipe(map(this.extractData));
  }

  getProdutosMaisEncomendados(token: string): Observable<any> {
    return this.httpClient.get(this.produtos_Url + "/maisEncomendados", { headers: new HttpHeaders({ 'x-access-token': token }) }).pipe(map(this.extractData));
  }

  getProdutosMaisVezesEncomendados(token: string): Observable<any> {
    return this.httpClient.get(this.produtos_Url + "/maisVezesEncomendados", { headers: new HttpHeaders({ 'x-access-token': token }) }).pipe(map(this.extractData));
  }

  getPlanoFabrico(desc: string): Observable<any> {
    return this.httpClient.get(this.mdpProdutos + "/planoFabrico/" + desc).pipe(map(this.extractData));
  }

  duracaoProducao(plano: PlanoFabrico, token: string) {
    return this.httpClient.post(this.ppDuracaoUrl, plano.planoFabrico, { headers: new HttpHeaders({ 'x-access-token': token }) }).pipe(map(this.extractData));
  }

  private extractData(res: Response) {
    return res || {};
  }

  registarEncomenda(order: Encomenda, token: string): Observable<any> {
    let novoArray = new Array<ProdutoDTO>();
    for (let i = 0; i < order.produtos.length; i++) {
      let produtodto: ProdutoDTO = { descricao: order.produtos[i].descricao, preco: order.produtos[i].preco, quantidade: order.produtos[i].quantidade };
      novoArray.push(produtodto);
    }
    let orderDTO: EncomendaDTO = { produtos: novoArray, tempoConc: order.tempoConc };
    return this.httpClient.post(this.geUrl + '/registarEncomenda/', orderDTO, { headers: new HttpHeaders({ 'x-access-token': token }) }).pipe(map(this.extractData));
  }

  getEncomenda(enc: string, token: string): Observable<any> {
    return this.httpClient.get(this.geUrl + '/encomenda/' + enc, { headers: new HttpHeaders({ 'x-access-token': token }) });
  }


  private handleError(err: HttpErrorResponse) {
    if (err.error instanceof ErrorEvent) {
      console.error('An error occurred: ', err.error.message);
    }
    else {
      console.error(
        `Web Api returned code ${err.status}, ` + ` Response body was: ${err.error}`
      );
    }
    return Observable.throw(err);
  }

}