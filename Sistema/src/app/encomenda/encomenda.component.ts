import { Component, OnInit } from '@angular/core';
import { Produto } from '../produto/produto';
import { EncomendaService } from './encomenda.service';
import { HttpErrorResponse } from '@angular/common/http';
import { Encomenda } from './encomenda';
import { repeatWhen } from 'rxjs/operators';
import { Produto_Encomenda } from '../produto/produto_encomenda';
import { ProdutoPlano } from './produtoPlano';
import { PlanoFabrico } from './planoFabrico';
import { ProdutoDuracao } from './produtoDuracao';

@Component({
  selector: 'app-encomenda',
  templateUrl: './encomenda.component.html',
  styleUrls: ['./encomenda.component.css']
})
export class EncomendaComponent implements OnInit {

  constructor(private eService: EncomendaService) { }

  produtos: Produto[];
  statusMessage: string;
  encomendaAtual: Encomenda = new Encomenda();
  token: string;
  prodEncomenda: Produto_Encomenda;

  maisEncomendados: Produto[];
  maisVezesEncomendados: Produto[];
  menorTempoFabrico: Produto[];
  preco: number = 0;

  ngOnInit() {
    this.token = window.sessionStorage.getItem('token');
    if (this.token == null) {
      window.location.replace('login');
    }
    this.getProdutos();
  }

  private getProdutos(): void {
    var table: HTMLTableElement = <HTMLTableElement>document.getElementById("produtosAll");

    this.eService.getProdutos().subscribe(data => {

      this.produtos = data;
      if (this.produtos.length == 0) {
        var row = table.insertRow();
        row.innerHTML = "Não existem produtos inseridos.";
      }
      this.getProdutosMaisEncomendados();
      this.getProdutosMaisVezesEncomendados();
      this.getProdutosMenorFabrico();
    },
      error => { this.statusMessage = "Error: Service Unavailable."; });
  }

  private getProdutosMaisVezesEncomendados(): void {
    var table: HTMLTableElement = <HTMLTableElement>document.getElementById("produtosMaisVezesEnc")

    this.eService.getProdutosMaisVezesEncomendados(this.token).subscribe(data => {

      let ites;

      if (data.produtos.length <= 5) {
        ites = data.produtos[0].length;
      } else {
        ites = 5;
      }

      this.maisVezesEncomendados = [];

      for (let i = 0; i < ites; i++) {
        for (let j = 0; j < this.produtos.length; j++) {
          if (this.produtos[j].descricao == data.produtos[0][i]) {
            let p = new Produto();
            p.descricao = this.produtos[j].descricao;
            p.preco = this.produtos[j].preco;
            this.maisVezesEncomendados.push(p);
            if (this.maisVezesEncomendados.length > 5) {
              this.maisVezesEncomendados.length = 5;
            }
            break;
          }
        }
      }

      if (this.produtos.length == 0) {
        var row = table.insertRow();
        row.innerHTML = "Não existem produtos inseridos.";
      }

    },
      error => { this.statusMessage = "Error: Service Unavailable."; })
  }

  private getProdutosMaisEncomendados(): void {
    var table: HTMLTableElement = <HTMLTableElement>document.getElementById("produtosMaisEnc");
    this.eService.getProdutosMaisEncomendados(this.token).subscribe(data => {

      let ites;

      if (data.produtos.length <= 5) {
        ites = data.produtos[0].length;
      } else {
        ites = 5;
      }

      this.maisEncomendados = [];

      for (let i = 0; i < ites; i++) {
        for (let j = 0; j < this.produtos.length; j++) {
          if (this.produtos[j].descricao == data.produtos[0][i]) {
            let p = new Produto();
            p.descricao = this.produtos[j].descricao;
            p.preco = this.produtos[j].preco;
            this.maisEncomendados.push(p);
            if (this.maisEncomendados.length > 5) {
              this.maisEncomendados.length = 5;
            }
            break;
          }
        }
      }

      if (this.produtos.length == 0) {
        var row = table.insertRow();
        row.innerHTML = "Não existem produtos inseridos.";
      }
    },
      error => { this.statusMessage = "Error: Service Unavailable."; });
  }

  private inserirNaEncomenda(prod: Produto, quant: Number): void {
    var table: HTMLTableElement = <HTMLTableElement>document.getElementById("encomendaCliente");

    this.preco = 0;
    let index = -1;

    for (let i = 0; i < this.encomendaAtual.produtos.length; i++) {
      if (this.encomendaAtual.produtos.length >= 1 && this.encomendaAtual.produtos[i].descricao == prod.descricao) {
        index = i;
        break
      }
    }
    for (let i = this.encomendaAtual.produtos.length; i > 0; i--) {
      table.deleteRow(i);
    }

    if (index != -1) {
      this.encomendaAtual.produtos[index].quantidade = parseInt(this.encomendaAtual.produtos[index].quantidade.toString()) + parseInt(quant.toString());
    } else {
      this.prodEncomenda = new Produto_Encomenda();
      this.prodEncomenda.descricao = prod.descricao
      this.prodEncomenda.preco = prod.preco
      this.prodEncomenda.quantidade = quant;
      this.encomendaAtual.produtos.push(this.prodEncomenda);
    }

    for (let i = 0; i < this.encomendaAtual.produtos.length; i++) {
      var row = table.insertRow();
      var descricaoP = row.insertCell(0);
      var quantidade = row.insertCell(1);
      var precoP = row.insertCell(2)
      descricaoP.innerHTML = this.encomendaAtual.produtos[i].descricao;
      quantidade.innerHTML = this.encomendaAtual.produtos[i].quantidade.toString();
      precoP.innerHTML = this.encomendaAtual.produtos[i].preco.toString() + "€";
      this.preco += parseInt(this.encomendaAtual.produtos[i].quantidade.toString()) * parseInt(this.encomendaAtual.produtos[i].preco.toString())
    }
  }

  private registarEncomenda(data: Date) {
    let today = new Date();
    let dataNew = new Date(data);

    if (dataNew > today) {

      var d = new Date(dataNew.getFullYear(), dataNew.getMonth(), dataNew.getDate(), 0, 0, 0, 0);
      this.encomendaAtual.tempoConc = d.toLocaleDateString();

      this.eService.registarEncomenda(this.encomendaAtual, this.token).subscribe(
        data => {
          alert(data.message);
        },
        error => { this.statusMessage = "Error: Service Unavailable."; });
    } else {
      alert("Por favor insira uma data superior à data de hoje.");
    }
  }

  private showMaisEncomendas(): void {
    document.getElementById('produtosMaisEncomendados').style.display = 'block';
    document.getElementById('allProdutos').style.display = 'none';
    document.getElementById('produtosMaisVezesEncomendados').style.display = 'none';
    document.getElementById('produtosMenorFabrico').style.display = 'none';
  }

  private showMaisVezesEncomendados(): void {
    document.getElementById('produtosMaisEncomendados').style.display = 'none';
    document.getElementById('allProdutos').style.display = 'none';
    document.getElementById('produtosMaisVezesEncomendados').style.display = 'block';
    document.getElementById('produtosMenorFabrico').style.display = 'none';
  }

  private showAll(): void {
    document.getElementById('produtosMaisEncomendados').style.display = 'none';
    document.getElementById('allProdutos').style.display = 'block';
    document.getElementById('produtosMaisVezesEncomendados').style.display = 'none';
    document.getElementById('produtosMenorFabrico').style.display = 'none';
  }

  private showMenorFabrico(): void {
    document.getElementById('produtosMaisEncomendados').style.display = 'none';
    document.getElementById('allProdutos').style.display = 'none';
    document.getElementById('produtosMaisVezesEncomendados').style.display = 'none';
    document.getElementById('produtosMenorFabrico').style.display = 'block';
  }

  private getProdutosMenorFabrico() {
    this.menorTempoFabrico = [];
    this.produtos.forEach(produto => {
      this.menorTempoFabrico.push(produto);
      this.menorTempoFabrico.sort((a, b) => a.duracao - b.duracao);
      if (this.menorTempoFabrico.length > 5) {
        this.menorTempoFabrico.length = 5;
      }
    })

  }



}