import { PlanoFabrico } from './planoFabrico';
import { Produto } from '../produto/produto';

export class ProdutoPlano {
    produto: Produto;
    planoFabrico: PlanoFabrico;
}