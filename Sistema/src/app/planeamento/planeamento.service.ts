import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PlaneamentoService {

  private geUrl = 'https://arqsi-3dc-3-ge.azurewebsites.net/encomendas/';
  private geUrlPlaneamento = 'https://arqsi-3dc-3-ge.azurewebsites.net/planeamento/';
  private ppUrl = 'http://127.0.0.1:8080/planeamento';

  constructor(private httpClient: HttpClient) { }


  getEncomendas(token: string): Observable<any> {
    return this.httpClient.get(this.geUrl + "processamento", { headers: new HttpHeaders({ 'x-access-token': token }) });
  }

  verificarPlaneamento(token: string): Observable<any> {
    return this.httpClient.get(this.geUrlPlaneamento + "verificarPlaneamento", { headers: new HttpHeaders({ 'x-access-token': token }) });
  }

  planeamentoProducao(token: string): Observable<any> {
    return this.httpClient.get(this.ppUrl, { headers: new HttpHeaders({ 'x-access-token': token }) }).pipe(map(this.extractData));
  }

  planeamentoDia(token: string): Observable<any> {
    return this.httpClient.get(this.geUrlPlaneamento + "planeamentoDia", { headers: new HttpHeaders({ 'x-access-token': token }) }).pipe(map(this.extractData));
  }

  produtoEmProducaoLinha(token: string, linha: string): Observable<any> {
    return this.httpClient.get(this.geUrlPlaneamento + "produtoEmProducaoLinha/" + linha, { headers: new HttpHeaders({ 'x-access-token': token }) }).pipe(map(this.extractData));
  }

  guardarPlaneamento(planeamento, token: string) {
    return this.httpClient.post(this.geUrlPlaneamento + "guardarPlaneamento", planeamento, { headers: new HttpHeaders({ 'x-access-token': token }) })
  }

  private extractData(res: Response) {
    return res || {};
  }

}
