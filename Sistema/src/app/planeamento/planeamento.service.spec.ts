import { TestBed } from '@angular/core/testing';

import { PlaneamentoService } from './planeamento.service';

describe('PlaneamentoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PlaneamentoService = TestBed.get(PlaneamentoService);
    expect(service).toBeTruthy();
  });
});
