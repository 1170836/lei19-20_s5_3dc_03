import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlaneamentoComponent } from './planeamento.component';

describe('PlaneamentoComponent', () => {
  let component: PlaneamentoComponent;
  let fixture: ComponentFixture<PlaneamentoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlaneamentoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlaneamentoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
