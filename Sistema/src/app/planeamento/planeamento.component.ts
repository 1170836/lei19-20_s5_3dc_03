import { Component, OnInit } from '@angular/core';
import { Encomenda } from '../encomenda/encomenda';
import { PlaneamentoService } from './planeamento.service';
import { HttpErrorResponse } from '@angular/common/http';
import { Produto_Encomenda } from '../produto/produto_encomenda';

@Component({
  selector: 'app-planeamento',
  templateUrl: './planeamento.component.html',
  styleUrls: ['./planeamento.component.css']
})
export class PlaneamentoComponent implements OnInit {

  constructor(private service: PlaneamentoService) { }

  private token: string;
  encomendas: Encomenda[];
  private planeamento = true;
  private produtos: Produto_Encomenda[] = [];
  private planeamentoDia = [];
  private planeamentoEncomenda = [];
  private index = 0;

  ngOnInit() {
    this.token = window.sessionStorage.getItem('tokenAdmin');
    if (this.token == null) {
      window.location.replace('login');
    }

    this.getEncomendas();
    this.verificarPlaneamento();
  }


  getEncomendas() {
    this.service.getEncomendas(this.token).subscribe(
      data => {
        this.encomendas = data.encomendas;
      }
    ),
      error => { this.handleError(error); };
  }

  private verificarPlaneamento() {
    this.service.verificarPlaneamento(this.token).subscribe(data => {
      this.planeamento = data.planeamento;
      if (this.planeamento == true) {
        this.getPlaneamentoDia();
      }
    });
  }

  private getPlaneamentoDia() {
    this.service.planeamentoDia(this.token).subscribe(data => {
      this.planeamentoDia = data.planeamento.plano;
      console.log(this.planeamentoDia);
    });
  }

  private planeamentoProducao() {
    this.service.planeamentoProducao(this.token).subscribe(data => {
      this.service.guardarPlaneamento(data.planeamento, this.token).subscribe(data => {
        this.ngOnInit();
      });
    });
    alert("Este processo pode demorar alguns segundos. Aguarde por favor.");
  }

  private handleError(error: HttpErrorResponse) {
    alert('Sessão Expirou!')
  }

  verDetalhes(enc) {

    for (let i = 0; i < enc.produtos.length; i++) {
      let produtoEncomenda = new Produto_Encomenda();
      produtoEncomenda.descricao = enc.produtos[i].descricao
      produtoEncomenda.preco = enc.produtos[i].preco
      produtoEncomenda.quantidade = enc.produtos[i].quantidade;
      this.produtos.push(produtoEncomenda);
    }

    for (let j = 0; j < this.planeamentoDia.length; j++) {
      var plano = this.planeamentoDia[j];
      if (enc._id == plano.idEnc) {
        this.planeamentoEncomenda.push({ produto: plano.produto, inicio: plano.inicio, fim: plano.fim, linha: plano.linha });
      }
    }


    let shand = document.getElementById('verDetalhes-modal');
    if (shand) {
      shand.style.display = "flex";
    }
  }

  fecharDetalhes() {
    let shand = document.getElementById('verDetalhes-modal');
    if (shand) {
      shand.style.display = "none";
    }
    this.produtos = [];
    this.planeamentoEncomenda = [];
  }

}
