export class Produto {
    id: string;
    preco: number;
    descricao: string;
    planoFabrico: string[];
    duracao: number;
}