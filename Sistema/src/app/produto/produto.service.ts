import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Produto } from './produto';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';

interface ProdutoDTO {
  preco: number;
  descricao: string;
  planoFabrico: string[];
  duracao: number;
}

@Injectable({
  providedIn: 'root'
})

export class ProdutoService {

  private mdpUrl = 'https://arqsi-3dc-3-mdp.azurewebsites.net/api/MDP/Produto/';
  private ppDuracaoUrl = 'http://127.0.0.1:8080/planeamento/duracao';

  constructor(private httpClient: HttpClient) { }

  addProduto(prec: number, desc: string, pf: string[], duracao: number): Observable<any> {
    let prodDto: ProdutoDTO = { preco: prec, descricao: desc, planoFabrico: pf, duracao: duracao };
    return this.httpClient.post(this.mdpUrl, prodDto).pipe(map(this.extractData));
  }

  duracaoFabrico(plano, token: string) {
    return this.httpClient.post(this.ppDuracaoUrl, plano, { headers: new HttpHeaders({ 'x-access-token': token }) }).pipe(map(this.extractData));
  }

  getProduto(idProd: string): Observable<any> {
    return this.httpClient.get(this.mdpUrl + idProd);
  }

  getPlanoFabrico(idProd: string): Observable<any> {
    return this.httpClient.get(this.mdpUrl + "PlanoFabrico/" + idProd).pipe(map(this.extractData));
  }

  getTodosProdutos(): Observable<any> {
    return this.httpClient.get(this.mdpUrl).pipe(map(this.extractData));
  }

  private extractData(res: Response) {
    return res || {};
  }

  private handleError(err: HttpErrorResponse) {
    if (err.error instanceof ErrorEvent) {
      console.error('An error occurred: ', err.error.message);
    }
    else {
      console.error(
        `Web Api returned code ${err.status}, ` + ` Response body was: ${err.error}`
      );
    }
    return Observable.throw(err);
  }
}