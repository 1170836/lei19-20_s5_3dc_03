import { Component, OnInit } from '@angular/core';
import { Produto } from './produto';
import { ProdutoService } from './produto.service';
import { HttpErrorResponse } from '@angular/common/http'

@Component({
  selector: 'app-produto',
  templateUrl: './produto.component.html',
  styleUrls: ['./produto.component.css']
})

export class ProdutoComponent implements OnInit {

  produto: Produto;
  ProdutoDesc: string;
  statusMessage: string;
  message: string = "";
  private token: string;

  constructor(private produtoServ: ProdutoService) { }

  ngOnInit() {
    this.token = window.sessionStorage.getItem('tokenAdmin');
    if (this.token == null) {
      window.location.replace('login');
    }
  }

  private addProduto(preco: number, descricaoProduto: string, planoFabrico: string): void {
    this.produtoServ.duracaoFabrico(planoFabrico.split(","), this.token).subscribe((data: { duracao: number }) => {
      console.log(data);
      var duracao: number = data.duracao;
      this.produtoServ.addProduto(preco, descricaoProduto, planoFabrico.split(","), duracao).subscribe(data => {
        this.produto = data;
        alert("Produto adicionado com sucesso");
      },
        error => { this.handleError(error) });
    })

  }

  private getProduto(idProd: string): void {
    this.produtoServ.getProduto(idProd).subscribe(data => {
      this.produto = data;
      alert("Preço: " + this.produto.preco + "\nDescrição: " + this.produto.descricao + "\nDuracão de Fabrico: " + this.produto.duracao + " segundos");
    },
      error => { this.handleError(error) });
  }

  private getPlanoFabrico(id: string): void {
    this.produtoServ.getPlanoFabrico(id).subscribe(data => {
      this.produto = data;

      alert("Plano de Fabrico: " + this.produto.planoFabrico);
    },
      error => { this.handleError(error) });
  }

  handleError(error: HttpErrorResponse) {
    if (error.status == 400) {
      alert("Pedido inválido");
    } else if (error.status == 404) {
      alert("Produto não existe")
    } else {
      alert("Serviço indisponível, tente mais tarde.");
    }

  }

}
