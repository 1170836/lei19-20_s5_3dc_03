var scene, camera, orbit, renderer, mesh, loader;
var meshFloor, ambientLight, light;

var machines = [];

var keyboard = {};
var player = { height: 1.8, speed: 0.2, turnSpeed: Math.PI * 0.01 };
var USE_WIREFRAME = false;


function init() {
	scene = new THREE.Scene();
	camera = new THREE.PerspectiveCamera(90, 1280 / 720, 0.1, 1000);

	//Chão
	meshFloor = new THREE.Mesh(
		new THREE.BoxGeometry(200, 200, 0),
		new THREE.MeshPhysicalMaterial({ color: 0xffffff })
	);
	meshFloor.rotation.x -= Math.PI / 2;
	meshFloor.receiveShadow = true;
	scene.add(meshFloor);

	var ceiling = new THREE.Mesh(
		new THREE.BoxGeometry(200, 200, 0.2),
		new THREE.MeshPhongMaterial({ color: 0xffffff })
	);
	ceiling.rotation.x -= Math.PI / 2;
	ceiling.position.set(0, 50, 0);
	ceiling.receiveShadow = true;
	scene.add(ceiling);

	//Paredes
	var geometry = new THREE.BoxGeometry(200, 50, 0);
	var material = new THREE.MeshPhongMaterial({ color: 0xaaaaaa });

	var rightWall = new THREE.Mesh(geometry, material);
	rightWall.rotation.y -= Math.PI / 2;
	rightWall.position.set(-100, 25, 0);
	rightWall.receiveShadow = true;
	scene.add(rightWall);

	var leftWall = new THREE.Mesh(geometry, material);
	leftWall.rotation.y -= Math.PI / 2;
	leftWall.position.set(100, 24.9, 0);
	leftWall.receiveShadow = true;
	scene.add(leftWall);

	var backWall = new THREE.Mesh(geometry, material);
	backWall.position.set(0, 25, -100);
	backWall.receiveShadow = true;
	scene.add(backWall);

	var frontWall = new THREE.Mesh(geometry, material);
	frontWall.position.set(0, 25, 100);
	frontWall.receiveShadow = true;
	scene.add(frontWall);

	//Iluminação
	ambientLight = new THREE.AmbientLight(0xffffff, 0.7);
	scene.add(ambientLight);

	light = new THREE.PointLight(0xffffff, 1, 25);
	light.position.set(0, 20, 0);
	light.castShadow = true;
	light.shadow.camera.near = 0.1;
	light.shadow.camera.far = 25;
	scene.add(light);

	var x = -14;
	// Máquinas
	for (var i = 0; i < 8; i++) {
		var crate = new THREE.Mesh(
			new THREE.BoxGeometry(3, 3, 3),
			new THREE.MeshPhongMaterial({
				color: 0xabcabc,
			})
		);
		scene.add(crate);
		crate.receiveShadow = true;
		crate.castShadow = true;

		crate.position.y = 1.5;
		crate.position.x = x;
		if (i <= 3) {
			crate.position.z = -10;
		} else {
			crate.position.z = 10;
		}

		x += 9;

		if (i == 3) {
			x = -14;
		}

		scene.add(crate);
		machines.push(crate);
	}

	camera.position.set(0, player.height, -5);
	camera.lookAt(new THREE.Vector3(0, player.height, 0));

	renderer = new THREE.WebGLRenderer();
	renderer.setSize(1920, 1000);

	renderer.shadowMap.enabled = true;
	renderer.shadowMap.type = THREE.BasicShadowMap;

	document.body.appendChild(renderer.domElement);

	//Adicionar/Remover Paredes
	document.getElementById('paredes').addEventListener("click", function () {
		rightWall.visible = !rightWall.visible;
		leftWall.visible = !leftWall.visible;
		backWall.visible = !backWall.visible;
		frontWall.visible = !frontWall.visible;
		ceiling.visible = !ceiling.visible;
	});

	//Drag and Drop
	/*var controls = new THREE.DragControls(machines, camera, renderer.domElement);
	controls.addEventListener('dragstart', dragStartCallback);
	controls.addEventListener('dragend', dragendCallback);*/

	orbit = new THREE.OrbitControls(camera, renderer.domElement);
	//camera.position.set(0,10,19);
	orbit.update();

	//orbit.addEventListener( 'change', renderer ); // call this only in static scenes (i.e., if there is no animation loop)
	orbit.enableDamping = true; // an animation loop is required when either damping or auto-rotation are enabled
	orbit.dampingFactor = 0.05;
	orbit.screenSpacePanning = false;
	orbit.minDistance = 10;
	orbit.maxDistance = 50;
	orbit.maxPolarAngle = Math.PI / 2;

	/*loader = new THREE.ColladaLoader();
	loader.load('model.DAE', function colladaReady(collada) {
		player = collada.scene;
		scene.add(player);
	});*/

	animate();

	//Orbit
	controls = new THREE.OrbitControls(camera, renderer.domElement);
	controls.update();
}

function dragStartCallback(event) {
	startColor = event.object.material.color.getHex();
	event.object.material.color.setHex(0x000000);
}

function dragendCallback(event) {
	event.object.material.color.setHex(startColor);
}
function IsInMachine(x, z, y) {
	for (var i = 0; i < machines.length; i++) {
		if (z <= machines[i].position.z + 2.9 && z >= machines[i].position.z - 2.9 &&
			x <= machines[i].position.x + 2.9 && x >= machines[i].position.x - 2.9 &&
			y <= machines[i].position.y + 2.9) {

			return machines[i];
		}
	}
	return null;
}


function animate() {
	requestAnimationFrame(animate);

	if (camera.position.x < 99 && camera.position.x > -99 && camera.position.z < 99 && camera.position.z > -99) {
		var maquina = IsInMachine(camera.position.x, camera.position.z, camera.position.y);
		if (maquina == null) {
			if (keyboard[87]) { // W 
				camera.position.x -= Math.sin(camera.rotation.y) * player.speed;
				camera.position.z -= -Math.cos(camera.rotation.y) * player.speed;
			}
			if (keyboard[83]) { // S 
				camera.position.x += Math.sin(camera.rotation.y) * player.speed;
				camera.position.z += -Math.cos(camera.rotation.y) * player.speed;
			}
			if (keyboard[65]) { // A 
				camera.position.x += Math.sin(camera.rotation.y + Math.PI / 2) * player.speed;
				camera.position.z += -Math.cos(camera.rotation.y + Math.PI / 2) * player.speed;
			}
			if (keyboard[68]) { // D 
				camera.position.x += Math.sin(camera.rotation.y - Math.PI / 2) * player.speed;
				camera.position.z += -Math.cos(camera.rotation.y - Math.PI / 2) * player.speed;
			}
		} else {
			var difz = camera.position.z - maquina.position.z;
			var difx = camera.position.x - maquina.position.x;
			var dify = camera.position.y - maquina.position.y;

			if ((difx * difx) > (difz * difz) && ((difx * difx) > (dify * dify))) {
				if (difx > 0) {
					camera.position.z += 0.01;
					camera.position.x += 0.01;
				} else {
					camera.position.z -= 0.01;
					camera.position.x -= 0.01;
				}
			} else {
				if ((difz * difz) > (dify * dify)) {
					if (difz > 0) {
						camera.position.x += 0.01;
						camera.position.z += 0.01;
					} else {
						camera.position.x -= 0.01;
						camera.position.z -= 0.01;
					}
				}
				else {
					camera.position.y += 0.01;
				}
			}

		}
	} else {
		if (camera.position.x >= 99) {
			camera.position.x -= 0.01;
		} else if (camera.position.x <= -99) {
			camera.position.x += 0.01;
		} else if (camera.position.z >= 99) {
			camera.position.z -= 0.01;
		} else if (camera.position.z <= -99) {
			camera.position.z += 0.01;
		}

	}
	if (keyboard[37]) { // Seta Esquerda
		camera.rotation.y -= player.turnSpeed;
	}
	if (keyboard[39]) { // Seta Direita
		camera.rotation.y += player.turnSpeed;
	}
	if (keyboard[82]) {
		camera.position.set(0, player.height, 0);
	}

	if (camera.position.y <= player.height) {
		camera.position.y = player.height;
	}


	if (camera.position.z >= 99) {
		camera.position.z = 99;
	}

	/* if (keyboard[80]) {
		orbit.enabled = false;
	}
	if (keyboard[79]) {
		orbit.enabled = true;
	} */
	orbit.update();
	renderer.render(scene, camera);
}


function keyDown(event) {
	keyboard[event.keyCode] = true;
}

function keyUp(event) {
	keyboard[event.keyCode] = false;
}

window.addEventListener('keydown', keyDown);
window.addEventListener('keyup', keyUp);

window.onload = init;


