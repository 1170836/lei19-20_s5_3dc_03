import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VisualizacaoComponent } from './visualizacao.component';
import {HttpClientModule} from '@angular/common/http';

describe('VisualizacaoComponent', () => {
  let component: VisualizacaoComponent;
  let fixture: ComponentFixture<VisualizacaoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VisualizacaoComponent ],
      imports:[HttpClientModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VisualizacaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  afterEach(() => {
    fixture.destroy();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
    
  });
});
