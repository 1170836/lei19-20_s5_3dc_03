import * as THREE from './three.min.js';
import { OrbitControls } from './OrbitControls.js';
import { Injectable, ElementRef, OnDestroy, NgZone, Output } from '@angular/core';
import { MaquinaService } from '../maquina/maquina.service';
import { LinhaProducaoService } from '../linha-producao/linha-producao.service';
import { ColladaLoader } from './ColladaLoader.js';
import { ProdutoService } from '../produto/produto.service.js';
import { ThenableWebDriver, EventEmitter } from 'selenium-webdriver';
import { ThrottleConfig } from 'rxjs/internal/operators/throttle';
import { DragControls } from './DragControls.js';
import { element } from 'protractor';
import { PlaneamentoService } from '../planeamento/planeamento.service.js';
import { EVENT_MANAGER_PLUGINS } from '@angular/platform-browser';

interface MaquinaVisualizacaoLinhaDTO {
    marca: string;
    modelo: string;
    nSequencia: number;
    linhaProducao: string;
    numeroSerie: string;
}

interface MaquinaLinhaDTO {
    id: string;
    marca: string;
    modelo: string;
    descricaoTipoMaquina: string;
}

interface LinhaProducaoDTO {
    id: string;
    maquinas: MaquinaVisualizacaoLinhaDTO[],
    x: number;
    y: number;
}

interface ProdutoDTO {
    preco: number;
    descricao: string;
    plano: string[];
}

@Injectable({
    providedIn: 'root'
})
export class VisualizacaoService implements OnDestroy {
    private canvas: HTMLCanvasElement;
    private renderer: THREE.WebGLRenderer;
    private camera: THREE.PerspectiveCamera;
    private scene: THREE.Scene;
    private light: THREE.AmbientLight;
    private group: THREE.Group;
    private groupIntersect: THREE.Group;
    private loader: ColladaLoader;

    private drag: DragControls;
    private groupMaquinas: THREE.Group;

    private raycaster: THREE.Raycaster;
    private raycaster2: THREE.Raycaster;

    private mouse: THREE.Vector2;

    private latestMouseProjection;
    private tooltipsArray = [];
    private linhasProducaoArray = [];
    private linhaArray = [];
    private tooltipsArrayProdutos = [];

    private meshFloor: THREE.Mesh;
    private orbit: OrbitControls;
    private keyboard = {};

    private document;

    readonly playerHeight = 2;
    readonly maquinasMaxPorLinha = 12;

    private frameId: number = null;
    private token: string;
    private planeamentoDia = [];

    public constructor(private ngZone: NgZone, private maquinaServ: MaquinaService, private linhaServ: LinhaProducaoService,
        private prodServ: ProdutoService, private planeamentoServ: PlaneamentoService) {
        this.token = window.sessionStorage.getItem('tokenAdmin');
    }

    public ngOnDestroy() {
        if (this.frameId != null) {
            cancelAnimationFrame(this.frameId);
        }
    }

    createScene(canvas: ElementRef<HTMLCanvasElement>): void {
        this.keyboard = new Array();

        this.canvas = canvas.nativeElement;

        this.renderer = new THREE.WebGLRenderer({
            canvas: this.canvas,
            alpha: true,    // transparent background
            antialias: true // smooth edges
        });


        this.renderer.setSize(window.innerWidth, window.innerHeight);
        this.renderer.shadowMap.enabled = true;
        this.renderer.shadowMap.type = THREE.BasicShadowMap;

        this.scene = new THREE.Scene();



        this.linhaServ.getLinhasProducao().subscribe(data => {
            var linhas: LinhaProducaoDTO[] = data;


            linhas.forEach(linha => {
                linha.maquinas.forEach(maquina => {
                    this.apresentaMaquina(linha.id, maquina, linha.x, linha.y);
                });
                this.apresentaCaixaAberta(linha.id, linha.x, linha.y, linha.maquinas.length);
                this.iniciarProducao(linha);
                this.linhaArray.push(linha);
            });
        });

        this.linhaServ.getMaquinas().subscribe(data => {
            var nMaquina: number = 0;
            var maquinas: MaquinaLinhaDTO[] = data;
            maquinas.forEach(element => {
                this.apresentaMaquinaLinha(element, nMaquina);
                nMaquina++;
            });
        });

        this.prodServ.getTodosProdutos().subscribe(data => {
            var nProduto: number = 0;
            var produtos: ProdutoDTO[] = data;
            produtos.forEach(element => {
                this.apresentaProduto(element, nProduto);
                nProduto++;
            });
        });

        this.camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);
        this.camera.position.set(0, 50 * Math.sin(Math.PI / 6), 0);
        this.scene.add(this.camera);

        this.light = new THREE.AmbientLight(0xffffff);
        this.scene.add(this.light);

        this.light = new THREE.PointLight(0xffffff, 0.3, 50);
        this.light.position.set(-25, 15, 25);
        this.scene.add(this.light);

        this.light = new THREE.PointLight(0xffffff, 0.3, 50);
        this.light.position.set(25, 30, 25);
        this.scene.add(this.light);

        this.light = new THREE.PointLight(0xffffff, 0.3, 50);
        this.light.position.set(-25, 30, -25);
        this.scene.add(this.light);

        this.light = new THREE.PointLight(0xffffff, 0.3, 50);
        this.light.position.set(25, 15, -25);
        this.scene.add(this.light);

        let directionalLight = new THREE.DirectionalLight(0xffffff, 0.3);
        directionalLight.castShadow = true;
        directionalLight.shadowDarkness = 0.5;
        directionalLight.shadowCameraVisible = true;
        this.scene.add(directionalLight);

        this.group = new THREE.Group();
        this.groupIntersect = new THREE.Group();
        this.groupMaquinas = new THREE.Group();

        this.scene.add(this.groupMaquinas);
        this.scene.add(this.group);
        this.scene.add(this.groupIntersect);

        this.carregarCandeeiro(1, 1);
        this.carregarCandeeiro(1, -1);
        this.carregarCandeeiro(-1, 1);
        this.carregarCandeeiro(-1, -1);

        //Chão
        var geometry = new THREE.BoxGeometry(100, 100, 0);
        var texture = new THREE.TextureLoader().load("/assets/factory_floor.jpg");
        texture.wrapS = texture.wrapT = THREE.RepeatWrapping;
        texture.offset.set(0, 0);
        texture.repeat.set(100, 100);
        var material = new THREE.MeshPhysicalMaterial({ map: texture });
        this.meshFloor = new THREE.Mesh(geometry, material);
        this.meshFloor.rotation.x = Math.PI / 2;
        this.scene.add(this.meshFloor);

        //Teto
        var ceiling = new THREE.Mesh(
            new THREE.BoxGeometry(100, 100, 0),
            new THREE.MeshPhysicalMaterial({ color: 0xffffff })
        );
        ceiling.rotation.x -= Math.PI / 2;
        ceiling.position.set(0, 30, 0);
        ceiling.receiveShadow = true;
        this.scene.add(ceiling);

        //Paredes
        var wallTexture = new THREE.TextureLoader().load("/assets/paredes.jpg");
        wallTexture.wrapS = wallTexture.wrapT = THREE.RepeatWrapping;
        wallTexture.offset.set(0, 0);
        wallTexture.repeat.set(100, 30);

        var geometryWalls = new THREE.BoxGeometry(100, 30, 0);
        var materialWalls = new THREE.MeshPhysicalMaterial({ map: wallTexture });

        var rightWall = new THREE.Mesh(geometryWalls, materialWalls);
        rightWall.rotation.y -= Math.PI / 2;
        rightWall.position.set(-50, 15, 0);
        rightWall.receiveShadow = true;
        this.group.add(rightWall);

        var leftWall = new THREE.Mesh(geometryWalls, materialWalls);
        leftWall.rotation.y -= Math.PI / 2;
        leftWall.position.set(50, 15, 0);
        leftWall.receiveShadow = true;
        this.group.add(leftWall);

        var backWall = new THREE.Mesh(geometryWalls, materialWalls);
        backWall.position.set(0, 15, -50);
        backWall.receiveShadow = true;
        this.group.add(backWall);

        var frontWall = new THREE.Mesh(geometryWalls, materialWalls);
        frontWall.position.set(0, 15, 50);
        frontWall.receiveShadow = true;
        this.group.add(frontWall);

        this.raycaster = new THREE.Raycaster();
        this.raycaster2 = new THREE.Raycaster();

        this.mouse = new THREE.Vector2(0, 0);

        this.scene.add(this.group);

        this.orbit = new OrbitControls(this.camera, this.renderer.domElement);
        this.orbit.dampingFactor = 0.05;
        this.orbit.screenSpacePanning = false;
        this.orbit.minDistance = 15;
        this.orbit.maxDistance = 30;
        this.orbit.maxPolarAngle = Math.PI / 3;
        this.orbit.minPolarAngle = Math.PI / 5;
        this.orbit.update();


        document.addEventListener('keydown', (event) => {
            this.keyboard[event.keyCode] = true;
            this.verificaJanela();
        }, false);

        document.addEventListener('mousedown', (event) => {
            setTimeout(function () { event }, 0);
            document.addEventListener('mousemove', (event) => {
                this.verificacoesPosicao();
            })
        }, false);

        this.drag = new DragControls(this.groupIntersect.children, this.camera, this.renderer.domElement);

        let arrayPecas;
        let desc;
        let id_linha;

        this.drag.addEventListener('dragstart', function (event) {
            arrayPecas = [];

            console.log(event.object)

            this.tooltipsArray.forEach(element => {
                if (event.object.uuid == element.modelo.uuid) {
                    desc = element.descricao;
                }
            }
            );
            this.tooltipsArray.forEach(element => {
                if (desc == element.descricao ) {
                    arrayPecas.push(element)
                }
            });
            this.orbit.enabled = false;
        }.bind(this));

        let desc_maq

        this.drag.addEventListener('dragend', function (event) {
            console.log(arrayPecas)
            arrayPecas.forEach(element => {
                if (element.modelo.__proto__.constructor.name == "ea") {
                    element.modelo.position.x = event.object.position.x;
                    element.modelo.position.y = event.object.position.y;
                    element.modelo.position.z = 0;
                    desc_maq = element.descricao;
                }
            })
            let n_seq = parseInt(desc_maq.split(" || ")[2].split(": ")[1]);

            this.linhasProducaoArray.forEach(element => {
                if (element.modelo.uuid == event.object.uuid) {
                    id_linha = element.linhaProducao;
                    return;
                }
            })
            let linha_des
            this.linhaArray.forEach(linha => {
                if (linha.id == id_linha) {
                    linha_des = linha;
                }
            });

            console.log(linha_des)
            console.log("LINHA_DES_X= ", linha_des.x)
            console.log("EVENT_OBJECT_POSITION_X= ", event.object.position.x)
            console.log("(100/19)", 100 / 19)
            console.log("*event", event.object.position.x*100/19)
            console.log("LINHA_DES_y= ", linha_des.y)
            console.log("EVENT_OBJECT_POSITION_y= ", event.object.position.y)
            console.log("*event", event.object.position.y*100/19)
            let x_des, y_des;
            x_des = (linha_des.x + (event.object.position.x) * (100 / 19));
            y_des = -(linha_des.y + (event.object.position.y) * (100 / 19));

            console.log(this.linhaArray)
            console.log(event.object.position)



            console.log(x_des);
            console.log(y_des);

            this.linhaServ.updatePosicaoLinha(linha_des.id, parseInt(x_des.toString()), parseInt(y_des.toString())).subscribe(data => {
                this.updateLinha(linha_des.id)
            })
            x_des = y_des = 0
            arrayPecas = [];
            this.orbit.enabled = true;
        }.bind(this));

        document.addEventListener('mouseip', (event) => {
            this.verificacoesPosicao();
        }, false);

        window.addEventListener("mousemove", (event) => {
            this.setMouseMove(event);
            this.setPickPosition(event);
        }, false);
    }

    setMouseMove(event) {
        this.mouse.x = (event.clientX / window.innerWidth) * 2 - 1;
        this.mouse.y = - ((event.clientY - this.canvas.offsetTop) / this.canvas.height) * 2 + 1;
        this.redraw();
    }
    verificacoesPosicao(): void {
        if (!(this.camera.position.x < 49 && this.camera.position.x > -49 && this.camera.position.z < 49 && this.camera.position.z > -49)) {

            //verificacao cantos
            if (this.camera.position.x <= -49 && this.camera.position.z <= -49) {
                this.camera.position.z = -48;
                this.camera.position.x = -48;
            } else if (this.camera.position.x <= -49 && this.camera.position.z >= 49) {
                this.camera.position.z = -48;
                this.camera.position.x = 48;
            } else if (this.camera.position.x >= 49 && this.camera.position.z <= -49) {
                this.camera.position.z = 48;
                this.camera.position.x = -48;
            } else if (this.camera.position.x >= 49 && this.camera.position.z >= 49) {
                this.camera.position.z = 48;
                this.camera.position.x = 48;
            }
            //verificacao paredes
            else if (this.camera.position.x >= 49) {
                this.camera.position.x = 49;
            } else if (this.camera.position.x <= -49) {
                this.camera.position.x = -49;
            } else if (this.camera.position.z >= 49) {
                this.camera.position.z = 49;
            } else if (this.camera.position.z <= -49) {
                this.camera.position.z = -49;
            }
        }
    }

    apresentaProduto(produto: ProdutoDTO, nProduto: number) {
        var x: number = (nProduto % 3) * 4.5;
        var y: number = Math.floor(nProduto / 3) * 10;
        this.carregarPrateleiras(produto, x, y);
    }

    apresentaMaquinaLinha(maquina: MaquinaLinhaDTO, nMaquina: number): void {

        this.loader.load('/assets/abb_irb52_7_120.dae', function (collada) {
            let dae = collada.scene;
            dae.traverse(function (child) {
                if (child.isMesh) {
                    // model does not have normals
                    child.material.flatShading = true;
                }
            });
            dae.scale.x = dae.scale.y = dae.scale.z = 5.0;
            dae.updateMatrix();


            dae.receiveShadow = true;
            dae.castShadow = true;

            var nLinha: number = Math.floor(nMaquina / this.maquinasMaxPorLinha);

            dae.position.y = 0;
            dae.position.x = -40 + (10 * nLinha);
            dae.position.z = 45 - (5 * ((nMaquina % this.maquinasMaxPorLinha)));

            var descricao: string = this.maquinaSemLinhaToString(maquina);
            this.adicionarChildren(dae.children, descricao);
            this.tooltipsArray.push({ modelo: dae, descricao: descricao });

            this.groupIntersect.add(dae);
        }.bind(this));

    }


    apresentaMaquina(idLinha: string, maq: MaquinaVisualizacaoLinhaDTO, x: number, y: number): void {
        this.loader = new ColladaLoader();
        this.loader.load('/assets/abb_irb52_7_120.dae', function (collada) {
            let dae = collada.scene;
            dae.traverse(function (child) {
                if (child.isMesh) {
                    // model does not have normals
                    child.material.flatShading = true;
                }
            });
            dae.scale.x = dae.scale.y = dae.scale.z = 5.0;
            dae.updateMatrix();

            dae.receiveShadow = false;
            dae.castShadow = true;

            dae.position.y = 0;
            dae.position.x = x + ((maq.nSequencia - 1) * 7);
            dae.position.z = y;

            this.loader.load('/assets/conveyor/model2.dae', function (collada2) {
                let dae2 = collada2.scene;
                dae2.traverse(function (child) {
                    if (child.isMesh) {
                        // model does not have normals
                        child.material.flatShading = true;
                    }
                });

                dae2.rotation.z = Math.PI / 2;
                dae2.scale.z = 0.05;
                dae2.scale.y = 0.04;
                dae2.scale.x = 0.1;
                dae2.updateMatrix();

                dae2.receiveShadow = false;
                dae2.castShadow = true;

                dae2.position.y = 3.1;
                dae2.position.x = dae.position.x + 1.5;
                dae2.position.z = dae.position.z - 1.1;

                this.linhasProducaoArray.push({ modelo: dae2, linhaProducao: idLinha })
                this.adicionarChildrenLinha(dae2.children, idLinha);

                this.scene.add(dae2);
            }.bind(this));

            this.groupMaquinas.add(dae);

            var descricao: string = this.maquinaLinhaToString(maq);
            this.adicionarChildren(dae.children, descricao);
            this.adicionarChildrenLinha(dae.children, idLinha);

            this.tooltipsArray.push({ modelo: dae, descricao: descricao });
            this.linhasProducaoArray.push({ modelo: dae, linhaProducao: idLinha })
            this.groupIntersect.add(dae);
        }.bind(this));

    }

    adicionarChildren(dae, descricao: string): void {

        if (dae == null || dae == undefined) {
            return;
        }

        dae.forEach(element => {
            this.tooltipsArray.push({ modelo: element, descricao: descricao });
            this.adicionarChildren(element.children, descricao);
        });
    }

    adicionarChildrenLinha(dae, idLinha: string): void {

        if (dae == null || dae == undefined) {
            return;
        }

        dae.forEach(element => {
            this.linhasProducaoArray.push({ modelo: element, linhaProducao: idLinha });
            this.adicionarChildrenLinha(element.children, idLinha);
        });


    }

    carregarCandeeiro(x_value: number, z_value: number): void {
        this.loader = new ColladaLoader();

        this.loader.load('/assets/cand.dae', function (collada, ) {
            let dae1 = collada.scene;
            dae1.traverse(function (child) {
                if (child.isMesh) {
                    // model does not have normals
                    child.material.flatShading = true;
                }
            });
            dae1.scale.x = dae1.scale.z = 0.3;
            dae1.scale.y = 0.5;
            dae1.updateMatrix();

            dae1.receiveShadow = true;
            dae1.castShadow = true;

            dae1.position.y = 25;
            dae1.position.x = 25 * x_value;
            dae1.position.z = 25 * z_value;
            this.scene.add(dae1);
        }.bind(this));
    }

    carregarPrateleiras(produto: ProdutoDTO, x_value: number, z_value: number): void {
        this.loader = new ColladaLoader();

        this.loader.load('/assets/shelf.dae', function (collada) {
            let dae1 = collada.scene;
            dae1.traverse(function (child) {
                if (child.isMesh) {
                    // model does not have normals
                    child.material.flatShading = true;
                }
            });
            dae1.scale.x = dae1.scale.y = dae1.scale.z = 0.05;
            dae1.updateMatrix();

            dae1.receiveShadow = true;
            dae1.castShadow = true;

            dae1.position.y = 0;
            dae1.position.x = -49 + x_value;
            dae1.position.z = -40 + z_value;

            for (var i = 0; i < 3; i++) {
                this.carregarCaixa(produto, dae1.position.x, dae1.position.z, i);
            }

            this.scene.add(dae1);
        }.bind(this));
    }

    carregarCaixa(produto: ProdutoDTO, x_value: number, z_value: number, numCaixa: number): void {
        this.loader = new ColladaLoader();

        this.loader.load('/assets/caixas.dae', function (collada) {
            let dae1 = collada.scene;
            dae1.traverse(function (child) {
                if (child.isMesh) {
                    // model does not have normals
                    child.material.flatShading = true;
                }
            });
            dae1.scale.x = dae1.scale.y = 0.09;
            dae1.scale.z = 0.05;
            dae1.rotation.z = - Math.PI / 2;

            dae1.updateMatrix();

            dae1.receiveShadow = true;
            dae1.castShadow = true;

            var descricao: string = this.produtoToString(produto);
            this.adicionarChildren(dae1.children, descricao);
            this.tooltipsArray.push({ modelo: dae1, descricao: descricao });


            dae1.position.y = 2.7 - (numCaixa * 0.8);
            dae1.position.x = x_value + 1;
            dae1.position.z = z_value - 1;

            this.groupIntersect.add(dae1);

        }.bind(this));
    }

    apresentaCaixaAberta(idLinha: string, x: number, z: number, nMaquinas: number): void {
        this.loader = new ColladaLoader();

        this.loader.load('/assets/caixaAberta/model.dae', function (collada) {
            let dae1 = collada.scene;
            dae1.traverse(function (child) {
                if (child.isMesh) {
                    // model does not have normals
                    child.material.flatShading = true;
                }
            });
            dae1.scale.x = 0.18;
            dae1.scale.y = 0.2;
            dae1.scale.z = 0.07;
            dae1.rotation.z = - Math.PI / 2;

            dae1.updateMatrix();

            dae1.receiveShadow = true;
            dae1.castShadow = true;

            dae1.position.y = 0.51;
            dae1.position.x = x + ((nMaquinas - 1) * 7) + 1.1;
            dae1.position.z = z - 4.2;

            this.adicionarChildrenLinha(dae1.children, idLinha);
            this.linhasProducaoArray.push({ modelo: dae1, linhaProducao: idLinha })

            this.scene.add(dae1);

        }.bind(this));
    }

    iniciarProducao(linha: LinhaProducaoDTO) {
        this.apresentaProdutoMaquina(linha);
    }

    mexerProduto(dae) {
        dae.position.x = dae.position.x + 0.1;
    }

    async apresentaProdutoMaquina(linha: LinhaProducaoDTO) {
        var prod: number = Math.floor(Math.random() * 3) + 1;

        var prodString: string;
        if (prod == 1) {
            prodString = "garfo";
        } else if (prod == 2) {
            prodString = "faca";
        } else {
            prodString = "colher";
        }

        this.planeamentoServ.produtoEmProducaoLinha(this.token,linha.id).subscribe(data => {
            
            var produtoApresentar = data.planeamento; 

            if (produtoApresentar != null) {
                return this.apresentaProdutoMaquina2(prodString, linha, produtoApresentar);
            }
        });


    }

    apresentaProdutoMaquina2(produto: string, linha: LinhaProducaoDTO, produtoApresentar) {
        this.loader = new ColladaLoader();


        this.loader.load('/assets/produtos/' + produto + '.dae', function (collada) {
            let dae1 = collada.scene;
            dae1.traverse(function (child) {
                if (child.isMesh) {
                    // model does not have normals
                    child.material.flatShading = true;
                }
            });
            dae1.scale.x = dae1.scale.y = dae1.scale.z = 0.2;

            dae1.rotation.z = - Math.PI / 2;

            dae1.updateMatrix();

            dae1.receiveShadow = true;
            dae1.castShadow = true;

            dae1.position.y = 3.5;
            dae1.position.x = linha.x - 6;
            dae1.position.z = linha.y - 3;

            this.adicionarChildrenLinha(dae1.children, linha.id);
            this.linhasProducaoArray.push({ modelo: dae1, linhaProducao: linha.id })

            var descricao: string = "Produto: " + produtoApresentar.produto +
                "|| Encomenda: " + produtoApresentar.idEnc +
                "|| Início: " + produtoApresentar.inicio +
                "|| Fim: " + produtoApresentar.fim;

            this.adicionarChildren(dae1.children, descricao);
            this.tooltipsArray.push({ modelo: dae1, descricao: descricao });
            console.log(dae1);

            this.groupIntersect.add(dae1);
            /* this.scene.add(dae1); */

            var maxLinha: number = linha.x + (linha.maquinas.length - 1) * 7;

            var mexer = setInterval(() => {
                dae1.position.x = dae1.position.x + 0.05;

                if (dae1.position.x >= maxLinha) {

                    clearInterval(mexer);
                    dae1.rotation.y = dae1.rotation.y + 0.3;

                    var rotateZ = parseFloat((Math.random() * (0.120 - 0.0200) + 0.0200).toFixed(4));

                    if (Math.random() * 4 % 2 == 0) {
                        rotateZ = rotateZ * -1;
                    }

                    var cair = setInterval(() => {

                        dae1.position.x = dae1.position.x + 0.15;
                        dae1.position.y = dae1.position.y - 0.2;
                        dae1.position.z = dae1.position.z + rotateZ;

                        if (dae1.position.x >= maxLinha + 2.5 || dae1.position.y <= 1) {
                            clearInterval(cair);
                            if (dae1.visible == false) {
                                return;
                            } else {
                                this.apresentaProdutoMaquina(linha);
                            }
                        }

                    }, 100);
                }
            }, 100);

        }.bind(this));
    }


    getCanvasRelativePosition(event) {
        const rect = this.canvas.getBoundingClientRect();
        return {
            x: event.clientX - rect.left,
            y: event.clientY - rect.top,
        };
    }

    setPickPosition(event) {
        const pos = this.getCanvasRelativePosition(event);
        this.mouse.x = (pos.x / this.canvas.clientWidth) * 2 - 1;
        this.mouse.y = (pos.y / this.canvas.clientHeight) * -2 + 1;  // note we flip Y
    }


    verificaJanela(): void {
        //Reset Posição
        if (this.keyboard[82]) {
            this.camera.position.set(0, 50 * Math.sin(Math.PI / 6), 0);
        }
    }

    animate(): void {


        this.ngZone.runOutsideAngular(() => {
            if (document.readyState !== 'loading') {
                this.render();
            } else {
                window.addEventListener('DOMContentLoaded', () => {
                    this.render();
                });
            }

            window.addEventListener('resize', () => {
                this.resize();
            });
        });


    }

    redraw = function () {
        this.orbit.update();
        this.raycaster.setFromCamera(this.mouse, this.camera)

        var intersects = this.raycaster.intersectObjects(this.groupIntersect.children, true);

        if (intersects.length == 0) {
            this.hideTooltip();
        }

        if (intersects.length == 1) {
            intersects.forEach(function (element) {
                var object = element.object;
                if (object instanceof THREE.Mesh) {
                    this.latestMouseProjection = intersects[0].point;
                    this.tooltipsArray.forEach(element => {
                        if (object.uuid == element.modelo.uuid) {
                            this.showTooltip(element.descricao);
                            return;
                        }
                    });
                }
            }.bind(this));
        } 

    }

    render() {
        this.frameId = requestAnimationFrame(() => {
            this.render();
        });

        this.renderer.render(this.scene, this.camera);
        this.renderer.shadowMap.enabled = true;
        this.renderer.shadowMap.type = THREE.PCFSoftShadowMap;
    }

    resize() {
        const width = window.innerWidth;
        const height = window.innerHeight;

        this.camera.aspect = width / height;
        this.camera.updateProjectionMatrix();

        this.renderer.setSize(width, height);
    }

    showTooltip(descricao: string) {
        var tool = document.getElementById("tooltip");
        tool.innerHTML = descricao;
        tool.style.display = "block";


        var canvasHalfWidth = this.renderer.domElement.offsetWidth / 2;
        var canvasHalfHeight = this.renderer.domElement.offsetHeight / 2;

        var tooltipPosition = this.latestMouseProjection.clone().project(this.camera);
        tooltipPosition.x = (tooltipPosition.x * canvasHalfWidth) + canvasHalfWidth + this.renderer.domElement.offsetLeft;
        tooltipPosition.y = -(tooltipPosition.y * canvasHalfHeight) + canvasHalfHeight + this.renderer.domElement.offsetTop;

        var tootipWidth = tool.offsetWidth;
        var tootipHeight = tool.offsetHeight;

        var left = tooltipPosition.x - tootipWidth / 2;
        var top = tooltipPosition.y - tootipHeight - 5;

        tool.style.left = left + 'px';
        tool.style.top = top + 'px';
    }


    hideTooltip() {
        var tool = document.getElementById("tooltip");
        tool.style.display = "none";
    }

    maquinaSemLinhaToString(maq: MaquinaLinhaDTO) {
        return "Máquina sem Linha de Produção:  " +
            "Marca: " + maq.marca +
            " || Modelo: " + maq.modelo +
            " || Tipo de Máquina: " + maq.descricaoTipoMaquina;
    }

    maquinaLinhaToString(maq: MaquinaVisualizacaoLinhaDTO) {
        return "Máquina em Linha de Produção:  " +
            "Marca: " + maq.marca +
            " || Modelo: " + maq.modelo +
            " || Número de Sequência: " + maq.nSequencia +
            " || Número de Série: " + maq.numeroSerie;
    }

    produtoToString(prod: ProdutoDTO) {
        return "Produto:  " +
            "Descricao: " + prod.descricao +
            " || Preço: " + prod.preco + "€";
    }

    updateLinha(idLinha: string) {
        this.linhasProducaoArray.forEach(element => {
            if (idLinha == element.linhaProducao) {
                element.modelo.visible = false;
            }
        });

        this.linhaServ.getLinhaProducao(idLinha).subscribe(data => {
            var linha: LinhaProducaoDTO = data;
            linha.maquinas.forEach(maquina => {
                this.apresentaMaquina(linha.id, maquina, linha.x, linha.y);
            });
            this.apresentaCaixaAberta(linha.id, linha.x, linha.y, linha.maquinas.length);
            this.iniciarProducao(linha);
        });
    }

    updateMaquinasSemLinha() {
        this.linhaServ.getMaquinas().subscribe(data => {
            var nMaquina: number = 0;
            var maquinas: MaquinaLinhaDTO[] = data;
            maquinas.forEach(element => {
                this.apresentaMaquinaLinha(element, nMaquina);
                nMaquina++;
            });
        });
    }
}
