import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { VisualizacaoService } from './visualizacao.service';
import { LinhaProducaoService } from '../linha-producao/linha-producao.service';
import { HttpErrorResponse } from '@angular/common/http';
import { MaquinaService } from '../maquina/maquina.service';
import { Maquina } from '../maquina/maquina';

interface MaquinaVisualizacaoLinhaDTO {
  marca: string;
  modelo: string;
  nSequencia: number;
  linhaProducao: string;
  numeroSerie: string;
}

interface LinhaProducaoDTO {
  id: string;
  maquinas: MaquinaVisualizacaoLinhaDTO[],
  x: number;
  y: number;
}

interface LinhaProducaoEditDTO {
  id: string
  nMaquinas: number;
  x: number;
  y: number;
}

interface PosicaoDTO {
  x: number;
  y: number;
}


@Component({
  selector: 'app-visualizacao',
  templateUrl: './visualizacao.component.html',
  styleUrls: ['./visualizacao.component.css']
})
export class VisualizacaoComponent implements OnInit {

  @ViewChild('rendererCanvas', { static: true })
  public rendererCanvas: ElementRef<HTMLCanvasElement>;

  constructor(private engServ: VisualizacaoService, private linhaServ: LinhaProducaoService, private maquinaService: MaquinaService) { }

  linhasEdit: LinhaProducaoEditDTO[] = [];
  maquinasEdit: MaquinaVisualizacaoLinhaDTO[] = [];
  posicao: PosicaoDTO;
  maquina: MaquinaVisualizacaoLinhaDTO;
  maquinaAdd: Maquina;
  private token: string;

  ngOnInit() {

    this.token = window.sessionStorage.getItem('tokenAdmin');
    if (this.token == null) {
      window.location.replace('login');
    }

    this.engServ.createScene(this.rendererCanvas);
    this.engServ.animate();
    this.getLinhasProducao();
  }

  mostrarPopupLinhas() {
    document.getElementById("myModalLinhas").style.display = "block";
  }

  fecharPopupLinhas() {
    document.getElementById("myModalLinhas").style.display = "none";
  }

  mostrarPopupMaquinas() {
    document.getElementById("myModalMaquinas").style.display = "block";
  }

  fecharPopupMaquinas() {
    document.getElementById("myModalMaquinas").style.display = "none";
  }

  mostrarPopupAdicionarMaquina() {
    document.getElementById("myModalAdicionarMaquina").style.display = "block";
  }

  fecharPopupAdicionarMaquina() {
    document.getElementById("myModalAdicionarMaquina").style.display = "none";
  }

  getLinhasProducao() {
    this.linhaServ.getLinhasProducao().subscribe(data => {
      var linhas: LinhaProducaoDTO[] = data;

      linhas.forEach(linha => {
        var newLinha: LinhaProducaoEditDTO = { id: linha.id, nMaquinas: linha.maquinas.length, x: linha.x, y: linha.y };
        this.linhasEdit.push(newLinha);
        linha.maquinas.forEach(maquina => {
          this.maquinasEdit.push(maquina);
        });
      });
    });
  }

  private addMaquina(marca: string, modelo: string, descricaoTipoMaquina: string, numeroSerie: string): void {
    this.maquinaService.addMaquina(marca, modelo, descricaoTipoMaquina, numeroSerie).subscribe(data => {
      this.maquinaAdd = data;
      alert("Máquina adicionada com sucesso")
      this.engServ.updateMaquinasSemLinha();
      this.fecharPopupAdicionarMaquina();
    },
      error => { alert("Erro no pedido") });
  }


  editarLinha(linha: LinhaProducaoEditDTO, x: number, y: number) {
    this.linhaServ.updatePosicaoLinha(linha.id, x, y).subscribe(data => {
      this.posicao = data;
      this.engServ.updateLinha(linha.id);
      alert("Linha de Produção alterada com sucesso");
      this.fecharPopupLinhas();
    },
      error => { this.handleErrorLinha(error); }
    );
  }

  editarMaquina(maquina: MaquinaVisualizacaoLinhaDTO, nSeq: number) {
    this.linhaServ.updateNumeroSequenciaMaquina(maquina.linhaProducao, maquina.numeroSerie, nSeq).subscribe(data => {
      this.maquina = data;
      this.engServ.updateLinha(maquina.linhaProducao);
      alert("Máquina e Linha de Produção alteradas com sucesso");
      this.fecharPopupMaquinas();
    },
      error => { this.handleErrorMaquina(error); }
    );
  }

  handleErrorLinha(error: HttpErrorResponse) {
    if (error.status == 400) {
      alert("Posição não válida na fábrica. Tente Novamente");
    }
  }

  handleErrorMaquina(error: HttpErrorResponse) {
    if (error.status == 400) {
      alert("Número de Sequência inválido");
    }
  }


}
