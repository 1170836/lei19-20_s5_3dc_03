import { Component, OnInit } from '@angular/core';
import { Cliente } from '../menu-cliente/cliente';
import { PerfilService } from './perfil.service';
import { HttpErrorResponse } from '@angular/common/http';
import { iif } from 'rxjs';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.css']
})
export class PerfilComponent implements OnInit {


  token: string;
  user: Cliente;
  pagamento: string;
  constructor(private perfilService: PerfilService) { }

  ngOnInit() {
    this.token = window.sessionStorage.getItem('token');

    if (this.token == null) {
      window.location.replace('login');
    }
    this.getCliente();
  }

  private getCliente() {
    this.perfilService.getClient(this.token).subscribe(
      data => {
        this.user = data.cliente;
        this.pagamento = this.user.numCartao.slice((5));
      }
    ),
      (error: HttpErrorResponse) => { this.handleErrorPerfil(error); };
  }

  private alterarCliente(empresa: string, nome: string, email: string, nif: string, iban: string, morada: string) {
    let flagEmail: boolean = false;

    if (this.user.email != email) {
      flagEmail = true;
    }

    if (email == "") {
      alert('Não pode eliminar o email, visto ser totalmente necessário para a autenticação na aplicação.');
      window.location.reload();
      return;
    }
    
/*     if(nif == ""){
      alert('Não pode eliminar o NIF, visto ser necessário por lei para identificar qualquer transação financeira');
      window.location.reload();
      return;
    }
 */
    this.perfilService.alterarCliente(this.token, empresa, nome, email, nif, iban, morada).subscribe(
      data => {
        this.user = data.novoUser;
        alert("Alterações efetuadas com sucesso!");
        if (flagEmail) {
          alert("Por favor volte a fazer login!");
          window.location.replace('login');
        } else {
          window.location.reload();
        }
      });
  }

  private alterarPassword(passwordAtual: string, passwordNova: string, passwordNova2: string) {

    if (passwordNova == passwordNova2) {
      this.perfilService.alterarPassword(this.token, passwordAtual, passwordNova).subscribe(
        data => {
          this.user = data.novoUser;
          alert("Alterações efetuadas com sucesso!");
          this.alterarPasswordClose();
        },
        (error: HttpErrorResponse) => { this.handleErrorPassword(error); this.clearPasswordInputs(); }
      );
    } else {
      this.clearPasswordInputs();
      alert("Passwords nao coincidem!");

    }
  }


  private alterarCartao(numCartao: string, ccv2: string, validade: Date) {
    this.perfilService.alterarCartao(this.token, numCartao, ccv2, validade).subscribe(
      data => {
        this.user = data.novoUser;
        alert("Alterações efetuadas com sucesso!");
      });
    this.alterarCartaoClose();
    window.location.reload();
  }

  private alterarCartaoOpen() {
    let shand = document.getElementById('alterarCartao-modal');

    if (shand) {
      shand.style.display = "flex";
    }
  }

  private alterarCartaoClose() {
    let shand = document.getElementById('alterarCartao-modal');
    if (shand) {
      shand.style.display = "none";
    }
  }

  private alterarPasswordOpen() {
    let shand = document.getElementById('alterarPassword-modal');

    if (shand) {
      shand.style.display = "flex";
    }
  }

  private alterarPasswordClose() {
    let shand = document.getElementById('alterarPassword-modal');
    if (shand) {
      shand.style.display = "none";
    }
  }

  private clearPasswordInputs() {
    let shand = document.getElementById('oldPassword').setAttribute("value", "");
    let shand2 = document.getElementById('newPassword').setAttribute('value', "");
    let shand3 = document.getElementById('newPassword2').nodeValue = '';

  }

  handleErrorPerfil(error: HttpErrorResponse) {
    alert('Sessão Expirou!');
  }

  handleErrorPassword(error: HttpErrorResponse) {
    if (error.status == 401) {
      alert('Password incorreta!');
    } else {
      alert("Sessao expirou");
    }
  }

}
