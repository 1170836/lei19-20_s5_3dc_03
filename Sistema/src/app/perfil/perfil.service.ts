import { Injectable } from '@angular/core';
import { Cliente } from '../menu-cliente/cliente';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs'


interface ClienteDTO {
  empresa: string;
  cliente: string;
  email: string;
  nif: string;
  iban: string;
  morada: string;
}

interface CartaoDTO {
  numCartao: string;
  ccv2: Number;
  validade: Date;
}

interface PasswordDTO {
  password: string;
  passwordNova: string;
}

@Injectable({
  providedIn: 'root'
})


export class PerfilService {

  private geUrl = 'https://arqsi-3dc-3-ge.azurewebsites.net/users';
  constructor(private httpClient: HttpClient) {
  }

  getClient(token: string): Observable<any> {
    return this.httpClient.get(this.geUrl + '/verPerfil', { headers: new HttpHeaders({ 'x-access-token': token }) }).pipe(map(this.extractData));
  }

  alterarCartao(token: string, numCartao2: string, ccv: string, validade2: Date): Observable<any> {
    let cartaoDTO: CartaoDTO = { numCartao: numCartao2, ccv2: Number(ccv), validade: validade2 };
    return this.httpClient.put(this.geUrl + '/alterarPagamento', cartaoDTO, { headers: new HttpHeaders({ 'x-access-token': token }) });
  }
  alterarCliente(token: string, empresa2: string, cliente2: string, email2: string, nif2: string, iban2: string, morada2: string): Observable<any> {
    let ClienteDTO: ClienteDTO = { empresa: empresa2, cliente: cliente2, email: email2, nif: nif2, iban: iban2, morada: morada2 };
    return this.httpClient.put(this.geUrl + '/alterarPerfil', ClienteDTO, { headers: new HttpHeaders({ 'x-access-token': token }) });
  }
  alterarPassword(token: string, oldPassword2: string, newPassword2: string): Observable<any> {
    let passDTO: PasswordDTO = { password: oldPassword2, passwordNova: newPassword2, };
    return this.httpClient.put(this.geUrl + "/alterarPassword", passDTO, { headers: new HttpHeaders({ 'x-access-token': token }) });
  }

  private extractData(res: Response) {
    return res || {};
  }

}