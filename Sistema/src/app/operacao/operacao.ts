export class Operacao {
    id: string;
    descricao: string;
    duracao: number;
    ferramenta: string;
}