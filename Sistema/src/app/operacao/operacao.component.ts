import { Component, OnInit } from '@angular/core';
import { Operacao } from './operacao';
import { OperacaoService } from './operacao.service';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';

@Component({
  selector: 'app-operacao',
  templateUrl: './operacao.component.html',
  styleUrls: ['./operacao.component.css']
})
export class OperacaoComponent implements OnInit {

  op: Operacao;
  statusMessage: string;
  private token: string;
  
  constructor(private operacaoServ: OperacaoService) { }

  ngOnInit() { 
    this.token = window.sessionStorage.getItem('tokenAdmin');
    if (this.token == null) {
      window.location.replace('login');
    }

  }

  private getOperacao(descricao: string): void {
    this.operacaoServ.getOperacao(descricao).subscribe(data => {
      this.op = data;
      alert("Descrição: " + this.op.descricao + "\nDuracao: " + this.op.duracao + "\nFerramenta: " + this.op.ferramenta);
    },
      error => { this.handleError(error); });
  }

  private addOperacao(descricao: string, duracao: number, ferramenta:string): void {
    this.operacaoServ.addOperacao(descricao, duracao,ferramenta).subscribe(data => {
      this.op = data;
      alert("Operação Adicionada com sucesso");
    },
      error => { this.handleError(error); });
  }

  handleError(error: HttpErrorResponse) {
    if (error.status == 404) {
      alert("Operação não encontrada");
    } else if (error.status == 200) {
      alert("Operação já existente");
    } else if (error.status == 400) {
      alert("Pedido inválido");
    } else {
      alert("Serviço indisponível, tente mais tarde.");
    }
  }

}
