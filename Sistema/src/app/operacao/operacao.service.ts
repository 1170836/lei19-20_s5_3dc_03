import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Operacao } from './operacao';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';

interface OperacaoDTO {
  descricao: string;
  duracao: number;
  ferramenta: string;
}

@Injectable({
  providedIn: 'root'
})
export class OperacaoService {



  private mdfUrl = 'https://arqsi-3dc-3-mdf.azurewebsites.net/api/MDF/Operacao/';

  constructor(private httpClient: HttpClient) { }

  getOperacao(desc: string): Observable<any> {
    return this.httpClient.get(this.mdfUrl + desc).pipe(map(this.extractData));
  }

  addOperacao(descricao2: string, duracao2: number, ferramenta2: string): Observable<any> {
    let opDto: OperacaoDTO = { descricao: descricao2, duracao: duracao2, ferramenta: ferramenta2 };
    return this.httpClient.post(this.mdfUrl, opDto).pipe(map(this.extractData));
  }

  private extractData(res: Response) {
    return res || {};
  }
}

