import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OperacaoComponent } from './operacao.component';
import { HttpClientModule } from '@angular/common/http';

describe('OperacaoComponent', () => {
  let component: OperacaoComponent;
  let fixture: ComponentFixture<OperacaoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OperacaoComponent],
      imports: [HttpClientModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OperacaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  afterEach(() => {
    fixture.destroy();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
