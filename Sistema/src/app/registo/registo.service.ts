import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

interface RegistoDTO {
  empresa: string;
  cliente: string;
  password: string;
  email: string;
  nif: number;
  morada: string;
  iban: number;
  numCartao: number;
  ccv2: number;
  validade: Date;
}

@Injectable({
  providedIn: 'root'
})
export class RegistoService {
  private geUrl = 'https://arqsi-3dc-3-ge.azurewebsites.net/users';

  constructor(private httpClient: HttpClient) { }

  registar(empresa2: string, cliente2: string, password2: string, email2: string, nif2: number, morada2: string, iban2: number, numCartao2: number, ccv22: number, validade2: Date): Observable<any> {
    let usDto: RegistoDTO = { empresa: empresa2, cliente: cliente2, email: email2, password: password2, nif: nif2, morada: morada2, iban: iban2, numCartao: numCartao2, ccv2: ccv22, validade: validade2 };
    return this.httpClient.post(this.geUrl + "/registo", usDto).pipe(map(this.extractData));
  }

  private extractData(res: Response) {
    return res || {};
  }
}
