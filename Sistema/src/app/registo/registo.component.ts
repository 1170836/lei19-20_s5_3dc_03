import { Component, OnInit } from '@angular/core';
import { RegistoService } from './registo.service';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-registo',
  templateUrl: './registo.component.html',
  styleUrls: ['./registo.component.css']
})
export class RegistoComponent implements OnInit {

  constructor(private regServ: RegistoService) { }

  ngOnInit() {
  }

  private registo(empresa: string, cliente: string, password1: string, password2: string, email: string, nif: number, morada: string, iban: number, numCartao: number, ccv2: number, validade: Date): void {

    let element = <HTMLInputElement>document.getElementById("consentimentoCheck");
    if (element.checked == false) {
      alert("Termos não aceites.");
      window.location.reload();
      return;
    }

    if (password1 != password2) {
      alert("Password's não coincidem");
    } else {
      if (!this.validaForm(empresa, cliente, password1, email, nif,morada)) {
        alert("Formulário mal preenchido");
      } else {
        this.regServ.registar(empresa, cliente, password1, email, nif, morada, iban, numCartao, ccv2, validade).subscribe(data => {
          alert(data.message);
          window.location.replace("login");
        },
          error => { this.handleError(error); });
      }
    }
  }

  handleError(error: HttpErrorResponse) {
    if (error.status == 200) {
      alert("Email já existente na nossa Base de Dados. Por favor faça login");
    } else if (error.status == 400) {
      alert("Pedido inválido");
    } else {
      alert("Serviço indisponível, tente mais tarde.");
    }

  }

  validaForm(empresa: string, cliente: string, password1: string, email: string, nif: number, morada: string) {
    if (empresa != "" && cliente != "" && password1 != "" && email != "" && nif != 0 && morada != "") {
      return true;
    }
    return false;
  }

  abrirPopup() {
    document.getElementById("consentimento").style.display = "block";
  }

  fecharPopup() {
    document.getElementById("consentimento").style.display = "none";
  }


}
