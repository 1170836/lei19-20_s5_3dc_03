import { Component, OnInit, AfterViewInit } from '@angular/core';
import { LinhaProducaoService } from './linha-producao.service';
import { LinhaProducao } from './linhaProducao';
import { Maquina } from '../maquina/maquina';
import { HttpErrorResponse } from '@angular/common/http';
import { registerLocaleData } from '@angular/common';

@Component({
  selector: 'app-linha-producao',
  templateUrl: './linha-producao.component.html',
  styleUrls: ['./linha-producao.component.css']
})
export class LinhaProducaoComponent implements AfterViewInit {


  linha: LinhaProducao;
  statusMessage: string;

  maquinasSemLinha: Maquina[];
  maquinasSelecionadas: string[] = [''];
  private token: string;
  
  constructor(private linhaServ: LinhaProducaoService) { }

  ngAfterViewInit(): void {
    this.token = window.sessionStorage.getItem('tokenAdmin');
    if (this.token == null) {
      window.location.replace('login');
    }

    this.getMaquinas();
  }

  private addLinhaProducao(x: number, y: number): void {
    if (x >= -50 && x <= 50 && y >= -50 && y <= 50) {
      this.linhaServ.addLinhaProducao(this.maquinasSelecionadas.slice(1), x, y).subscribe(data => {
        this.linha = data;
        alert("Linha Adicionada com sucesso");
        window.location.reload();
      },
        error => { this.handleError(error) });
    } else {
      alert("Os valores das posições devem estar compreendidos em -50 e 50");
    }
  }

  private getMaquinas(): void {
    var table: HTMLTableElement = <HTMLTableElement>document.getElementById("maquinasDisponiveis");

    this.linhaServ.getMaquinas().subscribe(data => {
      this.maquinasSemLinha = data;
    },
      error => { this.handleError(error) });

  }

  handleError(error: HttpErrorResponse) {
    if (error.status == 400) {
      alert("Linha de Produção inválida na fábrica (Posição)");
    } else {
      alert("Serviço indisponível, tente mais tarde.");
    }
  }

  selecionarMaquina(maquina: Maquina) {
    if (!this.maquinasSelecionadas.includes(maquina.id, 0)) {
      this.maquinasSelecionadas.push(maquina.id);
    } else {
      const i = this.maquinasSelecionadas.indexOf(maquina.id);
      if (i > -1) {
        this.maquinasSelecionadas.splice(i, 1);
      }
    }
  }
}
