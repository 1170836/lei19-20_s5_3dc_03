import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { LinhaProducao } from './linhaProducao';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';

interface LinhaDTO {
  maquinas: string[];
  x: number;
  y: number;
}

interface PosicaoDTO {
  x: number;
  y: number;
}

interface MaquinaNumeroSequenciaDTO {
  nSequencia: number;
  numeroSerie: string;
}

@Injectable({
  providedIn: 'root'
})
export class LinhaProducaoService {

  private mdfUrl = 'https://arqsi-3dc-3-mdf.azurewebsites.net/api/MDF/LinhaProducao/';
  private mdfEditMaquinaUrl = 'https://arqsi-3dc-3-mdf.azurewebsites.net/api/MDF/LinhaProducao/Maquina/';
  private mdfMaquinaUrl = 'https://arqsi-3dc-3-mdf.azurewebsites.net/api/MDF/Maquina/LinhaProducao/';

  constructor(private httpClient: HttpClient) { }

  addLinhaProducao(maquinas2: string[], x2: number, y2: number): Observable<any> {
    let lDto: LinhaDTO = { maquinas: maquinas2, x: x2, y: y2 };
    return this.httpClient.post(this.mdfUrl, lDto).pipe(map(this.extractData));
  }

  getLinhasProducao(): Observable<any> {
    return this.httpClient.get(this.mdfUrl).pipe(map(this.extractData));
  }

  getLinhaProducao(idLinha: string): Observable<any> {
    return this.httpClient.get(this.mdfUrl + idLinha).pipe(map(this.extractData));
  }

  getMaquinas(): Observable<any> {
    return this.httpClient.get(this.mdfMaquinaUrl).pipe(map(this.extractData));
  }

  updatePosicaoLinha(id: string, x2: number, y2: number): Observable<any> {
    let lDto: PosicaoDTO = { x: x2, y: y2 };
    return this.httpClient.put(this.mdfUrl + id, lDto).pipe(map(this.extractData));
  }

  updateNumeroSequenciaMaquina(linhaId: string, maqNumeroSerie2: string, nSeq2: number): Observable<any> {
    let maquinaDto: MaquinaNumeroSequenciaDTO = { nSequencia: nSeq2, numeroSerie: maqNumeroSerie2 };
    return this.httpClient.put(this.mdfEditMaquinaUrl + linhaId, maquinaDto).pipe(map(this.extractData));
  }

  private extractData(res: Response) {
    return res || {};
  }

}
