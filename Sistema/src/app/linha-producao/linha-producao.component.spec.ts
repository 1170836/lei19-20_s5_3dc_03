import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LinhaProducaoComponent } from './linha-producao.component';
import {HttpClientModule} from '@angular/common/http';
import { browser } from 'protractor';

describe('LinhaProducaoComponent', () => {
  let component: LinhaProducaoComponent;
  let fixture: ComponentFixture<LinhaProducaoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LinhaProducaoComponent ],
      imports:[HttpClientModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LinhaProducaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  afterEach(() => {
    fixture.destroy();
  });

  it('Deve criar componente', () => {
    expect(component).toBeTruthy();
  });

  
});
