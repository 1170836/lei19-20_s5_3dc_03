import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Encomenda } from '../encomenda/encomenda';

import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';


interface EncomendaDTO {
  id: string;
  produtos: Array<ProdutoDTO>;
  tempoConc: String;
}


interface ProdutoDTO {
  descricao: String;
  preco: Number;
  quantidade: Number;
}
@Injectable({
  providedIn: 'root'
})
export class AlterarEncomendaService {



  private geUrl = 'https://arqsi-3dc-3-ge.azurewebsites.net/encomendas';
  //private geUrl = 'http://127.0.0.1:8080/encomendas';
  private geUrlAuthCliente = 'https://arqsi-3dc-3-ge.azurewebsites.net/auth/cliente';

  constructor(private httpClient: HttpClient) { }


  alterarEncomenda(order, token: string): Observable<any> {
    let novoArray = new Array<ProdutoDTO>();
    for (let i = 0; i < order.produtos.length; i++) {
      let produtodto: ProdutoDTO = { descricao: order.produtos[i].descricao, preco: order.produtos[i].preco, quantidade: order.produtos[i].quantidade };
      novoArray.push(produtodto);
    }

    let encDTO: EncomendaDTO = { id: order._id, produtos: novoArray, tempoConc: order.tempoConc };

    return this.httpClient.put(this.geUrl + '/alterarEncomendaCliente', encDTO, { headers: new HttpHeaders({ 'x-access-token': token }) })
  }

  getAutorizacoesCliente(): Observable<any> {
    return this.httpClient.get(this.geUrlAuthCliente).pipe(map(this.extractData));;
  }

  private extractData(res: Response) {
    return res || {};
  }

}

