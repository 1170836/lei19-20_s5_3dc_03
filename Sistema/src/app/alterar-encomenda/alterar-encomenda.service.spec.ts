import { TestBed } from '@angular/core/testing';

import { AlterarEncomendaService } from './alterar-encomenda.service';

describe('AlterarEncomendaService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AlterarEncomendaService = TestBed.get(AlterarEncomendaService);
    expect(service).toBeTruthy();
  });
});
