import { Component, OnInit } from '@angular/core';
import { Encomenda } from '../encomenda/encomenda';
import { Produto } from '../produto/produto';
import { repeatWhen } from 'rxjs/operators';
import { Produto_Encomenda } from '../produto/produto_encomenda';
import { EncomendaService } from '../encomenda/encomenda.service';
import { ProdutoService } from '../produto/produto.service';
import { AlterarEncomendaService } from './alterar-encomenda.service';
import { stringify } from 'querystring';
import { MenuClientService } from '../menu-cliente/menu-client.service';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-alterar-encomenda',
  templateUrl: './alterar-encomenda.component.html',
  styleUrls: ['./alterar-encomenda.component.css']
})
export class AlterarEncomendaComponent implements OnInit {

  constructor(private cService: MenuClientService, private eService: EncomendaService, private aeService: AlterarEncomendaService) { }

  produtosEncomenda: Produto_Encomenda[] = [];
  produtos: Produto[];
  token: string;
  encomenda: Encomenda;
  prodEncomenda: Produto_Encomenda;
  preco: number = 0;
  statusMessage: string;



  ngOnInit() {

    this.token = window.sessionStorage.getItem('token');
    this.getEncomenda();

    this.checkAuths();
  }

  private checkAuths() {
    this.aeService.getAutorizacoesCliente().subscribe(
      data => {
        var autorizacoes = data.auth;
        autorizacoes.forEach(
          auth => {
            if (auth.auth == 'false') {
              if (auth.operacao == 'Alterar Encomenda') {
                window.location.replace("/menuCliente");
              }
            }
          });
      });
  }

  private getEncomenda() {
    var encomendaID = window.sessionStorage.getItem('encomenda');
    this.eService.getEncomenda(encomendaID, this.token).subscribe(
      data => {
        this.encomenda = data.encomenda;
        this.detalhesEncomenda();

      }, error => { this.handleErrorGet(error); }
    );
  }

  private handleErrorGet(error: HttpErrorResponse) {
    if (error.status) {
      alert("Erro na encomenda, tente novamente! ");
    } else {
      if (error.status == 403) {
        alert('Sessão Expirou!');
      } else {
        if (error.status == 500) {
          alert("Sistema indisponível! Tente mais tarde");
        }
      }
    }
  }
  
  private detalhesEncomenda() {

    var table: HTMLTableElement = <HTMLTableElement>document.getElementById("produtosAll");

    this.eService.getProdutos().subscribe(data => {

      this.produtos = data;
      if (this.produtos.length == 0) {
        var row = table.insertRow();
        row.innerHTML = "Não existem produtos inseridos.";
      }
    });

    var dataHTML = document.getElementById('quantidade');

    var tableEnc: HTMLTableElement = <HTMLTableElement>document.getElementById("encomendaCliente");

    for (let i = 0; i < this.encomenda.produtos.length; i++) {
      var row = tableEnc.insertRow();
      var descricaoP = row.insertCell(0);
      var quantidade = row.insertCell(1);
      var precoP = row.insertCell(2)
      descricaoP.innerHTML = this.encomenda.produtos[i].descricao;
      quantidade.innerHTML = this.encomenda.produtos[i].quantidade.toString();
      precoP.innerHTML = this.encomenda.produtos[i].preco.toString() + "€";
      this.preco += parseInt(this.encomenda.produtos[i].quantidade.toString()) * parseInt(this.encomenda.produtos[i].preco.toString())
    }

  }




  private inserirNaEncomenda(prod: Produto, quant: Number) {



    var table: HTMLTableElement = <HTMLTableElement>document.getElementById("encomendaCliente");
    this.preco = 0;
    let index = -1;

    for (let i = 0; i < this.encomenda.produtos.length; i++) {
      if (this.encomenda.produtos.length >= 1 && this.encomenda.produtos[i].descricao == prod.descricao) {
        index = i;
        break
      }
    }
    for (let i = this.encomenda.produtos.length; i > 0; i--) {
      table.deleteRow(i);
    }

    if (index != -1) {
      this.encomenda.produtos[index].quantidade = parseInt(this.encomenda.produtos[index].quantidade.toString()) + parseInt(quant.toString());
    } else {
      this.prodEncomenda = new Produto_Encomenda();
      this.prodEncomenda.descricao = prod.descricao
      this.prodEncomenda.preco = prod.preco
      this.prodEncomenda.quantidade = quant;
      this.encomenda.produtos.push(this.prodEncomenda);
    }


    for (let i = 0; i < this.encomenda.produtos.length; i++) {
      var row = table.insertRow();
      var descricaoP = row.insertCell(0);
      var quantidade = row.insertCell(1);
      var precoP = row.insertCell(2)
      descricaoP.innerHTML = this.encomenda.produtos[i].descricao;
      quantidade.innerHTML = this.encomenda.produtos[i].quantidade.toString();
      precoP.innerHTML = this.encomenda.produtos[i].preco.toString() + "€";
      this.preco += parseInt(this.encomenda.produtos[i].quantidade.toString()) * parseInt(this.encomenda.produtos[i].preco.toString())
    }
  }


  private alterarEncomenda(data: Date) {
    let today = new Date();
    let dataNew = new Date(data);
    if (this.encomenda.produtos.length != 0) {
      if (dataNew > today) {
        this.encomenda.tempoConc = dataNew.toLocaleDateString();
        this.aeService.alterarEncomenda(this.encomenda, this.token).subscribe(
          data => {
            alert("Encomenda alterada!");
            window.location.replace('/menuCliente');
          },
          error => { this.statusMessage = "Error: Service Unavailable."; });

      } else {
        alert("Por favor insira uma data superior à data de hoje.");
      }
    } else {
      var conf = confirm("Pretende cancelar a sua encomenda?");
      if (conf) {

        var encomendaID = window.sessionStorage.getItem('encomenda');
        this.cService.cancelarEncomenda(this.token, encomendaID).subscribe(
          data => {
            alert("Encomenda cancelada");
            window.location.replace('/menuCliente');
            window.sessionStorage.removeItem('encomenda');
          },
          error => { this.handleErrorCancelar(error); });

      }
    }
  }

  private retirarNaEncomenda(prod: Produto, quant: Number) {

    var table: HTMLTableElement = <HTMLTableElement>document.getElementById("encomendaCliente");
    this.preco = 0;
    let index = -1;

    for (let i = 0; i < this.encomenda.produtos.length; i++) {
      if (this.encomenda.produtos.length >= 1 && this.encomenda.produtos[i].descricao == prod.descricao) {
        index = i;
        break
      }
    }


    if (index == -1) {
      alert("Não tem o produto " + prod.descricao + " na sua encomenda");
    } else {
      for (let i = this.encomenda.produtos.length; i > 0; i--) {
        table.deleteRow(i);
      }
      var dif = parseInt(this.encomenda.produtos[index].quantidade.toString()) - parseInt(quant.toString());
      if (dif < 0) {

        alert("Não tem tantos produtos na sua encomenda, para os retirar por favor retire a quantidade indicada no lado direito do seu ecrã!")
      } else {
        if (dif == 0) {
          this.encomenda.produtos.splice(index, 1);
        } else {
          this.encomenda.produtos[index].quantidade = dif;
        }
      }

      for (let i = 0; i < this.encomenda.produtos.length; i++) {
        var row = table.insertRow();
        var descricaoP = row.insertCell(0);
        var quantidade = row.insertCell(1);
        var precoP = row.insertCell(2)
        descricaoP.innerHTML = this.encomenda.produtos[i].descricao;
        quantidade.innerHTML = this.encomenda.produtos[i].quantidade.toString();
        precoP.innerHTML = this.encomenda.produtos[i].preco.toString() + "€";
        this.preco += parseInt(this.encomenda.produtos[i].quantidade.toString()) * parseInt(this.encomenda.produtos[i].preco.toString())
      }

    }


  }
  handleErrorCancelar(error: HttpErrorResponse) {
    if (error.status == 403) {
      alert('Sessão Expirou!');
    } else {
      if (error.status == 500) {
        alert("Sistema indisponível! Tente mais tarde");
      }
    }
  }
}
