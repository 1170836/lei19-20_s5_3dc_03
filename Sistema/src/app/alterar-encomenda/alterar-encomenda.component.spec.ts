import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlterarEncomendaComponent } from './alterar-encomenda.component';

describe('AlterarEncomendaComponent', () => {
  let component: AlterarEncomendaComponent;
  let fixture: ComponentFixture<AlterarEncomendaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlterarEncomendaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlterarEncomendaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
