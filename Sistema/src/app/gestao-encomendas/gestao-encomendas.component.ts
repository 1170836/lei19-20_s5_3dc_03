import { Component, OnInit, AfterViewInit, AfterViewChecked } from '@angular/core';
import { GestaoEncomendasService } from './gestao-encomendas.service';
import { Encomenda } from '../encomenda/encomenda';
import { HttpErrorResponse } from '@angular/common/http';
import { Produto_Encomenda } from '../produto/produto_encomenda';
import { NavbarAdminComponent } from '../navbar-admin/navbar-admin.component';
import { NavbarAdminService } from '../navbar-admin/navbar-admin.service';

@Component({
  selector: 'app-gestao-encomendas',
  templateUrl: './gestao-encomendas.component.html',
  styleUrls: ['./gestao-encomendas.component.css']
})
export class GestaoEncomendasComponent implements OnInit, AfterViewInit {

  constructor(private service: GestaoEncomendasService) { }

  private token: string;
  private autorizacoes = [];
  encomendas: Encomenda[];
  produtos: Produto_Encomenda[] = [];
  private index: number = 0;

  private flagAlterar: boolean = true;
  private flagCancelar: boolean = true;

  ngOnInit() {
    this.token = window.sessionStorage.getItem('tokenAdmin');
    if (this.token == null) {
      window.location.replace('login');
    }

    this.getEncomendas();
  }

  ngAfterViewInit() {
    this.getAutorizacoesAdmin();
  }

  getEncomendas() {
    this.service.getEncomendas(this.token).subscribe(
      data => {
        this.encomendas = data.encomendas;
      }
    ),
      error => { this.handleError(error); };
  }

  private cancelarEncomenda(enc: Encomenda) {
    this.service.apagarEncomenda(this.token, enc).subscribe(
      data => {
        alert("Encomenda cancelada");
        window.location.reload();
      });
  }

  private alterarEncomenda(enc: Encomenda, tempoConc: string, estado: string) {
    this.service.alterarEncomenda(this.token, enc, tempoConc, estado).subscribe(
      data => {
        alert("Encomenda alterada");
        window.location.reload();
      });
  }

  verDetalhes(enc: Encomenda) {
    for (let i = 0; i < enc.produtos.length; i++) {
      let produtoEncomenda = new Produto_Encomenda();
      produtoEncomenda.descricao = enc.produtos[i].descricao
      produtoEncomenda.preco = enc.produtos[i].preco
      produtoEncomenda.quantidade = enc.produtos[i].quantidade;
      this.produtos.push(produtoEncomenda);
    }

    let shand = document.getElementById('verDetalhes-modal');
    if (shand) {
      shand.style.display = "flex";
    }
  }

  fecharDetalhes() {
    let shand = document.getElementById('verDetalhes-modal');
    if (shand) {
      shand.style.display = "none";
    }
    this.produtos = [];
  }

  confirmacaoApagar(enc: Encomenda) {
    var result = confirm("A encomenda vai ser cancelada!");
    if (result) {
      this.cancelarEncomenda(enc);
    }
  }

  private handleError(error: HttpErrorResponse) {
    alert('Sessão Expirou!')
  }

  private getAutorizacoesAdmin() {
    this.service.getAutorizacoesAdmin().subscribe(data => {
      this.autorizacoes = data.auth;
      this.verificarAuth();
    });
  }

  private verificarAuth() {
    this.autorizacoes.forEach(auth => {
      if (auth.auth != 'true') {
        if (auth.operacao == "Alterar Encomenda") {
          this.flagAlterar = false;
        } else if (auth.operacao == "Cancelar Encomenda") {
          this.flagCancelar = false;
        }
      }
    });

  }

}
