import { TestBed } from '@angular/core/testing';

import { GestaoEncomendasService } from './gestao-encomendas.service';

describe('GestaoEncomendasService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GestaoEncomendasService = TestBed.get(GestaoEncomendasService);
    expect(service).toBeTruthy();
  });
});
