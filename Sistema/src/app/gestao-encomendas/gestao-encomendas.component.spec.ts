import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GestaoEncomendasComponent } from './gestao-encomendas.component';

describe('GestaoEncomendasComponent', () => {
  let component: GestaoEncomendasComponent;
  let fixture: ComponentFixture<GestaoEncomendasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GestaoEncomendasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GestaoEncomendasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
