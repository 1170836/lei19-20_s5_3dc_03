import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Encomenda } from '../encomenda/encomenda';

interface EncomendaDTO {
  encomenda: string;
  tempoConc: string;
  estado: string;
}

@Injectable({
  providedIn: 'root'
})
export class GestaoEncomendasService {

  private geUrl = 'https://arqsi-3dc-3-ge.azurewebsites.net/encomendas';
  private geUrlAuth = 'https://arqsi-3dc-3-ge.azurewebsites.net/auth/admin';

  constructor(private httpClient: HttpClient) { }


  getEncomendas(token: string): Observable<any> {
    return this.httpClient.get(this.geUrl, { headers: new HttpHeaders({ 'x-access-token': token }) });
  }

  alterarEncomenda(token: string, encomenda2, tempoConc2: string, estado2: string): Observable<any> {
    let EncomendaDTO: EncomendaDTO = { encomenda: encomenda2._id, tempoConc: tempoConc2, estado: estado2 };
    return this.httpClient.put(this.geUrl + '/alterarEncomenda', EncomendaDTO, { headers: new HttpHeaders({ 'x-access-token': token }) });
  }

  apagarEncomenda(token: string, encomenda): Observable<any> {
    return this.httpClient.delete(this.geUrl + '/cancelarEncomenda/' + encomenda._id, { headers: new HttpHeaders({ 'x-access-token': token }) });
  }

  getAutorizacoesAdmin(): Observable<any> {
    return this.httpClient.get(this.geUrlAuth).pipe(map(this.extractData));;
  }

  private extractData(res: Response) {
    return res || {};
  }
}
