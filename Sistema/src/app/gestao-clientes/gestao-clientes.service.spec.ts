import { TestBed } from '@angular/core/testing';

import { GestaoClientesService } from './gestao-clientes.service';

describe('GestaoClientesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GestaoClientesService = TestBed.get(GestaoClientesService);
    expect(service).toBeTruthy();
  });
});
