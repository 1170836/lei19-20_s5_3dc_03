import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GestaoClientesComponent } from './gestao-clientes.component';

describe('GestaoClientesComponent', () => {
  let component: GestaoClientesComponent;
  let fixture: ComponentFixture<GestaoClientesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GestaoClientesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GestaoClientesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
