import { Component, OnInit } from '@angular/core';
import { GestaoClientesService } from './gestao-clientes.service';
import { Cliente } from '../menu-cliente/cliente';
import { Encomenda } from '../encomenda/encomenda';
import { HttpErrorResponse } from '@angular/common/http';
import { MenuClientService } from '../menu-cliente/menu-client.service';

@Component({
  selector: 'app-gestao-clientes',
  templateUrl: './gestao-clientes.component.html',
  styleUrls: ['./gestao-clientes.component.css']
})
export class GestaoClientesComponent implements OnInit {

  constructor(private GestaoClientesService: GestaoClientesService) { }

  private token: string;
  private clientes: Cliente[];
  private newCliente: Cliente;
  encomendas: Encomenda[];

  private alterarFlag: boolean = false;
  private autorizacoes = [];

  ngOnInit() {
    this.token = window.sessionStorage.getItem('tokenAdmin');
    if (this.token == null) {
      window.location.replace('login');
    }

    this.getAutorizacoesAdmin();
    this.getClientes();
  }



  private getClientes() {
    this.GestaoClientesService.getClients(this.token).subscribe(
      data => {
        console.log(this.alterarFlag);
        this.clientes = data.clientes;
      }
    ),
      error => { this.handleError(error); };
  }

  private alterarCliente(cliente1: Cliente, nome: string, morada: string) {

    this.GestaoClientesService.alterarCliente(this.token, cliente1.empresa, nome, cliente1.email, cliente1.nif, cliente1.iban, morada).subscribe(
      data => {
        this.newCliente = data.novoUser;
        alert("Alterações efetuadas com sucesso!");
      }),
      error => { this.handleError(error); };
  }

  private handleError(error: HttpErrorResponse) {
    if (error.status == 400) {
      alert("Pedido Inválido");
    }
  }

  private getAutorizacoesAdmin() {
    this.GestaoClientesService.getAutorizacoesAdmin().subscribe(data => {
      this.autorizacoes = data.auth;
      this.verificarAuth();
    });
  }

  private verificarAuth() {
    this.autorizacoes.forEach(auth => {
      console.log(auth);
      if (auth.auth == 'true') {
        if (auth.operacao == "Alterar Cliente") {
          this.alterarFlag=true;
          console.log('Mudei');
        }
      }
    });

  }
}
