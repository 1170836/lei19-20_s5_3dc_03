import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

interface ClienteDTO {
  empresa: string;
  cliente: string;
  email: string;
  nif: string;
  iban: string;
  morada: string;
}


@Injectable({
  providedIn: 'root'
})
export class GestaoClientesService {

  private geUrl = 'https://arqsi-3dc-3-ge.azurewebsites.net/users';
  private geUrlAuth = 'https://arqsi-3dc-3-ge.azurewebsites.net/auth/admin';

  constructor(private httpClient: HttpClient) { }


  getClients(token: string): Observable<any> {
    return this.httpClient.get(this.geUrl + "/clientes", { headers: new HttpHeaders({ 'x-access-token': token }) });
  }

  alterarCliente(token: string, empresa2: string, cliente2: string, email2: string, nif2: string, iban2: string, morada2: string): Observable<any> {
    let ClienteDTO: ClienteDTO = { empresa: empresa2, cliente: cliente2, email: email2, nif: nif2, iban: iban2, morada: morada2 };
    return this.httpClient.put(this.geUrl + '/alterarPerfilAdmin', ClienteDTO, { headers: new HttpHeaders({ 'x-access-token': token }) });
  }

  getAutorizacoesAdmin(): Observable<any> {
    return this.httpClient.get(this.geUrlAuth).pipe(map(this.extractData));;
  }

  private extractData(res: Response) {
    return res || {};
  }


}
