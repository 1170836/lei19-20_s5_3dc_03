import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OperacaoComponent } from './operacao/operacao.component';
import { LinhaProducaoComponent } from './linha-producao/linha-producao.component';
import { MaquinaComponent } from './maquina/maquina.component';
import { TipoMaquinaComponent } from './tipo-maquina/tipo-maquina.component';
import { ProdutoComponent } from './produto/produto.component';
import { VisualizacaoComponent } from './visualizacao/visualizacao.component';
import { LoginComponent } from './login/login.component';
import { RegistoComponent } from './registo/registo.component';
import { PerfilComponent } from './perfil/perfil.component';
import { MenuClienteComponent } from './menu-cliente/menu-cliente.component';
import { EncomendaComponent } from './encomenda/encomenda.component';
import { GestaoClientesComponent } from './gestao-clientes/gestao-clientes.component';
import { GestaoEncomendasComponent } from './gestao-encomendas/gestao-encomendas.component';
import { AtivacaoMaquinaComponent } from './ativacao-maquina/ativacao-maquina.component';
import { AlterarEncomendaComponent } from './alterar-encomenda/alterar-encomenda.component';
import { PlaneamentoComponent } from './planeamento/planeamento.component';

const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'operacao', component: OperacaoComponent },
  { path: 'linhaProducao', component: LinhaProducaoComponent },
  { path: 'maquina', component: MaquinaComponent },
  { path: 'tipoMaquina', component: TipoMaquinaComponent },
  { path: 'produto', component: ProdutoComponent },
  { path: 'visualizacao', component: VisualizacaoComponent },
  { path: 'login', component: LoginComponent },
  { path: 'registo', component: RegistoComponent },
  { path: 'perfil', component: PerfilComponent },
  { path: 'menuCliente', component: MenuClienteComponent },
  { path: 'encomenda', component: EncomendaComponent },
  { path: 'clientes', component: GestaoClientesComponent },
  { path: 'encomendas', component: GestaoEncomendasComponent },
  { path: 'ativarMaquina', component: AtivacaoMaquinaComponent },
  { path: 'alterarEncomenda', component: AlterarEncomendaComponent },
  { path: 'planeamento', component: PlaneamentoComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
