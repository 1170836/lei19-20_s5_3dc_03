import { TestBed } from '@angular/core/testing';

import { NavbarAdminService } from './navbar-admin.service';

describe('NavbarAdminService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: NavbarAdminService = TestBed.get(NavbarAdminService);
    expect(service).toBeTruthy();
  });
});
