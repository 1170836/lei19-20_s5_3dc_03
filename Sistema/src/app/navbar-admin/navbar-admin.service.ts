import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NavbarAdminService {

  private geUrl = 'https://arqsi-3dc-3-ge.azurewebsites.net/auth/admin';

  constructor(private httpClient: HttpClient) { }

  guardarAutorizacoesAdmin(autorizacoes: string[]): Observable<any> {
    return this.httpClient.post(this.geUrl, autorizacoes);
  }
}
