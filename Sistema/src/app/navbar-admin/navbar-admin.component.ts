import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NavbarAdminService } from './navbar-admin.service';


@Component({
  selector: 'app-navbar-admin',
  templateUrl: './navbar-admin.component.html',
  styleUrls: ['./navbar-admin.component.css']
})
export class NavbarAdminComponent implements OnInit {

  private autorizacoes: string[];
  title = 'My Own Cutlery';
  constructor(private http: HttpClient, private adminService: NavbarAdminService) { }

  ngOnInit() {
    this.lerFicheiroAutorizacao();
  }

  logout() {
    window.sessionStorage.removeItem('tokenAdmin');
    window.location.replace('login');
  }

  private lerFicheiroAutorizacao() {
    let fileString = "assets/autorizacoesAdmin.txt";

    this.http.get(fileString, { responseType: 'text' })
      .subscribe(data => {
        var operacaoAut: string[] = data.split(':');
        this.autorizacoes = operacaoAut[1].split(',');
        this.guardarAutorizacoes();
        this.mostrarOperacoesAdmin();
      });
  }

  private guardarAutorizacoes() {
    this.adminService.guardarAutorizacoesAdmin(this.autorizacoes).subscribe();
  }


  private mostrarOperacoesAdmin() {
    this.autorizacoes.forEach(aut => {
      var autorizacao = aut.split('-');

      if (autorizacao[1] != 'true') {
        if (autorizacao[0] == "Consultar Clientes") {
          this.esconderClientes();
        } else if (autorizacao[0] == "Consultar Encomendas") {
          this.esconderEncomendas();
        }
      }
    });
  }

  private esconderClientes() {
    var elem = document.getElementById('autClientes');
    elem.parentNode.removeChild(elem);
    return false;
  }

  private esconderEncomendas() {
    var elem = document.getElementById('autEncomendas');
    elem.parentNode.removeChild(elem);
    return false;
  }

}
