import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { TipoMaquina } from './tipo-maquina';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';

interface TipoMaquinaDTO {
    descricao: string;
    lOperacoes: string[];
}

@Injectable({
    providedIn: 'root'
})
export class TipoMaquinaService {

    private mdfUrl = 'https://arqsi-3dc-3-mdf.azurewebsites.net/api/MDF/TipoMaquina/';

    constructor(private httpClient: HttpClient) { }

    getDescTipoMaquina(idTm: string): Observable<any> {
        return this.httpClient.get(this.mdfUrl + idTm);
    }

    isTipoMaquina(idTm: string): Observable<any> {
        return this.httpClient.get(this.mdfUrl + "isTM/"+  idTm ,{ responseType: 'text' }) ;
    }

    getOpsTipoMaquina(desc: string): Observable<any> {
        return this.httpClient.get(this.mdfUrl + "Operacoes/" + desc).pipe(map(this.extractData));
    }

    addTipoMaquina(descricao1: string, lOperacoes1: string[]): Observable<any> {
        let tmDto: TipoMaquinaDTO = { descricao: descricao1, lOperacoes: lOperacoes1 };
        return this.httpClient.post(this.mdfUrl, tmDto).pipe(map(this.extractData));
    }

    editTipoMaquina(descricao1: string, lOperacoes1: string[]): Observable<any> {
        let tmDto: TipoMaquinaDTO = { descricao: descricao1, lOperacoes: lOperacoes1 };
        return this.httpClient.put(this.mdfUrl, tmDto, { responseType: 'text' });
    }

    private extractData(res: Response) {
        return res || {};
    }

    private handleError(err: HttpErrorResponse) {
        if (err.error instanceof ErrorEvent) {
            console.error('An error occurred: ', err.error.message);
        }
        else {
            console.error(
                `Web Api returned code ${err.status}, ` + ` Response body was: ${err.error}`
            );
        }
        return Observable.throw(err);
    }
}
