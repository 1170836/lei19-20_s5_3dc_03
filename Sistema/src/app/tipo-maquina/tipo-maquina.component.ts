import { Component, OnInit } from '@angular/core';
import { TipoMaquina } from './tipo-maquina';
import { TipoMaquinaService } from './tipo-maquina.service';
import { HttpErrorResponse } from '@angular/common/http';


@Component({
  selector: 'app-tipo-maquina',
  templateUrl: './tipo-maquina.component.html',
  styleUrls: ['./tipo-maquina.component.css']
})
export class TipoMaquinaComponent implements OnInit {

  tipoMaquina: TipoMaquina;
  tipoMaquinaDesc: string;
  statusMessage: string;
  message: string;
  private token: string;
  validation: string;

  constructor(private tipoMaquinaServ: TipoMaquinaService) { }

  ngOnInit() {
    this.token = window.sessionStorage.getItem('tokenAdmin');
    if (this.token == null) {
      window.location.replace('login');
    }
  }

  private addTipoMaquina(descricaoTipoMaquina: string, lOperacoes: string): void {
    this.tipoMaquinaServ.addTipoMaquina(descricaoTipoMaquina, lOperacoes.split(",")).subscribe(data => {
      this.tipoMaquina = data;
      alert("Tipo de Máquina adicionada!\nDescrição: " + this.tipoMaquina.descricao + "\nLista de Operações: " + this.tipoMaquina.lOperacoes);
    },
      error => { this.handleError(error) });
  }

  private getDescTipoMaquina(idTm: string): void {
    this.tipoMaquinaServ.getDescTipoMaquina(idTm).subscribe(data => {
      this.tipoMaquinaDesc = data;
      alert("Descrição: " + this.tipoMaquinaDesc);
    },
      error => { this.handleError(error) });
  }

  private isTipoMaquina(desc: string): void {    
    this.tipoMaquinaServ.isTipoMaquina(desc).subscribe(data => {
      console.log(data);
      this.validation = data;
      if (this.validation != null) {
        alert(this.validation);
      }
    },
      error => { this.handleError(error) });
  }
  private getOpsTipoMaquina(desc: string): void {
    this.tipoMaquinaServ.getOpsTipoMaquina(desc).subscribe(data => {
      this.tipoMaquina = data;
      alert("Descrição: " + this.tipoMaquina.descricao + "\nLista de Operações: " + this.tipoMaquina.lOperacoes);
    },
      error => { this.handleError(error) });
  }

  private editTipoMaquina(descricaoTipoMaquina: string, lOperacoes: string): void {
    this.tipoMaquinaServ.editTipoMaquina(descricaoTipoMaquina, lOperacoes.split(",")).subscribe(data => {
      this.message = data;
      alert(this.message);
    },
      error => { this.handleError(error) });
  }

  handleError(error: HttpErrorResponse) {
    if (error.status == 400) {
      alert("Pedido mal formulado. Tente outra vez.");
    } else if (error.status == 404) {
      alert("Tipo de Máquina inexistente.");
    } else {
      alert("Serviço indisponível, tente mais tarde.");
    }
  }
}
