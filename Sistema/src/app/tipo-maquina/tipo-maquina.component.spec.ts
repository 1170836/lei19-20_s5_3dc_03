import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TipoMaquinaComponent } from './tipo-maquina.component';
import { HttpClientModule } from '@angular/common/http';

describe('TipoMaquinaComponent', () => {
  let component: TipoMaquinaComponent;
  let fixture: ComponentFixture<TipoMaquinaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TipoMaquinaComponent ],
      imports:[HttpClientModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TipoMaquinaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  afterEach(() => {
    fixture.destroy();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
