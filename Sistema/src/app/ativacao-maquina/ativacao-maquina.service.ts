import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class AtivacaoMaquinaService {

  constructor(private httpClient: HttpClient) { }

  private mdfUrl = 'https://arqsi-3dc-3-mdf.azurewebsites.net/api/MDF/Maquina/';
  private mdfEstadoUrl = 'https://arqsi-3dc-3-mdf.azurewebsites.net/api/MDF/Maquina/estado/';
  private ppUrl = 'http://127.0.0.1:8080/planeamento';
  private geUrl = 'https://arqsi-3dc-3-ge.azurewebsites.net/encomendas/';
  private geUrlPlaneamento = 'https://arqsi-3dc-3-ge.azurewebsites.net/planeamento/';

  getMaquinas(): Observable<any> {
    return this.httpClient.get(this.mdfUrl).pipe(map(this.extractData));
  }

  verificarPlaneamento(token: string): Observable<any> {
    return this.httpClient.get(this.geUrlPlaneamento + "verificarPlaneamento", { headers: new HttpHeaders({ 'x-access-token': token }) });
  }

  mudarEstado(id: string): Observable<any> {
    return this.httpClient.put(this.mdfEstadoUrl + id, null, { responseType: "text" });
  }

  esperarEncomendas(token: string) {
    return this.httpClient.put(this.geUrl + "esperarEncomendas", '', { headers: new HttpHeaders({ 'x-access-token': token }) })
  }

  replaneamento(planeamento, token: string) {
    return this.httpClient.put(this.geUrlPlaneamento + "replaneamento", planeamento, { headers: new HttpHeaders({ 'x-access-token': token }) })
  }

  planeamentoProducao(token: string): Observable<any> {
    return this.httpClient.get(this.ppUrl, { headers: new HttpHeaders({ 'x-access-token': token }) }).pipe(map(this.extractData));
  }

  guardarPlaneamento(planeamento, token: string) {
    return this.httpClient.post(this.geUrlPlaneamento + "guardarPlaneamento", planeamento, { headers: new HttpHeaders({ 'x-access-token': token }) })
  }

  private extractData(res: Response) {
    return res || {};
  }
}
