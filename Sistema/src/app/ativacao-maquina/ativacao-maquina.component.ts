import { Component, OnInit } from '@angular/core';
import { Maquina } from '../maquina/maquina';
import { AtivacaoMaquinaService } from './ativacao-maquina.service';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-ativacao-maquina',
  templateUrl: './ativacao-maquina.component.html',
  styleUrls: ['./ativacao-maquina.component.css']
})
export class AtivacaoMaquinaComponent implements OnInit {

  constructor(private mService: AtivacaoMaquinaService) { }

  private token: string;
  maquinas: Maquina[];
  private validation: string;
  private planeamento = true;

  ngOnInit() {
    this.token = window.sessionStorage.getItem('tokenAdmin');
    if (this.token == null) {
      window.location.replace('login');
    }

    this.getMaquinas();
    this.verificarPlaneamento();
  }

  private getMaquinas(): void {

    var table: HTMLTableElement = <HTMLTableElement>document.getElementById("maquinasAll");

    this.mService.getMaquinas().subscribe(data => {

      this.maquinas = data;

      if (this.maquinas.length == 0) {
        var row = table.insertRow();
        row.innerHTML = "Não existem máquinas criadas";
      }
    },
    );
  }

  private async mudarEstado(maquina1: Maquina) {
    await this.mService.mudarEstado(maquina1.id).subscribe(async data => {
      this.validation = data;
      await this.planeamentoProducao();
      this.ngOnInit();
    }, error => { this.handleError(error) },
    );
  }

  private verificarPlaneamento() {
    this.mService.verificarPlaneamento(this.token).subscribe(data => {
      this.planeamento = data.planeamento;
    });
  }

  private async planeamentoProducao() {
    if (this.planeamento == true) {
      await this.replaneamento();
    } else {
      await this.primeiroPlaneamento();
    }
  }

  private async primeiroPlaneamento() {
    await this.mService.planeamentoProducao(this.token).subscribe(async data => {
      await this.mService.guardarPlaneamento(data.planeamento, this.token).subscribe(data => {
        alert('Processo efetuado com sucesso');
        this.ngOnInit();
      });
    });
    alert("Vai ser efetuado um planeamento tendo em conta as alterações feitas. \nEste processo pode demorar alguns segundos, aguarde por favor. Será informado no fim do processo.");
  }

  private async replaneamento() {
    await this.mService.esperarEncomendas(this.token).subscribe(async data => {
      await this.mService.planeamentoProducao(this.token).subscribe(async data => {
        await this.mService.replaneamento(data.planeamento, this.token).subscribe(data => {
          alert('Processo efetuado com sucesso');
          this.ngOnInit();
        });
      });
    });
    alert("Vai ser efetuado um replaneamento tendo em conta as alterações feitas. \nEste processo pode demorar alguns segundos, aguarde por favor. Será informado no fim do processo.");
  }

  handleError(error: HttpErrorResponse) {
    console.log(error);
  }

}
