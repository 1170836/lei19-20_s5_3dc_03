import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AtivacaoMaquinaComponent } from './ativacao-maquina.component';

describe('AtivacaoMaquinaComponent', () => {
  let component: AtivacaoMaquinaComponent;
  let fixture: ComponentFixture<AtivacaoMaquinaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AtivacaoMaquinaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AtivacaoMaquinaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
