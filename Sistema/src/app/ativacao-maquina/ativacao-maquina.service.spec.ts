import { TestBed } from '@angular/core/testing';

import { AtivacaoMaquinaService } from './ativacao-maquina.service';

describe('AtivacaoMaquinaService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AtivacaoMaquinaService = TestBed.get(AtivacaoMaquinaService);
    expect(service).toBeTruthy();
  });
});
