import { browser, logging, by, element, protractor } from 'protractor';

describe('Teste alterar o cliente - Cliente', () => {

    it('Deve fazer login como Cliente', function () {

        browser.get('http://localhost:4200');
        browser.driver.manage().window().maximize();

        element(by.id('email')).sendKeys("e2e@teste.com");
        element(by.id('password')).sendKeys("123456789");

        element(by.id('botaoLogin')).click();

    });

    it('Deve alterar os dados do cliente', function () {
        browser.driver.manage().window().maximize();

        element(by.id("navbarPerfil")).click();
        element(by.id("alterarPerfil")).click();
        element(by.id('nomeCliente')).clear();
        element(by.id('nomeCliente')).sendKeys("TesteE2E");
        element(by.id('botaoAtualizarCliente')).click();

        browser.wait(function () {
            return browser.switchTo().alert().then(
                function () { return true; },
                function () { return false; }
            );

        });


        browser.switchTo().alert().then((alert) => {
            expect(alert.getText()).toString().includes('Alterações efetuadas com sucesso!');
            alert.dismiss();
        })



        element(by.id("navbarPerfil")).click();
        element(by.id("navbarPerfilVer")).click();

        expect(element(by.id("nomeCliente")).getAttribute('value')).toBe('TesteE2E');


    });


    it('Nao deve alterar os dados do cliente', function () {
        browser.driver.manage().window().maximize();

        element(by.id("navbarPerfil")).click();
        element(by.id("alterarPerfil")).click();

        element(by.id('nifCliente')).clear();
        //element(by.id('nifCliente')).sendKeys("");

        element(by.id('botaoAtualizarCliente')).click();

        browser.wait(function () {
            return browser.switchTo().alert().then(
                function () { return true; },
                function () { return false; }
            );

        });

        browser.switchTo().alert().then((alert) => {
            expect(alert.getText()).toString().includes('Não pode eliminar o NIF, visto ser necessário por lei para identificar qualquer transação financeira');
            alert.dismiss();
        })
    });

});


/* describe('Teste alterar o cliente - Admin', () => {

    it('Deve fazer login como administrador', function () {

        browser.get('http://localhost:4200');
        browser.driver.manage().window().maximize();

        element(by.id('email')).sendKeys("1170836@isep.ipp.pt");
        element(by.id('password')).sendKeys("helder321");

        element(by.id('botaoLogin')).click();

    });
}); */

