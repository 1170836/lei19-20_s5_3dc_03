import { browser, logging, by, element, protractor } from 'protractor';

describe('Teste Login Admin', function () {

    it('Deve fazer login como administrador', function () {

        browser.get('http://localhost:4200');
        browser.driver.manage().window().maximize();

        element(by.id('email')).sendKeys("1170836@isep.ipp.pt");
        element(by.id('password')).sendKeys("helder321");

        element(by.id('botaoLogin')).click();

    });

    describe('Teste Navegador', () => {


        it('Deve ir para a página de operações', () => {

            browser.driver.manage().window().maximize();
            element(by.id("navbarOperacoes")).click();
            element(by.id("navbarOperacoesAdd")).click();
            browser.driver.manage().window().maximize();

            describe('Carregamento da Página', function () {

                it('Deve adicionar formulário de Adicionar Operação', function () {

                    expect(element(by.id('descricaoOperacao')).isPresent()).toBe(true);
                    expect(element(by.id('duracaoOperacao')).isPresent()).toBe(true);
                    expect(element(by.id('ferramenta')).isPresent()).toBe(true);
                    expect(element(by.id('botaoAdicionar')).isPresent()).toBe(true);
                });

                it('Deve adicionar formulário de Consultar Operação', function () {

                    expect(element(by.id('descOp')).isPresent());
                });
            });

        });

        it('Deve ir para a página de tipos de máquina', () => {
            browser.driver.manage().window().maximize();
            element(by.id("navbarTipos")).click();
            element(by.id("navbarTiposAdd")).click();
            browser.driver.manage().window().maximize();

            describe('Carregamento da Página', function () {

                browser.driver.manage().window().maximize();

                it('Deve adicionar formulário de Adicionar Tipo Máquina', function () {

                    expect(element(by.id('descTipoMaquina')).isPresent()).toBe(true);
                    expect(element(by.id('lOperacoestpm')).isPresent()).toBe(true);
                    expect(element(by.id('botaoAdicionar')).isPresent()).toBe(true);
                });

                it('Deve adicionar formulário de Consultar Tipo de Máquina', function () {

                    expect(element(by.id('idTipoMaquina')).isPresent()).toBe(true);
                    expect(element(by.id('botaoConsultarDesc')).isPresent()).toBe(true);
                });

                it('Deve adicionar formulário de Consultar Operações Tipo de Máquina', function () {

                    expect(element(by.id('descricaoTipoMaquina')).isPresent()).toBe(true);
                    expect(element(by.id('botaoAlterar')).isPresent()).toBe(true);
                });

                it('Deve adicionar formulário de Consultar Operações Tipo de Máquina', function () {

                    expect(element(by.id('descricaoTipoMaquina1')).isPresent()).toBe(true);
                    expect(element(by.id('botaoConsultarOps')).isPresent()).toBe(true);
                });
            });
        });

        it('Deve ir para a página de máquinas', () => {

            browser.driver.manage().window().maximize();
            element(by.id("navbarMaq")).click();
            element(by.id("navbarMaqAdd")).click();
            browser.driver.manage().window().maximize();

            describe('Carregamento da pagina /maquina', function () {

                it('Deve adicionar formulario de adicionar máquina', function () {
                    expect(element(by.id('marcaMaquina')).isPresent()).toBe(true);
                    expect(element(by.id('modeloMaquina')).isPresent()).toBe(true);
                    expect(element(by.id('descricaoTipoMaquina')).isPresent()).toBe(true);

                    expect(element(by.id('botaoAdicionar')).isPresent()).toBe(true);
                });


                it('Deve adicionar formulario de consultar máquina', function () {
                    expect(element(by.id('idMaquina2')).isPresent()).toBe(true);
                    expect(element(by.id('botaoConsultar')).isPresent()).toBe(true);
                });


                it('Deve adicionar formulario de Atualizar máquina', function () {
                    expect(element(by.id('idMaquina3')).isPresent()).toBe(true);
                    expect(element(by.id('novoTipoMaquina')).isPresent()).toBe(true);
                    expect(element(by.id('botaoAtualizar')).isPresent()).toBe(true);
                });

                it('Deve adicionar tabela das máquinas', function () {

                    var tabelaM = element(by.id("maquinasAll"));
                    expect(tabelaM.isPresent()).toBe(true);
                });
                it('Deve adicionar tabela dos tipo de máquinas', function () {

                    var tabelaTM = element(by.id("tiposMaquinaAll"));
                    expect(tabelaTM.isPresent()).toBe(true);
                });

            });

        });

        it('Deve ir para a página de ativar/desativar máquinas', () => {

            browser.driver.manage().window().maximize();
            element(by.id("navbarMaq")).click();
            element(by.id("navbarMaqAtivar")).click();
            browser.driver.manage().window().maximize();

            describe('Carregamento da pagina /ativar maquina', function () {
                it('Deve adicionar tabela de máquinas', function () {
                    expect(element(by.id('maquinasAll')).isPresent()).toBe(true);
                    expect(element(by.id('botaoEstado')).isPresent()).toBe(true);
                    expect(element(by.id('ativarMaquinas')).isPresent()).toBe(true);
                });
            });

        });

        it('Deve ir para a página de produtos', () => {

            browser.driver.manage().window().maximize();
            element(by.id("navbarProd")).click();
            element(by.id("navbarProdAdd")).click();
            browser.driver.manage().window().maximize();

            describe('Carregamento da Página', function () {


                it('Deve adicionar formulário de Adicionar produto', function () {

                    expect(element(by.id('precoProduto')).isPresent()).toBe(true);
                    expect(element(by.id('descricaoProduto')).isPresent()).toBe(true);
                    expect(element(by.id('planoFabricoProduto')).isPresent()).toBe(true);
                });

                it('Deve adicionar formulário de Consultar Produto', function () {
                    expect(element(by.id('idProduto')).isPresent()).toBe(true);
                });

                it('Deve adicionar formulário de Consultar Plano de Fabrico', function () {
                    expect(element(by.id('idProdutoPlano')).isPresent()).toBe(true);
                });
            });
        });

        it('Deve ir para a página de linhas de produção', () => {

            browser.driver.manage().window().maximize();
            element(by.id("navbarLinhas")).click();
            element(by.id("navbarLinhasAdd")).click();
            browser.driver.manage().window().maximize();

            describe('Deve receber Máquinas', function () {

                it('Tabela deve ser criado corretamente', () => {
                    var tabela = element(by.id("maquinasDisponiveis"));
                    expect(tabela.isPresent()).toBe(true);

                    element.all(by.css('tr')).then(function (totalRows) {
                        expect(totalRows.length).toBeGreaterThanOrEqual(0);
                    });
                });
            });
        });

        it('Deve ir para a página de Gestão de Clientes', () => {

            browser.driver.manage().window().maximize();
            element(by.id("navbarClientes")).click();
            browser.driver.manage().window().maximize();

            expect(element(by.id("gestaoClientes")).isPresent()).toBe(true);
        });

        it('Deve ir para a página de Gestão de Encomendas', () => {

            browser.driver.manage().window().maximize();
            element(by.id("navbarEncomenda")).click();
            browser.driver.manage().window().maximize();

            expect(element(by.id("encomendasAll")).isPresent()).toBe(true);
        });
    });

});

describe('Teste Login Cliente', function () {

    it('Deve fazer login como Cliente', function () {

        browser.get('http://localhost:4200');
        browser.driver.manage().window().maximize();

        element(by.id('email')).sendKeys("hjvp1999@hotmail.com");
        element(by.id('password')).sendKeys("123456789");

        element(by.id('botaoLogin')).click();

    });

    describe('Teste Navegador', () => {


        it('Deve ir para a página de Cliente', () => {

            browser.driver.manage().window().maximize();
            element(by.id("navbarPerfil")).click();
            element(by.id("navbarPerfilVer")).click();
            browser.driver.manage().window().maximize();

            describe('Carregamento da Página', function () {

                it('Deve mostrar dados do cliente', function () {
                    expect(element(by.id('nomeCliente')).isPresent()).toBe(true);
                    expect(element(by.id('emailCliente')).isPresent()).toBe(true);
                    expect(element(by.id('cartaoCliente')).isPresent()).toBe(true);
                    expect(element(by.id('nifCliente')).isPresent()).toBe(true);
                    expect(element(by.id('empresaCliente')).isPresent()).toBe(true);
                    expect(element(by.id('morada')).isPresent()).toBe(true);
                });

                it('Deve carregar encomendas', function () {
                    expect(element(by.id('encomendas')).isPresent()).toBe(true);
                });
            });
        });

        it('Deve ir para a página de alterar Cliente', () => {

            browser.driver.manage().window().maximize();
            element(by.id("navbarPerfil")).click();
            element(by.id("alterarPerfil")).click();
            browser.driver.manage().window().maximize();

            describe('Carregamento da Página', function () {

                it('Deve mostrar dados do cliente', function () {
                    expect(element(by.id('nomeCliente')).isPresent()).toBe(true);
                    expect(element(by.id('emailCliente')).isPresent()).toBe(true);
                    expect(element(by.id('cartaoCliente')).isPresent()).toBe(true);
                    expect(element(by.id('nifCliente')).isPresent()).toBe(true);
                    expect(element(by.id('empresaCliente')).isPresent()).toBe(true);
                    expect(element(by.id('morada')).isPresent()).toBe(true);
                });

                it('Deve carregar Botões', function () {
                    expect(element(by.id('botaoAtualizarCliente')).isPresent()).toBe(true);
                    expect(element(by.id('botaoPassword')).isPresent()).toBe(true);
                    expect(element(by.id('botaoCartao')).isPresent()).toBe(true);
                });
            });
        });

        it('Deve ir para a página de Encomenda', () => {

            browser.driver.manage().window().maximize();
            element(by.id("navbarEncomenda")).click();
            element(by.id("navbarVerEncomenda")).click();
            browser.driver.manage().window().maximize();

            describe('Carregamento da Página', function () {

                it('Deve mostrar tabela de produtos', function () {
                    expect(element(by.id('produtosAll')).isPresent()).toBe(true);
                    expect(element(by.id('quantidade')).isPresent()).toBe(true);
                    expect(element(by.id('botaoInserir')).isPresent()).toBe(true);
                });

                it('Deve mostrar encomenda atual', function () {
                    expect(element(by.id('encomendaCliente')).isPresent()).toBe(true);
                    expect(element(by.id('botaoSubmeter')).isPresent()).toBe(true);
                });
            });
        });


    });
});
