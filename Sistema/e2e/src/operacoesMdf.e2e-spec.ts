import { browser, logging, by, element, protractor } from 'protractor';

describe('Teste Adição Operacao', function () {

  it('Adiciona OperacaoInexistenteTeste99 com Duracao 22', function () {

    browser.get('http://localhost:4200/operacao');
    browser.driver.manage().window().maximize();

    element(by.id('descricaoOperacao')).sendKeys("OperacaoInexistenteTeste99");
    element(by.id('ferramenta')).sendKeys("FerramentaTeste");
    element(by.id('duracaoOperacao')).sendKeys(22);

    element(by.id('botaoAdicionar')).click();

    browser.wait(function () {
      return browser.switchTo().alert().then(
        function () { return true; },
        function () { return false; }
      );
    });

    browser.switchTo().alert().then((alert) => {
      expect(alert.getText()).toString().includes('Operacão Adicionada');
      expect(alert.getText()).toString().includes('Descrição: OperacaoInexistenteTeste99');
      expect(alert.getText()).toString().includes('Ferramenta: FerramentaTeste');
      expect(alert.getText()).toString().includes('Duração: 22');
      alert.dismiss();
    });
  });

  it('Tenta Adicionar Mesma Operação e dá erro', function () {

    browser.get('http://localhost:4200/operacao');
    browser.driver.manage().window().maximize();

    element(by.id('descricaoOperacao')).sendKeys("OperacaoInexistenteTeste99");
    element(by.id('ferramenta')).sendKeys("FerramentaTeste");
    element(by.id('duracaoOperacao')).sendKeys(22);

    element(by.id('botaoAdicionar')).click();

    browser.wait(function () {
      return browser.switchTo().alert().then(
        function () { return true; },
        function () { return false; }
      );
    });

    browser.switchTo().alert().then((alert) => {
      expect(alert.getText()).toString().includes('Operacão já existente');
      alert.dismiss();
    })
  });
});

describe('Teste Consulta Operação', function () {

  it('Consulta Operação Inexistente', function () {
    
    browser.get('http://localhost:4200/operacao');
    browser.debugger();
    
    element(by.id('descOp')).sendKeys('OperacaoInex');
    element(by.id('botaoConsultar')).click();

    browser.wait(function () {
      return browser.switchTo().alert().then(
        function () { return true; },
        function () { return false; }
      );
    });

    browser.switchTo().alert().then((alert) => {
      expect(alert.getText()).toString().includes('Operacão não encontrada');
      alert.dismiss();
    });

  });

});

afterEach(async () => {
  // Assert that there are no errors emitted from the browser
  const logs = await browser.manage().logs().get(logging.Type.BROWSER);
  expect(logs).not.toContain(jasmine.objectContaining({
    level: logging.Level.SEVERE,
  } as logging.Entry));
});


