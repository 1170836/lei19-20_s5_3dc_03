import { browser, logging, by, element, protractor } from 'protractor';

describe('Teste Adição Tipo Máquina', function () {
    it('Adiciona TipoDeMaquinaTeste12 com Operacao Furar', function () {

        browser.get('http://localhost:4200/tipoMaquina');
        browser.driver.manage().window().maximize();

        element(by.id('descTipoMaquina')).sendKeys("TipoDeMaquinaTeste12");
        element(by.id('lOperacoestpm')).sendKeys("Furar");

        element(by.id('botaoAdicionar')).click();

        browser.wait(function () {
            return browser.switchTo().alert().then(
                function () { return true; },
                function () { return false; }
            );
        });

        browser.switchTo().alert().then((alert) => {
            expect(alert.getText()).toString().includes('Tipo de Máquina adicionada');
            expect(alert.getText()).toString().includes('Descrição: TipoDeMaquinaTeste12');
            expect(alert.getText()).toString().includes('Lista de Operações: Furar');
            alert.dismiss();
        })
    })

    it('Tenta Adicionar Mesmo Tipo de Máquina e dá erro', function () {

        browser.get('http://localhost:4200/tipoMaquina');
        browser.driver.manage().window().maximize();

        element(by.id('descTipoMaquina')).sendKeys("TipoDeMaquinaTeste12");
        element(by.id('lOperacoestpm')).sendKeys("Furar");

        element(by.id('botaoAdicionar')).click();

        browser.wait(function () {
            return browser.switchTo().alert().then(
                function () { return true; },
                function () { return false; }
            );
        });

        browser.switchTo().alert().then((alert) => {
            expect(alert.getText()).toString().includes('Serviço indisponível, tente mais tarde.');
            alert.dismiss();
        })
    });
});

describe('Teste Consulta Tipo Máquina', function () {
    it('Consulta Descrição Tipo Máquina', function () {

        browser.get('http://localhost:4200/tipoMaquina');
        browser.driver.manage().window().maximize();

        element(by.id('idTipoMaquina')).sendKeys('4fe5b4e8-443f-408a-6de1-08d761ffe84d');

        element(by.id('botaoConsultarDesc')).click();

        browser.wait(function () {
            return browser.switchTo().alert().then(
                function () { return true; },
                function () { return false; }
            );
        });

        browser.switchTo().alert().then((alert) => {
            expect(alert.getText()).toString().includes('Descrição: TipoMaquinaTeste12');
            alert.dismiss();
        })
    });

    it('Consulta Descrição Tipo Máquina inexistente', function () {

        browser.get('http://localhost:4200/tipoMaquina');
        browser.driver.manage().window().maximize();

        element(by.id('idTipoMaquina')).sendKeys('4fa5b4e8-443f-408a-6de1-08d761ffe84d');

        element(by.id('botaoConsultarDesc')).click();

        browser.wait(function () {
            return browser.switchTo().alert().then(
                function () { return true; },
                function () { return false; }
            );
        });

        browser.switchTo().alert().then((alert) => {
            expect(alert.getText()).toString().includes('Pedido mal formulado. Tente outra vez.');
            alert.dismiss();
        })
    });

    it('Consulta Operações Tipo Máquina', function () {

        browser.get('http://localhost:4200/tipoMaquina');
        browser.driver.manage().window().maximize();

        element(by.id('idTipoMaquina')).sendKeys('Furador');

        element(by.id('botaoConsultarOps')).click();

        browser.wait(function () {
            return browser.switchTo().alert().then(
                function () { return true; },
                function () { return false; }
            );
        });

        browser.switchTo().alert().then((alert) => {
            expect(alert.getText()).toString().includes('Descrição: TipoMaquinaTeste12');
            expect(alert.getText()).toString().includes('Lista de Operações: Furar');
            alert.dismiss();
        })
    });

    it('Consulta Operações Tipo Máquina Inexistente', function () {

        browser.get('http://localhost:4200/tipoMaquina');
        browser.driver.manage().window().maximize();

        element(by.id('descricaoTipoMaquina')).sendKeys('MaquinaInexistente');

        element(by.id('botaoConsultarOps')).click();

        browser.wait(function () {
            return browser.switchTo().alert().then(
                function () { return true; },
                function () { return false; }
            );
        });

        browser.switchTo().alert().then((alert) => {
            expect(alert.getText()).toString().includes('Pedido mal formulado. Tente outra vez.');
            alert.dismiss();
        })
    });
});

describe('Teste Alterar Tipo Máquina', function () {
    it('Adiciona a TipoDeMaquinaTeste12 a Operacao Furar com broca de 5mm', function () {

        browser.get('http://localhost:4200/tipoMaquina');
        browser.driver.manage().window().maximize();

        element(by.id('descricaoTipoMaquina1')).sendKeys("TipoDeMaquinaTeste12");
        element(by.id('lops')).sendKeys("Furar com broca de 5mm");

        element(by.id('botaoAlterar')).click();

        browser.wait(function () {
            return browser.switchTo().alert().then(
                function () { return true; },
                function () { return false; }
            );
        });

        browser.switchTo().alert().then((alert) => {
            expect(alert.getText()).toString().includes('Tipo de Máquina atualizado com sucesso.');
            alert.dismiss();
        })
    })

    it('Tenta Adicionar mesma Operação ao  mesmo Tipo de Máquina', function () {

        browser.get('http://localhost:4200/tipoMaquina');
        browser.driver.manage().window().maximize();

        element(by.id('descricaoTipoMaquina1')).sendKeys("TipoDeMaquinaTeste12");
        element(by.id('lops')).sendKeys("Furar com broca de 5mm");

        element(by.id('botaoAdicionar')).click();

        browser.wait(function () {
            return browser.switchTo().alert().then(
                function () { return true; },
                function () { return false; }
            );
        });

        browser.switchTo().alert().then((alert) => {
            expect(alert.getText()).toString().includes('Tipo de Máquina já contém operação.');
            alert.dismiss();
        })
    });

    it('Tenta Adicionar Operação a Tipo de Máquina inexistente', function () {

        browser.get('http://localhost:4200/tipoMaquina');
        browser.driver.manage().window().maximize();

        element(by.id('descricaoTipoMaquina1')).sendKeys("TMINexistente");
        element(by.id('lops')).sendKeys("Furar com broca de 5mm");

        element(by.id('botaoAdicionar')).click();

        browser.wait(function () {
            return browser.switchTo().alert().then(
                function () { return true; },
                function () { return false; }
            );
        });

        browser.switchTo().alert().then((alert) => {
            expect(alert.getText()).toString().includes('Pedido mal formulado. Tente outra vez.');
            alert.dismiss();
        })
    });

    it('Tenta Adicionar Operação Inexistente a Tipo de Máquina inexistente', function () {

        browser.get('http://localhost:4200/tipoMaquina');
        browser.driver.manage().window().maximize();

        element(by.id('descricaoTipoMaquina1')).sendKeys("TMINexistente");
        element(by.id('lops')).sendKeys("OpInexistente");

        element(by.id('botaoAdicionar')).click();

        browser.wait(function () {
            return browser.switchTo().alert().then(
                function () { return true; },
                function () { return false; }
            );
        });

        browser.switchTo().alert().then((alert) => {
            expect(alert.getText()).toString().includes('Pedido mal formulado. Tente outra vez.');
            alert.dismiss();
        })
    });

    it('Tenta Adicionar Operação Inexistente a Tipo de Máquina', function () {

        browser.get('http://localhost:4200/tipoMaquina');
        browser.driver.manage().window().maximize();

        element(by.id('descricaoTipoMaquina1')).sendKeys("TipoDeMaquinaTeste12");
        element(by.id('lops')).sendKeys("OpInexistente");

        element(by.id('botaoAdicionar')).click();

        browser.wait(function () {
            return browser.switchTo().alert().then(
                function () { return true; },
                function () { return false; }
            );
        });

        browser.switchTo().alert().then((alert) => {
            expect(alert.getText()).toString().includes('Pedido mal formulado. Tente outra vez.');
            alert.dismiss();
        })
    });
});

afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
        level: logging.Level.SEVERE,
    } as logging.Entry));
});

