import { browser, logging, by, element, protractor } from 'protractor';

describe('Teste Criar Encomenda', function () {
    it('Adiciona Produto a Encomenda', function () {

        browser.get('http://localhost:4200/encomenda');
        browser.driver.manage().window().maximize();

        element(by.id('quantidade')).sendKeys(10);

        element(by.id('botaoInserir')).click();
    })

    it('Insere Encomenda com Data menor que a atual', function () {

        browser.get('http://localhost:4200/encomenda');
        browser.driver.manage().window().maximize();

        element(by.id('date')).sendKeys("07/12/2019");

        element(by.id('botaoSubmeter')).click();

        browser.wait(function () {
            return browser.switchTo().alert().then(
                function () { return true; },
                function () { return false; }
            );
        });

        browser.switchTo().alert().then((alert) => {
            expect(alert.getText()).toString().includes('Por favor insira uma data superior à data de hoje.');
            alert.dismiss();
        })
    });

    it('Insere Encomenda com Data menor que a atual', function () {

        browser.get('http://localhost:4200/encomenda');
        browser.driver.manage().window().maximize();

        element(by.id('date')).sendKeys("07/12/2019");

        element(by.id('botaoSubmeter')).click();

        browser.wait(function () {
            return browser.switchTo().alert().then(
                function () { return true; },
                function () { return false; }
            );
        });

        browser.switchTo().alert().then((alert) => {
            expect(alert.getText()).toString().includes('Por favor insira uma data superior à data de hoje.');
            alert.dismiss();
        })
    });

    it('Insere Encomenda com data possível', function () {

        browser.get('http://localhost:4200/encomenda');
        browser.driver.manage().window().maximize();

        element(by.id('date')).sendKeys("07/12/2020");

        element(by.id('botaoSubmeter')).click();

        browser.wait(function () {
            return browser.switchTo().alert().then(
                function () { return true; },
                function () { return false; }
            );
        });

        browser.switchTo().alert().then((alert) => {
            expect(alert.getText()).toString().includes('Encomenda realizada com sucesso!');
            expect(alert.getText()).toString().includes('Obrigado pela sua escolha!');
            alert.dismiss();
        })
    });
});

describe('Teste Consulta Encomenda', function () {
    it('Consulta Encomenda', function () {

        browser.get('http://localhost:4200/menuCliente');
        browser.driver.manage().window().maximize();

        element(by.id('botaoDetalhes')).click();

        var elm = element(by.id('abrirDetalhesEncomenda-modal'));
        expect(elm.isPresent()).toBeTruthy();

        element(by.id('botaoFecharDetalhes')).click();

    });
});

describe('Teste Botões de Consulta Produtos', function () {
    it('Consulta Todos os Produtos', function () {

        browser.get('http://localhost:4200/encomenda');
        browser.driver.manage().window().maximize();

        element(by.id('botaoTodos')).click();

        var elm = element(by.id('produtosAll'));
        expect(elm.isPresent()).toBeTruthy();
    });

    it('Consulta Produtos Mais Vezes Encomendados', function () {

        browser.get('http://localhost:4200/encomenda');
        browser.driver.manage().window().maximize();

        element(by.id('botaoMaisVezes')).click();

        var elm = element(by.id('produtosMaisVezesEnc'));
        expect(elm.isPresent()).toBeTruthy();
    });

    it('Consulta Produtos Mais Encomendados', function () {

        browser.get('http://localhost:4200/encomenda');
        browser.driver.manage().window().maximize();

        element(by.id('botaoMaisEncomendas')).click();

        var elm = element(by.id('produtosMaisEnc'));
        expect(elm.isPresent()).toBeTruthy();
    });

    it('Consulta Produtos com menor tempo de fabrico', function () {

        browser.get('http://localhost:4200/encomenda');
        browser.driver.manage().window().maximize();

        element(by.id('botaoMenorFabrico')).click();

        var elm = element(by.id('produtosMenorFab'));
        expect(elm.isPresent()).toBeTruthy();
    });
});

afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
        level: logging.Level.SEVERE,
    } as logging.Entry));
});

