import { browser, logging, by, element, protractor } from 'protractor';

describe('Teste alterar encomenda - Cliente', () => {

    it('Deve fazer login como Cliente', function () {
        browser.get('http://localhost:4200');
        browser.driver.manage().window().maximize();

        element(by.id('email')).sendKeys("e2e@teste.com");
        element(by.id('password')).sendKeys("123456789");

        element(by.id('botaoLogin')).click();
    });

     it('Deve fazer encomenda', function () {

        browser.get('http://localhost:4200/encomenda');
        browser.driver.manage().window().maximize();

        element(by.id('date')).sendKeys("07/12/2020");

        element(by.id('quantidade')).sendKeys('10');
        element(by.id('botaoInserir')).click();
        element(by.id('botaoSubmeter')).click();

        browser.wait(function () {
            return browser.switchTo().alert().then(
                function () { return true; },
                function () { return false; }
            );
        });

        browser.switchTo().alert().then((alert) => {
            expect(alert.getText()).toString().includes('Encomenda realizada com sucesso!');
            expect(alert.getText()).toString().includes('Obrigado pela sua escolha!');
            alert.dismiss();
        })
    }); 

    it('Deve alterar encomenda', function () {

        browser.get('http://localhost:4200/menuCliente');
        browser.driver.manage().window().maximize();

        element(by.id('botaoDetalhes')).click();

        var elm = element(by.id('abrirDetalhesEncomenda-modal'));
        expect(elm.isPresent()).toBeTruthy();

        element(by.id('btnAlterarEncomenda')).click();
        browser.driver.manage().window().maximize();

        element(by.id('date')).sendKeys("10/12/2020");
        element(by.id('quantidade')).sendKeys('9');
        element(by.id('botaoInserir')).click();

        element(by.id('botaoSubmeter')).click();

        browser.wait(function () {
            return browser.switchTo().alert().then(
                function () { return true; },
                function () { return false; }
            );
        });

        browser.switchTo().alert().then((alert) => {
            expect(alert.getText()).toString().includes('Encomenda alterada!');
            alert.dismiss();
        });
    });

    it('Consulta Encomenda', function () {

        browser.get('http://localhost:4200/menuCliente');
        browser.driver.manage().window().maximize();

        element(by.id('botaoDetalhes')).click();

        var elm = element(by.id('abrirDetalhesEncomenda-modal'));
        expect(elm.isPresent()).toBeTruthy();

        var elmQnt = element(by.id('tdQuantidade'));
      //  expect(elmQnt.getText().toString().includes("9")).toBeTruthy();
        
        element(by.id('botaoFecharDetalhes')).click();
    });


    it('Cancelar Encomenda', function () {

        browser.get('http://localhost:4200/menuCliente');
        element(by.id('botaoDetalhes')).click();

        var elm = element(by.id('abrirDetalhesEncomenda-modal'));
        expect(elm.isPresent()).toBeTruthy();

        element(by.id('btnCancelarEncomenda')).click();

        browser.wait(function () {
            return browser.switchTo().alert().then(
                function () { return true; },
                function () { return false; }
            );
        });

        browser.switchTo().alert().accept();

        browser.wait(function () {
            return browser.switchTo().alert().then(
                function () { return true; },
                function () { return false; }
            );
        });

        browser.switchTo().alert().then((alert) => {
            expect(alert.getText()).toString().includes('Encomenda cancelada');
            alert.dismiss();
        });

    });

});

