import { browser, logging, by, element, protractor } from 'protractor';


describe('Teste Adição Produto', function () {
  it('Adiciona ProdutoTeste99 com Preco 99 com Plano de Fabrico: OperacaoTeste99, OperacaoTeste98', function () {

    browser.get('http://localhost:4200/produto');
    browser.driver.manage().window().maximize();

    element(by.id('precoProduto')).sendKeys(99);
    element(by.id('descricaoProduto')).sendKeys("ProdutoTeste99");
    element(by.id('planoFabricoProduto')).sendKeys("OperacaoTeste99, OperacaoTeste98");

    element(by.id('botaoAdicionar')).click();

    browser.wait(function () {
      return browser.switchTo().alert().then(
        function () { return true; },
        function () { return false; }
      );
    });

    browser.switchTo().alert().then((alert) => {
      expect(alert.getText()).toString().includes('Produto Adicionado');
      expect(alert.getText()).toString().includes('Preco: 99');
      expect(alert.getText()).toString().includes('Descrição: ProdutoTeste99');
      expect(alert.getText()).toString().includes('Plano de Fabrico: OperacaoTeste99, OperacaoTeste98');
      alert.dismiss();
    })
  })

  it('Tenta Adicionar Produto com operações inválidas', function () {

    browser.get('http://localhost:4200/produto');
    browser.driver.manage().window().maximize();

    element(by.id('precoProduto')).sendKeys(98);
    element(by.id('descricaoProduto')).sendKeys("ProdutoTeste98");
    element(by.id('planoFabricoProduto')).sendKeys("OperacaoInvalida99, OperacaoInvalida98");

    element(by.id('botaoAdicionar')).click();

    browser.wait(function () {
      return browser.switchTo().alert().then(
        function () { return true; },
        function () { return false; }
      );
    });

    browser.switchTo().alert().then((alert) => {
      expect(alert.getText()).toString().includes('Pedido inválido');
      alert.dismiss();
    })
  });
});

describe('Teste Consulta Produto', function () {
  it('Consulta Produto Garfo', function () {

    browser.get('http://localhost:4200/produto');
    browser.driver.manage().window().maximize();

    element(by.id('idProduto')).sendKeys('517e03cb-b6c4-4c16-18c9-08d769f7738e');
    element(by.id('botaoConsultarProduto')).click();

    browser.wait(function () {
      return browser.switchTo().alert().then(
        function () { return true; },
        function () { return false; }
      );
    });

    browser.switchTo().alert().then((alert) => {
      expect(alert.getText()).toString().includes('Preco: 999');
      expect(alert.getText()).toString().includes('Descrição: Teste');
      alert.dismiss();
    })
  });
});

describe('Teste Consulta Produto Inexistente', function () {
  
  
  it('Consulta Produto Inexistente', function () {

    browser.get('http://localhost:4200/produto');
    browser.driver.manage().window().maximize();

    element(by.id('idProduto')).sendKeys('CODIGONAOEXISTENTE');

    element(by.id('botaoConsultarProduto')).click();

    browser.wait(function () {
      return browser.switchTo().alert().then(
        function () { return true; },
        function () { return false; }
      );
    });

    browser.switchTo().alert().then((alert) => {
      expect(alert.getText()).toString().includes('Pedido inválido');
      alert.dismiss();
    })
  });
});

describe('Teste Consulta Plano Fabrico', function () {
  it('Consulta Plano Fabrico Teste', function () {

    browser.get('http://localhost:4200/produto');
    browser.driver.manage().window().maximize();
    element(by.id('idProduto')).sendKeys('517e03cb-b6c4-4c16-18c9-08d769f7738e');

    element(by.id('botaoConsultarPlano')).click();

    browser.wait(function () {
      return browser.switchTo().alert().then(
        function () { return true; },
        function () { return false; }
      );
    });

    browser.switchTo().alert().then((alert) => {
      expect(alert.getText()).toString().includes('Plano de Fabrico: OperacaoTeste99, OperacaoTeste98');
      alert.dismiss();
    })
  });
});

describe('Teste Consulta Plano Fabrico Inexistente', function () {
  it('Consulta Plano Fabrico Inexistente', function () {

    browser.get('http://localhost:4200/produto');
    browser.driver.manage().window().maximize();
    
    element(by.id('idProduto')).sendKeys('CODIGONAOEXISTENTE');

    element(by.id('botaoConsultarPlano')).click();

    browser.wait(function () {
      return browser.switchTo().alert().then(
        function () { return true; },
        function () { return false; }
      );
    });

    browser.switchTo().alert().then((alert) => {
      expect(alert.getText()).toString().includes('Pedido inválido');
      alert.dismiss();
    })
  });
});

afterEach(async () => {
  // Assert that there are no errors emitted from the browser
  const logs = await browser.manage().logs().get(logging.Type.BROWSER);
  expect(logs).not.toContain(jasmine.objectContaining({
    level: logging.Level.SEVERE,
  } as logging.Entry));
});
