import { browser, logging, by, element, protractor, WebElement } from 'protractor';

var id;

describe('Teste Adição Maquina', function () {
    it('Adiciona Maquina de Teste com modelo de teste e tipo de teste', function () {

        browser.get('http://localhost:4200/maquina');

        element(by.id('marcaMaquina')).sendKeys("MarcaTeste");
        element(by.id('modeloMaquina')).sendKeys("ModeloTeste");
        element(by.id('descricaoTipoMaquina')).sendKeys("TipoMaquinaTesteMaquina");


        element(by.id('botaoAdicionar')).click();

        browser.wait(function () {
            return browser.switchTo().alert().then(
                function () { return true; },
                function () { return false; }
            );

        });

        browser.switchTo().alert().then((alert) => {
            expect(alert.getText()).toString().includes('Máquina Adicionada');
            expect(alert.getText()).toString().includes('Marca: MarcaTeste');
            expect(alert.getText()).toString().includes('Modelo: ModeloTeste');
            expect(alert.getText()).toString().includes('Tipo de Máquina: TipoMaquinaTesteMaquina');
            alert.dismiss();
        })

    });
})



describe('Teste Consulta Maquina', function () {
    it('Consulta Maquina de Testee', function () {

        browser.get('http://localhost:4200/maquina');
        element(by.id('idMaquina2')).sendKeys("691f923b-7ebf-4d49-92df-08d76aaad720");


        element(by.id('botaoAdicionar')).click();

        browser.wait(function () {
            return browser.switchTo().alert().then(
                function () { return true; },
                function () { return false; }
            );

        });

        browser.switchTo().alert().then((alert) => {
            expect(alert.getText()).toString().includes('Marca: MarcaTeste');
            expect(alert.getText()).toString().includes('Modelo: ModeloTeste');
            expect(alert.getText()).toString().includes('Tipo de Máquina: TipoMaquinaTesteMaquina');
            alert.dismiss();
        })

    });
})


describe('Teste Atualizar Maquina', function () {
    it('Atualizar Maquina de Teste para tipoMquinaTeste2', function () {

        browser.get('http://localhost:4200/maquina');

        element(by.id('idMaquina3')).sendKeys("691f923b-7ebf-4d49-92df-08d76aaad720");
        element(by.id('novoTipoMaquina')).sendKeys("TipoMaquinaTesteMaquina2");


        element(by.id('botaoAtualizar')).click();

        browser.wait(function () {
            return browser.switchTo().alert().then(
                function () { return true; },
                function () { return false; }
            );

        });

        browser.switchTo().alert().then((alert) => {
            expect(alert.getText()).toString().includes('Máquina Atualiazada');
            expect(alert.getText()).toString().includes('ID: 691f923b-7ebf-4d49-92df-08d76aaad720');
            expect(alert.getText()).toString().includes('Tipo de Máquina: TipoMaquinaTesteMaquina2');
            alert.dismiss();
        })

    });
})


