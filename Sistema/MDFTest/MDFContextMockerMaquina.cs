using Microsoft.EntityFrameworkCore;
using MDF.Models;
using MDF.Models.TiposMaquina;
using MDF.Models.Operacoes;
using MDF.Models.Maquinas;
using MDF.Models.Association;

public static class MDFContextMockerMaquina
{

    public static MDFContext GetMDFContext()
    {
        var options = new DbContextOptionsBuilder<MDFContext>().UseInMemoryDatabase("TodoList").Options;
        MDFContext dbContext = new MDFContext(options);
        Seed(dbContext);
        return dbContext;
    }

    private static void Seed(this MDFContext dbContext)
    {
        TipoMaquina tp = new TipoMaquina("tipoMaquina1");

        dbContext.TipoMaquinas.Add(tp);

        dbContext.SaveChanges();
    }
}