using System;
using Xunit;
using MDF.Models.Maquinas;
using MDF.Models;
using MDF.Models.LinhasProducao;
using MDF.DTO;
using MDF.Controllers;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Collections.Generic;


namespace MDFTest
{
    public class LinhaProducaoTest
    {



        [Fact]
        public void TestPost()
        {
            MDFContext context = MDFContextMockerLinhaProducao.GetMDFContext();
            LinhaProducaoController controller = new LinhaProducaoController(context);

            IQueryable<Maquina> maquinas1 = context.Maquinas.Where(maq => maq.marca.marca.Equals("m1"));
            Maquina m1 = maquinas1.Single();

            IQueryable<Maquina> maquinas2 = context.Maquinas.Where(maq => maq.marca.marca.Equals("m2"));
            Maquina m2 = maquinas2.Single();

            List<string> ids = new List<string>();
            ids.Add(m1.Id.ToString());
            ids.Add(m2.Id.ToString());

            LinhaProducaoDTO dto = new LinhaProducaoDTO(ids, 5, 5);

            ActionResult result = controller.AddLinhaProducao(dto);
            Assert.NotNull(result);

            IQueryable<LinhaProducao> linhas = context.LinhasProducao.Where(l => l.Id != null);

            int j = linhas.Count();
            Assert.Equal(1, j);

            LinhaProducao linha = linhas.Single();
            List<Maquina> maquinas = linha.maquinas;

            Assert.True(maquinas.Count == 2);
        }

        [Fact]
        public void TestPutPosicaoNegativa()
        {
            MDFContext context = MDFContextMockerUpdateLinhaProducao.GetMDFContext();
            LinhaProducaoController controller = new LinhaProducaoController(context);

            IQueryable<Maquina> maquinas1 = context.Maquinas.Where(maq => maq.marca.marca.Equals("m1"));
            Maquina m1 = maquinas1.Single();

            IQueryable<Maquina> maquinas2 = context.Maquinas.Where(maq => maq.marca.marca.Equals("m2"));
            Maquina m2 = maquinas2.Single();

            List<string> ids = new List<string>();
            ids.Add(m1.Id.ToString());
            ids.Add(m2.Id.ToString());

            LinhaProducaoDTO dto = new LinhaProducaoDTO(ids, 5, 5);
            ActionResult result = controller.AddLinhaProducao(dto);

            Guid id = context.LinhasProducao.First().Id;

            PosicaoDTO posDto = new PosicaoDTO(-55, -55);
            ActionResult result2 = controller.EditPosicaoLinha(id.ToString(), posDto);
            Assert.NotNull(result2);

            Posicao pos = context.LinhasProducao.First().pos;
            Assert.Equal(5, pos.x);
            Assert.Equal(5, pos.y);
        }

        [Fact]
        public void TestPutPosicaoForaDaFabrica()
        {
            MDFContext context = MDFContextMockerUpdateFabricaLinhaProducao.GetMDFContext();
            LinhaProducaoController controller = new LinhaProducaoController(context);

            IQueryable<Maquina> maquinas1 = context.Maquinas.Where(maq => maq.marca.marca.Equals("m1"));
            Maquina m1 = maquinas1.Single();

            IQueryable<Maquina> maquinas2 = context.Maquinas.Where(maq => maq.marca.marca.Equals("m2"));
            Maquina m2 = maquinas2.Single();

            List<string> ids = new List<string>();
            ids.Add(m1.Id.ToString());
            ids.Add(m2.Id.ToString());

            LinhaProducaoDTO dto = new LinhaProducaoDTO(ids, 5, 5);
            ActionResult result = controller.AddLinhaProducao(dto);

            Guid id = context.LinhasProducao.First().Id;

            PosicaoDTO posDto = new PosicaoDTO(55, 55);
            ActionResult result2 = controller.EditPosicaoLinha(id.ToString(), posDto);
            Assert.NotNull(result2);

            Posicao pos = context.LinhasProducao.First().pos;
            Assert.Equal(5, pos.x);
            Assert.Equal(5, pos.y);
        }

        [Fact]
        public void TestPutPosicaoCorreta()
        {
            MDFContext context = MDFContextMockerUpdateCorretoLinhaProducao.GetMDFContext();
            LinhaProducaoController controller = new LinhaProducaoController(context);

            IQueryable<Maquina> maquinas1 = context.Maquinas.Where(maq => maq.marca.marca.Equals("m1"));
            Maquina m1 = maquinas1.Single();

            IQueryable<Maquina> maquinas2 = context.Maquinas.Where(maq => maq.marca.marca.Equals("m2"));
            Maquina m2 = maquinas2.Single();

            List<string> ids = new List<string>();
            ids.Add(m1.Id.ToString());
            ids.Add(m2.Id.ToString());

            LinhaProducaoDTO dto = new LinhaProducaoDTO(ids, 5, 5);
            ActionResult result = controller.AddLinhaProducao(dto);

            Guid id = context.LinhasProducao.First().Id;

            PosicaoDTO posDto = new PosicaoDTO(15, 15);
            ActionResult result2 = controller.EditPosicaoLinha(id.ToString(), posDto);
            Assert.NotNull(result2);

            Posicao pos = context.LinhasProducao.First().pos;
            Assert.Equal(15, pos.x);
            Assert.Equal(15, pos.y);
        }

        [Fact]
        public void TestUpdateMaquinaPosicaoNegativa()
        {
            MDFContext context = MDFContextMockerUpdateMaquinaNegativo.GetMDFContext();
            LinhaProducaoController controller = new LinhaProducaoController(context);

            IQueryable<Maquina> maquinas1 = context.Maquinas.Where(maq => maq.marca.marca.Equals("m1"));
            Maquina m1 = maquinas1.Single();

            IQueryable<Maquina> maquinas2 = context.Maquinas.Where(maq => maq.marca.marca.Equals("m2"));
            Maquina m2 = maquinas2.Single();

            List<string> ids = new List<string>();
            ids.Add(m1.Id.ToString());
            ids.Add(m2.Id.ToString());

            LinhaProducaoDTO dto = new LinhaProducaoDTO(ids, 5, 5);
            ActionResult result = controller.AddLinhaProducao(dto);

            Guid id = context.LinhasProducao.First().Id;

            MaquinaNumeroSequenciaDTO maqDto = new MaquinaNumeroSequenciaDTO(-1, m1.numeroSerie.numeroSerie);

            ActionResult result2 = controller.EditPosicaoMaquinaNaLinha(id.ToString(), maqDto);
            Assert.NotNull(result2);

            IQueryable<Maquina> maquinas3 = context.Maquinas.Where(maq => maq.marca.marca.Equals("m1"));
            Maquina m1Depois = maquinas3.Single();
            Assert.Equal(1, m1Depois.nSequencia.numeroSeq);
        }

    }
}
