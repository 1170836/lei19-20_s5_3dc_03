using System;
using Xunit;
using MDF.Models.Maquinas;
using MDF.Models;
using MDF.Models.TiposMaquina;
using MDF.DTO;
using MDF.Controllers;
using Microsoft.AspNetCore.Mvc;
using System.Linq;


namespace MDFTest
{
    public class MaquinaTest
    {
        [Fact]
        public void TestarMudancaMaquinaParaLinhaProducao()
        {
            int n = 2;

            TipoMaquina t = new TipoMaquina("T123");
            Maquina maq = new Maquina("marca", "modelo", t, "123", true);

            maq.atualizarParaLinhaProducao(n);

            Assert.True(maq.nSequencia.numeroSeq == n);
        }

        [Fact]
        public void TestarMudancaMaquinaParaLinhaProducaoJaAtribuida()
        {
            int n = 2;

            TipoMaquina t = new TipoMaquina("T123");
            Maquina maq = new Maquina("marca", "modelo", t, "123", true);

            maq.atualizarParaLinhaProducao(n);

            Assert.Throws<InvalidOperationException>(() => maq.atualizarParaLinhaProducao(n + 1));
        }

        [Fact]
        public void TestarConversaoDaMaquinaEmDTO()
        {
            string marca = "Marca123";
            string modelo = "Modelo321";
            TipoMaquina t = new TipoMaquina("T123");
            string numeroSerie = "123";
            Maquina maq = new Maquina(marca, modelo, t, numeroSerie, true);

            MaquinaDTO dto = maq.toDTO();

            Assert.Equal(marca, dto.marca);
            Assert.Equal(modelo, dto.modelo);
            Assert.Equal(t.descricao.descricao, dto.descricaoTipoMaquina);
            Assert.Equal(numeroSerie, dto.numeroSerie);
        }


        [Fact]
        public void TestPost()
        {
            MDFContext context = MDFContextMockerMaquina.GetMDFContext();
            MaquinaController controller = new MaquinaController(context);

            MaquinaDTO dto = new MaquinaDTO("marca", "modelo", "tipoMaquina1", "123");

            int i = context.Maquinas.Where(maq => maq.marca.marca.Equals("marca")).Count();
            Assert.Equal(0, i);


            ActionResult result = controller.AddMaquina(dto);
            Assert.NotNull(result);

            IQueryable<Maquina> maquinas = context.Maquinas.Where(maq => maq.tipoMaquina.descricao.descricao.Equals("tipoMaquina1"));

            int j = maquinas.Count();
            Assert.Equal(1, j);

            Maquina m = maquinas.Single();
            Assert.Equal("marca", m.marca.marca);
            Assert.Equal("modelo", m.modelo.modelo);
            Assert.Equal("tipoMaquina1", m.tipoMaquina.descricao.descricao);
            Assert.Equal("123", m.numeroSerie.numeroSerie);
        }

        [Fact]
        public void TestUpdate()
        {
            MDFContext context = MDFContextMockerMaquinaUpdate.GetMDFContext();
            MaquinaController controller = new MaquinaController(context);

            MaquinaDTO dto = new MaquinaDTO("marca", "modelo", "tipoMaquina2", "123");

            ActionResult result = controller.AddMaquina(dto);
            IQueryable<Maquina> maquinas = context.Maquinas.Where(maq => maq.tipoMaquina.descricao.descricao.Equals("tipoMaquina2"));
            Maquina m = maquinas.Single();

            result = controller.alterarTipoMaquina(m.Id.ToString(), new DescricaoTipoMaquinaDTO("tipoMaquina3"));
            Assert.NotNull(result);

            maquinas = context.Maquinas.Where(maq => maq.tipoMaquina.descricao.descricao.Equals("tipoMaquina3"));

            int j = maquinas.Count();
            Assert.Equal(1, j);

            m = maquinas.Single();
            Assert.Equal("marca", m.marca.marca);
            Assert.Equal("modelo", m.modelo.modelo);
            Assert.Equal("tipoMaquina3", m.tipoMaquina.descricao.descricao);
            Assert.Equal("123", m.numeroSerie.numeroSerie);
        }

        [Fact]
        public void TestUpdateEstado()
        {
            MDFContext context = MDFContextMockerMaquinaUpdateEstado.GetMDFContext();
            MaquinaController controller = new MaquinaController(context);

            MaquinaDTO dto = new MaquinaDTO("marca", "modelo", "tipoMaquina1", "123");

            ActionResult result = controller.AddMaquina(dto);
            Maquina m = context.Maquinas.Single();

            Assert.Equal(true,m.estado.estado);

            ActionResult result2 = controller.alterarEstado(m.Id.ToString());

            Assert.Equal(false,m.estado.estado);
        }
    }
}
