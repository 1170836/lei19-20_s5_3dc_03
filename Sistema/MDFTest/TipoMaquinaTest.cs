using System;
using Xunit;
using MDF.Models.TiposMaquina;
using MDF.Models.Operacoes;
using MDF.Models;
using MDF.Models.LinhasProducao;
using MDF.DTO;
using MDF.Controllers;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Collections.Generic;


namespace MDFTest
{
    public class TipoMaquinaMaquinaTest
    {
        [Fact]
        public void TestarConversaoDaMaquinaEmDTO()
        {
            string desc = "Descrição 123";
            TipoMaquina maq = new TipoMaquina(desc);

            List<Operacao> lOpsT = new List<Operacao>();
            string[] descrip = new string[2];
            string descOp1 = "Descrição 321";
            string descOp2 = "Descrição 213";
            descrip[0] = descOp1;
            descrip[1] = descOp2;
            lOpsT.Add(new Operacao(10, descOp1, "Ferramenta1"));
            lOpsT.Add(new Operacao(10, descOp2, "Ferramenta2"));

            TipoMaquinaDTO dto = maq.toDTO(lOpsT);

            Assert.Equal(desc, dto.descricao);
            int i = 0;
            foreach (string item in dto.lOperacoes)
            {
                Assert.Equal(item, descrip[i]);
                i++;
            }
        }

        [Fact]
        public void TestPost()
        {
            MDFContext context = MDFContextMockerTipoMaquina.GetMDFContext();
            TipoMaquinaController controller = new TipoMaquinaController(context);

            string desc = "tm1";

            List<string> ops1 = new List<string>();
            ops1.Add("Operacao1");
            ops1.Add("Operacao2");
            TipoMaquinaDTO dto = new TipoMaquinaDTO(desc, ops1);

            ActionResult result = controller.AddTipoMaquina(dto);
            Assert.NotNull(result);
        }

        [Fact]
        public void TestUpdate()
        {
            MDFContext context = MDFContextMockerTipoMaquina.GetMDFContext();
            TipoMaquinaController controller = new TipoMaquinaController(context);

            string desc = "tm1";

            List<string> ops1 = new List<string>();
            ops1.Add("Operacao1");
            ops1.Add("Operacao2");
            TipoMaquinaDTO dto = new TipoMaquinaDTO(desc, ops1);

            ActionResult result = controller.AddTipoMaquina(dto);
            Assert.NotNull(result);

            ops1.Add("Operacao3");
            TipoMaquinaDTO dto1 = new TipoMaquinaDTO(desc, ops1);

            ActionResult result1 = controller.EditTipoMaquina(dto1);
            Assert.NotNull(result1);

            IQueryable<TipoMaquina> tm = context.TipoMaquinas.Where(maq => maq.descricao.descricao.Equals("tm1"));

            int j = tm.Count();
            Assert.Equal(1, j);
        }
    }
}