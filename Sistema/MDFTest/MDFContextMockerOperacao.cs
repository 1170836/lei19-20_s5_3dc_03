using Microsoft.EntityFrameworkCore;
using MDF.Models;
using MDF.Models.Operacoes;
using MDF.Models.Association;

public static class MDFContextMockerOperacao
{

    public static MDFContext GetMDFContext()
    {
        var options = new DbContextOptionsBuilder<MDFContext>().UseInMemoryDatabase("Operacoes").Options;
        MDFContext dbContext = new MDFContext(options);
        return dbContext;
    }

}