using System;
using Xunit;
using MDF.Models;
using MDF.Models.Operacoes;
using MDF.DTO;
using MDF.Controllers;
using Microsoft.AspNetCore.Mvc;
using System.Linq;


namespace MDFTest
{

    public class OperacaoTest
    {

        [Fact]
        public void TestPost()
        {
            MDFContext context = MDFContextMockerOperacao.GetMDFContext();

            OperacoesController controller = new OperacoesController(context);

            OperacaoDTO dto = new OperacaoDTO(3, "teste","Ferramenta1");

            int i = (int)context.Operacoes.LongCount();
            Assert.Equal(0, i);

            ActionResult result = controller.AddOperacao(dto);
            Assert.NotNull(result);

            IQueryable<Operacao> operacoes = context.Operacoes;
            int j = operacoes.Count();
            Assert.Equal(1, j);

            Operacao op = operacoes.Single();
            Assert.Equal(3, op.duracao.duracao);
            Assert.Equal("teste", op.descricao.descricao);
        }
        
    }


}