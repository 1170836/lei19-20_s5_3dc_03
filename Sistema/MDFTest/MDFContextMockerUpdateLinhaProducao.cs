using Microsoft.EntityFrameworkCore;
using MDF.Models;
using MDF.Models.TiposMaquina;
using MDF.Models.Operacoes;
using MDF.Models.Maquinas;
using MDF.Models.Association;

public static class MDFContextMockerUpdateLinhaProducao
{

    public static MDFContext GetMDFContext()
    {
        var options = new DbContextOptionsBuilder<MDFContext>().UseInMemoryDatabase("TodoList99").Options;
        MDFContext dbContext = new MDFContext(options);
        Seed(dbContext);
        return dbContext;
    }

    private static void Seed(this MDFContext dbContext)
    {
        TipoMaquina tp = new TipoMaquina("tipoMaquina1");
        dbContext.TipoMaquinas.Add(tp);

        Maquina m1 = new Maquina("m1", "mod1", tp,"123",true);
        Maquina m2 = new Maquina("m2", "mod2", tp,"123",true);

        dbContext.Maquinas.Add(m1);
        dbContext.Maquinas.Add(m2);

        dbContext.SaveChanges();
    }
}