using Microsoft.EntityFrameworkCore;
using MDF.Models;
using MDF.Models.TiposMaquina;
using MDF.Models.Operacoes;
using MDF.Models.Maquinas;
using MDF.Models.Association;

public static class MDFContextMockerTipoMaquina
{

    public static MDFContext GetMDFContext()
    {
        var options = new DbContextOptionsBuilder<MDFContext>().UseInMemoryDatabase("TodoList5").Options;
        MDFContext dbContext = new MDFContext(options);
        Seed(dbContext);
        return dbContext;
    }

    private static void Seed(this MDFContext dbContext)
    {
        Operacao op1 = new Operacao(10,"Operacao1","Ferramenta1");
        Operacao op2 = new Operacao(10,"Operacao2","Ferramenta2");
        Operacao op3 = new Operacao(10,"Operacao3","Ferramenta3");
        dbContext.Operacoes.Add(op1);
        dbContext.Operacoes.Add(op2);
        dbContext.Operacoes.Add(op3);

        dbContext.SaveChanges();
    }
}