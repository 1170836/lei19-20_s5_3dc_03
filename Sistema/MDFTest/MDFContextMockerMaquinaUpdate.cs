using Microsoft.EntityFrameworkCore;
using MDF.Models;
using MDF.Models.TiposMaquina;
using MDF.Models.Operacoes;
using MDF.Models.Maquinas;
using MDF.Models.Association;

public static class MDFContextMockerMaquinaUpdate
{

    public static MDFContext GetMDFContext()
    {
        var options = new DbContextOptionsBuilder<MDFContext>().UseInMemoryDatabase("TodoList2").Options;
        MDFContext dbContext = new MDFContext(options);
        Seed(dbContext);
        return dbContext;
    }

    private static void Seed(this MDFContext dbContext)
    {
        TipoMaquina tp = new TipoMaquina("tipoMaquina2");
        TipoMaquina tp2 = new TipoMaquina("tipoMaquina3");

        dbContext.TipoMaquinas.Add(tp);
        dbContext.TipoMaquinas.Add(tp2);

        dbContext.SaveChanges();
    }
}