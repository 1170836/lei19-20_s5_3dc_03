using Microsoft.EntityFrameworkCore;
using MDF.Models;
using MDP.Models;
using MDP.Models.Produtos;
using MDF.Models.Operacoes;

public static class MDPContextMockerProduto
{
    public static MDPContext GetMDPContext()
    {
        var options = new DbContextOptionsBuilder<MDPContext>().UseInMemoryDatabase("MDP").Options;
        MDPContext dbContext = new MDPContext(options);
        Seed(dbContext);
        return dbContext;
    }

    private static void Seed(this MDPContext dbContext)
    {
        Operacao op1 = new Operacao(1, "OperacaoTeste1","Ferramenta1");
        Operacao op2 = new Operacao(1, "OperacaoTeste2","Ferramenta2");

        dbContext.Operacoes.Add(op1);
        dbContext.Operacoes.Add(op2);
    
        dbContext.SaveChanges();
    }


}