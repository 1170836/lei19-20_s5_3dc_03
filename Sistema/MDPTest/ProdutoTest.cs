using System;
using Xunit;
using MDP.Models;
using MDP.Models.Produtos;
using MDP.Controllers;
using MDP.DTO;
using MDF.Models.Operacoes;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Collections.Generic;


namespace MDPTest
{
    public class ProdutoTest
    {

        [Fact]
        public void TestarConversaoDoProdutoEmDTO()
        {
            Preco preco = new Preco(10);

            DescricaoProduto descp = new DescricaoProduto("Descricao Produto");
            DuracaoFabrico dur = new DuracaoFabrico(45);

            List<SequenciaOperacao> lista = new List<SequenciaOperacao>();
            Operacao op1 = new Operacao(10, "Operacao1", "Ferramenta1");
            Operacao op2 = new Operacao(20, "Operacao2", "Ferramenta1");
            lista.Add(new SequenciaOperacao(1, op1));
            lista.Add(new SequenciaOperacao(2, op2));

            PlanoFabrico plano = new PlanoFabrico(lista);

            Produto pr = new Produto(preco, descp, plano, dur);

            ProdutoDTO dto = pr.toDTO();

            Assert.Equal(preco.preco.ToString(), dto.preco);
            Assert.Equal(descp.descricao.ToString(), dto.descricao);

            string[] planoArray = new string[2];

            int i = 0;
            foreach (SequenciaOperacao sqop in plano.sequenciaOperacoes)
            {
                planoArray[i] = sqop.operacaoId.ToString();
                i++;
            }

            int j = 0;
            foreach (string so in dto.planoFabrico)
            {
                Assert.Equal(so, planoArray[j]);
                j++;
            }
        }

        [Fact]
        public void TestarConversaoDoProdutoSemPlanoEmDTO()
        {
            Preco preco = new Preco(30);

            DescricaoProduto descp = new DescricaoProduto("Descricao ProdutoSemPlano");
            DuracaoFabrico dur = new DuracaoFabrico(0);

            List<SequenciaOperacao> lista = new List<SequenciaOperacao>();
            Operacao op1 = new Operacao(10, "Operacao1", "Ferramenta1");
            Operacao op2 = new Operacao(20, "Operacao2", "Ferramenta1");
            lista.Add(new SequenciaOperacao(1, op1));
            lista.Add(new SequenciaOperacao(2, op2));

            PlanoFabrico plano = new PlanoFabrico(lista);

            Produto pr = new Produto(preco, descp, plano, dur);

            ProdutoSemPlanoDTO dto = pr.toDTOSemPlano();

            Assert.Equal(preco.preco.ToString(), dto.preco);
            Assert.Equal(descp.descricao.ToString(), dto.descricao);
        }

        [Fact]
        public void TestarConversaoDoPlanoFabricoEmDTO()
        {
            Preco preco = new Preco(30);

            DescricaoProduto descp = new DescricaoProduto("Descricao Plano");
            DuracaoFabrico dur = new DuracaoFabrico(15);

            List<SequenciaOperacao> lista = new List<SequenciaOperacao>();
            Operacao op1 = new Operacao(10, "Operacao1", "Ferramenta1");
            Operacao op2 = new Operacao(20, "Operacao2", "Ferramenta1");
            lista.Add(new SequenciaOperacao(1, op1));
            lista.Add(new SequenciaOperacao(2, op2));

            PlanoFabrico plano = new PlanoFabrico(lista);

            Produto pr = new Produto(preco, descp, plano, dur);

            PlanoFabricoDTO dto = pr.toDTOPlano();

            string[] planoArray = new string[2];

            int i = 0;
            foreach (SequenciaOperacao sqop in plano.sequenciaOperacoes)
            {
                planoArray[i] = sqop.operacaoId.ToString();
                i++;
            }

            int j = 0;
            foreach (string so in dto.planoFabrico)
            {
                Assert.Equal(so, planoArray[j]);
                j++;
            }

        }


        [Fact]
        public void TestPost()
        {
            MDPContext context = MDPContextMockerProduto.GetMDPContext();
            ProdutoController controller = new ProdutoController(context);

            List<string> plano = new List<string>();
            plano.Add("OperacaoTeste1");
            plano.Add("OperacaoTeste2");

            ProdutoDTO dto = new ProdutoDTO("12", "ProdutoTeste", plano, "35");

            int i = context.Produtos.Where(ptd => ptd.descricao.descricao.Equals("ProdutoTeste")).Count();
            Assert.Equal(0, i);

            ActionResult result = controller.AddProduto(dto);
            Assert.NotNull(result);

            IQueryable<Produto> produtos = context.Produtos.Where(ptd => ptd.descricao.descricao.Equals("ProdutoTeste"));

            int j = produtos.Count();
            Assert.Equal(1, j);

            Produto p = produtos.Single();
            Assert.Equal((float)12, p.preco.preco);
            Assert.Equal("ProdutoTeste", p.descricao.descricao);

            List<SequenciaOperacao> so = p.planoFabrico.sequenciaOperacoes;

            Assert.Equal("OperacaoTeste1", so.ElementAt(0).operacao.descricao.descricao);
            Assert.Equal("OperacaoTeste2", so.ElementAt(1).operacao.descricao.descricao);

            /*  foreach (SequenciaOperacao so in )
             {
                 Assert.Equal()
             } */

        }
    }
}
